<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->softDeletes();
            $table->string('sku')->nullable();
            $table->string('barcode')->nullable();

            $table->json('name')->nullable();
            $table->json('short_description')->nullable();
            $table->json('description')->nullable();

            $table->json('images')->nullable();

            $table->float('price')->nullable();
            $table->float('compare_price')->nullable();
            $table->float('quantity')->nullable();

            $table->boolean('enabled')->default(1)->index();
            $table->integer('manufacturer_id')->nullable()->index();

            $table->integer('product_weight')->nullable();
            $table->integer('product_height')->nullable();
            $table->integer('product_lenght')->nullable();
            $table->integer('product_width')->nullable();
            $table->integer('packaging_weight')->nullable();
            $table->integer('packaging_height')->nullable();
            $table->integer('packaging_lenght')->nullable();
            $table->integer('packaging_width')->nullable();

            $table->integer('productgroup_id')->nullable()->index();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
