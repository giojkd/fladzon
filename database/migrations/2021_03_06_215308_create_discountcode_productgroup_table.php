<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiscountcodeProductgroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discountcode_productgroup', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('discountcode_id')->nullable();
            $table->integer('productgroup_id')->nullable();
            $table->unique(['discountcode_id', 'productgroup_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discountcode_productgroup');
    }
}
