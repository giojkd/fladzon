<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableCollectionProductgroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collection_productgroup', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('collection_id')->index()->nullable();
            $table->integer('productgroup_id')->index()->nullable();
            $table->unique(['collection_id', 'productgroup_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collection_productgroup');
    }
}
