<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCollectiondiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collectiondiscounts', function (Blueprint $table) {

            $table->id();
            $table->timestamps();
            $table->softDeletes();

            $table->integer('collection_id')->unique()->nullable();
            $table->float('discount')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collectiondiscounts');
    }
}
