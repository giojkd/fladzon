<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesgroupCountryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countriesgroup_country', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('countriesgroup_id')->index()->nullable();
            $table->integer('country_id')->index()->nullable();
            $table->unique(['countriesgroup_id', 'country_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countriesgroup_country');
    }
}
