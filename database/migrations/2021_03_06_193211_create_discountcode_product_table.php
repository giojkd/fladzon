<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiscountcodeProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discountcode_product', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('discountcode_id')->nullable();
            $table->integer('product_id')->nullable();
            $table->unique(['discountcode_id', 'product_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discountcode_product');
    }
}
