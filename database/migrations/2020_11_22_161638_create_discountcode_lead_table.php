<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiscountcodeLeadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discountcode_lead', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->integer('discountcode_id')->nullable()->index();
            $table->integer('lead_id')->nullable()->index();
            $table->unique(['discountcode_id','lead_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discountcode_lead');
    }
}
