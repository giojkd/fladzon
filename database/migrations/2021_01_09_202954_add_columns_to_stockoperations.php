<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToStockoperations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stockoperations', function (Blueprint $table) {
                //
                $table->integer('stockoperationable_id')->nullable()->index();
                $table->string('stockoperationable_type')->nullable()->index();
                $table->integer('parent_id')->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stockoperations', function (Blueprint $table) {
            //
        });
    }
}
