<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('user_id')->nullable()->index();
            $table->json('shipping_address')->nullable();
            $table->json('billing_address')->nullable();
            $table->integer('paymentmethod_id')->index()->nullable();
            $table->json('payment_details')->nullable();
            $table->integer('leadstatus_id')->index();
            $table->float('products_total')->nullable();
            $table->float('shipping_total')->nullable();
            $table->float('discount_total')->nullable();
            $table->float('grand_total')->nullable();
            $table->boolean('has_requested_invoice')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leads');
    }
}
