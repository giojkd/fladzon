<?php

namespace App\Console\Commands;

use App\Models\Collectiondiscount;
use Illuminate\Console\Command;

class ApplyCollectionDiscounts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fladzon:applycollectiondiscounts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $collectiondiscounts = Collectiondiscount::whereNull('applied_at')->get();
        if(!is_null($collectiondiscounts)){
            foreach($collectiondiscounts as $discount){
                $discount->applyDiscount();
            }
            $this->call('scout:import "App/Models/Product"');
        }
    }
}
