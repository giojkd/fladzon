<?php


namespace App\Console\Commands;

use App\Models\Product;
use App\Models\Productgroup;
use Illuminate\Console\Command;
use App;
use App\Models\Category;
use App\Models\Attribute;
use App\Models\Attributevalue;
use Exception;
use ChrisKonnertz\DeepLy\DeepLy;
use Str;
use Stichoza\GoogleTranslate\GoogleTranslate;
use Google\Cloud\Translate\V2\TranslateClient;

class TranslateAttributesAndCategories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fladzon:translateattributesandcategories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $attributes = Attribute::where('created_at','>','2021-05-01')->get();
        $attributevalues = Attributevalue::where('created_at', '>', '2021-05-01')->get();
        $categories = Category::where('created_at', '>', '2021-05-01')->get();

        $translate = new TranslateClient([
            'key' => env('GOOGLE_TRANSLATE_API_KEY')
        ]);


        $defaultLocale = 'it';
        App::setLocale($defaultLocale);
        $locales = collect(config('backpack.crud.locales'));
        unset($locales[$defaultLocale]);

        $translator = new GoogleTranslate();


        foreach ($locales as $localeShort => $localeLong) {

            $translatedName = null;
            $translatedDescription = null;
            $localeIndex = strtolower((substr($localeShort, 0, 2)));
            $this->line('['.$localeIndex.']');
            foreach($attributes as $attribute){
                $result = $translate->translate($attribute->getTranslation('name', $defaultLocale), [ 'target' => $localeIndex ]);
                Attribute::where('id',$attribute->id)->update([ 'name->' . $localeIndex => $result['text'] ]);
                #$this->line($attribute->getTranslation('name', $defaultLocale).' -> '.$result['text']);
            }
            foreach ($categories as $category) {
                $result = $translate->translate($category->getTranslation('name', $defaultLocale), ['target' => $localeIndex]);
                Category::where('id', $category->id)->update(['name->' . $localeIndex => $result['text']]);
                #$this->line($category->getTranslation('name', $defaultLocale) . ' -> ' . $result['text']);
            }
            foreach ($attributevalues as $attributevalue) {
                if($attributevalue->attribute->is_translatable){
                    $result = $translate->translate($attributevalue->getTranslation('name', $defaultLocale), ['target' => $localeIndex]);
                    $translatedName = $result['text'];
                }else{
                    $translatedName = $attributevalue->getTranslation('name', $defaultLocale);
                }

                Attributevalue::where('id', $attributevalue->id)->update(['name->' . $localeIndex => $translatedName]);
            }

        };


    }
}
