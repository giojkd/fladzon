<?php

namespace App\Console\Commands;

use App\Models\Productgroup;
use Illuminate\Console\Command;
use Recombee\RecommApi\Client;
use Recombee\RecommApi\Requests as Reqs;
use Recombee\RecommApi\Exceptions as Ex;

class SeedRecombeeProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fladzon:seedrecombeeproducts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $groups = Productgroup::get();
        $requests = array();

        $client = new Client(env('RECOMBEE_DB_ID'), env('RECOMBEE_DB_KEY'));

        $groups->each(function($item,$key) use(&$requests){
            $data = $item->toSearchableArray();

            $data['cover'] = url($data['cover']);
            $data['second_cover'] = url($data['second_cover']);
            $data['photos'] = collect($data['photos'])->transform(function($item,$key){
                return url($item);
            })->toArray();

            $r = new Reqs\SetItemValues(
                $item->id,
                //values:
                $data,
                //optional parameters:
                ['cascadeCreate' => true] // Use cascadeCreate for creating item
                // with given itemId, if it doesn't exist]
            );
            array_push($requests, $r);
        });

        $result =  $client->send(new Reqs\Batch($requests));

        dd($result);
    }
}
