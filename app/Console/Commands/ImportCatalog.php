<?php

namespace App\Console\Commands;

use App\Models\Manufacturer;
use App\Models\Product;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Str;
use Rap2hpoutre\FastExcel\FastExcel;
use App;
use App\Models\Category;
use App\Models\Productgroup;

class ImportCatalog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fladzon:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import catalog from file described in .env under IMPORT_FILE row';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $locale = 'it';
        $features = 5;
        App::setLocale($locale);
        $path = env('IMPORT_FILE');
        $fullPath = Storage::disk(config('backpack.base.root_disk_name'))->getAdapter()->getPathPrefix() . $path;
        $items = (new FastExcel)->import($fullPath);

        $items->each(function ($item, $key) use ($locale) {
            $rowKey = $key+2;
            if ($item['SKU'] != '') { #perform row checks validity
                $manufacturer = Manufacturer::firstOrCreate(['name' => $item['Brand']]);
                $product = Product::firstOrCreate([
                    'sku' => $item['SKU'],
                    'manufacturer_id' => $manufacturer->id
                ]);
                $this->line('Key: '. $key.' SKU: ' . $product->sku);

                $group = Productgroup::firstOrCreate(['ext_id' => $item['Gruppo']]);
                if ($item['Predefinito'] == 'si') {
                    $group->product_id = $product->id;
                    $group->save();
                }

                $category = Category::find($item['Categoria']);

                $product->name = [$locale => $item['Nome']];
                $product->short_description = [$locale => $item['Descrizione breve']];
                $product->description = [$locale => $item['Descrizione']];
                $product->extended_description = [$locale => $item['Descrizione Extra']];
                $product->category_id = $category->id;
                $product->price = $item['Prezzo'];
                $product->price_per = [$locale => $item['Prezzo al']];
                $product->minimum_increase = $item['Minimo incremento.'];
                $product->min_quantity = ($item['Q. minima'] != '') ? $item['Q. minima'] : 1;
                $product->warranty_years = ($item['Garanzia (anni)'] != '') ? $item['Garanzia (anni)'] : 2;
                $product->reassortment_days = $item['Giorni per disp.'];
                $product->shipping_days = $item['Giorni per sped.'];
                $product->quantity = $item['Disponibili'];
                $product->product_weight = $item['Peso (g)'];
                $product->product_width = $item['Larghezza (cm)'] * 10;
                $product->product_height = $item['Altezza (cm)'] * 10;
                $product->product_length = $item['Profondità (cm)'] * 10;
                $product->productgroup_id = $group->id;
                $product->save();

                for ($i = 1; $i < 5; $i++) {
                    $feature = $item['Caratteristica ' . $i];
                    if ($feature != '') {
                        if (count(explode(':', $feature)) == 2) {
                            list($attr, $value) = explode(':', $feature);
                            $product->addAttribute($attr, $value);
                        }
                    }
                }

                #photos

                #$photosFolder = env('IMPORT_PHOTOS');

                $photosPath = env('IMPORT_PHOTOS') . '/'. $item['SKU'];

                $this->line($photosPath);


                $dir = Storage::disk(config('backpack.base.root_disk_name'));
                $prefix = Storage::disk(config('backpack.base.root_disk_name'))->getAdapter()->getPathPrefix();
                if ($dir->exists($photosPath)) {
                    $files = collect($dir->files($photosPath));
                    if ($files->count() > 0) {
                        $photos = collect([]);
                        $files->each(function ($file, $key) use ($prefix, $photos) {
                            $pathInfo = pathinfo($file);
                            $allowedExtensions = ['jpg','JPG','JPEG','jpeg'];
                            if (in_array($pathInfo['extension'], $allowedExtensions)) {
                                $photos->push((string)Str::of($file)->replace('public', ''));
                            }
                        });
                        $product->photos = $photos;
                        $product->save();
                    }
                }

            }
        });

        return 0;
    }
}
