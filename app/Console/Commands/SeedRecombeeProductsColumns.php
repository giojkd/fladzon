<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Productgroup;
use Recombee\RecommApi\Client;
use Recombee\RecommApi\Requests as Reqs;
use Recombee\RecommApi\Exceptions as Ex;

class SeedRecombeeProductsColumns extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fladzon:seedrecombeeproductscolumns';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $client = new Client(env('RECOMBEE_DB_ID'), env('RECOMBEE_DB_KEY'));
        $group = Productgroup::find(110);

        $data = $group->toSearchableArray();
        $keys = array_keys($data);

        $keyType = [
            'collections_it' => 'set',
            'collections_en' => 'set',
            'collections_de' => 'set',
            'skus' => 'set',
            'photos' => 'imageList',
            'price' => 'double',
            'price_compare' => 'double',
            'discount_percentage' => 'double',
            'is_on_sale' => 'boolean',
            'active' => 'boolean',
            'cover' => 'image',
            'second_cover' => 'image'
        ];


        #$client->send(new Reqs\ResetDatabase()); // Clear everything from the database

        foreach ($keys as $key) {
            $type = (isset($keyType[$key])) ? $keyType[$key] : 'string';
            $client->send(new Reqs\AddItemProperty($key, $type));
        }
    }
}
