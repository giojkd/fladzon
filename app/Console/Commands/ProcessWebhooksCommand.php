<?php

namespace App\Console\Commands;

use App\Models\Lead;
use App\Models\Webhook;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ProcessWebhooksCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fladzon:processwebhooks';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $hooks = Webhook::where('processed',0)->get();
        if($hooks->count() > 0){
            foreach($hooks as $hook){
                try{


                $content = $hook->content;

                switch($hook->referrer){
                    case 'stripe':

                        switch ($content['type']) {
                            case 'charge.succeeded':

                                $lead = Lead::find($content['data']['object']['metadata']['order_id']);
                                if(!is_null($lead)){
                                    if ($content['data']['object']['status'] == 'succeeded') {
                                        if ($lead->leadstatus_id != 2) {
                                            $lead->leadstatus_id = 2;
                                            $lead->confirmed_at = date('Y-m-d H:i:s');
                                            $lead->saveWithoutEvents();
                                        }
                                    }
                                }

                                break;
                        }

                    break;
                    case 'paypal':

                        switch ($content['event_type']){

                            #case 'CHECKOUT.ORDER.APPROVED':
                            case 'PAYMENT.CAPTURE.COMPLETED':

                                #$lead = Lead::find($content['resource']['purchase_units'][0]['custom_id']);
                                $lead = Lead::find($content['resource']['custom_id']);
                                if (!is_null($lead)) {
                                    if ($lead->leadstatus_id != 2) {
                                        $lead->leadstatus_id = 2;
                                        $lead->confirmed_at = date('Y-m-d H:i:s');
                                        $lead->saveWithoutEvents();
                                    }
                                }

                                break;

                        }


                    break;
                }

                $hook->processed = 1;
                $hook->save();
                }catch(Exception $e){
                    Log::debug($e);
                    $hook->processed = -1;
                    $hook->save();
                }
            }
        }

    }
}
