<?php

namespace App\Console\Commands;

use App\Models\Lead;
use Illuminate\Console\Command;

class SendLeadConfirmations extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fladzon:sendleadconfirmations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Lead::whereNotNull('confirmed_at')->whereNull('confirmation_sent_at')->get()->each(function($lead,$key){
            $lead->sendConfirmations();
            $lead->confirmation_sent_at = date('Y-m-d H:i:s');
            $lead->save();
        });
    }
}
