<?php

namespace App\Console\Commands;

use App\Models\Product;
use App\Models\Productgroup;
use Illuminate\Console\Command;
use App;
use Exception;
use ChrisKonnertz\DeepLy\DeepLy;
use Str;
use Stichoza\GoogleTranslate\GoogleTranslate;
use Google\Cloud\Translate\V2\TranslateClient;



class TranslateProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fladzon:translateproducts {productimport_id=0}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $productimport_id = $this->argument('productimport_id');

        if($productimport_id == 0){
            $this->line('You must specify and import id');
            exit;
        }

        $translate = new TranslateClient([
            'key' => env('GOOGLE_TRANSLATE_API_KEY')
        ]);

        $defaultLocale = 'it';
        App::setLocale($defaultLocale);

        $locales = collect(config('backpack.crud.locales'));
        unset($locales[$defaultLocale]);

        $translator = new GoogleTranslate();

        $groups = Productgroup::where('productimport_id', $productimport_id)->get();

        foreach($groups as $group){
            $product = $group->products->first();
            if (is_object($product)) {
                foreach ($locales as $localeShort => $localeLong) {

                    $translatedName = null;
                    $translatedDescription = null;

                    $localeIndex = strtolower((substr($localeShort, 0, 2)));

                    $this->line('group: '.$group->id);

                    $this->line('Translating: ' . $product->name);


                    if(!is_null($product->name) && $product->name != ''){

                        $result = $translate->translate($product->name, [
                            'target' => $localeIndex,
                        ]);
                        $translatedName = $result['text'];

                        $group->products()->update([
                            'name->' . $localeIndex => $translatedName
                        ]);
                    }

                    if(!is_null($product->description) && $product->description != ''){

                        $result = $translate->translate(preg_replace("/\r|\n/", "", strip_tags($product->description)), [
                            'target' => $localeIndex,
                        ]);
                        $translatedDescription = $result['text'];
                        $group->products()->update([
                            'description->' . $localeIndex => $translatedDescription
                        ]);
                    }

                    $this->line('in ' . $localeIndex . ' is: ' . $translatedName);
                    $this->line('applying translations to ' . $group->products()->count() . ' products');


                    $this->line('.................................................');
                }

            }

        }

    }
}
