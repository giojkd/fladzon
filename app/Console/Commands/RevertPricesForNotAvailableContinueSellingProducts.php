<?php

namespace App\Console\Commands;

use App\Models\Product;
use Illuminate\Console\Command;

class RevertPricesForNotAvailableContinueSellingProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fladzon:revertpricesfornotavailablecontinuesellingproducts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $products =
        Product::where('continue_selling',1)
        ->where('quantity','<=',0)
        ->where('price','!=', 'compare_price')
        ->get();

        if($products->count()){
            $products->each(function($product,$key){
                $product->price = $product->compare_price;
                $product->saveWithoutEvents();
            });
        }
    }
}
