<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Product;
use Str;
use Storage;
use League\Csv\Writer;
use App;

class GenerateFacebookShopCatalog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fladzon:generatefacebookshopcatalog {locale=it} {override=en_UK}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    function strip_tags_content($string)
    {
        // ----- remove HTML TAGs -----
        $string = preg_replace('/<[^>]*>/', ' ', $string);
        // ----- remove control characters -----
        $string = str_replace("\r", '', $string);
        $string = str_replace("\n", ' ', $string);
        $string = str_replace("\t", ' ', $string);
        // ----- remove multiple spaces -----
        $string = trim(preg_replace('/ {2,}/', ' ', $string));
        return $string;
    }

    public function handle()
    {
        //

        $csv = collect([]);

        $locale = $this->argument('locale');
        $override = $this->argument('override');

        $genders = [
            1 => 'male',
            2 => 'female',
            3 => 'unisex'
        ];

        App::setLocale($locale);

        Product::whereEnabled(1)->chunk(50, function ($products) use ($csv, $genders,$override) {

            foreach ($products as $product) {

                $cover = (!is_null($product->photos)) ? collect($product->photos)->first() : null;
                if (!is_null($cover)) {
                    $cover =  url(Route('ir', ['size' => 'h480', 'filename' => $cover], false));
                }

                $this->line('product ' . $product->id);
                if (
                    $product->name != '' &&
                    $product->description != '' &&
                    !is_null($cover) &&
                    !is_null($product->manufacturer)
                ) {
                    $csv->push([
                        'id' => $product->id,
                        'title' => $product->name,
                        'override' => $override,
                        'description' =>  substr($this->strip_tags_content($product->description), 0, 5000),
                        'availability' => ((float)$product->quantity > 0) ? 'in stock' : 'out of stock',
                        'condition' => 'new',
                        'price' => number_format($product->price, 2) . ' EUR',
                        'link' => url($product->makeUrl()),
                        'image_link' => $cover,
                        'brand' => $product->manufacturer->name,
                    ]);
                }
            }
        });

        $csvWriter = Writer::createFromString('');

        $csvWriter->setDelimiter(';');
        #$csvWriter->setEnclosure('');

        $header = ['id', 'title', 'override', 'description', 'availability', 'condition', 'price', 'link', 'image_link', 'brand'];

        $csvWriter->insertOne($header);
        $csvWriter->insertAll($csv);

        $file = $csvWriter->getContent();

        Storage::disk('public')->put('facebook_shop_catalog_'.$locale.'.csv', $file);
    }
}

/*
Facebook shop feed info

id: # Campo obbligatorio | Un ID unico per l'articolo. Usa l'SKU se puoi. Inserisci ogni ID una sola volta; altrimenti l'articolo non sarà caricato. Per le inserzioni dinamiche; questo ID deve corrispondere esattamente all'ID del contenuto per lo stesso articolo nel tuo pixel di Facebook. Limite di caratteri: 100.


title: # Campo obbligatorio | Un titolo specifico e pertinente per l'articolo. Includi parole chiave come brand; caratteristiche o condizioni. Limite di caratteri: 150.

description: # Campo obbligatorio | Una descrizione breve e pertinente dell'articolo. Includi le caratteristiche specifiche o uniche del prodotto; come il materiale o il colore. Usa testo semplice e non inserire solo lettere maiuscole. Limite di caratteri: 5000.

availability: # Campo obbligatorio | La disponibilità attuale dell'articolo nel tuo negozio. | Valori supportati: in stock; available for order; preorder; out of stock; discontinued

inventory: # Campo obbligatorio | Il numero di prodotti disponibili per questo articolo. Le persone non possono acquistare questo articolo a meno che il numero di prodotti disponibili non sia 1 o superiore. Nota: nello shop di una Pagina; un articolo verrà visualizzato come non disponibile se i prodotti disponibili sono 0; anche se risulta "disponibile".

condition: # Campo obbligatorio | Le condizioni dell'articolo. | Valori supportati: new; refurbished; used

price: # Campo obbligatorio | Il costo e la valuta dell'articolo. Il prezzo consiste in un numero seguito dal codice di valuta a 3 cifre (standard ISO 4217). Usa un punto (".") per i decimali.

link: # Campo obbligatorio | L'URL della pagina del prodotto in cui le persone possono acquistare l'articolo. Se non disponi di un URL; fornisci un fallback; come un collegamento alla Pagina Facebook aziendale.

image_link: # Campo obbligatorio | L'URL per l'immagine principale del tuo articolo. Usa un'immagine quadrata (1:1) con una risoluzione di 1024 x 1024 pixel o superiore.

brand: # Campo obbligatorio | Il nome del brand; il codice unico del produttore (MPN) o il Global Trade Item Number (GTIN) dell'articolo. Devi inserire solo uno di questi; non tutti. Per quanto riguarda il GTIN; inserisci il codice UPC; EAN; JAN o ISBN dell'articolo. Limite di caratteri: 100.

google_product_category: # Facoltativo | La categoria di prodotti di Google per l'articolo. Scopri di più sulle categorie dei prodotti: https://www.facebook.com/business/help/526764014610932. Fornisci una categoria nei campi fb_product_category field o google_product_category; oppure in entrambi.

fb_product_category: # Facoltativo | La categoria di prodotti di Facebook per l'articolo. Scopri di più sulle categorie dei prodotti: https://www.facebook.com/business/help/526764014610932. Fornisci una categoria nei campi fb_product_category field o google_product_category; oppure in entrambi.

sale_price: # Facoltativo | Il prezzo scontato e la valuta dell'articolo; se in promozione. Il prezzo consiste in un numero seguito dal codice di valuta (standard ISO 4217). Usa un "." per i decimali. Se desideri usare un overlay per i prezzi scontati; è necessario un prezzo promozionale.

sale_price_effective_date: # Facoltativo | L'intervallo di tempo per il periodo promozionale; inclusi la data; l'ora e il fuso orario di inizio e di fine del periodo promozionale. Se non inserisci le date della promozione; tutti gli articoli con un sale_price rimarranno in promozione fino a quando non rimuoverai il prezzo promozionale. Usa questo formato: AAAA-MM-GGT23:59+00:00/AAAA-MM-GGT23:59+00:00. Inserisci la data di inizio come AAAA-MM-GG. Inserisci una "T". Inserisci poi l'ora di inizio nel formato a 24 ore (dalle 00:00 alle 23:59); seguita dal fuso orario UTC (da -12:00 a +14:00). Inserisci uno "/"; quindi usa lo stesso formato per la data e l'ora di fine. La riga di esempio di seguito usa il fuso orario PST (-08:00).

item_group_id: # Facoltativo | Se l'articolo è una variante; usa questa colonna per inserire lo stesso ID gruppo per tutte le varianti all'interno dello stesso gruppo di prodotti. Ad esempio; T-shirt Blu Facebook è una variante di T-Shirt Facebook. Facebook selezionerà una variante da mostrare per ciascun gruppo di prodotti in base alla pertinenza o alla popolarità. Limite di caratteri: 100.

gender: # Facoltativo | Il genere delle persone a cui è destinato l'articolo. | Valori supportati: female; male; unisex

color: # Facoltativo | Il colore dell'articolo. Usa una o più parole per descrivere il colore; non un codice esadecimale. Limite di caratteri: 200.

size: # Facoltativo | Le dimensioni dell'articolo scritte come parola; abbreviazione o numero; come piccolo; XL o 12. Limite di caratteri: 200.

age_group: # Facoltativo | La fascia di età a cui è destinato l'articolo. | Valori supportati: adult; all ages; infant; kids; newborn; teen; toddler

material: # Facoltativo | Il materiale di cui è composto l'articolo; come cotone; jeans o pelle. Limite di caratteri: 200.

pattern: # Facoltativo | Il motivo o la stampa grafica sull'articolo. Limite di caratteri: 100.

product_type: # Facoltativo | La categoria a cui appartiene l'articolo in base al sistema di categorizzazione dei prodotti della tua azienda; se presente. Puoi anche inserire una categoria prodotto Google. Limite di caratteri: 1000.

shipping: # Facoltativo | Dettagli di spedizione per l'articolo; scritti come: Paese:Area geografica:Servizio:Prezzo. Includi il codice di valuta ISO a 3 cifre nel prezzo. Per usare l'overlay di spedizione gratuita nelle tue inserzioni; inserisci il prezzo come 0.0. Usa una ";" per separare più dettagli di spedizione per diverse aree geografiche o Paesi. Solo le persone che si trovano nell'area geografica o nel Paese specificati vedranno i dettagli di spedizione per quell'area geografica o quel Paese. Puoi lasciare l'area geografica (mantieni i doppi "::") se i tuoi dettagli di spedizione sono gli stessi per un intero Paese.

shipping_weight: # Facoltativo | Il peso di spedizione dell'articolo espresso in lb; oz; g o kg.

rich_text_description: # Facoltativo | HTML description using a restricted set of tags
*/
