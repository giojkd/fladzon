<?php

namespace App\Console\Commands;

use App\Models\Googlecategory;
use Illuminate\Console\Command;
use Storage;
use Str;

class ImportGoogleProductTaxonomy extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fladzon:importgoogleproducttaxonomy';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import google product taxonomy from https://www.google.com/basepages/producttype/taxonomy.it-IT.txt';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        #$file = file_get_contents('https://www.google.com/basepages/producttype/taxonomy.it-IT.txt');
        $file = file_get_contents('http://www.google.com/basepages/producttype/taxonomy-with-ids.it-IT.txt');
        $rows = explode(PHP_EOL, $file);
        $excludedIndexes = [0, (count($rows) - 1)];
        $array = [];
        $currentCountItems = 0;
        $parent_id = null;
        $catIndexes = [];
        $bar = $this->output->createProgressBar(count($rows));
        $bar->start();
        foreach ($rows as $indexRow => $row) {
            if (!in_array($indexRow, $excludedIndexes)) {
                $items = collect(explode(' > ', $row));
                $firstItem = $items->shift();
                list($googleId, $firstCategory) = explode(' - ', $firstItem);
                $items->prepend($firstCategory);
                $catName = $items->pop();
                $parentIdIndex = $items->implode(' > ');

                if (isset($catIndexes[$parentIdIndex])) {
                    $parent_id = $catIndexes[$parentIdIndex];
                } else {
                    $parent_id = null;
                }

                $cat = Googlecategory::create(['name' => $catName, 'parent_id' => $parent_id, 'depth' => $items->count(), 'google_id' => $googleId]);
                $catIndexes[$items->push($catName)->implode(' > ')] = $cat->id;
            }
            $bar->advance();
        }
        $bar->finish();
    }
}
