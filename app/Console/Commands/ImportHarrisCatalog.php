<?php

namespace App\Console\Commands;

use App\Models\Manufacturer;
use App\Models\Product;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Str;
use Rap2hpoutre\FastExcel\FastExcel;
use App;
use App\Models\Category;
use App\Models\Collection;
use App\Models\Productgroup;
use App\Models\Productimport;

class ImportHarrisCatalog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fladzon:import_harris';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Product::withoutSyncingToSearch(
            function () {
                $import = new Productimport();
                $import->save();
                $locale = 'it';
                $features = 2;
                App::setLocale($locale);
                $path = env('IMPORT_FILE');
                $fullPath = Storage::disk(config('backpack.base.root_disk_name'))->getAdapter()->getPathPrefix() . $path;

                $items = (new FastExcel)->import($fullPath);

                $imagesByGroup = [];

                $groupsArray =

                $items->each(function ($item, $key) use ($locale, &$lastGroupName, &$imagesByGroup) {

                    #if($item['Image Src'] != ''){
                    if($item['Gruppo'] != ''){
                        #$img = collect(Str::of($item['Image Src'])->explode('/'))->last();
                        #$img = collect(Str::of($img)->explode('?'))->first();


                        $dir = Storage::disk(config('backpack.base.root_disk_name'));
                        $photosPath = env('IMPORT_PHOTOS') . '/' . $item['Gruppo univoco'];

                        $files = collect($dir->files($photosPath))->transform(function($item,$key){
                            return Str::replaceFirst('public/', '', $item);
                        });

                        $imagesByGroup[$item['Gruppo univoco']] = $files;
                    }

                });

                $lastGroupName = '';
                $categoryName = '';
                $description = '';
                $enabled = true;
                $group = [];
                $counter = 0;

                $items->each(function ($item, $key) use (&$counter, $import,&$attrExtGroup, &$attrExtGroupForSiblings,$locale, &$lastGroupName, $imagesByGroup, $features, &$categoryName, &$description, &$enabled, &$group) {

                    #dd($item);

                    $this->line('Counter: '.$counter);

                    if ($item['Gruppo univoco'] != '' && $item['Variant SKU'] != '') { #perform row checks validity

                        if ($item['Gruppo univoco'] != $lastGroupName){

                            $lastGroupName = $item['Gruppo univoco'];
                            $isDefault = true;
                            $enabled = ($item['Published'] == 'true') ? 1 : 0;
                            $categoryName = trim(strtolower($item['categoria']));
                            $description = $item['descrizione'];
                            $enabled = ($item['Published'] == 'true') ? true : false;

                            $attrExtGroup = (isset($item['GRUPPO IDENTIFICATIVO'])) ? $item['GRUPPO IDENTIFICATIVO'] : '';
                            $attrExtGroupForSiblings = $item['Gruppo'];

                            $group = Productgroup::create([
                                'ext_id' => $item['Gruppo univoco'],
                                'productimport_id' => $import->id,
                                ]);

                            $group->continue_selling = ($item['Variant Inventory Policy'] == 'continue') ? 1 : 0;
                            $group->price = $item['Variant Price'];
                            $group->compare_price = (!is_null($item['Variant Compare At Price']) && $item['Variant Compare At Price'] != '') ? $item['Variant Compare At Price'] : $item['Variant Price'];
                            $group->active = $enabled;
                            $group->is_on_sale = ($item['Vendor'] == 'Madè Firenze') ? 0 : 1;

                            $group->ext_id_for_siblings = $attrExtGroupForSiblings;

                            $group->save();

                        }else{
                            $isDefault = false;
                        }

                        $manufacturer = Manufacturer::firstOrCreate(['name' => $item['Vendor']]);


                        $category = Category::where('name->' . $locale, $categoryName)->first();
                        if (is_null($category)) {
                            $category = Category::create([
                                'name' => $categoryName
                            ]);
                        }

                        $product = Product::create([
                            'sku' => $item['Variant SKU'],
                            'manufacturer_id' => $manufacturer->id,
                            'productimport_id' => $import->id,
                            'category_id' => $category->id

                        ]);

                        if(isset($item['Season']) && $item['Season'] != ''){

                            $collection = Collection::where('name->it',$item['Season'])->first();

                            if(is_null($collection)){
                                $collection = Collection::create(['name' =>  $item['Season']]);
                            }

                            $collection->products()->attach($product);
                            $collection->productgroups()->attach($group);

                        }

                        $this->line('Key: ' . $key . ' SKU: ' . $product->sku);

                        if ($isDefault) {
                            $group->product_id = $product->id;
                            $group->save();
                        }



                        $product->attributevalueextensiongroup_id = $attrExtGroup;
                        $product->name = [$locale => $item['nome']];
                        #$product->short_description = [$locale => $item['Descrizione breve']];
                        $product->description = [$locale => strip_tags($description)];
                        #$product->extended_description = [$locale => $item['Descrizione Extra']];
                        $product->category_id = $category->id;
                        $product->quantity = $item['Quantità'];
                        $product->price = $item['Variant Price'];
                        $product->compare_price = (!is_null($item['Variant Compare At Price']) && $item['Variant Compare At Price'] != '') ? $item['Variant Compare At Price'] : $item['Variant Price'];
                        #$product->price_per = [$locale => $item['Prezzo al']];
                        #$product->minimum_increase = $item['Minimo incremento.'];
                        #$product->min_quantity = ($item['Q. minima'] != '') ? $item['Q. minima'] : 1;
                        #$product->warranty_years = ($item['Garanzia (anni)'] != '') ? $item['Garanzia (anni)'] : 2;
                        #$product->reassortment_days = $item['Giorni per disp.'];
                        $product->reassortment_days = 0;
                        #$product->shipping_days = $item['Giorni per sped.'];
                        $product->shipping_days = 2;
                        #$product->quantity = $item['Disponibili'];
                        #$product->quantity = 0;#100000;
                        #$product->product_weight = $item['Peso (g)'];
                        #$product->product_width = $item['Larghezza (cm)'] * 10;
                        #$product->product_height = $item['Altezza (cm)'] * 10;
                        #$product->product_length = $item['Profondità (cm)'] * 10;
                        $product->productgroup_id = $group->id;
                        $product->enabled = $enabled;
                        $product->photos = isset($imagesByGroup[$item['Gruppo univoco']]) ? $imagesByGroup[$item['Gruppo univoco']] : [];

                        $continueSelling = ($item['Variant Inventory Policy'] == 'continue') ? 1 : 0;

                        $product->continue_selling = $continueSelling;
                        $product->gender_id = $item['Gender'];

                        $product->save();

                        for ($i = 1; $i <= $features; $i++) {
                            $feature = $item['Caratteristica ' . $i];
                            if ($feature != '') {
                                if (count(explode(':', $feature)) == 2) {
                                    list($attr, $value) = explode(':', $feature);
                                    $product->addAttribute($attr, $value);
                                }
                            }
                        }


                    }
                });
            }
        );

        return 0;
    }
}
