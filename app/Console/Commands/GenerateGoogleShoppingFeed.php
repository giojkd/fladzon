<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Product;
use Str;
use Storage;
use League\Csv\Writer;
use App;
use Exception;

class GenerateGoogleShoppingFeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fladzon:generategoogleshoppingfeed {locale=it}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    function strip_tags_content($string)
    {
        // ----- remove HTML TAGs -----
        $string = preg_replace('/<[^>]*>/', ' ', $string);
        // ----- remove control characters -----
        $string = str_replace("\r", '', $string);
        $string = str_replace("\n", ' ', $string);
        $string = str_replace("\t", ' ', $string);
        // ----- remove multiple spaces -----
        $string = trim(preg_replace('/ {2,}/', ' ', $string));
        return $string;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $csv = collect([]);

        $locale = $this->argument('locale');
        $genders = [
            1 => 'male',
            2 => 'female',
            3 => 'unisex'
        ];

        $this->line('Google Shopping Feed in "'.Str::upper($locale).'"');

        App::setLocale($locale);

        Product::with(['attributevalues'])->whereEnabled(1)->chunk(50, function ($products) use ($csv, $genders) {

            foreach ($products as $product) {

                    $attributeValues = $product->attributevalues->keyBy('attribute_id');

                    $cover = (!is_null($product->photos)) ? collect($product->photos)->first() : null;
                    if (!is_null($cover)) {
                        $cover =  url(Route('ir', ['size' => 'h480', 'filename' => $cover], false));
                    }

                    $this->line('product ' . $product->id);
                    if (
                        $product->name != '' &&
                        $product->description != '' &&
                        !is_null($cover) &&
                        !is_null($product->manufacturer) &&
                        !is_null($product->category)
                    ) {

                        $shippingWeight = (!is_null($product->packaging_weight)) ? $product->packaging_weight : $product->product_weight;
                        $shippingWeight = (!is_null($shippingWeight)) ? $shippingWeight . ' g' : null;

                        $push = [
                            'id' => $product->id,
                            'title' => $product->name,
                            'description' =>  substr($this->strip_tags_content($product->description), 0, 5000),

                            'link' => url($product->makeUrl()),
                            'price' => number_format($product->price, 2) . ' EUR',

                            'condition' => 'new',
                            'availability' => ((float)$product->quantity > 0) ? 'in stock' : 'out of stock',

                            'image_link' => $cover,


                            'shipping_weight' =>  $shippingWeight,
                            'shipping' => 'IT:::' . number_format($product->getShippingCost(), 2) . 'EUR',

                            'item_group_id' => $product->productgroup_id,
                            'color' => (isset($attributeValues[env('COLOR_ATTRIBUTE_ID')])) ? $attributeValues[env('COLOR_ATTRIBUTE_ID')]->name : null,
                            'mpn' => $product->sku,
                            'size' => (isset($attributeValues[env('SIZE_ATTRIBUTE_ID')])) ? $attributeValues[env('SIZE_ATTRIBUTE_ID')]->name : null,
                            'gender' => (!is_null($product->gender_id)) ? $genders[$product->gender_id] : '',
                            'age_group' => $product->age_group,
                            'brand' => $product->manufacturer->name,

                            'google_product_category' => (!is_null($product->category->googlecategory)) ? $product->printGooglecategoryBreadcrumb() : null,

                            'tax' => '',
                            'product_type' => '',

                        ];

                        $csv->push($push);
                    }

            }
        });

        $csvWriter = Writer::createFromString('');

        $csvWriter->setDelimiter('|');
        #$csvWriter->setEnclosure('');

        $header = ['id', 'title', 'description', 'link', 'price', 'condition', 'availability', 'image_link', 'shipping_weight', 'shipping', 'item_group_id', 'color', 'mpn', 'size', 'gender', 'age_group', 'brand', 'google_product_category', 'tax', 'product_type'];

        $csvWriter->insertOne($header);
        $csvWriter->insertAll($csv);

        $file = $csvWriter->getContent();

        Storage::disk('public')->put('google_shopping_feed_' . $locale . '.csv', $file);
    }
}
