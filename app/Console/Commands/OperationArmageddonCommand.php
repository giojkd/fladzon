<?php

namespace App\Console\Commands;

use App\Http\Requests\GuestRequest;
use App\Models\Article;
use App\Models\Attributevalueextensiongroup;
use App\Models\Attributevalueextensiongrouprow;
use App\Models\Carousel;
use App\Models\Category;
use App\Models\Collection;
use App\Models\Collectiondiscount;
use App\Models\Conversionscale;
use App\Models\Conversionscalevalue;
use App\Models\Discountcode;
use App\Models\Dump;
use App\Models\Event;
use App\Models\Guest;
use App\Models\Lead;
use App\Models\Manufacturer;
use App\Models\Menu;
use App\Models\Menuitem;
use App\Models\Metatag;
use App\Models\Newslettersubscription;
use App\Models\Product;
use App\Models\Productgroup;
use App\Models\Productimport;
use App\Models\Review;
use App\Models\Shipping;
use App\Models\Shippingrow;
use App\Models\Slide;
use App\Models\Stockoperation;
use App\Models\Warehouse;
use App\Models\Warehouseavailability;
use App\Models\Webhook;
use App\User;
use Attribute;
use Illuminate\Console\Command;
use Venturecraft\Revisionable\Revision;
use Illuminate\Support\Facades\DB;

class OperationArmageddonCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fladzon:operationarmageddon';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Lead::truncate();

        DB::table('lead_product')->truncate();

        Guest::truncate();
        Article::truncate();
        Attributevalueextensiongroup::truncate();
        Attributevalueextensiongrouprow::truncate();

        #

        DB::table('attributevalue_product')->truncate();

        Carousel::truncate();
        Category::truncate();
        Collectiondiscount::truncate();

        #Collection::truncate();
        #
        #

        DB::table('collection_product')->truncate();
        DB::table('collection_productgroup')->truncate();

        Conversionscale::truncate();
        Conversionscalevalue::truncate();
        Discountcode::truncate();

        #
        #
        #

        DB::table('discountcode_productgroup')->truncate();
        DB::table('discountcode_product')->truncate();
        DB::table('discountcode_lead')->truncate();


        Dump::truncate();
        Event::truncate();
        Manufacturer::truncate();
        #Menuitem::truncate();
        #Menu::truncate();
        Metatag::truncate();
        Newslettersubscription::truncate();
        Product::truncate();
        Productgroup::truncate();
        Productimport::truncate();
        Review::truncate();
        Revision::truncate();
        Shippingrow::truncate();
        Shipping::truncate();
        Slide::truncate();
        Stockoperation::truncate();
        User::truncate();
        #
        DB::table('user_warehouse')->truncate();


        Warehouseavailability::truncate();
        #Warehouse::truncate();
        Webhook::truncate();
    }
}
