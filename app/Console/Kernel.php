<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('fladzon:sendleadconfirmations')->everyMinute();
        $schedule->command('fladzon:applycollectiondiscounts')->everyMinute();
        $schedule->command('fladzon:processwebhooks')->everyMinute();

        $schedule->command('fladzon:generatefacebookshopcatalog it it_IT')->everySixHours();
        $schedule->command('fladzon:generatefacebookshopcatalog en en_XX')->everySixHours();
        $schedule->command('fladzon:generatefacebookshopcatalog de de_DE')->everySixHours();

        $schedule->command('fladzon:generategoogleshoppingfeed it')->everySixHours();
        $schedule->command('fladzon:generategoogleshoppingfeed en')->everySixHours();
        $schedule->command('fladzon:generategoogleshoppingfeed de')->everySixHours();

        $schedule->command('fladzon:seedrecombeeproducts')->daily();

        //$schedule->command('fladzon:revertpricesfornotavailablecontinuesellingproducts')->everyMinute();

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
