<?php

namespace App\Traits;

use Str;
use App;


trait Scalapay
{

    public function getScalapayCheckoutUrl()
    {

        $authorization = new \Scalapay\Scalapay\Model\Merchant\Authorization(
            env('SCALAPAY_ENDPOINT'),
            env('SCALAPAY_SECRET_KEY')
        );

        //Customr information
        $consumer = new \Scalapay\Scalapay\Model\Merchant\Consumer();
        if(isset($this->billing_address['email'])){
            $consumer->setEmail($this->billing_address['email']);
        }
        if (isset($this->billing_address['name'])) {
            $consumer->setGivenNames($this->billing_address['name']);
        }
        if (isset($this->billing_address['surname'])) {
            $consumer->setSurname($this->billing_address['surname']);
        }
        if (isset($this->billing_address['telephone'])) {
            $consumer->setPhoneNumber($this->billing_address['telephone']);
        }

        /*
        //Billing details
        $billing = new \Scalapay\Scalapay\Model\Merchant\Contact();
        $billing->setName('John Doe');
        #$billing->setLine1('123 Test Street');
        #$billing->setSuburb('Test');
        #$billing->setPostcode('00000');
        #$billing->setCountryCode('IT');
        $billing->setPhoneNumber('0522789651');

        //Shipping details
        $shipping = new \Scalapay\Scalapay\Model\Merchant\Contact();
        $shipping->setName('John shipping');
        $shipping->setLine1('123 Test Street');
        $shipping->setSuburb('Test');
        $shipping->setPostcode('00000');
        $shipping->setCountryCode('IT');
        $shipping->setPhoneNumber('0522789651');

        //Products details
        $item = new \Scalapay\Scalapay\Model\Merchant\Item();
        $item->setName('test product');
        $item->setSku('test123');
        $item->setQuantity(1);
        $itemPrice = new \Scalapay\Scalapay\Model\Merchant\Money();
        $itemPrice->setAmount(100);
        $item->setPrice($itemPrice);
        $itemList = array();
        $itemList[] = $item;
*/

        //Confirm & failure URLS
        $merchantOptions = new \Scalapay\Scalapay\Model\Merchant\MerchantOptions();
        $merchantOptions->setRedirectConfirmUrl(env('SCALAPAY_REDIRECT_SUCCESS_URL'));
        $merchantOptions->setRedirectCancelUrl(env('SCALAPAY_REDIRECT_ERROR_URL'));

        //Order total
        $totalAmount = new \Scalapay\Scalapay\Model\Merchant\Money();
        $totalAmount->setAmount($this->grand_total);

        //Tax total
        $tax = $this->grand_total - ($this->grand_total/1.22);
        $taxAmount = new \Scalapay\Scalapay\Model\Merchant\Money();
        $taxAmount->setAmount($tax);

        //Shipping total
        $shippingAmount = new \Scalapay\Scalapay\Model\Merchant\Money();
        $shippingAmount->setAmount($this->shipping_total);

        //Discount total
        $discountAmount = new \Scalapay\Scalapay\Model\Merchant\Money();
        $discountAmount->setAmount($this->discount_total);

        $discount = new \Scalapay\Scalapay\Model\Merchant\Discount();
        $discount->setDisplayName("scalapay");
        $discount->setAmount($discountAmount);
        $discountList = array();
        $discountList[] = $discount;

        //Order detail
        $orderDetails = new \Scalapay\Scalapay\Model\Merchant\OrderDetails();
        $orderDetails->setConsumer($consumer);
        $orderDetails->setMerchant($merchantOptions);
        $orderDetails->setTotalAmount($totalAmount);
        $orderDetails->setShippingAmount($shippingAmount);
        $orderDetails->setTaxAmount($taxAmount);
        $orderDetails->setDiscounts($discountList);
        //merchant reference is cart_id or order_id in your system
        $orderDetails->setMerchantReference(str_pad($this->id, 9, "0", STR_PAD_LEFT));

        #$orderDetails->setItems($itemList);
        #$orderDetails->setBilling($billing);
        #$orderDetails->setShipping($shipping);


        /*Create Order
        You will get order token and redirect url
        */

        $scalapayApi = new \Scalapay\Scalapay\Factory\Api();
        $apiResponse  = $scalapayApi->createOrder($authorization, $orderDetails);

        $token = $apiResponse->getToken();

        $this->scalapay_token = $token;
        $this->saveWithoutEvents();

        return ['url' => $apiResponse->getCheckoutUrl(),'token'=> $token,'expires'=> $apiResponse->getExpires()];

    }

    public function captureScalapayPayment(){

        $authorization = new \Scalapay\Scalapay\Model\Merchant\Authorization(
            env('SCALAPAY_ENDPOINT'),
            env('SCALAPAY_SECRET_KEY')
        );

        //You will get $token from createOrder
        //Capture will work after successful payment

        $token = $this->scalapay_token;

        $scalapayApi = new \Scalapay\Scalapay\Factory\Api();
        $apiResponse  = $scalapayApi->capture($authorization, $token);

        return $apiResponse;
    }



}
