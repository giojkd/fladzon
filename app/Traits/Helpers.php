<?php

namespace App\Traits;
use Str;
use App;

trait Helpers
{



    public function getModelName()
    {
        $reflection = new \ReflectionClass($this);
        $modelName = implode("\\", [$reflection->getNamespaceName(), $reflection->getShortName()]);
        return $modelName;
    }

    public function saveWithoutEvents(array $options = [])
    {
        return static::withoutEvents(function () use ($options) {
            return $this->save($options);
        });
    }


    public function makeUrl($locale = null)
    {
        $locale = (!is_null($locale)) ? $locale : App::getLocale();

        $reflection = new \ReflectionClass($this);
        #$modelName = implode("\\", [$reflection->getNamespaceName(), $reflection->getShortName()]);;
        $modelName = Str::lower($reflection->getShortName());
        App::setLocale($locale);
        $name = Str::slug($this->name);
        if(!is_null($name) && $name != ''){
            return Route($modelName . 'Page', ['locale' => $locale, 'name' => $name, 'id' => $this->id], false);
        }
        return '';

    }

    public function reviews(){
        return $this->morphMany('App\Models\Review','reviewable');
    }

    public function updateReviewsScore(){
        $reviews = $this->reviews;
        $avg = $reviews->avg('score');


        $this->reviews_score  = $avg;
        $this->saveWithoutEvents();
    }

}
