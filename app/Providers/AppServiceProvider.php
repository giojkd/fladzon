<?php

namespace App\Providers;

use App\Models\Guest;
use App\Models\Product;
use App\Models\Productgroup;
use App\Models\Productimport;
use App\Models\Review;
use App\Models\Shipping;
use App\Observers\ReviewObserver;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use App\Observers\StockoperationObserver;
use App\Models\Stockoperation;
use App\Observers\GuestObserver;
use App\Observers\ProductgroupObserver;
use App\Observers\ProductimportObserver;
use App\Observers\ProductObserver;
use App\Observers\ShippingObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //


        if (env('FORCE_HTTPS')) {
            \URL::forceScheme('https');
        }

        Stockoperation::observe(StockoperationObserver::class);
        Review::observe(ReviewObserver::class);
        Shipping::observe(ShippingObserver::class);
        Productgroup::observe(ProductgroupObserver::class);
        Product::observe(ProductObserver::class);
        Productimport::observe(ProductimportObserver::class);
        Guest::observe(GuestObserver::class);

        Blade::directive('hss', function ($param) {
            return "<?php echo '<div class=\"height-spacer\" style=\"height: '.$param.'px\"></div>'; ?>";
        });
        Blade::directive('fp', function ($param) {
            #$param = (double)$param;
            return "<?php echo '&euro;'. number_format($param,2); ?>";
        });
        Blade::directive('cim', function ($param) {
            return "<?php echo $param; ?>";


        });

        Blade::directive('cim_backup', function ($param) {

            return "<?php echo 'https://awkxoooseq.cloudimg.io/v7/'.str_replace(['https://','http://'],'',$param); ?>";
        });

    }
}
