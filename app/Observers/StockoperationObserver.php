<?php

namespace App\Observers;
use App\Models\Stockoperation;
use App\Models\Product;

class StockoperationObserver
{
    /**
     * Handle the models credit operation "created" event.
     *
     * @param  \App\Model\Stockoperation  $Stockoperation
     * @return void
     */
    public function created(Stockoperation $Stockoperation)
    {
        //

#        dd($Stockoperation);

        if(env('MULTI_WAREHOUSE')){
            Product::withoutSyncingToSearch(function () use($Stockoperation){
                    $product = $Stockoperation->product;

                    #$product->quantity = $product->quantity + ($Stockoperation->quantity * $Stockoperation->sign);



                    $wa = $product->warehouseavailabilities()->where('warehouse_id', $Stockoperation->warehouse_id)->first();
                    if(!is_null($wa)){
                        $wa->quantity = $wa->quantity + ($Stockoperation->quantity * $Stockoperation->sign);
                        $wa->save();
                    }else{
                        $product->warehouseavailabilities()->create([
                            'warehouse_id' => $Stockoperation->warehouse_id,
                            'quantity' => $Stockoperation->quantity * $Stockoperation->sign
                        ]);
                    }

                    $product->quantity = $product->warehouseavailabilities->pluck('quantity')->sum();


                    #$product->saveWithoutEvents();
                    $product->save();

            });
        }

    }

    /**
     * Handle the models credit operation "updated" event.
     *
     * @param  \App\Stockoperation  $Stockoperation
     * @return void
     */
    public function updated(Stockoperation $Stockoperation)
    {
        //
    }

    /**
     * Handle the models credit operation "deleted" event.
     *
     * @param  \App\Stockoperation  $Stockoperation
     * @return void
     */
    public function deleted(Stockoperation $Stockoperation)
    {
        //
    }

    /**
     * Handle the models credit operation "restored" event.
     *
     * @param  \App\Stockoperation  $Stockoperation
     * @return void
     */
    public function restored(Stockoperation $Stockoperation)
    {
        //
    }

    /**
     * Handle the models credit operation "force deleted" event.
     *
     * @param  \App\Stockoperation  $Stockoperation
     * @return void
     */
    public function forceDeleted(Stockoperation $Stockoperation)
    {
        //
    }
}
