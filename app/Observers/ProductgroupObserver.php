<?php

namespace App\Observers;

use App\Models\Productgroup;

class ProductgroupObserver
{
    /**
     * Handle the productgroup "created" event.
     *
     * @param  \App\Productgroup  $productgroup
     * @return void
     */

    public function updating(Productgroup $productgroupNew){

        //Product::withoutEvents(function () use ($productgroupNew) {

            $productgroupOld = Productgroup::find($productgroupNew->id);
            $update = [];

            if($productgroupOld->continue_selling != $productgroupNew->continue_selling){
                $update['continue_selling'] = $productgroupNew->continue_selling;
            }

            if ($productgroupOld->price != $productgroupNew->price) {
                $update['price'] = $productgroupNew->price;
            }

            if ($productgroupOld->compare_price != $productgroupNew->compare_price) {
                $update['compare_price'] = $productgroupNew->compare_price;
            }

            if ($productgroupOld->active != $productgroupNew->active) {
                $update['enabled'] = $productgroupNew->active;
            }




            if(count($update) > 0 ){
                if(!is_null($productgroupNew->products)){
                    foreach($productgroupNew->products as $product){
                        $product->fill($update)->save();
                    }
                }
                #$productgroupNew->products()->update($update);
            }

        //});

    }

    public function created(Productgroup $productgroup)
    {
        //
    }

    /**
     * Handle the productgroup "updated" event.
     *
     * @param  \App\Productgroup  $productgroup
     * @return void
     */
    public function updated(Productgroup $productgroup)
    {
        //
    }

    /**
     * Handle the productgroup "deleted" event.
     *
     * @param  \App\Productgroup  $productgroup
     * @return void
     */
    public function deleted(Productgroup $productgroup)
    {
        //

        $productgroup->collections()->detach();
    }

    /**
     * Handle the productgroup "restored" event.
     *
     * @param  \App\Productgroup  $productgroup
     * @return void
     */
    public function restored(Productgroup $productgroup)
    {
        //
    }

    /**
     * Handle the productgroup "force deleted" event.
     *
     * @param  \App\Productgroup  $productgroup
     * @return void
     */
    public function forceDeleted(Productgroup $productgroup)
    {
        //
    }
}
