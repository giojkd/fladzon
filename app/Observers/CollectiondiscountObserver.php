<?php

namespace App\Observers;

use App\collectiondiscount;

class CollectiondiscountObserver
{
    /**
     * Handle the collectiondiscount "created" event.
     *
     * @param  \App\collectiondiscount  $collectiondiscount
     * @return void
     */
    public function created(collectiondiscount $collectiondiscount)
    {
        //
    }

    /**
     * Handle the collectiondiscount "updated" event.
     *
     * @param  \App\collectiondiscount  $collectiondiscount
     * @return void
     */
    public function updated(collectiondiscount $collectiondiscount)
    {
        //
    }

    /**
     * Handle the collectiondiscount "deleted" event.
     *
     * @param  \App\collectiondiscount  $collectiondiscount
     * @return void
     */
    public function deleted(collectiondiscount $collectiondiscount)
    {
        //
    }

    /**
     * Handle the collectiondiscount "restored" event.
     *
     * @param  \App\collectiondiscount  $collectiondiscount
     * @return void
     */
    public function restored(collectiondiscount $collectiondiscount)
    {
        //
    }

    /**
     * Handle the collectiondiscount "force deleted" event.
     *
     * @param  \App\collectiondiscount  $collectiondiscount
     * @return void
     */
    public function forceDeleted(collectiondiscount $collectiondiscount)
    {
        //
    }
}
