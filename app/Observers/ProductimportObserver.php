<?php

namespace App\Observers;

use App\Models\Productimport;

class ProductimportObserver
{
    /**
     * Handle the productimport "created" event.
     *
     * @param  \App\Productimport  $productimport
     * @return void
     */
    public function created(Productimport $productimport)
    {
        //
    }

    /**
     * Handle the productimport "updated" event.
     *
     * @param  \App\Productimport  $productimport
     * @return void
     */
    public function updated(Productimport $productimport)
    {
        //
    }

    /**
     * Handle the productimport "deleted" event.
     *
     * @param  \App\Productimport  $productimport
     * @return void
     */



    public function deleted(Productimport $productimport)
    {
        //


        $productimport->products()->delete();
        $productimport->productgroups()->delete();

    }

    /**
     * Handle the productimport "restored" event.
     *
     * @param  \App\Productimport  $productimport
     * @return void
     */
    public function restored(Productimport $productimport)
    {
        //
    }

    /**
     * Handle the productimport "force deleted" event.
     *
     * @param  \App\Productimport  $productimport
     * @return void
     */
    public function forceDeleted(Productimport $productimport)
    {
        //
    }
}
