<?php

namespace App\Observers;

use App\Models\Shipping;

class ShippingObserver
{
    /**
     * Handle the shipping "created" event.
     *
     * @param  \App\Shipping  $shipping
     * @return void
     */
    public function created(Shipping $shipping)
    {
        //
    }

    /**
     * Handle the shipping "updated" event.
     *
     * @param  \App\Shipping  $shipping
     * @return void
     */
    public function updated(Shipping $shipping)
    {
        //
    if(!is_null($shipping->tracking_number)){
        $shipping->sentTrackingNumber();
    }

    }

    /**
     * Handle the shipping "deleted" event.
     *
     * @param  \App\Shipping  $shipping
     * @return void
     */
    public function deleted(Shipping $shipping)
    {
        //
    }

    /**
     * Handle the shipping "restored" event.
     *
     * @param  \App\Shipping  $shipping
     * @return void
     */
    public function restored(Shipping $shipping)
    {
        //
    }

    /**
     * Handle the shipping "force deleted" event.
     *
     * @param  \App\Shipping  $shipping
     * @return void
     */
    public function forceDeleted(Shipping $shipping)
    {
        //
    }
}
