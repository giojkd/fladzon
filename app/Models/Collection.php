<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Illuminate\Support\Facades\DB;

class Collection extends Model
{
    use CrudTrait;
    use HasTranslations;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'collections';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    public $translatable = ['name','subtitle','short_description','description'];
    protected $casts = ['images' => 'json'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function revertProductGroupsPrice(){
        if(!is_null($this->productgroups)){
            foreach($this->productgroups as $group){
                if(!is_null($group->products)){
                    foreach ($group->products as $product) {
                        $product->revertPrice();
                    }
                }
            }
        }
    }

    public function revertProductsPrice()
    {
        if(!is_null($this->products)){
            foreach ($this->products as $product) {
                $product->revertPrice();
            }
        }
    }


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */



    public function products(){
        return $this->belongsToMany(Product::class);
    }

    public function productgroups()
    {
        return $this->belongsToMany(Productgroup::class);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
