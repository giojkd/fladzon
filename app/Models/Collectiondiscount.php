<?php

namespace App\Models;

use App\Traits\Helpers;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Collectiondiscount extends Model
{
    use CrudTrait;
    use Helpers;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'collectiondiscounts';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function applyDiscount()
    {
        $collectionDiscount = $this->discount;
        $collection = $this->collection;
        $groups = $collection->productgroups()->where('block_discounts',0)->get();
        if(!is_null($groups)){
            foreach($groups as $group){

                Productgroup::withoutSyncingToSearch(function () use($group, $collectionDiscount) {
                    $discount = $group->compare_price * $collectionDiscount / 100;
                    $group->price = $group->compare_price - $discount;
                    $group->save();
                });

                $products = $group->products;
                if(!is_null($products)){
                    foreach($products as $product){
                        Product::withoutSyncingToSearch(function() use($product, $collectionDiscount){
                            $discount = $product->compare_price * $collectionDiscount / 100;
                            if($product->continue_selling && $product->quantity <= 0){
                                $product->revertPrice();
                            }else{
                                $product->price = $product->compare_price - $discount;
                                $product->save();
                            }

                        });
                    }
                }

            }
        }
        $this->applied_at = date('Y-m-d H:i:s');
        $this->save();
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function collection(){
        return $this->belongsTo(Collection::class);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
