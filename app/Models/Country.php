<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Country extends \Lwwcas\LaravelCountries\Models\Country
{
    use HasFactory;

    public function countriesgroups(){
        return $this->belongsToMany(Countriesgroup::class);
    }

}
