<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;
use App;
use App\Traits\Helpers;
use Str;
use Recombee\RecommApi\Client;
use Recombee\RecommApi\Requests as Reqs;
use Recombee\RecommApi\Exceptions as Ex;
use Illuminate\Database\Eloquent\SoftDeletes;

class Productgroup extends Model
{
    use CrudTrait;
    use HasTranslations;
    use Searchable;
    use Helpers;
    use SoftDeletes;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'productgroups';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    protected $translatable = ['name'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function sendToRecombee(){
        $client = new Client(env('RECOMBEE_DB_ID'), env('RECOMBEE_DB_KEY'));

        $data = $this->toSearchableArray();

        $requests = array();

            $itemId = $this->id;
            $r = new Reqs\SetItemValues(
                    $itemId,
                    $data,
                    ['cascadeCreate' => true]
                );
            array_push($requests, $r);

        $result =  $client->send(new Reqs\Batch($requests));

        return $result;
    }

    public function searchableAs()
    {
        return env('THEME_NAME').'_'.$this->table;
    }

    public function toSearchableArray()
    {
        $defaultLocale = 'it';
        App::setLocale($defaultLocale);
        $locales = array_keys(config('backpack.crud.locales'));
        #$product = $this->products->first();
        $product = $this->product;
        $attrs = $product->attributevalues;

        $productCollections = $this->collections;

        foreach ($locales as $locale) {
            $locale = strtolower((substr($locale, 0, 2)));
            $array['name_' . $locale] = $product->getTranslation('name', $locale);
            $array['description_' . $locale] = preg_replace("/\r|\n/", "", strip_tags($product->getTranslation('description', $locale)));
            $array['short_description_' . $locale] = preg_replace("/\r|\n/", "", strip_tags($product->getTranslation('short_description', $locale)));
            $array['category_' . $locale] = $product->category->getTranslation('name', $locale);
            $array['url_' . $locale] = $product->makeUrl($locale);
            if ($attrs->count() > 0) {
                foreach ($attrs as $attr) {
                    if($attr->attribute->is_visual){
                        $attrName = $attr->attribute->getTranslations()['name'][$defaultLocale];
                        $array[$attrName . '_' . $locale] = $attr->getTranslation('name', $locale);
                    }
                }
            }
            if (!is_null($product->gender)) {
                $array['gender_' . $locale] = $product->gender->getTranslation('name', $locale);
            }


            if(!is_null($productCollections)){
                foreach($productCollections as $collection){
                    $array['collections_'.$locale][] = $collection->getTranslation('name', $locale);
                }
            }

            $array['attributes_line_'.$locale] = $product->getAttrbutesLine($locale);

        }
        $array['brand'] = (!is_null($product->manufacturer)) ? $product->manufacturer->name : '';
        $array['price'] = $this->products->sortBy('price')->first()->price;
        $array['compare_price'] = $this->products->sortBy('compare_price')->first()->compare_price;
        $array['discount_percentage'] = $product->discount_percentage;

        $array['is_on_sale'] = $this->is_on_sale;

        $array['active'] = $this->active;

        $cover = (!is_null($product->photos)) ? collect($product->photos)->first() : null;
        if (!is_null($cover)) {

            $cover =  Route('ir', ['size' => 'h480', 'filename' => $cover], false);
            $secondCover = (collect($product->photos)->count() > 1) ? collect($product->photos)->get(1) : $cover;
            $secondCover =  Route('ir', ['size' => 'h480', 'filename' => $secondCover], false);

        }

        $array['cover'] = $cover;
        if(isset($secondCover))
            $array['second_cover'] = $secondCover;

        $array['skus'] = $this->products->pluck('sku');
        $array['photos'] = $this->product->photos;
        $array['created_at'] = $this->product->created_at;

        #$array = $this->transform($array);

        return $array;
    }

    public function getAdminImageAttribute(){
        return $this->cover('h240');
    }

    public function cover($size = 'h480'){
        if(is_object($this->product)){
            $product = $this->product;
            $cover = (!is_null($product->photos)) ? collect($product->photos)->first() : null;
            if (!is_null($cover)) {
                $cover =  Route('ir', ['size' => $size, 'filename' => $cover], false);
            }
            return $cover;
        }
        return '';
    }

    public function pdfCover($size = 'h480')
    {
        if (is_object($this->product)) {
            $product = $this->product;
            $cover = (!is_null($product->photos)) ? collect($product->photos)->first() : null;
            return asset($cover);
        }
        return '';
    }

    public function shouldBeSearchable()
    {

        if(is_object($this->product) && $this->active){
            return true;
        }
        return false;
    }

    public function countProducts(){
        return $this->products->count();
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

       public function collections(){
        return $this->belongsToMany(Collection::class);
    }


    public function products(){
        return $this->hasMany('App\Models\Product');
    }

    public function product(){
        return $this->belongsTo('App\Models\Product');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    public function getProductgroupQuantityAttribute(){
        return $this->products->sum('quantity');
    }

    public function getSkuAttribute(){
        if(is_object($this->product))
            return $this->product->sku;
        return '';
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
