<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Warehouse;
use App\Models\Lead;
use App\Models\Shippingrow;
use App\Scopes\WarehouseScope;
use Illuminate\Support\Facades\Mail;
use App\Mail\ShippingTrackingNumberSet;

class Shipping extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'shippings';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function printProducts()
    {
        $products = $this->products;
        $return = [];
        if(count($products) > 0 ){
            foreach ($products as $product) {
                $return[] = '<a target="_blank" href="' . $product->makeUrl() . '"><img height="40" src="' . $product->cover('h40') . '"></a> ' . $product->name . ' (' . $product->sku . ' | '.$product->attributes_line.') | €'.$product->sold_at_price.' x ' . $product->sold_quantity;
            }
        }
        return collect($return)->implode('<br>');
    }

    public function sentTrackingNumber(){


        $lead = $this->lead;

        $recipient = $lead->billing_address['email'];

        $data = [
            'content' => [
                'order_id' => $lead->id,
                'user_full_name' => $lead->billing_address['name'] . ' ' . $lead->billing_address['surname'],
                'lead' => $lead,
                'tracking_number' => $this->tracking_number
            ]
        ];

        Mail::to($recipient)->send(new ShippingTrackingNumberSet($data));
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function warehouse(){
        return $this->belongsTo(Warehouse::class);
    }

    public function lead(){
        return $this->belongsTo(Lead::class);
    }

    public function shippingrows(){
        return $this->hasMany(Shippingrow::class);
    }

    public function shipTo(){
        return $this->ship_to;
    }

    public function getShipToAttribute(){
        $lead = $this->lead;
        if(!is_null($lead)){
            if(!is_null($lead->shipping_address)){
                return $lead->printShippingAddress();
            }else{
                return $lead->printBillingAddress();
            }

        }
        return '';
    }

    public function getProductsAttribute(){

        $return = [];
        if($this->shippingrows->count() > 0){
            $this->shippingrows->each(function ($row, $key) use (&$return) {
                if(is_object($row->lead_product)){
                    $product = $row->lead_product->product;
                    $product->sold_quantity = $row->quantity;
                    $product->sold_at_price = $row->lead_product->sub_total;
                    $return[] = $product;
                }
            });
        }
        return collect($return);#->implode(', ');
    }

    public function getWarehouseNameAttribute(){
        return $this->warehouse->name;
    }


    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    protected static function booted()
    {
        static::addGlobalScope(new WarehouseScope);
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
