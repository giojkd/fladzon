<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Manufacturer;
use App\Models\Category;
use App\Models\Shippingrule;
use App\Models\Attributevalue;
use App\Models\Productgroup;
use App\Models\Warehouseavailability;
use App\Traits\Helpers;
use Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
use Str;
use App;
use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use CrudTrait;
    use HasTranslations;
    use Helpers;
    use Searchable;
    use SoftDeletes;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'products';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];
    protected $casts = ['photos' => 'array'];
    public $translatable = ['name', 'short_description','description','extended_description'];



    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function revertPrice(){
        $this->price = $this->compare_price;
        $this->saveWithoutEvents();
    }

    public function getShortDescription($limit = 100){
        $ellipsis = '...';
        return (!is_null($this->short_description) && $this->short_description != '') ? $this->short_description : Str::limit(strip_tags($this->description),$limit, $ellipsis);
    }

    public function getSiblings(){
        if(is_object($this->group)){
            $groups = Productgroup::whereExtIdForSiblings($this->group->ext_id_for_siblings)->where('id','!=',$this->group->id)->get();
            $return = collect([]);
            if(!is_null($groups)){
                foreach($groups as $group){
                    $return->push($group->product);
                }
                return collect($return->filter()->all());
            }
        }

        return collect([]);

    }

    public function variants(){
        return Product::where('productgroup_id',$this->productgroup_id)->get();
    }

    public function shouldBeSearchable()
    {
        return true;
        #return ($this->enabled && !is_null($this->category));
    }

    public function getShippingCost(){

        $weight = (!is_null($this->packaging_weight)) ? $this->packaging_weight : $this->product_weight ;

        if(!is_null($weight)){
            $shippingRule = Shippingrule::whereRaw('weight_from <= ' . $weight . ' AND weight_to >' . $weight)->first();
            if (!is_null($shippingRule)) {
                return $shippingRule->price;
            }
        }

        return 0;

    }

    public function collections(){
        return $this->belongsToMany(Collection::class);
    }

    public function cover($size = 'h480'){

        $cover = (!is_null($this->photos)) ? collect($this->photos)->first() : null;

        if (!is_null($cover)) {
            $cover =  url(Route('ir', ['size' => $size, 'filename' => $cover], false));
            return $cover;
        }

        return null;

    }

    public function pdfCover($size = 'h480')
    {

        $cover = (!is_null($this->group->product->photos)) ? collect($this->group->product->photos)->first() : null;

        if (!is_null($cover)) {
            return asset($cover);
        }

        return null;
    }

    public function secondCover($size = 'h480'){
        $cover = $this->cover($size);
        $secondCover = (collect($this->photos)->count() > 1) ? collect($this->photos)->get(1) : $cover;
        $secondCover =  Route('ir', ['size' => $size, 'filename' => $secondCover], false);
        return $secondCover;
    }

    public function toSearchableArray()
    {

        App::setLocale('it');

        $attributes = [];

        $attrs = $this->attributevalues;

        $locales = array_keys(config('backpack.crud.locales'));

        foreach($locales as $locale){

            $array['name_'. $locale] = $this->getTranslation('name', $locale);
            $array['description_'. $locale] = $this->getTranslation('description', $locale);
            $array['short_description_'. $locale] = $this->getTranslation('short_description', $locale);
            $array['category_'. $locale] = $this->category->getTranslation('name', $locale);
            $array['url_'.$locale] = $this->makeUrl($locale);

            if ($attrs->count() > 0) {
                foreach ($attrs as $attr) {
                    $array[$attr->attribute->name.'_'.$locale] = $attr->getTranslation('name', $locale);
                }
            }

            if(!is_null($this->gender)){
                $array['gender_' . $locale] = $this->gender->getTranslation('name', $locale);
            }

            $array['attributes_line_' . $locale] = $this->getAttrbutesLine($locale);

        }

        $array['sku'] = $this->sku;
        $array['brand'] = (!is_null($this->manufacturer)) ? $this->manufacturer->name : '';
        $array['price'] = $this->price;

        $cover = (!is_null($this->photos)) ? collect($this->photos)->first() : null;
        if(!is_null($cover)){
            $cover =  Route('ir', ['size' => 'h480', 'filename' => $cover],false);
        }

        $array['cover'] = $cover;




        // Applies Scout Extended default transformations:
        //$array = $this->transform($array);

        return $array;
    }

    public function addAttribute($attribute, $value, $cfg = [])
    {
        $attribute = trim(Str::lower(str_replace('_', ' ', $attribute)));
        $value = trim(Str::lower(str_replace('_', ' ', $value)));
        $locale = (isset($cfg['locale'])) ? $cfg['locale'] : 'it';
        App::setLocale($locale);
        if($attribute != '' && $value != ''){
            $attrObj = Attribute::whereRaw("JSON_EXTRACT(name, '$.".$locale."') = '". addslashes($attribute)."'")->first();
            if(is_null($attrObj)){
                $attrObj = Attribute::create(['name' => $attribute]);
            }
            $attributeValueObj = AttributeValue::whereRaw("JSON_EXTRACT(name, '$.".$locale."') = '".addslashes($value). "' AND attribute_id = ". $attrObj->id)->first();
            if (is_null($attributeValueObj)) {
                $attributeValueObj = AttributeValue::create(['name' => $value,'attribute_id' => $attrObj->id]);
            }
            $this->attributevalues()->syncWithoutDetaching($attributeValueObj->id);
            $this->save();
        }
    }

    public function groupName(){
        if(!is_null($this->group)){
            if($this->group->name != '')
                return $this->group->name;
            return $this->group->id;
        }
        return 'Nessun gruppo';

    }

    public function getQuantityByWarehouseId($warehouseId)
    {
        $warehouseavailabilities = $this->warehouseavailabilities->keyBy('warehouse_id');
        return (isset($warehouseavailabilities[$warehouseId]['quantity'])) ? $warehouseavailabilities[$warehouseId]['quantity'] : 0;
    }

    public function setQuantityByWarehouseId($warehouseId,$quantity,$reason = ''){
        $currentQuantity = $this->getQuantityByWarehouseId($warehouseId);
        if($quantity != $currentQuantity){
            $diff = $quantity - $currentQuantity;
            $this->stockoperations()->create([
                'sign' => ($quantity > $currentQuantity) ? 1 : -1,
                'quantity' => abs($diff),
                'warehouse_id' => $warehouseId,
                'description' => $reason
            ]);
        }
    }


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function discountcodes(){
        return $this->belongsToMany(Discountcode::class);
    }

    public function gender(){
        return $this->belongsTo(Gender::class);
    }

    public function printGooglecategoryBreadcrumb()
    {
        return collect($this->category->googlecategory->ancestors->pluck('name'))->push($this->category->googlecategory->name)->implode(' > ');
    }

    public function warehouseavailabilities(){
        return $this->hasMany(Warehouseavailability::class);
    }


    public function manufacturer(){
        return $this->belongsTo(Manufacturer::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function attributevalueextensiongroup(){
        return $this->belongsTo(Attributevalueextensiongroup::class);
    }

    public function attributevalues()
    {
        return $this->belongsToMany(Attributevalue::class);
    }

    public function group(){
        return $this->belongsTo(Productgroup::class,'productgroup_id');
    }

    public function productgroup()
    {
        return $this->belongsTo(Productgroup::class, 'productgroup_id');
    }

    public function stockoperations(){
        return $this->hasMany(Stockoperation::class);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    public function getDiscountPercentageAttribute(){
        $product = $this;
        if($product->compare_price > 0){
            $discount = $product->compare_price - $product->price;
            $discountPercentage = $discount *  100 / $product->compare_price;
            return round($discountPercentage);
        }
        return 0;
    }


    public function getAttributesLineAttribute(){
        $return = '';
        $attrValues = $this->attributevalues;
        $returnArray = [];
        if(!is_null($attrValues)){
            foreach($attrValues as $attrVal){
                $returnArray[] = Str::ucfirst($attrVal->attribute->name).': '. Str::lower($attrVal->name);
            }
            $return = collect($returnArray)->implode(', ');
        }
        return $return;
    }

    public function getAttributesLineDefaultLocaleAttribute()
    {
        $locale = 'it';
        $return = '';
        $attrValues = $this->attributevalues;
        $returnArray = [];
        if (!is_null($attrValues)) {
            foreach ($attrValues as $attrVal) {
                $returnArray[] = Str::ucfirst($attrVal->attribute->getTranslations()['name'][$locale]) . ': ' . Str::lower($attrVal->getTranslations()['name'][$locale]);
            }
            $return = collect($returnArray)->implode(', ');
        }
        return $return;
    }

    public function getAttrbutesLine($locale = 'it'){

        $return = '';
        $attrValues = $this->attributevalues;
        $returnArray = [];
        if (!is_null($attrValues)) {
            foreach ($attrValues as $attrVal) {
                $attrName = isset($attrVal->attribute->getTranslations()['name'][$locale]) ? Str::ucfirst($attrVal->attribute->getTranslations()['name'][$locale]) : Str::ucfirst($attrVal->attribute->getTranslations()['name']['it']);
                $attrValue = isset($attrVal->getTranslations()['name'][$locale]) ? Str::lower($attrVal->getTranslations()['name'][$locale]) : Str::lower($attrVal->getTranslations()['name']['it']);

                $returnArray[] = $attrName . ': ' . $attrValue ;
            }
            $return = collect($returnArray)->implode(', ');
        }
        return $return;

    }

    public function getNameWithIdentifiersAttribute(){
        return $this->name.' | '.$this->sku;
    }

    public function getFtDataAttribute(){
        $locale = 'it';
        return [
            'id' => $this->id,
            'name' => $this->getTranslations()['name'][$locale],
            'price' => number_format($this->price,2),
            'brand' => $this->manufacturer->name,
            'category' => $this->category->getTranslations()['name'][$locale],
            'variant' => $this->attributes_line_default_locale
        ];
    }

    public function getSibDataAttribute(){
        $locale = 'it';
        return [
            'product_id' => $this->id,
            'product_name' => $this->getTranslations()['name'][$locale],
            'price' => number_format($this->price, 2),
            'brand' => $this->manufacturer->name,
            'category' => $this->category->getTranslations()['name'][$locale],
            'variant' => $this->attributes_line_default_locale,
            'currency' => 'EUR',
            'image' => $this->cover()
        ];
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
