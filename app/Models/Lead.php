<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\Shippingrule;
use Backpack\Settings\app\Models\Setting;
use Illuminate\Support\Facades\Mail;
use App\Mail\LeadConfirmation;
use App\Traits\Helpers;
use App\Traits\Scalapay;
use Laravel\Scout\Searchable;

class Lead extends Model
{
    use CrudTrait;
    use Searchable;
    use Helpers;
    use Scalapay;
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'leads';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];
    protected $casts = [
        'shipping_address' => 'json',
        'billing_address' => 'json',
        'billing_address_json' => 'json',
        'shipping_address_json' => 'json',
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    public function getShippingAdvancementStatusAttribute(){
        $allShippingsCount = $this->shippings->count();
        $shippedShippings = $this->shippings()->where('is_shipped',1)->get()->count();
        return  $shippedShippings .'/'. $allShippingsCount;
    }

    public function printPaymentMethod(){
        $paymentMethodLabels = [
            'stripe' => 'Carta di credito (Stripe)',
            'paypal' => 'PayPal',
            'cashondelivery' => '<h3>CONTRASSEGNO</h3>',
        ];
        $method = $this->pay_with;
        if(in_array($method,array_keys($paymentMethodLabels))){
            return $paymentMethodLabels[$method];
        }
        return '';
    }


    public function toSearchableArray()
    {
        $array = [];

        $array = [
            'billing_address' => $this->billing_address,
            'shipping_address' => $this->shipping_address,
            'pay_with' => $this->pay_with,
            'id' => $this->id
        ];

        return $array;
    }


    public function shouldBeSearchable()
    {
        return ($this->leadstatus_id != 1);
        #return ($this->enabled && !is_null($this->category));
    }

    public function availableWarehouses(){
        return  $this->load('products', 'products.warehouseavailabilities', 'products.warehouseavailabilities.warehouse')->products->reduce(function($carry,$product){
            foreach($product->warehouseavailabilities as $wa){
                $carry->push($wa->warehouse);
            }
            return $carry;
        },collect([]))->unique();
    }

    public function checkIfIsShippable(){
        return $this->is_shippable;
    }

    public function printBillingAddress(){

        $address = $this->billing_address;

        if (is_null($address)) {
            return '';
        }

        $fields = ['email','telephone','name','surname','address','fiscal_code','vat_number','sdi_code','pec'];

        foreach($fields as $field){
            $address[$field] = (isset($address[$field])) ? $address[$field] : '';
        }

        $return = '<table class="table">';
        if(isset($address['email'])){
            $return.='<tr>';
            $return.='<td>Email</td><td>'.$address['email'].'</td>';
            $return .= '</tr>';
        }
        if(isset($address['telephone'])){
            $return .= '<tr>';
            $return.= '<td>Tel</td><td>'.$address['telephone'].'</td>';
            $return .= '</tr>';
        }
        if (isset($address['name']) && isset($address['surname'])) {
            $return .= '<tr>';
            $return.= '<td>Nome</td><td>'.$address['name'].' '. $address['surname'].'</td>';
            $return .= '</tr>';
        }
        if(isset($address['address'])){
            $return .= '<tr>';
            $return.= '<td>Indirizzo</td><td>'.$address['address'].' <a target="_blank" href="https://www.google.com/maps/place/'.urlencode($address['address']).'"><i class="fa fa-map"></i></a>';;
            $return .= '</tr>';
        }
        if($this->invoice_request){
            $return .= '<tr>';
            $return.='<td>Codice fiscale</td><td>'.$address['fiscal_code'].'</td>';
            $return .= '</tr>';
            $return .= '<tr>';
            $return.='<td>PIVA</td><td>'.$address['vat_number'].'</td>';
            $return .= '</tr>';
            $return .= '<tr>';
            $return.='<td>SDI</td><td>'.$address['sdi_code'].'</td>';
            $return .= '</tr>';
            $return .= '<tr>';
            $return.='<td>PEC</td><td>'.$address['pec'].'</td>';
            $return .= '</tr>';
        }
        $return .= '</table>';
        return $return;
    }

    /*
    public function printShippingAddress()
    {
        $address = $this->shipping_address;
        if(is_null($address)){
            return 'Same as billing';
        }
        return 'Email: ' . $address['email'] . ' | Tel: ' . $address['telephone'] . ' | Nome: ' . $address['name'] . ' ' . $address['surname'] . ' | Indirizzo: ' . $address['address'];
    }
    */

    public function printShippingAddress()
    {
        $address = $this->shipping_address;
        if (is_null($address)) {
            return 'Same as billing';
        }
        $return = '<table class="table">';
        if(isset($address['email'])){
            $return .= '<tr>';
            $return .= '<td>Email</td><td>' . $address['email'] . '</td>';
            $return .= '</tr>';
        }
        if(isset($address['telephone'])){
            $return .= '<tr>';
            $return .= '<td>Tel</td><td>' . $address['telephone'] . '</td>';
            $return .= '</tr>';
        }
        if(isset($address['name']) && isset($address['surname'])){
            $return .= '<tr>';
            $return .= '<td>Nome</td><td>' . $address['name'] . ' ' . $address['surname'] . '</td>';
            $return .= '</tr>';
        }
        if(isset($address['address'])){
            $return .= '<tr>';
            $return .= '<td>Indirizzo</td><td>' . $address['address']. ' <a target="_blank" href="https://www.google.com/maps/place/'.urlencode($address['address']).'"><i class="fa fa-map"></i></a>';
            $return .= '</tr>';
        }

        $return .= '</table>';
        return $return;
    }

    public function printGrandTotal(){
        return $this->grand_total;
    }

    public function dumps(){
        return $this->hasMany('App\Models\Dump');
    }

    public function printNotes(){
        return $this->notes;
    }

    public function printProducts(){
        $products = $this->products;
        $return = [];
        foreach($products as $product){
            $return[] = '<a target="_blank" href="'.$product->makeUrl().'"><img height="40" src="'.$product->cover('h40').'"></a> '.$product->name.' ('.$product->sku.' | '.$product->attributes_line.') | x'.$product->pivot->quantity;
        }
        return collect($return)->implode('<br>');
    }

    public function printConfirmedAt(){
        return date('d/m/Y',strtotime($this->confirmed_at));
    }


    public function calculateShippingCost()
    {

        /* Calculate shipping cost logic */

        ###################################

        $shippingCost = 0;

        $products = $this->products;

        if(!is_null($products)){

            $weight = $products->reduce(function($carry,$product){
                if(!is_null($product->packaging_weight)){
                   return $carry + ( $product->packaging_weight * $product->pivot->quantity );
                }
                else if (!is_null($product->product_weight)) {
                    return $carry + ( $product->product_weight * $product->pivot->quantity );
                }
                return 0;
            },0);

            $shippingRule = Shippingrule::whereRaw('weight_from <= '.$weight.' AND weight_to >'. $weight)->first();

            if(!is_null($shippingRule)){

                $shippingCost += $shippingRule->price;

            }


        }



        $shippingCost += $this->extra_shipping_costs;



        $this->shipping_total = $shippingCost;
        $this->save();


    }

    public function calculateTotals()
    {


        $this->calculateShippingCost();

        $products = $this->products;
        $productsTotal = 0;
        if (!is_null($products)) {
            foreach ($products as $product) {
                $productsTotal += $product->pivot->sub_total;
            }
        }
        $this->products_total = $productsTotal;
        $shippingTotal = (!is_null($this->shipping_total)) ? $this->shipping_total : 0;

        $cartTotalBeforeFreeShipping = Setting::get('cart_total_before_free_shipping');

        if(
            !is_null($cartTotalBeforeFreeShipping) &&
            $cartTotalBeforeFreeShipping > 0 &&
            $productsTotal >= $cartTotalBeforeFreeShipping
        ){
            $shippingTotal = 0;
            $this->shipping_total = 0;
        }

        #CALCULATE DISCOUNT TOTALS

        $discountTotal = 0;
        $discountCodes = $this->discountcodes;

        $partialTotal = $productsTotal + $shippingTotal;

        $totalBeforeDiscount = $partialTotal;

        $partialDiscount = 0;

        if(!is_null($discountCodes) && $partialTotal > 0){

            $discountCodes = $discountCodes->sortByDesc('discountcode_type_id'); #apply percentage discounts firstt

            foreach($discountCodes as $code){
                if($code->min_grand_total <= $totalBeforeDiscount || is_null($code->min_grand_total)){
                    if($code->active){
                        if($code->products->count() > 0 || $code->productgroups->count() > 0){
                            if (!is_null($products)) {
                                foreach ($products as $product) {
                                    $partialDiscount = 0;
                                    switch ($code->discountcode_type_id) {
                                        case 2:
                                            if (
                                                in_array($product->id, $code->products->pluck('id')->toArray())
                                                ||
                                                in_array($product->productgroup->id, $code->productgroups->pluck('id')->toArray())
                                                ) {
                                                $partialDiscount = $product->pivot->sub_total * $code->value / 100;
                                            }
                                        break;
                                    }
                                    $partialTotal -= $partialDiscount;
                                    $discountTotal += $partialDiscount;
                                }
                            }
                        }else{

                            switch ($code->discountcode_type_id) {
                                case 1:
                                    $partialDiscount = $code->value;
                                    break;
                                case 2:
                                    $partialDiscount = $partialTotal * $code->value / 100;
                                    break;
                            }

                            $partialTotal -= $partialDiscount;
                            $discountTotal += $partialDiscount;

                        }
                    }
                }
            }
        }

        $paymentMethodTotal = (!is_null($this->payment_method_total) && $this->payment_method_total > 0) ? $this->payment_method_total : 0;

        $this->discount_total = $discountTotal;

        $partialTotal += $paymentMethodTotal;

        $this->grand_total = $partialTotal;# - $discountTotal;
        $this->save();
    }

    public function consolidateOrder(){
        Product::withoutSyncingToSearch(function () {
            if(is_null($this->consolidated_at)){
                $this->products->each(function($product,$key){
                    $product->quantity-=$product->pivot->quantity;
                    #Product::withoutEvents(function() use($product){
                        $product->save();
                    #});
                });
            }
        });
        $lead = $this;
        Lead::withoutEvents(function () use ($lead) {
            $lead->consolidated_at = date('Y-m-d H:i:s');
            $lead->save();
        });

    }


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function emitsReceipt(){
        return $this->belongsTo(Warehouse::class, 'emits_receipt_warehouse_id');
    }

    public function shippings(){
        return $this->hasMany(Shipping::class);
    }

    public function products()
    {
        return $this->belongsToMany('App\Models\Product')->withPivot([
            'created_at',
            'updated_at',
            'quantity',
            'merchant_id',
            'sub_total',
        ]);
    }

    public function leadproducts(){
        return $this->hasMany(Leadproduct::class);
    }

    public function discountcodes()
    {
        return $this->belongsToMany('App\Models\Discountcode');
    }

    public function sendConfirmations()
    {

        $stores = $this->stores;
        $lead_id = $this->id;

        $clientEmail = $this->billing_address['email'];

        $data = [
            'content' => [
                'order_id' => $this->id,
                'user_full_name' => $this->billing_address['name'] . ' ' . $this->billing_address['surname'],
                'lead' => $this,
                'products' => $this->products
            ]
        ];

        #$data['content']['rows'] = $this->leadrows;

        $recipients = explode(',',env('ADMIN_ORDER_NOTIFICATION_EMAILS'));
        $recipients[] = $clientEmail;

        if($clientEmail != ''){
            foreach ($recipients as $recipient) {
                Mail::to($recipient)->send(new LeadConfirmation($data));
            }
        }

    }



    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
