<?php

namespace App\Http\Controllers;

use App\Models\Lead;
use App\Models\Leadproduct;
use Illuminate\Http\Request;

class ShippingsManager extends Controller
{
    //
    public function show($id){
        $data = [];

        $lead = Lead::findOrFail($id);

        $lead->shippings->each(function ($shipping, $key) {
            $shipping->shippingrows->each(function ($row, $key) {
                $row->stockoperation->revert();
            });
            $shipping->shippingrows()->delete();
        });
        $lead->shippings_prepared_at = null;
        $lead->save();
        $lead->shippings()->delete();

        $lead->load([
            'leadproducts',
            'leadproducts.product',
            'leadproducts.product.warehouseavailabilities',
            'leadproducts.product.warehouseavailabilities.warehouse',
            #'leadproducts.shippingrows',
            #'shippings'
        ]);

        $data['lead'] = $lead;


        /*
        $shippings = [];
        $data['lead']->shippings->each(function($shipping,$keyShipping) use(&$shippings){
            $shipping->shippingrows->each(function($row,$keyRow) use (&$shippings, $shipping){
                $shippings[$shipping->warehouse_id][$row->lead_product_id] = $row->quantity;
            });
        });
        $data['shippings'] = $shippings;
        */

        $data['shippings'] = [];

        return view('admin.shippingsManager.index',$data);
    }

    public function store(Request $request){

        #dd($request->all());


        foreach($request->shippingrows as $lead_id => $warehouses){
            $lead = Lead::find($lead_id);

            foreach($warehouses as $warehouse_id => $lead_products){
                #create shippig here
                if(collect($lead_products)->sum() > 0){
                    $shipping =  $lead->shippings()->create(['warehouse_id' => $warehouse_id]);
                    foreach($lead_products as $lead_product_id => $quantity){

                            #attachrows to shipping here
                            $leadProduct = Leadproduct::find($lead_product_id);
                            $shippingRow = $shipping->shippingrows()->create([
                                'lead_product_id' => $lead_product_id,
                                'quantity' => $quantity
                            ]);
                            $shippingRow->stockoperation()->create([
                                'sign' => -1,
                                'quantity' => $quantity,
                                'warehouse_id' => $warehouse_id,
                                'product_id'=> $leadProduct->product_id
                            ]);

                    }
                }
            }
            $lead->shippings_prepared_at = date('Y-m-d H:i:s');

            if($request->has('abroad_invoice')){
                $lead->abroad_invoice = $request->abroad_invoice;
            }else{
                $lead->abroad_invoice = null;
            }

            if ($request->has('emits_receipt_warehouse_id')) {
                $lead->emits_receipt_warehouse_id = $request->emits_receipt_warehouse_id;
            }else{
                $lead->emits_receipt_warehouse_id = null;
            }


            $lead->save();
        }

        return redirect('/admin/lead');



    }
}
