<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Attributevalueextensiongroup;
use App\Models\Carousel;
use App\Models\Category;
use App\Models\Guest;
use App\Models\Conversionscale;
use App\Models\Discountcode;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\Lead;
use App\Models\Menu;
use App\Models\Newslettersubscription;
use App\Models\Productgroup;
use App\Models\Warehouse;
use App\User;
use Auth;
use Backpack\Settings\app\Models\Setting;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Validator;
use App;
use App\Models\Event;
use App\Models\Metatag;
use App\Models\Webhook;
use Illuminate\Support\Facades\Auth as FacadesAuth;
use Stripe\Discount;
use Jenssegers\Agent\Agent;
use Recombee\RecommApi\Client;
use Recombee\RecommApi\Requests as Reqs;
use Recombee\RecommApi\Exceptions as Ex;
use Str;
use Illuminate\Support\Facades\Artisan;
#use  Lwwcas\LaravelCountries\Models\Country;
use App\Models\Country;

class FrontController extends Controller
{
    //

    public function getMeta($page,$name,$fallback = ''){
        $tag = Metatag::where('page', $page)->where('name',$name)->first();

        if(!is_null($tag)){
            return ($tag->content != '') ? $tag->content : strip_tags($fallback);
        }else{
            Metatag::create(['page' => $page,'name' => $name]);
        }

        return $fallback;
    }

    public function getDataDefaults($locale,$request)
    {

        App::setLocale($locale);
        $agent = new Agent();

        $data['defaults']['motto'] = Setting::get('motto');
        $data['defaults']['telephone'] = Setting::get('telephone');
        $data['defaults']['email'] = Setting::get('contact_email');
        $data['defaults']['address'] = Setting::get('address');
        $data['defaults']['address'] = Setting::get('address');
        $data['defaults']['facebook'] = Setting::get('facebook_link');
        $data['defaults']['instagram'] = Setting::get('instagram_link');
        $data['defaults']['twitter'] = Setting::get('twitter_link');
        $data['defaults']['youtube'] = Setting::get('youtube_link');
        $data['defaults']['coordinates'] = explode(',', Setting::get('coordinates'));

        $data['guest'] = $this->getGuest();
        $data['lead'] = $this->getLead();

        $data['locale'] = $locale;
        $data['menus'] =
        Menu::with(
            ['items'=> function ($query) {
                $query->orderBy('rgt', 'ASC');
            }])
        ->get()
        ->groupBy('menu_type_id');

        #dd($data['menus']);

        $data['cartTotalBeforeFreeShipping'] = Setting::get('cart_total_before_free_shipping');
        $locales_ = collect(config('backpack.crud.locales'));
        $locales = [];
        $locales_->each(function($locale,$key) use (&$locales){
            $localeIndex = strtolower((substr($key, 0, 2)));
            $locales[$localeIndex] = $locale;
        });
        $data['locales'] = $locales;
        $data['isMobile'] = $agent->isMobile();

        if($request->has('ref_sub')){
            $data['lead']->ref_sub = $request->ref_sub;
            $data['lead']->save();
        }

        $data['meta'] = [
            'title' => '',
            'description' => ''
        ];


        #dd($data);
        return $data;
    }

    public function mainRedirect(Request $request){

        $locales_ = collect(config('backpack.crud.locales'));
        $localeIndexes = [];

        $locales_->each(function ($locale, $key) use (&$localeIndexes) {
            $localeIndex = strtolower((substr($key, 0, 2)));
            $localeIndexes[] = $localeIndex;
        });

        $locale = Str::lower(Str::substr($request->getPreferredLanguage(),0,2));
        if(in_array($locale, $localeIndexes)){
            return redirect($locale);
        }
        return redirect('en');
    }

    public function addDiscountCode($locale,$code = null){
        $lead = $this->getLead();
        $code = Discountcode::where('code',trim($code))->firstOrFail();

        if(!env('MULTIPLE_DISCOUNTCODES')){
            if($lead->discountcodes->count() >= 1){
                return redirect(Route('cart', ['locale' => $locale]));
            }
        }

        if($code->min_grand_total <= $lead->grand_total && (is_null($code->expires_at) || strtotime($code->expires_at) >= time())){
            $lead->discountcodes()->syncWithoutDetaching([$code->id]);
        }

        return redirect(Route('cart',['locale' => $locale]));

    }

    public function removeDiscountCode(Request $request){
        $lead = $this->getLead();
        $lead->discountcodes()->detach($request->id);

        return ['status' => 1];
    }

    public function subscribeNewsletter(Request $request)
    {

        $ns = $newsletterSubscription = Newslettersubscription::create([
            'email' => $request->email
        ]);

        return redirect(Route('thankYouPageNewsletterSubscription', ['locale' => $request->locale]));
    }

    public function thankYouPageNewsletterSubscription(Request $request, $locale)
    {
        $data = $this->getDataDefaults($locale, $request);
        $data['locale'] = $locale;

        $data['meta'] = [
            'title' => $this->getMeta('thankYouPageNewsletterSubscription','title'),
            'description' => $this->getMeta('thankYouPageNewsletterSubscription', 'description')
        ];

        return view(env('THEME_NAME') . '.thank_you_page_newsletter_subscription', $data);
    }

    public function home(Request $request, $locale)
    {
        $data = $this->getDataDefaults($locale, $request);
        $data['locale'] = $locale;
        $data['shopWindow'] = Product::where('show_in_shop_window', 1)->whereNotNull('photos')->where('enabled', 1)->orderBy('id', 'DESC')->get();

        $data['newArrivals'] = Category::with(['products' => function ($query) {
            $query
            ->orderBy('created_at', 'DESC')
            ->whereNotNull('photos')
            ->groupBy('productgroup_id')
            ->where('enabled',1)
            ->orderBy('id','DESC')
            ->limit(8);
        }])->where('show_in_new_arrivals', 1)->get()->sortByDesc(function($category,$key){
            return $category->products->count();
        })->values();





        $data['newArticles'] = Article::orderBy('updated_at', 'DESC')->where('is_blog',1)->limit(3)->get();

        $data['carousels'] = Carousel::with(['slides'])->get()->keyBy('tag');

        $data['meta'] = [
            'title' => $this->getMeta('home', 'title'),
            'description' => $this->getMeta('home', 'description')
        ];

#        dd($data['menus'][1]);
        return view(env('THEME_NAME') . '.home', $data);
    }

    public function getGuest()
    {
        #dd(session()->all());
        if (session()->has('guest_id')) {
            $guest_id = session('guest_id');
            $guest = Guest::find($guest_id);
            if (is_null($guest)) {

                $guest = Guest::create();
                session(['guest_id' => $guest->id]);
            }
        } else {
            $guest = Lead::create();
            session(['guest_id' => $guest->id]);
        }

        return $guest;
    }


    public function getLead()
    {
        #dd(session()->all());
        $guest = $this->getGuest();

        if (session()->has('lead_id')) {
            $lead_id = session('lead_id');
            $lead = Lead::find($lead_id);

            if (is_null($lead)) {

                $lead = Lead::create(['leadstatus_id' => 1, 'guest_id' => $guest->id]);
                session(['lead_id' => $lead->id]);
            }

            if($lead->leadstatus_id != 1){

                $lead = Lead::create(['leadstatus_id' => 1, 'guest_id' => $guest->id]);
                session(['lead_id' => $lead->id]);

                return redirect(App::getLocale());

            }


        } else {
            $lead = Lead::create(['leadstatus_id' => 1,'guest_id' => $guest->id]);
            session(['lead_id' => $lead->id]);
        }

        return $lead;
    }

    public function cacheCartfield(Request $request){
        $request->validate([
            'name' => 'required',
            'value' => 'required'
        ]);
        $lead = $this->getLead();
        $value = $request->value;
        $name = $request->name;
        $nameExploded = explode('.',$name);
        if(count($nameExploded) > 1){
            $index = $nameExploded[0];
            $currentVal = $lead->$index;
            $currentVal[$nameExploded[1]] = $value;
            $lead->$index = $currentVal;
        }else{
            $lead->$name = $value;
        }

        $lead->saveWithoutEvents();

        Event::create([
            'lead_id' => $lead->id,
            'action' => 'set field',
            'payload' => ['name' => $request->name,'value' => $request->value]
        ]);

    }

    public function trackEvent(Request $request){
        $request->validate([
            'action' => 'required'
        ]);
        $lead = $this->getLead();
        $payload = ($request->has('payload')) ? json_decode($request->payload) : [];
        Event::create([
            'lead_id' => $lead->id,
            'action' => $request->action,
            'payload' => $payload
        ]);
    }

    public function setCartField(Request $request)
    {

        $lead = $this->getLead();
        $field = $request->name;
        $value = $request->value;
        $address = $lead->shipping_address;
        $address[$field] = $value;
        $lead->shipping_address = $address;
        $lead->save();

        $refresh = 0;

        if ($field == 'address') {
            $lead->calculateShippingCost();
            $refresh = 1;
        }

        return ['status' => 1, 'refresh' => $refresh];

    }

    public function cart(Request $request, $locale)
    {


        $data = $this->getDataDefaults($locale, $request);
        $lead = $this->getLead();

        $lead->calculateTotals();

        if($request->has('product_id') && $request->has('quantity')){

            $addedProduct = Product::findOrFail($request->product_id)->ft_data;
            $addedProduct['quantity'] = $request->quantity;
            $data['addToCart'] = ['products'=>[$addedProduct]];

        }

        $data['products'] = $lead->products;
        $data['lead'] = $lead;

        $sibProducts = clone $lead->products;

        $data['sibCartUpdatedData'] = [
            'id' => 'lead_id_'.$lead->id,
            'data' => [
                'products' => $sibProducts->transform(function($product,$key){
                    $return = $product->sib_data;
                    $return['amount'] = $product->pivot->quantity;
                    return $return;
                })
            ]
        ];




        if ($lead->products->count() > 0) {
            /* if (Auth::check()) {
                $user = Auth::user();
                $lead->user_id = $user->id;
                $lead->save();
                $data['user'] = $user;
                $data['client_secret_intent'] = $user->createSetupIntent();
                $data['stripe_customer_id'] = $user->createOrGetStripeCustomer();
                $data['payment_methods'] = collect($user->paymentMethods());
                $billing_address = [
                    'name' => (isset($lead->billing_address['name'])) ? $lead->billing_address['name'] : $user->name,
                    'surname' => (isset($lead->billing_address['surname'])) ? $lead->billing_address['surname'] : $user->name,
                    'email' => (isset($lead->billing_address['email'])) ? $lead->billing_address['email'] : $user->email,
                    'telephone' => (isset($lead->billing_address['telephone'])) ? $lead->billing_address['telephone'] : $user->telephone,
                    'address' => (isset($lead->billing_address['address'])) ? $lead->billing_address['address'] : '',
                ];
                $data['billing_address'] = $billing_address;
            } else { */

                $billing_address = [
                    'name' => '',
                    'surname' => '',
                    'email' => '',
                    'telephone' => '',
                    'address' => '',
                    'sdi_code' => '',
                    'company_name' => '',
                    'vat_number' => '',
                    'pec' => '',
                    'fiscal_code' => ''
                ];
                $shipping_address = [
                    'name' => '',
                    'surname' => '',
                    'email' => '',
                    'telephone' => '',
                    'address' => '',
                ];

                if($request->has('name') && $request->has('email')){ #this is for facebook login
                    $name = explode(' ',$request->name);
                    $email = $request->email;
                    $billing_address['name'] = $name[0];
                    $billing_address['surname'] = $name[1];
                    $billing_address['email'] = $email;
                    $shipping_address['name'] = $name[0];
                    $shipping_address['surname'] = $name[1];
                    $shipping_address['email'] = $email;
                }

                $data['billing_address'] = $billing_address;
                $data['shipping_address'] = $shipping_address;
                $data['notes'] = '';


                $stripe = new \Stripe\StripeClient(env('STRIPE_SECRET'));

                $data['client_secret_intent'] = $stripe->paymentIntents->create([
                    'currency' => 'eur',
                    'amount' => (int)($lead->grand_total * 100),
                    'metadata' => ['order_id' => $lead->id],
                    'description' => 'Order '.$lead->id,
                    'payment_method_types' => ['ideal','card'],
                ]);
            //}
        }





        $data['meta'] = [
            'title' => $this->getMeta('cart', 'title'),
            'description' => $this->getMeta('cart', 'description')
        ];

        $data['cod_countries'] = (env('COD_COUNTRIES') != '') ? collect(explode(',', env('COD_COUNTRIES'))) : collect([]);

        if($request->has('preset_address')){
            $data['billing_address']['address'] = $request->preset_address;
        }


        return view(env('THEME_NAME') . '.cart', $data);
    }

    public function calculateCartShippingCost(Request $request){
        $request->validate([
            'country' => 'required'
        ]);

        $shippingCost = 0;

        $country = Country::whereIso(Str::upper($request->country))->first();
        $countriesgroups = $country->countriesgroups()->orderBy('extra_shipping_cost','desc')->get();

        if($countriesgroups->count() > 0){
            $shippingCost += $countriesgroups->first()->extra_shipping_cost;
        }



        $lead = $this->getLead();
        $lead->extra_shipping_costs = $shippingCost;
        $lead->save();

        $lead->calculateTotals();

        return ['shipping_cost' => $lead->shipping_total];

    }

    public function confirmCart(Request $request)
    {





        $lead = $this->getLead();
        $lead->dumps()->create(['content' => $request->all()]);

        $validator = Validator::make(
            $request->all(),
            $this->getCartConfirmationValidationRules(),
            $this->getCartConfirmationValidationMessages(),
            $this->getCartConfirmationValidationAttributes(),
        )->validate();



        if ($request->has('has_requested_invoice')) {
            $lead->has_requested_invoice = $request->has_requested_invoice;
        }

        if (Auth::check()) {
            $user = Auth::user();
        } else {
            if ($request->has('create_an_account')) {
                $user = User::create(
                    [
                        'name' => $request->data['billing_address']['name'],
                        'surname' => $request->data['billing_address']['surname'],
                        'telephone' => $request->data['billing_address']['telephone'],
                        'email' => $request->data['billing_address']['email'],
                        'password' => bcrypt($request->password)
                    ]
                );
                Auth::login($user, true);
            }
        }

        $redirectUrlScalapay = [];
        $redirectUrlScalapay['url'] = '';

        switch($request->pay_with){

            case 'paypal':
                #IN CASE OF PAYPAL THE PAYMENT IS ALREADY COMPLETED
            break;

            case 'stripe':
                #IN CASE OF STRIPE THE PAYMENT IS ALREADY COMPLETED

                if (isset($user)) {
                    $lead->user_id = $user->id;
                    #$user->addPaymentMethod($request->data['payment_method']);
                }

                /*
                $amount = (int)($lead->grand_total * 100);
                try {
                    $stripeCharge = $user->charge($amount, $request->data['payment_method']);
                } catch (Exception $e) {
                    return $e;
                }
                */
            break;

            case 'cashondelivery':

                break;

            case 'scalapay':

                $redirectUrlScalapay = $lead->getScalapayCheckoutUrl();

                break;

        }

        $pms = [
            'stripe' => [
                'id' => 1,
                'status' => 2,
                'redirectUrl' => Route('thankYouPageEcommerceCart', ['locale' => $request->locale, 'lead_id' => $lead->id, 'value' => $lead->grand_total])
            ],
            'paypal' => [
                'id' => 2,
                'status' => 2,
                'redirectUrl' => Route('thankYouPageEcommerceCart', ['locale' => $request->locale, 'lead_id' => $lead->id, 'value' => $lead->grand_total])
            ],
            'cashondelivery' => [
                'id' => 3,
                'status' => 2,
                'redirectUrl' => Route('thankYouPageEcommerceCart', ['locale' => $request->locale, 'lead_id' => $lead->id, 'value' => $lead->grand_total])
            ],
            'scalapay' => [
                'id' => 4,
                'status' => 1
            ],
            'bankwiretransfer' => [
                'id' => 5,
                'status' => 2,
                'redirectUrl' => Route('thankYouPageEcommerceCart', ['locale' => $request->locale, 'lead_id' => $lead->id, 'value' => $lead->grand_total])
            ]
        ];

        $paymentMethodTotal = ($request->pay_with == 'cashondelivery') ? 5 : 0; #if cashondelivery must add 5€ to total

        $lead->fill([
            'paymentmethod_id' => $pms[$request->pay_with]['id'], #($request->pay_with == 'stripe') ? 1 : 2, # 1 is stripe, 2 is paypal,
            'billing_address' => $request->data['billing_address'],
            'shipping_address' => ($request->different_shipping_address) ? $request->data['shipping_address'] : null,
            'notes' => $request->data['notes'],
            'invoice_request' => $request->invoice_request,
            'leadstatus_id' => $pms[$request->pay_with]['status'],
            'confirmed_at' => date('Y-m-d H:i:s'),
            'pay_with' => $request->pay_with,
            'billing_address_json' => $request->billing_address_json,
            'shipping_address_json' => $request->shipping_address_json,
            'has_subscribed_newsletter' => ($request->has('has_subscribed_newsletter')) ? $request->has_subscribed_newsletter : 0,
            'payment_method_total' => $paymentMethodTotal
        ]);

        $lead->save();

        $lead->calculateTotals();

        $lead->consolidateOrder();

        session()->forget('lead_id'); #start a new lead

        $redirects = [
            'stripe' => Route('thankYouPageEcommerceCart', ['locale' => $request->locale, 'lead_id' => $lead->id, 'value' => $lead->grand_total]),
            'paypal' => Route('thankYouPageEcommerceCart', ['locale' => $request->locale, 'lead_id' => $lead->id, 'value' => $lead->grand_total]),
            'cashondelivery' =>  Route('thankYouPageEcommerceCart', ['locale' => $request->locale, 'lead_id' => $lead->id, 'value' => $lead->grand_total]),
            'scalapay' => $redirectUrlScalapay['url'],
            'bankwiretransfer' => Route('thankYouPageEcommerceCart', ['locale' => $request->locale, 'lead_id' => $lead->id, 'value' => $lead->grand_total])
        ];

        return redirect($redirects[$request->pay_with]);

    }

    public function scalapayRedirectSuccess(Request $request){

        $request->validate([
            'orderToken' => 'required'
        ]);

        $lead = Lead::where('scalapay_token' , $request->orderToken)->firstOrFail();
##########################################
            #confirm order
##########################################

        $captureResult = $lead->captureScalapayPayment();

        if($captureResult == 'APPROVED'){
            $lead->leadstatus_id = 2;
            $lead->confirmed_at = date('Y-m-d H:i:s');
            $lead->saveWithoutEvents();

            return redirect(
                Route(
                    'thankYouPageEcommerceCart',
                    [
                        'locale' => $lead->locale,
                        'lead_id' => $lead->id,
                        'value' => $lead->grand_total
                    ]
                )
            );
        }else{
            return redirect(Route('paymentFailed', ['locale' => $lead->locale]));
        }
    }

    public function scalapayRedirectFail(Request $request){
        $lead = $this->getLead();
        return redirect(Route('paymentFailed',['locale' => $lead->locale]));
    }

    public function idealRedirect(Request $request){

        #dd($request->all());

        $request->validate([
            'redirect_status' => 'required'
        ]);

        $lead = $this->getLead();

        if($request->redirect_status == 'succeeded'){

            $lead->leadstatus_id = 2;
            $lead->confirmed_at = date('Y-m-d H:i:s');
            $lead->saveWithoutEvents();

            session()->forget('lead_id'); #start a new lead

            return redirect(
                Route(
                    'thankYouPageEcommerceCart',
                    [
                        'locale' => $lead->locale,
                        'lead_id' => $lead->id,
                        'value' => $lead->grand_total
                    ]
                )
            );
        }else{
              return redirect(
                Route(
                    'cart',
                    [
                        'locale' => $lead->locale
                    ]
                )
            );
        }
    }



    public function paymentFailed(Request $request, $locale){
        $data = $this->getDataDefaults($locale, $request);
        return view(env('THEME_NAME') . '.payment_failed', $data);
    }

    public function thankYouPageEcommerceCart(Request $request,$locale,$lead_id,$value)
    {

        $data = $this->getDataDefaults($locale, $request);

        $lead = Lead::with(['products'])->findOrFail($lead_id);

        $data['meta'] = [
            'title' => $this->getMeta('thankYouPageEcommerceCart', 'title'),
            'description' => $this->getMeta('thankYouPageEcommerceCart', 'description')
        ];


        $data['conversion'] = [
            'lead_id' => $lead_id,
            'value' => $value
        ];
        $data['lead'] = $lead;

        /*
         {                            // List of productFieldObjects.
        'name': 'Triblend Android T-Shirt',     // Name or ID is required.
        'id': '12345',
        'price': '15.25',
        'brand': 'Google',
        'category': 'Apparel',
        'variant': 'Gray',
        'quantity': 1,
        'coupon': ''                            // Optional fields may be omitted or set to empty string.
       },
       {
        'name': 'Donut Friday Scented T-Shirt',
        'id': '67890',
        'price': '33.75',
        'brand': 'Google',
        'category': 'Apparel',
        'variant': 'Black',
        'quantity': 1
       }
        */



        App::setLocale('it');

        $tax = $lead->grand_total - ($lead->grand_total / 1.22);

        $data['leadForConversion']['data'] = [
            'id' => $lead->id,
            'affiliation' => env('APP_NAME'),
            'revenue' => number_format($lead->grand_total,2),
            'shipping' => $lead->shipping_total,
            'tax' => number_format($tax,2),
            'coupon' => ($lead->discountcodes->count() > 0) ? $lead->discountcodes->first()->code : ''
        ];
        foreach($lead->products as $product){
            $data['leadForConversion']['products'][] = [
                'name' => $product->getTranslations()['name']['it'],
                'id' => $product->id,
                'brand' => $product->manufacturer->name,
                'category' => $product->category->getTranslations()['name']['it'],
                'variant' => $product->attributes_line,
                'quantity' => $product->pivot->quantity,
                'price' => number_format($product->price, 2)
            ];
        }

        App::setLocale($locale);

        $data['user_ip'] = $request->ip();


        return view(env('THEME_NAME').'.thank_you_page_ecommerce', $data);
    }

    public function removeFromCart(Request $request){

            $lead = $this->getLead();
            $lead->dumps()->create(['content' => $request->all()]);

        $request->validate([
            'id' => 'required'
        ]);

            $lead->products()->detach($request->id);

            $lead->calculateTotals();

            return ['status' => 1];

    }

    public function addToCart(Request $request)
    {

        $lead = $this->getLead();
        $lead->dumps()->create(['content' => $request->all()]);

        $request->validate([
            'quantity' => 'required|numeric',
            'product_id' => 'required|numeric',
            'locale' => 'required'
        ]);

        $product = Product::findOrFail($request->product_id);


        $lead->products()->syncWithoutDetaching([
            $product->id =>
            [
                'quantity' => $request->quantity,
                'sub_total' => $product->price * $request->quantity
            ]
        ]);
        $lead->calculateTotals();
        return redirect(Route('cart', ['locale' => $request->locale]).'?product_id=' . $product->id . '&quantity=' . $request->quantity);
    }

    public function searchResults()
    {
        return view(env('THEME_NAME') . '.search_results');
    }

    public function productGrid(Request $request, $locale)
    {

        $data = $this->getDataDefaults($locale, $request);


        $data['facets'] = ($request->has('facets')) ? $request->facets : [];
        $data['keyword'] = ($request->has('keyword')) ? $request->keyword : '';
        $data['onlyOnSale'] = ($request->has('only_on_sale')) ? $request->only_on_sale : 0;

        $data['meta'] = [
            'title' => $this->getMeta('productGrid', 'title'),
            'description' => $this->getMeta('productGrid', 'description')
        ];

        return view(env('THEME_NAME') . '.product_grid', $data);
    }

    public function productPage(Request $request, $locale, $productName, $id)
    {
        $data = $this->getDataDefaults($locale, $request);

        $product = Product::with([
            'reviews',
            'category'
        ])->findOrFail($id);


        #if product group of the product is not active redirects to home

        if(!is_null($product->productgroup)){
            if(!$product->productgroup->active){
                return redirect(App::getLocale().'/pg');
            }
        }

        if(!$product->enabled){
            return redirect(App::getLocale() . '/pg');
        }




        $photos = [];
        if(!is_null($product['photos'])){
            foreach($product['photos'] as $photo){
                $photos[] = [
                    'src' => Route('ir', ['size' => 'h800', 'filename' => $photo]),
                    'w' => 600,
                    'h' => 743
                ];
            }
        }

        $product->photos = $photos;


        $data['product'] = $product;
        $data['variants'] = $product->variants();
        $data['siblings'] = $product->getSiblings();
        if(env('ENABLE_SIZE_CONVERSION')){
            $conversionScales = Conversionscale::with(['conversionscalevalues'])->whereAttributeId(2)->get(); #2 is size;



            $data['conversionscales'] = $conversionScales;
            $conversionScalesIndexed = [];
            foreach($conversionScales as $scale){
                foreach($scale->conversionscalevalues as $value){
                    $conversionScalesIndexed[$value->conversionscale_id][$value->attributevalue_id][$value->gender_id] = $value->name;
                }
            }
            $data['conversionscalesindexed'] = $conversionScalesIndexed;

            $data['product']->load(['attributevalueextensiongroup', 'attributevalueextensiongroup.attributevalueextensiongrouprows']);
        }

        #dd($data['conversionscalesindexed']);

        $data['meta'] = [
            'title' => $this->getMeta('productPage', 'title',$product->name),
            'description' => $this->getMeta('productPage', 'description',$product->description)
        ];

        return view(env('THEME_NAME') . '.product_page', $data);
    }

    public function articlePage(Request $request, $locale, $productName, $id)
    {
        $data = $this->getDataDefaults($locale, $request);
        $article = Article::find($id);
        $data['article'] = $article;

        $data['meta'] = [
            'title' => $this->getMeta('article', 'title', $article->name),
            'description' => $this->getMeta('article', 'description', $article->description_short)
        ];

        return view(env('THEME_NAME') . '.article',$data);
    }


    public function FacebookLoginRedirectToProvider()
    {
        return Socialite::driver('facebook')->stateless()->redirect();

    }


    public function FacebookLoginCallbackHandler(Request $request)
    {

        $user = Socialite::driver('facebook')->stateless()->user();

        $queryString = http_build_query(['name' => $user->name,'email' => $user->email]);
        $redirectRoute = implode('?', [Route('cart', ['locale' => 'it']), $queryString]);

        return redirect($redirectRoute);

    }

    public function getCartConfirmationValidationAttributes(){
         return [
            'payment_method' => 'metodo di pagamento',
            'data.billing_address.email' => 'email dell\'indirizzo di fatturazione',
            'data.billing_address.name' => 'nome dell\'indirizzo di fatturazione',
            'data.billing_address.surname' => 'cognome dell\'indirizzo di fatturazione',
            'data.billing_address.telephone' => 'telefono dell\'indirizzo di fatturazione',
            'data.billing_address.address' => 'indirizzo dell\'indirizzo di fatturazione',
            'data.shipping_address.email' => 'email dell\'indirizzo di spedizione',
            'data.shipping_address.name' => 'nome dell\'indirizzo di spedizione',
            'data.shipping_address.surname' => 'cognome dell\'indirizzo di spedizione',
            'data.shipping_address.telephone' => 'telefono dell\'indirizzo di spedizione',
            'data.shipping_address.address' => 'indirizzo dell\'indirizzo di spedizione',
         ];
    }

    public function getCartConfirmationValidationMessages(){
        return [
            'required' => 'Il campo :attribute è obbligatorio',
            'required_if' => 'Il campo :attribute è obbligatorio',
            'email' => 'Il campo :attribute deve essere un indirizzo email valido'
        ];
    }

    public function getCartConfirmationValidationRules(){
        return [

            #'data.payment_method' => 'sometimes|required',
            #'data.billing_address.email' => 'required|email|unique:users,email',
            'data.billing_address.email' => 'required|email',
            'data.billing_address.name' => 'required',
            'data.billing_address.surname' => 'required',
            'data.billing_address.telephone' => 'required',
            'data.billing_address.address' => 'required',
            #'data.billing_address.fiscal_code' => 'required',

            'different_shipping_address' => 'sometimes|required',
            'invoice_request' => 'sometimes|required',
            'create_an_account' => 'sometimes|required',

            'password' => 'required_if:create_an_account,true',

            'data.billing_address.vat_number' => 'required_if:invoice_request,true',
            'data.billing_address.sdi_code' => 'required_if:invoice_request,true',

            'data.shipping_address.email' => 'required_if:different_shipping_address,true',
            'data.shipping_address.name' => 'required_if:different_shipping_address,true',
            'data.shipping_address.surname' => 'required_if:different_shipping_address,true',
            'data.shipping_address.telephone' => 'required_if:different_shipping_address,true',
            'data.shipping_address.address' => 'required_if:different_shipping_address,true',
            'locale' => 'required'


        ];
    }

    public function shops(Request $request, $locale){
        $data = $this->getDataDefaults($locale, $request);
        $shops = Warehouse::where('is_shop',1)->get();
        $data['shops'] = $shops->groupBy(function ($item, $key) {
            return $item['address']['country'].' - '.$item['address']['administrative_area_level_1'];
        });

        $data['meta'] = [
            'title' => $this->getMeta('shops', 'title'),
            'description' => $this->getMeta('shops', 'description')
        ];
        return view(env('THEME_NAME') . '.shops', $data);
    }

    public function aboutUs(Request $request, $locale){
        $data = $this->getDataDefaults($locale, $request);
        $shops = Warehouse::where('is_shop', 1)->get();
        $data['shops'] = $shops;
        $data['meta'] = [
            'title' => $this->getMeta('aboutUs', 'title'),
            'description' => $this->getMeta('aboutUs', 'description')
        ];
        return view(env('THEME_NAME') . '.about_us', $data);
    }

    public function relatedProductsToProduct($product_id, $user_id, $how_many_products){

        $client = new Client(env('RECOMBEE_DB_ID'), env('RECOMBEE_DB_KEY'));

        $recommended = $client->send(new Reqs\RecommendItemsToItem(
            $product_id,
            $user_id,
            $how_many_products,
            [
               #'returnProperties' => true,
               #'filter' => "'city' == context_item[\"city\"] AND 'date' >= now() AND \"ballet\" in 'genres'"
               'filter' => "'active' == true"
            ]
        ));

        if(count($recommended['recomms']) > 0){
            $products = Productgroup::find($recommended['recomms']);
            return view('harris.related-products',['products' => $products]);
        }

        return '';

    }


    public function scoutImportProductgroups()
    {
        Artisan::call('scout:import', ["model" => "App\\Models\\Productgroup"]);

    }

    public function webhook(Request $request,$source){
        Webhook::create([
            'referrer' => $source,
            'content' => $request->all()
        ]);
    }

    public function donut(){
        return view('donut');
    }

    public function checkLeadValidiy(){
        $lead = $this->getLead();
        if($lead->leadstatus_id != 1){
            session()->forget('lead_id'); #start a new lead
            return 0;
        }else{
            return 1;
        }
    }

}
