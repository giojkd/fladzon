<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Productgroup;
use League\Csv\Writer;

class TranslationsController extends Controller
{
    //

    public function downloadXlsxWithProductTranslations()
    {
        $locales = collect(config('backpack.crud.locales'));
        $groups = Productgroup::get();
        $texts = [];

        foreach ($groups as $group) {
            $item['group_id'] = $group->id;
            $product = $group->products->first();
            if (is_object($product)) {
                foreach(['name','description'] as $field){
                    foreach ($locales as $localeShort => $localeLong) {
                        $localeIndex = strtolower((substr($localeShort, 0, 2)));
                        $item[$field.'_'.$localeIndex] = preg_replace("/\r|\n/", "", strip_tags($product->getTranslation($field, $localeIndex)));
                    }
                }

            }
            $texts[] = $item;
        }



        $csvWriter = Writer::createFromString('');

        $csvWriter->setDelimiter(';');
        $csvWriter->setEnclosure(' ');

        $header = ['group_id'];
        foreach (['name', 'description'] as $field) {
            foreach ($locales as $localeShort => $localeLong) {
                $localeIndex = strtolower((substr($localeShort, 0, 2)));
                $header[] = $field . '_' . $localeIndex;
            }
        }




        $csvWriter->insertOne($header);
        $csvWriter->insertAll($texts);

        $file = $csvWriter->getContent();
        $filename = 'product_translations_' . date('YmdHis') . '.csv';

        return response($file)
        ->header('Content-type', 'text/plain')
        ->header('Content-Disposition', 'attachment; filename="' . $filename . '"');

    }

}


