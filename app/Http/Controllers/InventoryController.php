<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Productgroup;
use App\Models\Warehouse;
use App\Models\Attribute;
use Auth;

use Illuminate\Http\Request;

class InventoryController extends Controller
{
    //
    public function index(Request $request){
        $data = [];
        if (Auth::user()->hasRole(['Superadmin'])) {
            $data['warehouses'] = Warehouse::get();
        }else{
            $data['warehouses'] = Auth::user()->warehouses;
        }
        $data['productgroups'] = Productgroup::
        with([
            'products' => function($query){
                $query->whereEnabled(1);
            },
            'product',
            'products.warehouseavailabilities',
            'products.attributevalues',
            'products.attributevalues.attribute',
            'products.warehouseavailabilities'
        ])
        ->whereActive(1)
        ->get()
        ->sortByDesc('productgroup_quantity');

        $data['filters']['warehouse_id'] = ($request->has('warehouse_id')) ? $request->warehouse_id :  $data['warehouses']->first()->id;
        return view('admin.inventoryController.index',$data);
    }

    public function update(Request $request){

        $request->validate([
            'warehouse_id' => 'required',
            'product_id' => 'required',
        ]);
        $product = Product::find($request->product_id);
        $product->setQuantityByWarehouseId($request->warehouse_id,$request->quantity,'Manual quantity update');

    }

}
