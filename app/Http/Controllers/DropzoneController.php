<?php

namespace App\Http\Controllers;

use App\Http\Requests\DropzoneRequest;
use Illuminate\Http\Request;

class DropzoneController extends Controller
{
    //

    public function handleDropzoneUpload(DropzoneRequest $request)
    {

        $disk = config('backpack.base.root_disk_name'); //
        $destination_path = "uploads/product_photos";
        $full_destination_path = "public/" . $destination_path;

        $file = $request->file('file');

        try {
            $image = \Image::make($file);
            $filename = md5($file->getClientOriginalName() . time()) . '.jpg';
            \Storage::disk($disk)->put($full_destination_path . '/' . $filename, $image->stream());
            return response()->json(['success' => true, 'filename' => $destination_path . '/' . $filename]);
        } catch (\Exception $e) {
            dd($e);
            if (empty($image)) {
                return response('Not a valid image type', 412);
            } else {
                return response('Unknown error', 412);
            }
        }
    }
}
