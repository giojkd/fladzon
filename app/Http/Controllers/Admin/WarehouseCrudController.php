<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\WarehouseRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class WarehouseCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class WarehouseCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Warehouse::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/warehouse');
        CRUD::setEntityNameStrings('warehouse', 'warehouses');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
#        CRUD::setFromDb(); // columns
        $this->crud->addColumns([
            [
                'label' => 'Name',
                'name' => 'name',
                'type' => 'text',
            ],
            [
                'name' => 'is_shop',
                'type' => 'boolean',
                'label' => 'Is a shop',

            ],
        ]);
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(WarehouseRequest::class);

        #CRUD::setFromDb(); // fields
        $this->crud->addFields([
            [
                'label' => 'Name',
                'name' => 'name',
                'type' => 'text',
            ],
            [
                'label' => 'Short Description',
                'name' => 'short_description',
                'type' => 'textarea',
            ],
            [
                'label' => 'Description',
                'name' => 'description',
                'type' => 'wysiwyg',
            ],
            [
                'label' => 'Opening details',
                'name' => 'opening_details',
                'type' => 'wysiwyg',
            ],
            [
                'name' => 'is_shop',
                'type' => 'checkbox',
                'label' => 'Is a shop',

            ],
            [   // Address google
                'name'          => 'address',
                'label'         => 'Address',
                'type'          => 'address_google',
                // optional
                'store_as_json' => true
            ],
            [
                'name' => 'photos', // db column name
                'label' => 'Photos', // field caption
                'type' => 'dropzone', // voodoo magic
                'prefix' => '/uploads/product_photos', // upload folder (should match the driver specified in the upload handler defined below)
                'upload-url' => '/media-dropzone', // POST route to handle the individual file uploads

            ],
             [
                'label' => 'Phone',
                'name' => 'phone',
                'type' => 'text',
            ],
            [
                'label' => 'Email',
                'name' => 'email',
                'type' => 'email',
            ]
        ]);



        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
