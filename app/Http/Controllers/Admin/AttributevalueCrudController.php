<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AttributevalueRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class AttributevalueCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class AttributevalueCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Attributevalue');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/attributevalue');
        $this->crud->setEntityNameStrings('attributevalue', 'Attribute Values');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();

        $this->crud->addColumn([
            'name' => 'attribute_id', // The db column name
            'label' => "Attributo", // Table column heading
            'type' => 'select',
            'name' => 'attribute_id',
            'entity' => 'attribute',
            'attribute' => 'name',
            'model' => "App\Models\Attribute"
        ]);

        $this->crud->addColumn([
            'name' => 'name', // The db column name
            'label' => "Nome", // Table column heading
            'type' => 'text'
        ]);

        $this->crud->addColumn([
            'name' => 'color', // The db column name
            'label' => "Colore", // Table column heading
            'type' => 'text'
        ]);


    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(AttributevalueRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();
        $this->crud->addField([
            'name' => 'name',
            'type' => 'text',
            'label' => "Name"
        ]);

        $this->crud->addField([  // Select2
            'label' => "Attribute",
            'type' => 'select2',
            'name' => 'attribute_id', // the db column for the foreign key
            'entity' => 'attribute', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => "App\Models\Attribute", // foreign key model

            // optional
            //'default' => 2, // set the default value of the select2
            //'options'   => (function ($query) {
            //return $query->orderBy('name', 'ASC')->where('depth', 1)->get();
            //}), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
        ]);

        $this->crud->addField([   // color_picker
            'label'                => 'Color',
            'name'                 => 'color',
            'type'                 => 'color_picker',
            'default'              => '#000000',
        ]);

        // image
        $this->crud->addField([
            'label' => "Image",
            'name' => "texture",
            'type' => 'image',
            'crop' => true, // set to true to allow cropping, false to disable
            //'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
            // 'disk'      => 's3_bucket', // in case you need to show images from a different disk
            // 'prefix'    => 'uploads/images/profile_pictures/' // in case your db value is only the file name (no path), you can use this to prepend your path to the image src (in HTML), before it's shown to the user;
        ]);


    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
