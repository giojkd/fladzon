<?php

namespace App\Http\Controllers\Admin\Charts;

use App\Models\Lead;
use Backpack\CRUD\app\Http\Controllers\ChartController;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class WeeklyUsersChartController
 * @package App\Http\Controllers\Admin\Charts
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class LastWeekSalesChartController extends ChartController
{

    public $daysBack = 90;

    public function setup()
    {
        $this->chart = new Chart();

        // MANDATORY. Set the labels for the dataset points

        $this->chart->labels($this->getLabels());

        // RECOMMENDED. Set URL that the ChartJS library should call, to get its data using AJAX.
        $this->chart->load(backpack_url('charts/last-week-sales'));

        // OPTIONAL
        // $this->chart->minimalist(false);
        // $this->chart->displayLegend(true);



    }



    /**
     * Respond to AJAX calls with all the chart data points.
     *
     * @return json
     */

     public function getLabels(){

        $labels = [];
        for ($i = 0; $i <= $this->daysBack; $i++) {
            $labels[] = date('d-m-Y', strtotime('-' . $i . 'days'));
        }
        return array_reverse($labels);
     }

    public function data()
    {

        $labels = $this->getLabels();

        $sevenDaysAgo = date('Y-m-d', strtotime('-'.$this->daysBack.'days'));
        $salesByDay =
            Lead::where('leadstatus_id', '=', 2)
            ->where('confirmed_at', '>', $sevenDaysAgo)
            ->get()
            ->transform(function ($item, $key) {
                $item->day = date('Ymd', strtotime($item->confirmed_at));
                return $item;
            })

            ->groupBy('day')

            ->sortKeys()
            ->transform(function ($items, $key) {
                return $items->sum('grand_total');
            })
            ->toArray();




        foreach($this->getLabels() as $label){
            $labelIndex  = date('Ymd',strtotime($label));
            $salesByDay[$labelIndex] = (isset($salesByDay[$labelIndex])) ? $salesByDay[$labelIndex] : 0;
        }



#        return ($salesByDay);



        $this->chart->dataset('Last quarter sales', 'bar',
            array_values($salesByDay)
        )
            ->color('rgba(124, 105, 239, 1)')
            ->backgroundColor('rgba(124, 105, 239, 1)');
    }
}
