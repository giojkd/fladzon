<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProductgroupRequest;
use App\Models\Product;
use App\Models\Productgroup;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ProductgroupCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ProductgroupCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Productgroup::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/productgroup');
        CRUD::setEntityNameStrings('Product group', 'Product groups');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        #CRUD::setFromDb(); // columns
        $this->crud->disableResponsiveTable();
        #$this->crud->enableBulkActions();

        $this->crud->addColumn([

            'type'           => 'checkbox_toggle',
            'name'           => 'active',
            'label'          => 'Active',
            'priority'       => 1,
            'searchLogic'    => false,
            'orderable'      => false,
            'visibleInModal' => false,

        ])->makeFirstColumn();

        $this->crud->addColumn([
            'name' => 'continue_selling',
            'label' => "Continue selling",
            'type'           => 'checkbox_toggle',
            'priority'       => 1,
            'searchLogic'    => false,
            'orderable'      => false,
            'visibleInModal' => false,

        ])->makeFirstColumn();



        $this->crud->addFilter(
            [
                'name' => 'active',
                'type' => 'simple',
                'label' => 'Active',
            ],
            false,
            function ($value) { // if the filter is active
                $this->crud->addClause('where','active','1');
            }
        );

        $this->crud->addColumns([

            [
                'name'      => 'admin_image', // The db column name
                'label'     => '', // Table column heading
                'type'      => 'image',
                'height'  => '240px',
                'width'  => '320px',
            ],
            [
                'name' => 'id',
                'label' => 'ID',
                'searchLogic' => function ($query, $column, $searchTerm) {
                    $query->whereIn('id', Productgroup::search($searchTerm)->get()->pluck('id'));
                }
            ],

            [
                'name' => 'block_discounts',
                'type' => 'boolean',
                'label' => "Block discounts",
            ],
            [
                'name' => 'sku',
                'type' => 'text',
                'label' => "SKU",
            ],
            [
                'name' => 'ext_id',
                'type' => 'text',
                'label' => "Import File ID (Product Group Ext ID)",
            ],
            [
                'name' => 'price',
                'type' => 'text',
                'label' => "Prezzo",

            ],
           /* [
                'label'     => "Main Product",
                'type'      => 'select',
                'name'      => 'product_id', // the db column for the foreign key
                'entity'    => 'product', // the method that defines the relationship in your Model
                'model'     => "App\Models\Product", // foreign key model
                'attribute' => 'name', // foreign key attribute that is shown to user
            ], */
            [
                'label'     => "Main Product ID",
                'name'      => 'product_id', // the db column for the foreign key
            ],
            [
                'name' => 'products',
                'label' => 'Products',
                'type'  => 'model_function',
                'function_name' => 'countProducts'
            ],
            [
                'name' => 'is_on_sale',
                'label' => 'Is on sale',
                'type' => 'boolean'
            ]
,

        [    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Collections",
            'type'      => 'select_multiple',
            'name'      => 'collections', // the method that defines the relationship in your Model
            'entity'    => 'collections', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model'     => "App\Models\Collection", // foreign key model
        ]

        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ProductgroupRequest::class);

        #CRUD::setFromDb(); // fields


        $this->crud->addFields([
                [
                    'name' => 'active',
                    'type' => 'checkbox',
                    'label' => 'Active',

                ],
            [
                'name' => 'block_discounts',
                'type' => 'checkbox',
                'label' => "Block discounts",
            ],
            [
                'name' => 'name',
                'type' => 'text',
                'label' => "Product Group Name",
            ],
            [
                'name' => 'ext_id',
                'type' => 'text',
                'label' => "Import File ID (Product Group Ext ID)",
            ],
                [
                    'name' => 'ext_id_for_siblings',
                    'type' => 'text',
                    'label' => "Id for siblings",
                ],


            [
                'label'     => "Main Product",
                'type'      => 'select2',
                'name'      => 'product_id', // the db column for the foreign key
                'entity'    => 'product', // the method that defines the relationship in your Model
                'model'     => "App\Models\Product", // foreign key model
                'attribute' => 'name_with_identifiers', // foreign key attribute that is shown to user
            ],
            [
                'name' => 'is_on_sale',
                'label' => 'Is on sale',
                'type' => 'checkbox'
            ],
            [
                'name' => 'continue_selling',
                'type' => 'checkbox',
                'label' => "Continue selling",
            ],

            [
            'name' => 'price',
            'type' => 'text',
            'label' => "Prezzo",

            ],

            [
                'name' => 'compare_price',
                'type' => 'text',
                'label' => "Compara il prezzo con",

            ],
             [    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Collection",
            'type'      => 'select2_multiple',
            'name'      => 'collections', // the method that defines the relationship in your Model
            'entity'    => 'collections', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model'     => "App\Models\Collection", // foreign key model
            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?

        ]
        ]);





        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
