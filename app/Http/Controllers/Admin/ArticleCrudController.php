<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ArticleRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ArticleCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ArticleCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Article');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/article');
        $this->crud->setEntityNameStrings('article', 'articles');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();
        $this->crud->addColumn([
            'name' => 'name', // The db column name
            'label' => "Article Name", // Table column heading
            'type' => 'Text'
        ]);

        $this->crud->addColumn([
            'name' => 'name', // The db column name
            'label' => "Article Intro", // Table column heading
            'type' => 'Text'
        ]);

        $this->crud->addColumn([
            'name' => 'url', // The db column name
            'label' => "Url", // Table column heading
            'type' => 'model_function',
            'function_name' => 'makeUrl',
            'limit' => 120
        ]);

        $this->crud->addColumn([
            'name' => 'is_blog',
            'type' => 'boolean',
            'label' => "Is blog",
        ]);

        $this->crud->addField([
            'name' => 'is_contact_page',
            'type' => 'is_blog',
            'label' => "Is contact page",
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(ArticleRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();

        $this->crud->addField([
            'name' => 'is_blog',
            'type' => 'checkbox',
            'label' => "Is blog",
        ]);

        $this->crud->addField([
            'name' => 'is_contact_page',
            'type' => 'checkbox',
            'label' => "Is contact page",
        ]);

        $this->crud->addField([
            'name' => 'name',
            'type' => 'text',
            'label' => "Title",
        ]);

        $this->crud->addField([
            'label' => 'Excerpt',
            'name' => 'description_short',
            'type' => 'textarea',
        ]);

        $this->crud->addField([
            'label' => 'Content',
            'name' => 'description',
            'type' => 'summernote',
        ]);

        $this->crud->addField([
            'name' => 'tag',
            'type' => 'text',
            'label' => "Tag",
        ]);



        $this->crud->addField([
            'name' => 'cover',
            'type' => 'browse',
            'label' => "Cover",
        ]);

    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
