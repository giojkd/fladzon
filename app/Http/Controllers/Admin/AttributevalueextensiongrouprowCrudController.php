<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AttributevalueextensiongrouprowRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class AttributevalueextensiongrouprowCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AttributevalueextensiongrouprowCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Attributevalueextensiongrouprow::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/attributevalueextensiongrouprow');
        CRUD::setEntityNameStrings('attributevalueextensiongrouprow', 'attributevalueextensiongrouprows');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        #        CRUD::setFromDb(); // columns

        $this->crud->addColumns([
            [
                'name' => 'name',
                'label' => 'Name',
                'type' => 'text'
            ],
            [
                'label'     => 'Attribute value extension group',
                'type'      => 'select',
                'name'      => 'attributevalueextensiongroup_id', // the db column for the foreign key
                'entity'    => 'attributevalueextensiongroup', // the method that defines the relationship in your Model
                'attribute' => 'name',
            ],
            [
                'label'     => 'Attribute value',
                'type'      => 'select',
                'name'      => 'attributevalue_id', // the db column for the foreign key
                'entity'    => 'attributevalue', // the method that defines the relationship in your Model
                'attribute' => 'name',
            ],
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(AttributevalueextensiongrouprowRequest::class);

        #CRUD::setFromDb(); // fields

        $this->crud->addFields([
            [
                'name' => 'name',
                'label' => 'Name',
                'type' => 'text'
            ],
            [
                'label'     => 'Attribute value extension group',
                'type'      => 'select2',
                'name'      => 'attributevalueextensiongroup_id', // the db column for the foreign key
                'entity'    => 'attributevalueextensiongroup', // the method that defines the relationship in your Model
                'attribute' => 'name',
            ],
            [
                'label'     => 'Attribute value',
                'type'      => 'select2',
                'name'      => 'attributevalue_id', // the db column for the foreign key
                'entity'    => 'attributevalue', // the method that defines the relationship in your Model
                'attribute' => 'name',
            ],
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
