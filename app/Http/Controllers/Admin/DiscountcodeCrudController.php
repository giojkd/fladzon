<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\DiscountcodeRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class DiscountcodeCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class DiscountcodeCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Discountcode::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/discountcode');
        CRUD::setEntityNameStrings('discountcode', 'discountcodes');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */

    public function getDiscountTypeOptions(){
        return [1 => 'Amount (€)', 2 => 'Percentage (%)'];
    }

    protected function setupListOperation()
    {
        #CRUD::setFromDb(); // columns

          $this->crud->addColumns([
              [
                'name' => 'active',
                'label' => 'Attivo',
                'type' => 'boolean'
            ],
            [
                'name' => 'code',
                'label' => 'Code',
                'type' => 'text'
            ],
            [
                'name' => 'value',
                'label' => 'Value',
                'type' => 'text'
            ],
            [
                'name' => 'discountcode_type_id',
                'label' => 'Type',
                'type' => 'select_from_array',
                'options'     => $this->getDiscountTypeOptions(),
            ],
            [
                'name' => 'min_grand_total',
                'label' => 'Min total',
                'type' => 'text'
            ],
            [
                'name' => 'expires_at',
                'label' => 'Expires at',
                'type' => 'datetime'
            ],
          ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(DiscountcodeRequest::class);

        #CRUD::setFromDb(); // fields

        $this->crud->addFields([
            [
                'name' => 'active',
                'label' => 'Attivo',
                'type' => 'checkbox'
            ],
            [
                'name' => 'code',
                'label' => 'Code',
                'type' => 'text'
            ],
            [
                'name' => 'value',
                'label' => 'Value',
                'type' => 'text'
            ],
            [
                'name' => 'discountcode_type_id',
                'label' => 'Type',
                'type' => 'select_from_array',
                'options'     => $this->getDiscountTypeOptions(),
                'allows_null' => false,
                'default'     => '1',
            ],
            [
                'name' => 'min_grand_total',
                'label' => 'Min total',
                'type' => 'text'
            ],
            [
                'name' => 'expires_at',
                'label' => 'Expires at',
                'type' => 'datetime'
            ],
            [    // Select2Multiple = n-n relationship (with pivot table)
                'label'     => "Product groups",
                'type'      => 'select2_multiple',
                'name'      => 'productgroups', // the method that defines the relationship in your Model

                // optional
                'entity'    => 'productgroups', // the method that defines the relationship in your Model
                'model'     => "App\Models\Productgroup", // foreign key model
                'attribute' => 'ext_id', // foreign key attribute that is shown to user
                'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
                // 'select_all' => true, // show Select All and Clear buttons?

                // optional
                'options'   => (function ($query) {
                    return $query->orderBy('ext_id', 'ASC')->get();
                }),
            ],
            [    // Select2Multiple = n-n relationship (with pivot table)
                'label'     => "Products",
                'type'      => 'select2_multiple',
                'name'      => 'products', // the method that defines the relationship in your Model

                // optional
                'entity'    => 'products', // the method that defines the relationship in your Model
                'model'     => "App\Models\Product", // foreign key model
                'attribute' => 'name_with_identifiers', // foreign key attribute that is shown to user
                'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
                // 'select_all' => true, // show Select All and Clear buttons?

                // optional
                'options'   => (function ($query) {
                    return $query->orderBy('sku', 'ASC')->get();
                }),
            ],
          ]);



        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
