<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CategoryRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Http\Requests\DropzoneRequest;
use Symfony\Component\HttpFoundation\Request;
use App\Models\Category;


/**
 * Class CategoryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class CategoryCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation;


    public function setup()
    {
        $this->crud->setModel('App\Models\Category');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/category');
        $this->crud->setEntityNameStrings('category', 'categories');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();
        $this->crud->addColumn([
            'name' => 'id',
            'type' => 'text',
            'label' => "ID"
        ]);

        $this->crud->addColumn([
            'name' => 'show_in_new_arrivals',
            'type' => 'boolean',
            'label' => "Show in new arrivals"
        ]);

        $this->crud->addColumn([
            'name' => 'name', // The db column name
            'label' => "Name", // Table column heading
            'type' => 'Text'
        ]);
        $this->crud->addColumn([
            'name' => 'depth', // The db column name
            'label' => "Profondità", // Table column heading
            'type' => 'Text'
        ]);

        $this->crud->addColumn([
            'name' => 'products', // The db column name
            'label' => "Prodotti", // Table column heading
            'type' => 'model_function',
            'function_name' => 'countProducts'
        ]);
        $this->crud->addColumn([
            'name' => 'parent_id', // The db column name
            'label' => "Parent", // Table column heading
            'type' => 'select',
            'name' => 'parent_id',
            'entity' => 'parentcategory',
            'attribute' => 'name',
            'model' => "App\Models\Category"
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(CategoryRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        #$this->crud->setFromDb();

        $this->crud->addField([
            'name' => 'show_in_new_arrivals',
            'type' => 'checkbox',
            'label' => "Show in new arrivals"
        ]);

        $this->crud->addField([
            'name' => 'name',
            'type' => 'text',
            'label' => "Name"
        ]);



        $this->crud->addField([  // Select2
            'label'     => 'Category parent',
            'type'      => 'select2',
            'name'      => 'parent_id', // the db column for the foreign key
            'entity'    => 'parentcategory', // the method that defines the relationship in your Model
            'attribute' => 'name'
        ]);


        $this->crud->addField([  // Select2
            'label'     => 'Google Category',
            'type'      => 'select2',
            'name'      => 'googlecategory_id', // the db column for the foreign key
            'entity'    => 'googlecategory', // the method that defines the relationship in your Model
            'attribute' => 'name',

            #'default' => 1,
            #'options'   => (function ($query) {
            #  return $query->orderBy('name', 'DESC')->get();
            #})
            // foreign key attribute that is shown to user
            // 'wrapperAttributes' => [
            //     'class' => 'form-group col-md-6'
            //   ], // extra HTML attributes for the field wrapper - mostly for resizing fields
            #'tab' => 'Basic Info',
        ]);



        $this->crud->addField([
            'label' => "Cover",
            'name' => "cover",
            'type' => 'image',
            'upload' => true,
            'crop' => true, // set to true to allow cropping, false to disable
            //'aspect_ratio' => 1, // ommit or set to 0 to allow any aspect ratio
            // 'disk' => 's3_bucket', // in case you need to show images from a different disk
            // 'prefix' => 'uploads/images/profile_pictures/' // in case your db value is only the file name (no path), you can use this to prepend your path to the image src (in HTML), before it's shown to the user;
        ]);
    }

    protected function setupReorderOperation()
    {
        // define which model attribute will be shown on draggable elements
        $this->crud->set('reorder.label', 'name');
        // define how deep the admin is allowed to nest the items
        // for infinite levels, set it to 0
        $this->crud->set('reorder.max_level', 0);
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }


}
