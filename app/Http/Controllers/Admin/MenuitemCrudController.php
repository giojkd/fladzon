<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\MenuitemRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class MenuitemsCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class MenuitemCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Menuitem');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/menuitem');
        $this->crud->setEntityNameStrings('menuitem', 'menuitems');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();

        $this->crud->addColumn([
            'name' => 'id'

        ]);
        $this->crud->addColumn([
            'name' => 'name', // The db column name
            'label' => "Item Name", // Table column heading
            'type' => 'text'
        ]);


        $this->crud->addColumn([  // Select
            'label' => "Menu",
            'type' => 'select',
            'name' => 'menu_id', // the db column for the foreign key
            'entity' => 'menu', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => "App\Models\Menu",

            // optional
            //'options'   => (function ($query) {
            //  return $query->orderBy('name', 'ASC')->where('depth', 1)->get();
            //}), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
        ]);

        $this->crud->addColumn([
            'name' => 'parent_id'

        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(MenuitemRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        //$this->crud->setFromDb();

        $this->crud->addField([
            'name' => 'name', // The db column name
            'label' => "Item Name", // Table column heading
            'type' => 'text'
        ]);

        $this->crud->addField([
            'name' => 'href', // The db column name
            'label' => "Item Href", // Table column heading
            'type' => 'text'
        ]);

        $this->crud->addField([  // Select
            'label' => "Menu",
            'type' => 'select',
            'name' => 'menu_id', // the db column for the foreign key
            'entity' => 'menu', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model' => "App\Models\Menu",

            // optional
            //'options'   => (function ($query) {
            //  return $query->orderBy('name', 'ASC')->where('depth', 1)->get();
            //}), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
        ]);

        $this->crud->addField([
            'name' => 'photos', // db column name
            'label' => 'Photos', // field caption
            'type' => 'dropzone', // voodoo magic
            'prefix' => '/uploads/product_photos', // upload folder (should match the driver specified in the upload handler defined below)
            'upload-url' => '/media-dropzone', // POST route to handle the individual file uploads

        ]);
    }

    protected function setupReorderOperation()
    {
        // define which model attribute will be shown on draggable elements
        $this->crud->set('reorder.label', 'name');
        // define how deep the admin is allowed to nest the items
        // for infinite levels, set it to 0
        $this->crud->set('reorder.max_level', 0);
    }



    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
