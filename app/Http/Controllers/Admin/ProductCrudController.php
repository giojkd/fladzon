<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ProductRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Http\Requests\DropzoneRequest;
use App\Models\Category;
use App\Models\Product;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ProductCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class ProductCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation {
        update as traitUpdate;
    }


    public function update(Request $request)
    {
        // do something before validation, before save, before everything; for example:
        // $this->crud->request->request->add(['author_id'=> backpack_user()->id]);
        // $this->crud->addField(['type' => 'hidden', 'name' => 'author_id']);
        // $this->crud->request->request->remove('password_confirmation');
        // $this->crud->removeField('password_confirmation');


        if (empty($request->get('photos'))) {
            $this->crud->update(\Request::get($this->crud->model->getKeyName()), ['photos' => '[]']);
        }

        if (empty($request->get('attributevalueextensiongroup_id'))) {
            $this->crud->update(\Request::get($this->crud->model->getKeyName()), ['attributevalueextensiongroup_id' => null]);
        }


        $response = $this->traitUpdate();
        // do something after save
        return $response;
    }

    public function setup()
    {
        $this->crud->setModel('App\Models\Product');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/product');
        $this->crud->setEntityNameStrings('product', 'products');
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        #$this->crud->setFromDb();

        $this->crud->addFilter(
            [
                'name' => 'enabled',
                'type' => 'simple',
                'label' => 'Active',
            ],
            false,
            function ($value) { // if the filter is active
                $this->crud->addClause('where','enabled','1');
            }
        );


        $this->crud->addColumn([
            'name' => 'id',
            'type' => 'text',
            'label' => "ID",
            'searchLogic' => function ($query, $column, $searchTerm) {
                $query->whereIn('id', Product::search($searchTerm)->get()->pluck('id'));
            }
        ]);

        $this->crud->addColumn([
            'name' => 'attributes_line',
            'label' => 'Features',
            'type'  => 'text',
            'limit' => '200'


        ]);

        $this->crud->addColumn([
            'name' => 'group',
            'label' => 'Group',
            'type'  => 'model_function',
            'function_name' => 'groupName',

        ]);

        $this->crud->addColumn([
            'name' => 'enabled',
            'type' => 'boolean',
            'label' => "Enabled",
        ]);

        $this->crud->addColumn([
            'name' => 'show_in_shop_window',
            'type' => 'boolean',
            'label' => "ShopWindow",
        ]);

        $this->crud->addColumn([
            'name' => 'sku',
            'type' => 'sku',
            'label' => "SKU",
        ]);


        $this->crud->addField([
            'name' => 'barcode',
            'type' => 'text',
            'label' => "Barcode",
            'tab' => 'Main'
        ]);

        $this->crud->addColumn([
            'name' => 'name',
            'type' => 'text',
            'label' => "Name",
        ]);

        $this->crud->addColumn([
            'name' => 'price',
            'type' => 'text',
            'label' => "Prezzo",
        ]);

        $this->crud->addColumn([
            'name' => 'quantity',
            'type' => 'number',
            'label' => "Q.ty",
        ]);

        $this->crud->addColumn([  // Select2
            'label'     => 'Category',
            'type'      => 'select',
            'name'      => 'category_id', // the db column for the foreign key
            'entity'    => 'category', // the method that defines the relationship in your Model
            'attribute' => 'name',
            'model' => Category::class
        ]);

        $this->crud->addColumn([
            'label' => 'Manufacturer',
            'type' => 'select',
            'name' => 'manufacturer_id',
            'entity' => 'manufacturer',
            'attribute' => 'name',
        ]);

        $this->crud->addColumn([
            'name' => 'continue_selling',
            'type' => 'boolean',
            'label' => "Continue selling",
        ]);

        $this->crud->addColumn([    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Collections",
            'type'      => 'select_multiple',
            'name'      => 'collections', // the method that defines the relationship in your Model
            'entity'    => 'collections', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model'     => "App\Models\Collection", // foreign key model
        ]);



    }

    protected function setupUpdateOperation(){
        $this->setupCreateOperation();
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(ProductRequest::class);

        // TODO: remove setFromDb() and manually define Fields



        $this->crud->addField([
            'name' => 'sku',
            'type' => 'text',
            'label' => "SKU",
            'tab' => 'Main'
        ]);
        $this->crud->addField([
            'name' => 'barcode',
            'type' => 'text',
            'label' => "Barcode",
            'tab' => 'Main'
        ]);

        $this->crud->addField([
            'name' => 'name',
            'type' => 'text',
            'label' => "Product name",
            'tab' => 'Main'
        ]);

        $this->crud->addField([
            'label' => 'Short Description',
            'name' => 'short_description',
            'type' => 'textarea',
            'tab' => 'Main'
        ]);

        $this->crud->addField([
            'label' => 'Description',
            'name' => 'description',
            'type' => 'wysiwyg',
            'tab' => 'Main'
        ]);

        $this->crud->addField([
            'label' => 'Extended Description',
            'name' => 'extended_description',
            'type' => 'wysiwyg',
            'tab' => 'Main'
        ]);

        $this->crud->addField([
            'name' => 'price',
            'type' => 'text',
            'label' => "Prezzo",
            'tab' => 'Main'
        ]);

        $this->crud->addField([
            'name' => 'compare_price',
            'type' => 'text',
            'label' => "Compara il prezzo con",
            'tab' => 'Main'
        ]);

        $this->crud->addField([
            'label' => 'Manufacturer',
            'type' => 'select2',
            'name' => 'manufacturer_id',
            'entity' => 'manufacturer',
            'attribute' => 'name',
            'tab' => 'Main'
        ]);


        $this->crud->addField([  // Select2
            'label'     => 'Group',
            'type'      => 'select2',
            'name'      => 'productgroup_id', // the db column for the foreign key
            'entity'    => 'group', // the method that defines the relationship in your Model
            'attribute' => 'id',
            'tab' => 'Main'
            #'default' => 1,
            #'options'   => (function ($query) {
            #  return $query->orderBy('name', 'DESC')->get();
            #})
            // foreign key attribute that is shown to user
            // 'wrapperAttributes' => [
            //     'class' => 'form-group col-md-6'
            //   ], // extra HTML attributes for the field wrapper - mostly for resizing fields
            #'tab' => 'Basic Info',
        ]);


        $this->crud->addField([  // Select2
            'label'     => 'Category',
            'type'      => 'select2',
            'name'      => 'category_id', // the db column for the foreign key
            'entity'    => 'category', // the method that defines the relationship in your Model
            'attribute' => 'name',
            'tab' => 'Main'
            #'default' => 1,
            #'options'   => (function ($query) {
            #  return $query->orderBy('name', 'DESC')->get();
            #})
            // foreign key attribute that is shown to user
            // 'wrapperAttributes' => [
            //     'class' => 'form-group col-md-6'
            //   ], // extra HTML attributes for the field wrapper - mostly for resizing fields
            #'tab' => 'Basic Info',
        ]);

        $this->crud->addField([  // Select2

            'label'     => 'Attribute value extension group',
            'type'      => 'relationship',
            'name'      => 'attributevalueextensiongroup', // the db column for the foreign key
            #'entity'    => 'attributevalueextensiongroup', // the method that defines the relationship in your Model
            #'attribute' => 'name',
            'placeholder' => 'Select an option',
            'tab' => 'Main',

        ]);




        /*  $this->crud->addField([   // Upload
    'name' => 'photos',
    'label' => 'Photos',
    'type' => 'upload_multiple',
    'upload' => true,
    //'disk' => 'uploads', // if you store files in the /public folder, please ommit this; if you store them in /storage or S3, please specify it;
    'tab' => 'Images',
    // optional:
    //'temporary' => 10 // if using a service, such as S3, that requires you to make temporary URL's this will make a URL that is valid for the number of minutes specified
  ]); */

        $this->crud->addField([
            'name' => 'enabled',
            'label' => 'Enabled',
            'type' => 'checkbox',
            'tab' => 'Main',
        ]);


        $this->crud->addField([
            'name' => 'continue_selling',
            'type' => 'checkbox',
            'label' => "Continue selling",
            'tab' => 'Main',
        ]);

        $this->crud->addField([
            'name' => 'show_in_shop_window',
            'label' => 'Shop Windows',
            'type' => 'checkbox',
            'tab' => 'Main',
        ]);

        $this->crud->addField([
            'name' => 'quantity',
            'type' => 'number',
            'label' => "Quantità",
            'tab' => 'Stock',
        ]);

        $this->crud->addField([
            'label' => 'Larghezza prodotto',
            'name' => 'product_width',
            'type' => 'number',
            'tab' => 'Dimensions',
            'prefix' => 'cm'
        ]);

        $this->crud->addField([
            'label' => 'Altezza prodotto',
            'name' => 'product_height',
            'type' => 'number',
            'tab' => 'Dimensions',
            'prefix' => 'cm'
        ]);


        $this->crud->addField([
            'label' => 'Profondità prodotto',
            'name' => 'product_length',
            'type' => 'number',
            'tab' => 'Dimensions',
            'prefix' => 'cm'
        ]);



        $this->crud->addField([
            'label' => 'Peso prodotto',
            'name' => 'product_weight',
            'type' => 'number',
            'tab' => 'Dimensions',
            'prefix' => 'Kg'
        ]);

        $this->crud->addField([
            'label' => 'Larghezza imballaggio',
            'name' => 'packaging_width',
            'type' => 'number',
            'tab' => 'Dimensions',
            'prefix' => 'cm'
        ]);

        $this->crud->addField([
            'label' => 'Altezza imballaggio',
            'name' => 'packaging_height',
            'type' => 'number',
            'tab' => 'Dimensions',
            'prefix' => 'cm'
        ]);


        $this->crud->addField([
            'label' => 'Profondità imballaggio',
            'name' => 'packaging_length',
            'type' => 'number',
            'tab' => 'Dimensions',
            'prefix' => 'cm'
        ]);



        $this->crud->addField([
            'label' => 'Peso imballaggio',
            'name' => 'packaging_weight',
            'type' => 'number',
            'tab' => 'Dimensions',
            'prefix' => 'Kg'
        ]);


        $this->crud->addField([  // Select2
            'label'     => 'Gender',
            'type'      => 'select2',
            'name'      => 'gender_id', // the db column for the foreign key
            'entity'    => 'gender', // the method that defines the relationship in your Model
            'attribute' => 'name',
            'tab' => 'Main'
            #'default' => 1,
            #'options'   => (function ($query) {
            #  return $query->orderBy('name', 'DESC')->get();
            #})
            // foreign key attribute that is shown to user
            // 'wrapperAttributes' => [
            //     'class' => 'form-group col-md-6'
            //   ], // extra HTML attributes for the field wrapper - mostly for resizing fields
            #'tab' => 'Basic Info',
        ]);
/*
        $this->crud->addField([
            'name' => 'gender',
            'type' => 'select_from_array',
            'label' => "Gender",
            'tab' => 'Main',
            'options' => [
                'male' => 'Male',
                'female' => 'Female',
                'unisex' => 'Unisex',
            ]
        ]);

        $this->crud->addField([
            'name' => 'age_group',
            'type' => 'select_from_array',
            'label' => "Age group",
            'tab' => 'Main',
            'options' => [
                'newborn' => 'Up to 3 months old. Newborn sizes are often identified by the age range in months (0–3) or just “newborn.”',
                'infant' => '3–12 months old. Infant sizes are often identified by the age range in months (3–12).',
                'toddler' => '1–5 years old. Toddler sizes are often identified by the age range in months (12–24) or years (1–5).',
                'kids' => '5–13 years old. All sizes within this age group have been manufactured to fit a child in that age range.',
                'adult' => 'Typically teens or older. All sizes within this age group have been manufactured to fit an adult or teen.',
            ]
        ]);
*/

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Attribute values",
            'type'      => 'select2_multiple',
            'name'      => 'attributevalues', // the method that defines the relationship in your Model
            'entity'    => 'attributevalues', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model'     => "App\Models\AttributeValue", // foreign key model
            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            'tab' => 'Attributes',
            // 'select_all' => true, // show Select All and Clear buttons?

            // optional
            #'options'   => (function ($query) {
            #  return $query->orderBy('name', 'ASC')->where('depth', 1)->get();
            #}), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
        ]);


        $this->crud->addField([
            'name' => 'photos', // db column name
            'label' => 'Photos', // field caption
            'type' => 'dropzone', // voodoo magic
            'prefix' => '/uploads/product_photos', // upload folder (should match the driver specified in the upload handler defined below)
            'upload-url' => '/' . config('backpack.base.route_prefix') . '/media-dropzone', // POST route to handle the individual file uploads
            'tab' => 'Photos',
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Collection",
            'type'      => 'select2_multiple',
            'name'      => 'collections', // the method that defines the relationship in your Model
            'entity'    => 'collections', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model'     => "App\Models\Collection", // foreign key model
            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            'tab' => 'Main',
        ]);


    }

    public function handleDropzoneUpload(DropzoneRequest $request)
    {

        $disk = config('backpack.base.root_disk_name'); //
        $destination_path = "uploads/product_photos";
        $full_destination_path = "public/". $destination_path;

        $file = $request->file('file');

        try {
            $image = \Image::make($file);
            $filename = md5($file->getClientOriginalName() . time()) . '.jpg';
            \Storage::disk($disk)->put($full_destination_path . '/' . $filename, $image->stream());
            return response()->json(['success' => true, 'filename' => $destination_path . '/' . $filename]);
        } catch (\Exception $e) {
            if (empty($image)) {
                return response('Not a valid image type', 412);
            } else {
                return response($e->getMessage(), 412);
            }
        }
    }


}
