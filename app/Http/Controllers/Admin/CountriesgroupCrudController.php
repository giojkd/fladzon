<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CountriesgroupRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Lwwcas\LaravelCountries\Models\Country;

/**
 * Class CountriesgroupCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CountriesgroupCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Countriesgroup::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/countriesgroup');
        CRUD::setEntityNameStrings('countries group', 'countries groups');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {

        $this->crud->addColumn([
            'name' => 'id',
            'type' => 'text',
            'label' => "ID"
        ]);


        $this->crud->addColumn([
            'name' => 'name', // The db column name
            'label' => "Name", // Table column heading
            'type' => 'Text'
        ]);

        $this->crud->addColumn([
            'name' => 'extra_shipping_cost',
            'type' => 'number',
            'label' => "Extra Shipping Cost",
            'prefix' => '€'
        ]);

        $this->crud->addColumn([    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Countries",
            'type'      => 'select_multiple',
            'name'      => 'countries', // the method that defines the relationship in your Model
            'entity'    => 'countries', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model'     => Country::class, // foreign key model
            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        #CRUD::setValidation(CountriesgroupRequest::class);

        $this->crud->addField([
            'name' => 'name',
            'type' => 'text',
            'label' => "Name"
        ]);

        $this->crud->addField([
            'name' => 'extra_shipping_cost',
            'type' => 'number',
            'attributes' => ['step' => 'any'],
            'label' => "Extra Shipping Cost",
            'prefix' => '€'
        ]);

        $this->crud->addField( [    // Select2Multiple = n-n relationship (with pivot table)
                'label'     => "Countries",
                'type'      => 'select2_multiple',
                'name'      => 'countries', // the method that defines the relationship in your Model
                'entity'    => 'countries', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model'     => Country::class, // foreign key model
                'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
