<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\LeadproductRequest;
use App\Models\Lead;
use App\Models\Product;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class LeadproductCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class LeadproductCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Leadproduct::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/leadproduct');
        CRUD::setEntityNameStrings('Riga di un ordine', 'Righe degli ordini');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        #CRUD::setFromDb(); // columns

        $this->crud->addColumns([
            [
                'label'     => 'ID',
                'type'      => 'text',
                'name'      => 'id', // the db column for the foreign key

            ],
            [  // Select2
                'label'     => 'Lead',
                'type'      => 'select2',
                'name'      => 'lead_id', // the db column for the foreign key
                'entity'    => 'lead', // the method that defines the relationship in your Model
                'attribute' => 'id',
                'model' => Lead::class
            ],
            [  // Select2
                'label'     => 'Product',
                'type'      => 'select',
                'name'      => 'product_id', // the db column for the foreign key
                'entity'    => 'product', // the method that defines the relationship in your Model
                'attribute' => 'name_with_identifiers',
                'model' => Product::class
            ],
            [
                'label'     => 'Quantity',
                'type'      => 'number',
                'name'      => 'quantity', // the db column for the foreign key

            ],
            [
                'label'     => 'Subtotal',
                'type'      => 'text',
                'name'      => 'sub_total', // the db column for the foreign key

            ]]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        #CRUD::setValidation(LeadproductRequest::class);

        $this->crud->addFields([
                [  // Select2
                    'label'     => 'Lead',
                    'type'      => 'select2',
                    'name'      => 'lead_id', // the db column for the foreign key
                    'entity'    => 'lead', // the method that defines the relationship in your Model
                    'attribute' => 'id',
                    'model' => Lead::class,
                    'options' =>  (function($query){
                        return $query->where('leadstatus_id','!=',1)->get();
                    })
                ],
            [  // Select2
                'label'     => 'Product',
                'type'      => 'select2',
                'name'      => 'product_id', // the db column for the foreign key
                'entity'    => 'product', // the method that defines the relationship in your Model
                'attribute' => 'name_with_identifiers',
                'model' => Product::class
            ],[
                'label'     => 'Quantity',
                'type'      => 'number',
                'name'      => 'quantity', // the db column for the foreign key

            ],
            [
                'label'     => 'Subtotal',
                'type'      => 'number',
                'name'      => 'sub_total', // the db column for the foreign key
                'attributes' => ["step" => "any"], // allow decimals
            ]
        ]);




        #CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }



}
