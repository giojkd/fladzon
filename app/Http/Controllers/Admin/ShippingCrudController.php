<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ShippingRequest;
use App\Models\Warehouse;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Auth;

/**
 * Class ShippingCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ShippingCrudController extends CrudController
{

    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;

    #use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Shipping::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/shipping');
        CRUD::setEntityNameStrings('shipping', 'shippings');



        if (!Auth::user()->hasRole('Superadmin')) {

            $this->crud->denyAccess('delete');
            $this->crud->denyAccess('create');
        }

    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {


        if(!Auth::user()->hasRole('Orders and shippings')){
            $this->crud->disableResponsiveTable();
        }


        #CRUD::setFromDb(); // columns

        $this->crud->enableExportButtons();

        $this->crud->addColumns([
            [
                'name' => 'id',
                'label' => 'ID',
            ],
            [
                'label'     => 'Warehouse',
                'type'      => 'select',
                'name'      => 'warehouse_id', // the db column for the foreign key
                'entity'    => 'Warehouse', // the method that defines the relationship in your Model
                'attribute' => 'name',
                'model' => Warehouse::class
            ],
            [
                'name' => 'lead_id',
                'label' => 'Ordine',
                'type' => 'text',
            ],
            [
                'name' => 'ship_to',
                'label' => 'Ship to',
                'type' => 'model_function',
                'function_name' => 'shipTo',
                'limit' => 10000
            ],
            [
                'name' => 'products',
                'label' => 'Products',
                'type' => 'model_function',
                'function_name' => 'printProducts',
                'limit' => 10000
            ],
            [
                'name' => 'is_ready',
                'label' => 'Is ready',
                'type' => 'boolean'

            ],
            [
                'name' => 'is_shipped',
                'label' => 'Is shipped',
                'type' => 'boolean'

            ],
            [
                'name' => 'tracking_number',
                'label' => 'Tracking number',
                'type' => 'text'

            ],
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ShippingRequest::class);

        #CRUD::setFromDb(); // fields

        $this->crud->addFields([

            [
                'name' => 'is_ready',
                'label' => 'Is ready',
                'type' => 'checkbox'

            ],
            [
                'name' => 'is_shipped',
                'label' => 'Is shipped',
                'type' => 'checkbox'

            ],
            [
                'name' => 'tracking_number',
                'label' => 'Tracking number',
                'type' => 'text'

            ],
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
