<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\LeadRequest;
use App\Models\Lead;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use PDF;
use Auth;
use Backpack\CRUD\app\Library\Widget;

/**
 * Class LeadCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class LeadCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Lead::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/lead');
        CRUD::setEntityNameStrings('ordine', 'ordini');

        $user = Auth::user();

        if(!$user->hasRole('Superadmin')){
            $this->crud->addClause('whereIn', 'emits_receipt_warehouse_id', $user->warehouses->pluck('id') );
            $this->crud->denyAccess('create');
            $this->crud->denyAccess('update');
            $this->crud->denyAccess('delete');
            $this->crud->denyAccess('show');
        }

    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {




        $this->crud->enableExportButtons();

        #if (!Auth::user()->hasRole('Orders and shippings')) {
            $this->crud->disableResponsiveTable();
        #}

        $this->crud->addClause('where','leadstatus_id', '!=', '1');
        $this->crud->addClause('where','grand_total', '>', '0');

        #CRUD::setFromDb(); // columns

        $this->crud->denyAccess('update');

        $user = Auth::user();
        if ($user->hasRole('Superadmin')) {
            $this->crud->addButtonFromView('line', 'manage_shippings', 'manage_shippings', 'end');
            $this->crud->addButtonFromView('line', 'print_order', 'print_order', 'end');
        }

        if (!$this->crud->getRequest()->has('order')) {
            $this->crud->orderBy('confirmed_at', 'DESC');
        }

        $this->crud->addColumn([
            'name' => 'id',
            'type' => 'text',
            'label' => "ID",
            'searchLogic' => function ($query, $column, $searchTerm) {
                $query->whereIn('id', Lead::search($searchTerm)->get()->pluck('id'));
            }
        ]);

        $this->crud->addColumns([
            [
                'label' => '<i class="fa fa-truck"></i>',
                'type' => 'text',
                'name' => 'shipping_advancement_status'
            ],
            [
                'name' => 'billing_address',
                'label' => 'Billing Address',
                'type' => 'model_function',
                'function_name' => 'printBillingAddress',
                'limit' => 10000,
                'wrapper' => [
                    'element' => 'div',
                    'style' => 'max-width: 320px; white-space: normal; '
                ]
            ],
            [
                'name' => 'shipping_address',
                'label' => 'Shipping Address',
                'type' => 'model_function',
                'function_name' => 'printShippingAddress',
                'limit' => 10000,
                'wrapper' => [
                    'element' => 'div',
                    'style' => 'max-width: 240px; white-space: normal; '
                ]
            ],
            [
                'name' => 'notes',
                'label' => 'Notes',
                'type' => 'model_function',
                'function_name' => 'printNotes',
                'limit' => 200,
                'wrapper' => [
                    'element' => 'div',
                    'style' => 'max-width: 200px; white-space: normal; '
                ]
            ],
            [
                'name' => 'products',
                'label' => 'Products',
                'type' => 'model_function',
                'function_name' => 'printProducts',
                'limit' => 10000,
                'priority' => 0
            ],
            [
                'name' => 'grand_total',
                'label' => 'Totale',
                'type' => 'model_function',
                'function_name' => 'printGrandTotal',
                'limit' => 200,
                'priority' => 0
            ],
            [
                'name' => 'pay_with',
                'label' => 'Pagato con',
                'limit' => 200,
                'type' => 'model_function',
                'function_name' => 'printPaymentMethod'
            ],
            [
                'name' => 'confirmed_at',
                'label' => 'Confermato il',
                'type' => 'datetime',
                'priority' => 0
                #'function_name' => 'printConfirmedAt',
                #'limit' => 200
            ],
            [
                    'name' => 'invoice_request',
                    'label' => 'Fattura',
                    'type' => 'boolean'
            ],
            [
                'name' => 'abroad_invoice',
                'label' => 'Fattura estero',
                'type' => 'boolean'
            ],
                [
                    // 1-n relationship
                    'label'     => 'Shippings', // Table column heading
                    'type'      => 'select_multiple',
                    'name'      => 'shippings', // the column that contains the ID of that connected entity;
                    'entity'    => 'shippings', // the method that defines the relationship in your Model
                    'attribute' => 'warehouse_name', // foreign key attribute that is shown to user
                    'model'     => "App\Models\Shipping", // foreign key model
                ],
            [
                // 1-n relationship
                'label'     => 'Emits receipt', // Table column heading
                'type'      => 'select',
                'name'      => 'emits_receipt_warehouse_id', // the column that contains the ID of that connected entity;
                'entity'    => 'emitsReceipt', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model'     => "App\Models\Warehouse", // foreign key model
            ],
            [
                'label' => 'Newsletter',
                'type' => 'boolean',
                'name' => 'has_subscribed_newsletter',
            ],


        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }



    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(LeadRequest::class);

        CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        #$this->setupCreateOperation();
        $this->crud->addFields([
            [
                'name' => 'leadstatus_id',
                'type' => 'select_from_array',
                'options' => [
                    1 => 'New',
                    2 => 'Confirmed'
                ],
                'label' => 'Status'
            ],
            [
                'name' => 'pay_with',
                'type' => 'select_from_array',
                'options' => [
                    'stripe' => 'Stripe',
                    'paypal' => 'Paypal',
                    'cashondelivery' => 'Cash on delivery'
                ],
                'label' => 'Paid with'
            ],
            [
                'name' => 'confirmed_at',
                'type' => 'datetime',
                'label' => 'Confirmed at'
            ],
            [
                'name' => 'notes',
                'type' => 'textarea',
                'label' => 'Notes'
            ],
            [
                'name' => 'products_total',
                'label' => 'Products total',
                'type' => 'text'
            ],
            [
                'name' => 'shipping_total',
                'label' => 'Shipping total',
                'type' => 'text'
            ],
            [
                'name' => 'discount_total',
                'label' => 'Discount total',
                'type' => 'text'
            ],
            [
                'name' => 'grand_total',
                'label' => 'Grand total',
                'type' => 'text'
            ]
        ]);
    }

    public function printOrder(LeadRequest $request,$id)
    {
        $lead = Lead::with(['products'])->findOrFail($id);
        $data = ['lead' => $lead];
        $pdf = PDF::loadView('admin.Order.print', $data);
        return $pdf->download('order_'.$lead->id.'.pdf');

    }
}
