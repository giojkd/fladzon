<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CollectiondiscountRequest;
use App\Models\Collectiondiscount;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Prologue\Alerts\Facades\Alert;

/**
 * Class CollectiondiscountCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CollectiondiscountCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Collectiondiscount::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/collectiondiscount');
        CRUD::setEntityNameStrings('collectiondiscount', 'collectiondiscounts');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {

        $this->crud->addButtonFromView('line', 'reapply_collectiondiscount', 'reapply_collectiondiscount', 'end');

        #CRUD::setFromDb(); // columns

        $this->crud->addColumns([
            [  // Select2
            'label'     => 'Collection',
            'type'      => 'select',
            'name'      => 'collection_id', // the db column for the foreign key
            'entity'    => 'collection', // the method that defines the relationship in your Model
            'attribute' => 'name',

            ],
            [
                'label' => 'Variation',
                'name' => 'discount',
                'suffix' => '%'
            ],
            [
                'label' => 'Applied at',
                'name' => 'applied_at',
            ]
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(CollectiondiscountRequest::class);

        #CRUD::setFromDb(); // fields

        $this->crud->addFields([
            [
                'label' => 'Collection',
                'name' => 'collection_id',
            ],
            [
                'label' => 'Discount',
                'name' => 'discount',
                'prefix' => '-',
                'suffix' => '%'
            ]
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function resetCollectionDiscount(CollectiondiscountRequest $request,$id){
        $collectionDiscount = Collectiondiscount::findOrFail($id);
        $collectionDiscount->applied_at = null;
        $collectionDiscount->saveWithoutEvents();


        // Display the alerts in the admin dashboard view.
        Alert::success('Collection discount will be applied again very soon...')->flash();
        return back()->with('alerts', Alert::all());
    }
}
