<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SlideRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class SlideCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class SlideCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Slide::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/slide');
        CRUD::setEntityNameStrings('slide', 'slides');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        #CRUD::setFromDb(); // columns

        $this->crud->addColumns([
            [
                'name' => 'name',
                'type' => 'text',
                'label' => 'Name',
            ],
            [
                'name' => 'carousel_id',
                'type' => 'select',
                'label' => 'Carousel',
                'entity' => 'carousel',
                'model' => 'App\Models\Carousel'
            ],
            [
                'name' => 'file',
                'type' => 'text',
                'label' => 'File',
            ],

        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(SlideRequest::class);

        #CRUD::setFromDb(); // fields

        $this->crud->addFields([
            [
                'name' => 'carousel_id',
                'type' => 'select2',
                'label' => 'Carousel',
                'entity' => 'carousel',
                'model' => 'App\Models\Carousel'
            ],
            [
                'name' => 'file',
                'type' => 'browse',
                'label' => 'File',
            ],
            [
                'name' => 'name',
                'type' => 'text',
                'label' => 'Name',
            ],
            [
                'name' => 'subtitle',
                'type' => 'text',
                'label' => 'Subtitle',
            ],
            [
                'name' => 'short_description',
                'type' => 'textarea',
                'label' => 'Short description',
            ],
            [
                'name' => 'description',
                'type' => 'wysiwyg',
                'label' => 'Description',
            ],
            [
                'name' => 'link',
                'type' => 'text',
                'label' => 'Link',
            ],
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
