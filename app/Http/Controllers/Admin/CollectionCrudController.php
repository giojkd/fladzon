<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CollectionRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class CollectionCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CollectionCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Collection::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/collection');
        CRUD::setEntityNameStrings('collection', 'collections');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
#        CRUD::setFromDb(); // columns
        $this->crud->addColumns([
            [
                'label' => 'Name',
                'name' => 'name',
                'type' => 'text'
            ]
        ]);
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(CollectionRequest::class);

        #CRUD::setFromDb(); // fields

        $this->crud->addFields([
            [
                'label' => 'Name',
                'name' => 'name',
                'type' => 'text'
            ],
            [
                'label' => 'Short description',
                'name' => 'short_description',
                'type' => 'textarea'
            ],
            [
                'label' => 'Description',
                'name' => 'description',
                'type' => 'wysiwyg'
            ],
            [
                'label' => 'Is top collection',
                'name' => 'is_top_collection',
                'type' => 'checkbox'
            ],
            [    // Select2Multiple = n-n relationship (with pivot table)
                'label'     => "Products",
                'type'      => 'select2_multiple',
                'name'      => 'products', // the method that defines the relationship in your Model
                'entity'    => 'products', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model'     => "App\Models\Product", // foreign key model
                'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            ],
            [    // Select2Multiple = n-n relationship (with pivot table)
                'label'     => "Product Groups",
                'type'      => 'select2_multiple',
                'name'      => 'productgroups', // the method that defines the relationship in your Model
                'entity'    => 'productgroups', // the method that defines the relationship in your Model
                'attribute' => 'id', // foreign key attribute that is shown to user
                'model'     => "App\Models\Productgroup", // foreign key model
                'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            ],
            [
                'name' => 'images', // db column name
                'label' => 'Photos', // field caption
                'type' => 'dropzone', // voodoo magic
                'prefix' => '/uploads/product_photos', // upload folder (should match the driver specified in the upload handler defined below)
                'upload-url' => '/media-dropzone', // POST route to handle the individual file uploads
            ]

        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
