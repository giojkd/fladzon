<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ConversionscalevalueRequest;
use App\Models\Attributevalue;
use App\Models\Conversionscale;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ConversionscalevalueCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ConversionscalevalueCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Conversionscalevalue::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/conversionscalevalue');
        CRUD::setEntityNameStrings('conversionscalevalue', 'conversionscalevalues');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        #CRUD::setFromDb(); // columns


        $this->crud->addColumns([

            [
                'label'     => 'Conversion scale',
                'type'      => 'select',
                'name'      => 'conversionscale_id', // the db column for the foreign key
                'entity'    => 'conversionscale', // the method that defines the relationship in your Model
                'attribute' => 'name',
                'model' => Conversion::class
            ],
            [
                'label'     => 'Attribute value',
                'type'      => 'select',
                'name'      => 'attributevalue_id', // the db column for the foreign key
                'entity'    => 'attributevalue', // the method that defines the relationship in your Model
                'attribute' => 'name',
                'model' => Attributevalue::class
            ],
            [
                'name' => 'name',
                'type' => 'text',
                'label' => 'Name'
            ],
            [  // Select2
                'label'     => 'Gender',
                'type'      => 'select',
                'name'      => 'gender_id', // the db column for the foreign key
                'entity'    => 'gender', // the method that defines the relationship in your Model
                'attribute' => 'name',

                #'default' => 1,
                #'options'   => (function ($query) {
                #  return $query->orderBy('name', 'DESC')->get();
                #})
                // foreign key attribute that is shown to user
                // 'wrapperAttributes' => [
                //     'class' => 'form-group col-md-6'
                //   ], // extra HTML attributes for the field wrapper - mostly for resizing fields
                #'tab' => 'Basic Info',
            ]
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ConversionscalevalueRequest::class);

        #CRUD::setFromDb(); // fields

        $this->crud->addFields([

            [
                'label'     => 'Conversion scale',
                'type'      => 'select2',
                'name'      => 'conversionscale_id', // the db column for the foreign key
                'entity'    => 'conversionscale', // the method that defines the relationship in your Model
                'attribute' => 'name',
                'model' => Conversionscale::class
            ],
            [
                'label'     => 'Attribute value',
                'type'      => 'select2',
                'name'      => 'attributevalue_id', // the db column for the foreign key
                'entity'    => 'attributevalue', // the method that defines the relationship in your Model
                'attribute' => 'name',
                'model' => Attributevalue::class
            ],
            [
                'name' => 'name',
                'type' => 'text',
                'label' => 'Name'
            ],

            [  // Select2
                'label'     => 'Gender',
                'type'      => 'select2',
                'name'      => 'gender_id', // the db column for the foreign key
                'entity'    => 'gender', // the method that defines the relationship in your Model
                'attribute' => 'name',

                #'default' => 1,
                #'options'   => (function ($query) {
                #  return $query->orderBy('name', 'DESC')->get();
                #})
                // foreign key attribute that is shown to user
                // 'wrapperAttributes' => [
                //     'class' => 'form-group col-md-6'
                //   ], // extra HTML attributes for the field wrapper - mostly for resizing fields
                #'tab' => 'Basic Info',
            ]
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
