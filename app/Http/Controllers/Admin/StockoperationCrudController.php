<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StockoperationRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class StockoperationCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class StockoperationCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    #use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Stockoperation::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/stockoperation');
        CRUD::setEntityNameStrings('stockoperation', 'stockoperations');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        #CRUD::setFromDb(); // columns

        $this->crud->addColumns([
            [
                'label' => 'ID',
                'name' => 'id',
                'type' => 'text',
            ],
            [  // Select2
                'label'     => 'Prodotto',
                'type'      => 'select',
                'name'      => 'product_id', // the db column for the foreign key
                'entity'    => 'product', // the method that defines the relationship in your Model
                'attribute' => 'name_with_identifiers',
                #'default' => 1,
                #'options'   => (function ($query) {
                #  return $query->orderBy('name', 'DESC')->get();
                #})
                // foreign key attribute that is shown to user
                // 'wrapperAttributes' => [
                //     'class' => 'form-group col-md-6'
                //   ], // extra HTML attributes for the field wrapper - mostly for resizing fields
                #'tab' => 'Basic Info',
            ],
            [  // Select2
                'label'     => 'Warehouse',
                'type'      => 'select',
                'name'      => 'warehouse_id', // the db column for the foreign key
                'entity'    => 'warehouse', // the method that defines the relationship in your Model
                'attribute' => 'name',
                #'default' => 1,
                #'options'   => (function ($query) {
                #  return $query->orderBy('name', 'DESC')->get();
                #})
                // foreign key attribute that is shown to user
                // 'wrapperAttributes' => [
                //     'class' => 'form-group col-md-6'
                //   ], // extra HTML attributes for the field wrapper - mostly for resizing fields
                #'tab' => 'Basic Info',
            ],
            [
                'label' => 'Quantity',
                'name' => 'quantity',
                'type' => 'number',
                'default' => 1,
                'attributes' => [
                    'min' => 1
                ]
            ],
            [
                'label' => 'Operation',
                'name' => 'sign',
                'type' => 'select_from_array',
                'options' => [1 => 'Carica', '-1' => 'Scarica'],
                'default' => 1
            ],

        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(StockoperationRequest::class);

        #CRUD::setFromDb(); // fields

        $this->crud->addFields([

            [  // Select2
                'label'     => 'Prodotto',
                'type'      => 'select2',
                'name'      => 'product_id', // the db column for the foreign key
                'entity'    => 'product', // the method that defines the relationship in your Model
                'attribute' => 'name_with_identifiers',
                #'default' => 1,
                #'options'   => (function ($query) {
                #  return $query->orderBy('name', 'DESC')->get();
                #})
                // foreign key attribute that is shown to user
                // 'wrapperAttributes' => [
                //     'class' => 'form-group col-md-6'
                //   ], // extra HTML attributes for the field wrapper - mostly for resizing fields
                #'tab' => 'Basic Info',
                ],
            [  // Select2
                'label'     => 'Warehouse',
                'type'      => 'select2',
                'name'      => 'warehouse_id', // the db column for the foreign key
                'entity'    => 'warehouse', // the method that defines the relationship in your Model
                'attribute' => 'name',
                #'default' => 1,
                #'options'   => (function ($query) {
                #  return $query->orderBy('name', 'DESC')->get();
                #})
                // foreign key attribute that is shown to user
                // 'wrapperAttributes' => [
                //     'class' => 'form-group col-md-6'
                //   ], // extra HTML attributes for the field wrapper - mostly for resizing fields
                #'tab' => 'Basic Info',
            ],
                [
                    'label' => 'Quantity',
                    'name' => 'quantity',
                    'type' => 'number',
                    'default' => 1,
                    'attributes' => [
                        'min' => 1
                    ]
                ],
                [
                    'label' => 'Operation',
                    'name' => 'sign',
                    'type' => 'select_from_array',
                    'options' => [1=>'Carica','-1'=>'Scarica'],
                    'default' => 1
                ],
            [
                'label' => 'Description',
                'name' => 'description',
                'type' => 'text',

            ],

        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
