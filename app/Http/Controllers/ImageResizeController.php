<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Image;

class ImageResizeController extends Controller
{
    public function thumbs($filename)
    {
        $file = storage_path('app/public/' . $filename);

        $img = Image::make($file)
            ->resize(120, 120);
        return $img->response('jpg');
    }

    public function resize($size = null, $filename)
    {

        #$filename = basename($filename);
        $file = public_path( $filename);



        switch ($size[0]) {
            case 'h':
                $size = substr($size, 1);

                $img =  Image::cache(function($image) use ($file,$size){
                    $image->make($file)->resize(null, $size, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }, 14400, true);



                break;
            case 'w':
                $size = substr($size, 1);

                $img =  Image::cache(function ($image) use ($file, $size) {
                    $image->make($file)->resize( $size,null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }, 14400, true);

                break;
            default:

                $size = explode('x', $size);

                if (count($size) > 1) {
                    $img = Image::make($file)
                        ->resize($size[0], $size[1]);
                }

                if (count($size) == 1) {
                    $size[0] = ($size[0] != '') ? $size[0] : 120;
                    $img = Image::make($file)->resize($size[0], null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }

                break;
        }




        return $img->response('jpg');
    }
}
