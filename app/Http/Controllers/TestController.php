<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use Illuminate\Support\Facades\Storage;
use Str;
use App\Helpers\Helper;
use App\Http\Controllers\Admin\Charts\LastWeekSalesChartController;
use App\Models\Googlecategory;
use App\Models\Guest;
use App\Models\Lead;
use App\Models\Productgroup;
use App\Models\Productimport;
use App\Models\Stockoperation;
use App\Models\Shipping;
use App\User;
use Recombee\RecommApi\Client;
use Recombee\RecommApi\Requests as Reqs;
use Recombee\RecommApi\Exceptions as Ex;
use League\Csv\Reader;
use Laravel\Socialite\Facades\Socialite;


class TestController extends Controller
{
    //
    public function index(Request $request){

        $lead = Lead::find(101);
        $lead->calculateTotals();
        return $lead;


    }
}
