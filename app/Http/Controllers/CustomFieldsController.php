<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CustomFieldsController extends Controller
{
    //
    public function toggleBoolean(Request $request){

        $request->validate([
            'model' => 'required',
            'column' => 'required',
            'id' => 'required|numeric',
            'value' => 'boolean'
        ]);

        $model = $request->model;
        $column = $request->column;

        $entry = $model::find($request->id);
        $entry->$column = $request->value;
        $entry->save();

    }
}
