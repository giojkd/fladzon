<?php

use Illuminate\Support\Facades\Route;
use League\Csv\Reader;

$file = public_path(env('REDIRECT_FILE'));

$csv = Reader::createFromPath($file, 'r');
$csv->setDelimiter(';');

$redirects = $csv->getRecords();

foreach($redirects as $redirect){

    $from = $redirect[0];
    $to = $redirect[1];

    if($to != ''){
        Route::redirect($from, $to, 301);
    }

}
