<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('calculate-cart-shipping-cost', 'FrontController@calculateCartShippingCost');

Route::get('/scalapay/success', 'FrontController@scalapayRedirectSuccess')->name('scalapayRedirectSuccess');
Route::get('/scalapay/fail', 'FrontController@scalapayRedirectFail')->name('scalapayRedirectFail');

Route::get('/ideal', 'FrontController@idealRedirect')->name('idealRedirect');


Route::get('/donut/flip', 'FrontController@donut')->name('donut');


Route::get('artisan/import-productgroups', 'FrontController@scoutImportProductgroups');

Route::get('download-xlsx-with-product-translations', 'TranslationsController@downloadXlsxWithProductTranslations');

Route::get('/debug-sentry', function () {
    throw new Exception('My first Sentry error!');
});


Route::get('/', 'FrontController@mainRedirect');


Route::get('/thumbs/dropzone/{filename}', 'ImageResizeController@thumbs')->where(["filename" => ".*"]);

Route::get('/ir/{size?}/{filename}', 'ImageResizeController@resize')->where(["filename" => ".*"])->name('ir');

Route::get('search-results','FrontController@searchResults');
Route::get('{locale}/pg','FrontController@productGrid')->name('productGrid');

Route::get('{locale}/p/{name}/{id}','FrontController@productPage')->name('productPage');
Route::get('{locale}/a/{name}/{id}', 'FrontController@articlePage')->name('articlePage');
Route::get('{locale}/shops', 'FrontController@shops')->name('shops');
Route::get('{locale}/about-us', 'FrontController@aboutUs')->name('aboutUs');

Route::get('/test','TestController@index');

Route::get('add-to-cart', 'FrontController@addToCart')->name('addToCart');
Route::post('remove-from-cart', 'FrontController@removeFromCart')->name('removeFromCart');

Route::get('{locale}/add-discount-code/{code?}', 'FrontController@addDiscountCode')->name('addDiscountCode');
Route::post('remove-discount-code', 'FrontController@removeDiscountCode')->name('removeDiscountCode');

Route::get('{locale}/cart', 'FrontController@cart')->name('cart');

Route::get('{locale}/thank-you-page-ecommerce-cart/{lead_id}/{value}', 'FrontController@thankYouPageEcommerceCart')->name('thankYouPageEcommerceCart');
Route::get('{locale}/payment-failed/', 'FrontController@paymentFailed')->name('paymentFailed');

Route::post('confirm-cart', 'FrontController@confirmCart')->name('confirmCart');

Route::post('subscribe-newsletter', 'FrontController@subscribeNewsletter')->name('subscribeNewsletter');
Route::get('{locale}/thank-you-page-newsletter-subscription', 'FrontController@thankYouPageNewsletterSubscription')->name('thankYouPageNewsletterSubscription');

Route::get('{locale?}', 'FrontController@home')->name('home');

Route::get('login/facebook', 'FrontController@FacebookLoginRedirectToProvider')->name('FacebookLoginRedirectToProvider');
Route::get('login/facebook/callback', 'FrontController@FacebookLoginCallbackHandler')->name('FacebookLoginCallbackHandler');


Route::get('admin/shippings-manager/{id}','ShippingsManager@show')->name('shippingsManager');
Route::post('admin/shippings-manager/','ShippingsManager@store')->name('shippingsManagerStore');

Route::post('media-dropzone', 'DropzoneController@handleDropzoneUpload');

Route::post('toggle-boolean','CustomFieldsController@toggleBoolean')->name('toggleBoolean');

Route::get('related-products-to-product/{product_id}/{user_id}/{how_many_products}', 'FrontController@relatedProductsToProduct')->name('relatedProductsToProduct');

Route::post('/functions/cache-cart-field', 'FrontController@cacheCartfield')->name('cacheCartField');

Route::post('/functions/track-event', 'FrontController@trackEvent')->name('trackEvent');

#Route::post('ping-lead-validity', 'FrontController@checkLeadValidiy')->name('checkLeadValidiy');
Route::post('ping-lead-validity', function(){return 1;})->name('checkLeadValidiy');



