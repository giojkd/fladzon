<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => [
        config('backpack.base.web_middleware', 'web'),
        #config('backpack.base.middleware_key', 'admin'),
    ],
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes

    Route::post('media-dropzone', 'ProductCrudController@handleDropzoneUpload');

    Route::crud('attribute', 'AttributeCrudController');
    Route::crud('attributevalue', 'AttributevalueCrudController');
    Route::crud('category', 'CategoryCrudController');
    Route::crud('product', 'ProductCrudController');
    Route::crud('order', 'OrderCrudController');
    Route::crud('manufacturer', 'ManufacturerCrudController');
    Route::crud('productgroup', 'ProductgroupCrudController');
    Route::crud('user', 'UserCrudController');
    Route::crud('review', 'ReviewCrudController');
    Route::crud('lead', 'LeadCrudController');
    Route::crud('leadstatus', 'LeadstatusCrudController');
    Route::crud('paymentmethod', 'PaymentmethodCrudController');
    Route::crud('article', 'ArticleCrudController');
    Route::crud('menu', 'MenuCrudController');
    Route::crud('menuitem', 'MenuitemCrudController');
    Route::crud('carousel', 'CarouselCrudController');
    Route::crud('slide', 'SlideCrudController');
    Route::crud('newslettersubscription', 'NewslettersubscriptionCrudController');
    Route::crud('shippingrule', 'ShippingruleCrudController');
    Route::crud('discountcode', 'DiscountcodeCrudController');
    Route::crud('dump', 'DumpCrudController');
    Route::crud('googlecategory', 'GooglecategoryCrudController');
    Route::crud('warehouse', 'WarehouseCrudController');
    Route::crud('stockoperation', 'StockoperationCrudController');
    Route::crud('attributevalueextensiongroup', 'AttributevalueextensiongroupCrudController');
    Route::crud('attributevalueextensiongrouprow', 'AttributevalueextensiongrouprowCrudController');
    Route::crud('shipping', 'ShippingCrudController');
    Route::crud('shippingrow', 'ShippingrowCrudController');
    Route::crud('leadproduct', 'LeadproductCrudController');
    Route::crud('warehouseavailability', 'WarehouseavailabilityCrudController');
    Route::crud('collection', 'CollectionCrudController');
    Route::crud('conversionscale', 'ConversionscaleCrudController');
    Route::crud('conversionscalevalue', 'ConversionscalevalueCrudController');

    Route::get('inventory','\App\Http\Controllers\InventoryController@index')->name('inventory');
    Route::post('inventory', '\App\Http\Controllers\InventoryController@update')->name('inventoryUpdate');

    Route::crud('gender', 'GenderCrudController');

    Route::get('print-order/{id}','LeadCrudController@printOrder')->name('printOrder');
    Route::crud('collectiondiscount', 'CollectiondiscountCrudController');

    Route::get('collectiondiscount/reset/{id}', 'CollectiondiscountCrudController@resetCollectionDiscount')->name('resetCollectionDiscount');
    Route::crud('productimport', 'ProductimportCrudController');
    Route::crud('guest', 'GuestCrudController');
    Route::crud('event', 'EventCrudController');
    Route::crud('webhook', 'WebhookCrudController');
    Route::get('charts/weekly-users', 'Charts\WeeklyUsersChartController@response')->name('charts.weekly-users.index');
    Route::get('charts/last-week-sales', 'Charts\LastWeekSalesChartController@response')->name('charts.last-week-sales.index');
    Route::crud('metatag', 'MetatagCrudController');
    Route::crud('countriesgroup', 'CountriesgroupCrudController');
}); // this should be the absolute last line of this file