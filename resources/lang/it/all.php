<?php

return array (
  'shop now' => 'Acquista ora',
  'new arrivals' => 'Nuovi arrivi',
  'latest from the blog' => 'Ultime dal blog',
  'stay up to date on our products' => 'Rimani aggiornato sui nostri prodotti',
  'sign up' => 'Registrati',
  'featured products' => 'Prodotti in vetrina',
  'search here' => 'Cerca qui',
  'category' => 'Categoria',
  'color' => 'Colore',
  'price range' => 'Fascia di prezzo',
  'sizes' => 'Taglie',
  'code' => 'Codice',
  'outlet' => 'Outlet',
  'intro newsletter' => 'Iscriviti alla newsletter e ricevi un sconto del 10% sulla nuova collezione',
  'on deal' => 'Outlet',
  'free shipping' => 'Spedizione gratuita',
  'sku' => 'Sku',
  'Add To Cart' => 'Aggiungi al carrello',
  'description' => 'Descrizione',
  'discounts' => 'Sconto',
  'VIEW ALL PRODUCTS' => 'GUARDA TUTTI I PRODOTTI',
  'start searching here' => 'Inizia qui la tua ricerca',
  'REQUEST INFO' => 'RICHIEDI INFORMAZIONI',
  'access with facebook' => 'Compila con Facebook',
  'address' => 'Indirizzo',
  'billing address' => 'Indirizzo di fatturazione',
  'cash on delivery' => 'Pagamento alla consegna',
  'company name' => 'Ragione sociale',
  'confirm and pay' => 'Conferma e paga',
  'confirm the order' => 'Conferma l\'ordine',
  'credit card' => 'Carta di credito',
  'do you need and invoice for this order' => 'Vuoi richiedere una fattura per questo ordine',
  'do you want to ship this order to a different address than the billing one' => 'Vuoi spedire ad un indirizzo diverso da quello di fatturazione',
  'email' => 'Email',
  'fiscal code' => 'Codice fiscale',
  'home' => 'Home',
  'name' => 'Nome',
  'none' => 'Nessuno',
  'notes' => 'Note',
  'notes placeholder' => 'Eventuali note sulla consegna, piano, etc..',
  'order summary' => 'Riassunto dell\'ordine',
  'pay with' => 'Paga con',
  'pec' => 'PEC',
  'phone' => 'Telefono',
  'products' => 'Prodotti',
  'remove' => 'Rimuovi',
  'sdi code' => 'Codice SDI',
  'shipping' => 'Spedizione',
  'total' => 'Totale',
  'vat number' => 'Partita IVA',
  'would you like to create an account' => 'Vuoi creare un account',
  'surname' => 'Cognome',
  'subtotal' => 'Totale parziale',
  'write a password' => 'Scrivi una password',
  'SEARCH FOR' => 'RICERCA PER',
  'add to cart' => 'Aggiungi al Carrello',
  'request info' => 'Richiedi informazioni',
  'view all products' => 'GUARDA TUTTI I PRODOTTI',
  'search for' => 'Ricerca per',
  'thank you for your order!' => 'Grazie per il tuo ordine',
  'thank your for your order extended text' => 'La ringraziamo per aver scelto di acquistare i nostri prodotti, ci auguriamo che l\'alta qualità degli stessi la riconduca al nostro sito presto.',
  'thank you for your order' => 'Grazie per il tuo ordine',
  'continue shopping' => 'Continua lo shopping',
  'product is not purchaseable' => 'Prodotto attualmente non disponibile',
  'continue' => 'Continua',
  'welcome to harris' => 'Benvenuti in Madè Firenze',
  'welcome to Harris' => 'Benvenuti in Madè Firenze',
  'our team' => 'Il nostro Team',
  'shops' => 'I negozi',
  'shops intro after logo' => 'Tocca con mano i nostri prodotti',
  'i am interested into' => 'Mi interessano queste calzature',
  'show filters' => 'Mostra i filtri',
  'clean refinements' => 'Rimuovi filtri',
  'collections' => 'Collezioni',
  'welcome to' => 'Benvenuti in',
  'i am interested into :name' => 'Mi interessano queste calzature',
  'thank you for your subscription' => 'Grazie per la tua iscrizione',
  'newsletter subscription confirmation' => 'Ti confermiamo la tua iscrizione alla newsletter Madè Firenze',
  'you might also like' => 'Potrebbero interessarti anche',
  'attributes' => 'Attributi',
  'available also in' => 'Disponibile anche in',
  'newsletter discount' => 'Grazie per la sua registrazione, usi il codice NEWSS21 per ottenere uno sconto del 10% sulla nuova collezione',
  'shipped in' => 'Spedito in',
  'newsletter subscription cart checkbox label' => 'Voglio iscrivermi alla newsletter',
  'all' => 
  array (
    'continue shopping, your cart is still empty' => 'Continua lo shopping, il tuo carrello è vuoto',
  ),
  'cash on delivery cart hint' => 'Inserisci il tuo indirizzo in Italia per pagare in contrassegno',
  'continue shopping, your cart is still empty' => 'Continua lo shopping, il tuo carrello è vuoto',
  'banner content' => 'Spedizione Gratuita',
  'help' => 'Help',
  'bags' => 'Borse',
  'get in touch with us fill the form' => 'Mettiti in contatto con noi  compilando il form di seguito',
  'OPEN COOKIE PREFERENCES' => 'Apri preferenze cookie',
  'claim first line' => 'Non serve un bagaglio per viaggiare, ma avere con se’ un BELLA borsa',
  'claim second line' => 'Cit. Ilaria Iemmi',
  'accept privacy' => 'Accetto la privacy policy',
);
