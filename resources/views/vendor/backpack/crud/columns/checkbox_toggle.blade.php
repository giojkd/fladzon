@php
    $name = $column['name'];
@endphp
<span>
    <input type="checkbox"

    		class="checkbox-toggle"

            data-model="{{ $entry->getModelName() }}"
            data-column="{{ $column['name'] }}"
    		data-id="{{ $entry->getKey() }}"

    		style="width: 16px; height: 16px;"

            onchange="toggleColumnsCheckbox(this)"

            @if ($entry->$name)
                checked
            @endif

    		>


</span>
