<a href="{{ Route('shippingsManager',['id' => $entry->id]) }}">
    @if(!is_null($entry->shippings_prepared_at))
        <i class="fa fa-check"></i>
    @endif
    <i class="fa fa-truck"></i>
</a>
