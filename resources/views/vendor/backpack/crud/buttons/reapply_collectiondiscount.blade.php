
    @if (!is_null($entry->applied_at))
        Applied at {{ date('d/m/Y H:i',strtotime($entry->applied_at)) }}
        <a href="{{ Route('resetCollectionDiscount',['id' => $entry->id]) }}" class="bnt btn-sm btn-primary">Reapply now</a>
    @else
        Application pending...
    @endif


