@role('Superadmin')
<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>


<li class='nav-item'><a class='nav-link' href='{{ backpack_url('lead') }}'><i class='nav-icon  fa fa-shopping-cart'></i> Ordini</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('leadproduct') }}'><i class='nav-icon  fa fa-shopping-cart'></i> Righe degli Ordini</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('shipping') }}'><i class='nav-icon  fa fa-truck'></i> Shippings</a></li>

<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon  fa fa-list"></i> Contents</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('article') }}'><i class='nav-icon la la-blog'></i> Articles</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('menu') }}'><i class='nav-icon la la-bars'></i> Menus</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('menuitem') }}'><i class='nav-icon la la-bars'></i> Menu items</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('carousel') }}'><i class='nav-icon la la-slideshare'></i> Carousels</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('slide') }}'><i class='nav-icon la la-slideshare'></i> Slides</a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('elfinder') }}\"><i class="nav-icon la la-files-o"></i> <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>
    </ul>
</li>

<li class="nav-item nav-dropdown">
	<a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon  fa fa-book"></i> Catalog</a>
    <ul class="nav-dropdown-items">

        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('productgroup') }}'><i class='nav-icon fa fa-list'></i> Product groups</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('product') }}'><i class='nav-icon la la-question'></i> Products</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('attribute') }}'><i class='nav-icon fas fa-swatchbook'></i> Attributes</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('attributevalue') }}'><i class='nav-icon la la-question'></i> Attribute values</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('attributevalueextensiongroup') }}'><i class='nav-icon la la-question'></i> Attribute value extension groups</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('attributevalueextensiongrouprow') }}'><i class='nav-icon la la-question'></i> Attribute value extension group rows</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('category') }}'><i class='nav-icon fa fa-sitemap'></i> Categories</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('googlecategory') }}'><i class='nav-icon la la-question'></i> Google categories</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ Route('inventory') }}'><i class='nav-icon la la-question'></i> Inventario</a></li>

        {{-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('order') }}'><i class='nav-icon la la-question'></i> Orders</a></li>  --}}
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('manufacturer') }}'><i class='nav-icon la la-question'></i> Manufacturers</a></li>


        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('warehouse') }}'><i class='nav-icon fa fa-warehouse'></i> Warehouses</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('stockoperation') }}'><i class='nav-icon la la-question'></i> Stock operations</a></li>
            <!--<li class='nav-item'><a class='nav-link' href='{{ backpack_url('leadproduct') }}'><i class='nav-icon la la-question'></i> Leadproducts</a></li>-->
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('warehouseavailability') }}'><i class='fa fa-boxes'></i> Warehouse availabilities</a></li>

        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('collection') }}'><i class='nav-icon la la-question'></i> Collections</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('conversionscale') }}'><i class='nav-icon la la-question'></i> Conversion scales</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('conversionscalevalue') }}'><i class='nav-icon la la-question'></i> Conversion scale values</a></li>

        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('productimport') }}'><i class='nav-icon la la-question'></i> Product imports</a></li>
    </ul>
</li>




<li class="nav-item nav-dropdown">
	<a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-dollar-sign"></i> eCommerce</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('lead') }}'><i class='nav-icon fa fa-shopping-cart'></i> Ordini</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('shippingrule') }}'><i class='nav-icon fa fa-truck'></i> Shipping rules</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('discountcode') }}'><i class='nav-icon fa fa-percentage'></i> Discount codes</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('collectiondiscount') }}'><i class='nav-icon fa fa-percentage'></i> Collection discounts</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('review') }}'><i class='nav-icon fa fa-star'></i> Reviews</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('newslettersubscription') }}'><i class='nav-icon fa fa-envelope'></i> Newsletter subscriptions</a></li>
    </ul>
</li>

<li class="nav-item nav-dropdown">
	<a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-users"></i> Authentication</a>
	<ul class="nav-dropdown-items">
	  <li class="nav-item"><a class="nav-link" href="{{ backpack_url('user') }}"><i class="nav-icon la la-user"></i> <span>Users</span></a></li>
	  <li class="nav-item"><a class="nav-link" href="{{ backpack_url('role') }}"><i class="nav-icon la la-id-badge"></i> <span>Roles</span></a></li>
	  <li class="nav-item"><a class="nav-link" href="{{ backpack_url('permission') }}"><i class="nav-icon la la-key"></i> <span>Permissions</span></a></li>
	</ul>
</li>

<li class="nav-item nav-dropdown">
	<a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-gear"></i> Settings</a>
	<ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('countriesgroup') }}'><i class='nav-icon la la-globe'></i> Countries Groups</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('setting') }}'><i class='nav-icon la la-cog'></i> <span>System settings</span></a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('leadstatus') }}'><i class='nav-icon la la-question'></i> Lead statuses</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('paymentmethod') }}'><i class='nav-icon fa fa-money-bill-wave'></i> Payment methods</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('dump') }}'><i class='nav-icon la la-question'></i> Dumps</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('gender') }}'><i class='nav-icon la la-question'></i> Genders</a></li>
    </ul>
</li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('metatag') }}'><i class='nav-icon nav-icon la la-blog'></i> Meta Tags</a></li>
@endrole





@role('Warehouse Manager')
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('lead') }}'><i class="fas fa-receipt"></i> Scontrini</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('shipping') }}'><i class='nav-icon  fa fa-truck'></i> Shippings</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ Route('inventory') }}'><i class="fas fa-boxes"></i> Inventario</a></li>
@endrole





@role('Orders and shippings')
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('lead') }}'><i class='nav-icon  fa fa-shopping-cart'></i> Ordini</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('shipping') }}'><i class='nav-icon  fa fa-truck'></i> Shippings</a></li>
@endrole

<li class='nav-item'><a class='nav-link' href='{{ backpack_url('event') }}'><i class='nav-icon la la-search'></i> Events</a></li>
{{--  <li class='nav-item'><a class='nav-link' href='{{ backpack_url('webhook') }}'><i class='nav-icon la la-question'></i> Webhooks</a></li> --}}


