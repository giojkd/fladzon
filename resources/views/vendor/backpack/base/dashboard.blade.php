@extends(backpack_view('blank'))

@php
    $widgets['before_content'][] = [
            'type'       => 'chart',
            'to' => 'before_content',
            'controller' => \App\Http\Controllers\Admin\Charts\LastWeekSalesChartController::class,
            'wrapper' => ['class' => 'col-md-12']
        ];
@endphp

@section('content')
  <!--<p>Your custom HTML can live here</p>-->
@endsection
