@include(env('THEME_NAME').'.notifications.include.header')
    <!-- hero-dark-button -->
    <table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
      <tbody>
        <tr>
          <td class="o_bg-dark o_px-md o_py-xl o_xs-py-md" align="center" style="background-color: {{env('HIGHLIGHT_COLOR')}};padding-left: 24px;padding-right: 24px;padding-top: 64px;padding-bottom: 64px;">
            <!--[if mso]><table width="584" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td align="center"><![endif]-->
            <div class="o_col-6s o_sans o_text-md o_text-white o_center" style="font-family: Helvetica, Arial, sans-serif;margin-top: 0px;margin-bottom: 0px;font-size: 19px;line-height: 28px;max-width: 584px;color: #ffffff;text-align: center;">
              <h2 class="o_heading o_mb-xxs" style="font-family: Helvetica, Arial, sans-serif;font-weight: bold;margin-top: 0px;margin-bottom: 4px;font-size: 30px;line-height: 39px;">Confermiamo il tuo ordine</h2>
              <p class="o_mb-md" style="margin-top: 0px;margin-bottom: 24px;">Ciao {{ $user_full_name }}, confermiamo il tuo ordine con numero {{ $order_id }}</p>
              <!--<table align="center" cellspacing="0" cellpadding="0" border="0" role="presentation">
                <tbody>
                  <tr>
                    <td width="300" class="o_btn o_bg-primary o_br o_heading o_text" align="center" style="font-family: Helvetica, Arial, sans-serif;font-weight: bold;margin-top: 0px;margin-bottom: 0px;font-size: 16px;line-height: 24px;mso-padding-alt: 12px 24px;background-color: #333;border-radius: 4px;">
                      <a class="o_text-white" href="https://www.jinnone.com/it/area-utente?tab=ordersTab#order_{{ $order_id }}" style="text-decoration: none;outline: none;color: #ffffff;display: block;padding: 12px 24px;mso-text-raise: 3px;">Guarda l'ordine</a>
                    </td>
                  </tr>
                </tbody>
              </table>
            -->
            </div>
            <!--[if mso]></td></tr></table><![endif]-->
          </td>
        </tr>
      </tbody>
    </table>
    <!-- spacer -->
    <table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
      <tbody>
        <tr>
          <td class="o_bg-white" style="font-size: 24px;line-height: 24px;height: 24px;background-color: #ffffff;">&nbsp; </td>
        </tr>
      </tbody>
    </table>
    <!-- order-summary -->
    <table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
      <tbody>
        <tr>
          <td class="o_bg-white o_sans o_text-xs o_text-light o_px-md o_pt-xs" align="center" style="font-family: Helvetica, Arial, sans-serif;margin-top: 0px;margin-bottom: 0px;font-size: 14px;line-height: 21px;background-color: #ffffff;color: #82899a;padding-left: 24px;padding-right: 24px;padding-top: 8px;">
            <p style="margin-top: 0px;margin-bottom: 0px;">Riepilogo dell'ordine</p>
            <table cellspacing="0" cellpadding="0" border="0" role="presentation">
              <tbody>
                <tr>
                  <td width="584" class="o_re o_bb-light" style="font-size: 8px;line-height: 8px;height: 8px;vertical-align: top;border-bottom: 1px solid #d3dce0;">&nbsp; </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table>
    <!-- product-plain-lg -->
    @foreach($products as $product)
        @if($product->pivot->quantity > 0)
        <table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
        <tbody>
            <tr>
            <td class="o_re o_bg-white o_px o_pt" align="center" style="font-size: 0;vertical-align: top;background-color: #ffffff;padding-left: 16px;padding-right: 16px;padding-top: 16px;">
                <!--[if mso]><table cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td width="200" align="center" valign="top" style="padding: 0px 8px;"><![endif]-->
                <div class="o_col o_col-2" style="display: inline-block;vertical-align: top;width: 100%;max-width: 200px;">
                <div class="o_px-xs o_sans o_text o_center" style="font-family: Helvetica, Arial, sans-serif;margin-top: 0px;margin-bottom: 0px;font-size: 16px;line-height: 24px;text-align: center;padding-left: 8px;padding-right: 8px;">
                    <p style="margin-top: 0px;margin-bottom: 0px;"><a class="o_text-primary" href="https://www.jinnon.com/{{ $product->makeUrl() }}" style="text-decoration: none;outline: none;color: {{env('HIGHLIGHT_COLOR')}};"><img src="{{ $product->cover() }}" width="184" height="184" alt="" style="max-width: 184px;-ms-interpolation-mode: bicubic;vertical-align: middle;border: 0;line-height: 100%;height: auto;outline: none;text-decoration: none;"></a></p>
                </div>
                </div>
                <!--[if mso]></td><td width="400" align="left" valign="top" style="padding: 0px 8px;"><![endif]-->
                <div class="o_col o_col-4" style="display: inline-block;vertical-align: top;width: 100%;max-width: 400px;">
                <div style="font-size: 24px; line-height: 24px; height: 24px;">&nbsp; </div>
                <div class="o_px-xs o_sans o_text o_text-light o_left o_xs-center" style="font-family: Helvetica, Arial, sans-serif;margin-top: 0px;margin-bottom: 0px;font-size: 16px;line-height: 24px;color: #82899a;text-align: left;padding-left: 8px;padding-right: 8px;">
                    <h4 class="o_heading o_text-dark o_mb-xxs" style="font-family: Helvetica, Arial, sans-serif;font-weight: bold;margin-top: 0px;margin-bottom: 4px;color: #242b3d;font-size: 18px;line-height: 23px;">{{ $product->name }}</h4>
                    <p class="o_text-secondary o_mb-xs" style="color: #424651;margin-top: 0px;margin-bottom: 8px;">{{ $product->short_description }}</p>
                    <p class="o_text-xs o_mb" style="font-size: 14px;line-height: 21px;margin-top: 0px;margin-bottom: 16px;">
                        @foreach($product->attributevalues as $av)
                            @if($av->name != '')
                                {{ $av->attribute->name }}: {{ $av->name }}<br>
                            @endif
                        @endforeach
                    </p>
                    <p class="o_text-md o_text-primary" style="font-size: 19px;line-height: 28px;color: {{env('HIGHLIGHT_COLOR')}};margin-top: 0px;margin-bottom: 0px;"><strong>@fp($product->pivot->sub_total) ({{ $product->pivot->quantity }} x @fp($product->pivot->sub_total/$product->pivot->quantity))</strong></p>
                </div>
                </div>
                <!--[if mso]></td></tr><tr><td colspan="2" style="padding: 0px 8px;"><![endif]-->
                <div class="o_px-xs" style="padding-left: 8px;padding-right: 8px;">
                <table cellspacing="0" cellpadding="0" border="0" role="presentation">
                    <tbody>
                    <tr>
                        <td width="584" class="o_re o_bb-light" style="font-size: 16px;line-height: 16px;height: 16px;vertical-align: top;border-bottom: 1px solid #d3dce0;">&nbsp; </td>
                    </tr>
                    </tbody>
                </table>
                </div>
                <!--[if mso]></td></tr></table><![endif]-->
            </td>
            </tr>
        </tbody>
        </table>
        @endif
    @endforeach
    <!-- invoice-total-alt -->
    <table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
      <tbody>
        <tr>
          <td class="o_re o_bg-white o_px-md o_py" align="center" style="font-size: 0;vertical-align: top;background-color: #ffffff;padding-left: 24px;padding-right: 24px;padding-top: 16px;padding-bottom: 16px;">
            <!--[if mso]><table width="584" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td align="right"><![endif]-->
            <div class="o_col-6s o_right" style="max-width: 584px;text-align: right;">
              <table class="o_right" role="presentation" cellspacing="0" cellpadding="0" border="0" style="text-align: right;margin-left: auto;margin-right: 0;">
                <tbody>
                  <tr>
                    <td width="284" align="left">
                      <table width="100%" role="presentation" cellspacing="0" cellpadding="0" border="0">
                        <tbody>
                          <tr>
                            <td width="50%" class="o_pt-xs o_px" align="left" style="padding-left: 16px;padding-right: 16px;padding-top: 8px;">
                              <p class="o_sans o_text o_text-secondary" style="font-family: Helvetica, Arial, sans-serif;margin-top: 0px;margin-bottom: 0px;font-size: 16px;line-height: 24px;color: #424651;">Subtotale</p>
                            </td>
                            <td width="50%" class="o_pt-xs o_px" align="right" style="padding-left: 16px;padding-right: 16px;padding-top: 8px;">
                              <p class="o_sans o_text o_text-secondary" style="font-family: Helvetica, Arial, sans-serif;margin-top: 0px;margin-bottom: 0px;font-size: 16px;line-height: 24px;color: #424651;">@fp($lead->product_total)</p>
                            </td>
                          </tr>
                          <tr>
                            <td width="50%" class="o_pt-xs o_px" align="left" style="padding-left: 16px;padding-right: 16px;padding-top: 8px;">
                              <p class="o_sans o_text o_text-secondary" style="font-family: Helvetica, Arial, sans-serif;margin-top: 0px;margin-bottom: 0px;font-size: 16px;line-height: 24px;color: #424651;">Costi di spedizione</p>
                            </td>
                            <td width="50%" class="o_pt-xs o_px" align="right" style="padding-left: 16px;padding-right: 16px;padding-top: 8px;">
                              <p class="o_sans o_text o_text-secondary" style="font-family: Helvetica, Arial, sans-serif;margin-top: 0px;margin-bottom: 0px;font-size: 16px;line-height: 24px;color: #424651;">@if($lead->shipping_total > 0)  @fp($lead->shipping_total) @else Spedizione gratuita @endif</p>
                            </td>
                          </tr>
                          <tr>
                            <td class="o_pt" style="padding-top: 16px;">&nbsp; </td>
                            <td class="o_pt" style="padding-top: 16px;">&nbsp; </td>
                          </tr>
                          <tr>
                            <td width="50%" class="o_py o_px o_bg-ultra_light o_br-l" align="left" style="background-color: #ececec;border-radius: 4px 0px 0px 4px;padding-left: 16px;padding-right: 16px;padding-top: 16px;padding-bottom: 16px;">
                              <p class="o_sans o_text o_text-secondary" style="font-family: Helvetica, Arial, sans-serif;margin-top: 0px;margin-bottom: 0px;font-size: 16px;line-height: 24px;color: #424651;"><strong>Totale</strong></p>
                            </td>
                            <td width="50%" class="o_py o_px o_bg-ultra_light o_br-r" align="right" style="background-color: #ececec;border-radius: 0px 4px 4px 0px;padding-left: 16px;padding-right: 16px;padding-top: 16px;padding-bottom: 16px;">
                              <p class="o_sans o_text o_text-primary" style="font-family: Helvetica, Arial, sans-serif;margin-top: 0px;margin-bottom: 0px;font-size: 16px;line-height: 24px;color: {{env('HIGHLIGHT_COLOR')}};"><strong>@fp($lead->grand_total)</strong></p>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <!--[if mso]></td></tr></table><![endif]-->
          </td>
        </tr>
      </tbody>
    </table>
    <!-- customer-details-alt -->
    <table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
      <tbody>
        <tr>
          <td class="o_re o_bg-white o_px o_pb-md" align="center" style="font-size: 0;vertical-align: top;background-color: #ffffff;padding-left: 16px;padding-right: 16px;padding-bottom: 24px;">
            <!--[if mso]><table cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td width="300" align="center" valign="top" style="padding: 0px 8px;"><![endif]-->

                      <div class="o_col o_col-3 o_col-full" style="display: inline-block;vertical-align: top;width: 100%;max-width: 300px;">
              <div style="font-size: 24px; line-height: 24px; height: 24px;">&nbsp; </div>
              <div class="o_px-xs" style="padding-left: 8px;padding-right: 8px;">
                <table width="100%" role="presentation" cellspacing="0" cellpadding="0" border="0">
                  <tbody>
                    <tr>
                      <td class="o_b-light o_br o_px o_py o_sans o_text-xs o_text-secondary" align="left" style="font-family: Helvetica, Arial, sans-serif;margin-top: 0px;margin-bottom: 0px;font-size: 14px;line-height: 21px;color: #424651;border: 1px solid #d3dce0;border-radius: 4px;padding-left: 16px;padding-right: 16px;padding-top: 16px;padding-bottom: 16px;">
                        <p class="o_mb-xs" style="margin-top: 0px;margin-bottom: 8px;"><strong>Indirizzo di fatturazione</strong></p>
                        @if(!is_null($lead->bill_to))
                            <p class="o_mb-md" style="margin-top: 0px;margin-bottom: 24px;">{{ $lead->bill_to->name }} {{ $lead->bill_to->surname }}<br>
                            {{ $lead->bill_to->address }}<br>
                            </p>
                        @endif
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>

            <!--[if mso]></td><td width="300" align="center" valign="top" style="padding: 0px 8px;"><![endif]-->


                <div class="o_col o_col-3 o_col-full" style="display: inline-block;vertical-align: top;width: 100%;max-width: 300px;">
              <div style="font-size: 24px; line-height: 24px; height: 24px;">&nbsp; </div>
              <div class="o_px-xs" style="padding-left: 8px;padding-right: 8px;">
                <table width="100%" role="presentation" cellspacing="0" cellpadding="0" border="0">
                  <tbody>
                    <tr>
                      <td class="o_b-light o_br o_px o_py o_sans o_text-xs o_text-secondary" align="left" style="font-family: Helvetica, Arial, sans-serif;margin-top: 0px;margin-bottom: 0px;font-size: 14px;line-height: 21px;color: #424651;border: 1px solid #d3dce0;border-radius: 4px;padding-left: 16px;padding-right: 16px;padding-top: 16px;padding-bottom: 16px;">
                        <p class="o_mb-xs" style="margin-top: 0px;margin-bottom: 8px;"><strong>Indirizzo di spedizione</strong></p>
                        <p class="o_mb-md" style="margin-top: 0px;margin-bottom: 24px;">{{ $lead->billing_address['name'] }} {{ $lead->billing_address['surname'] }}<br>
                          {{ $lead->billing_address['address'] }}<br>
                        </p>
                        <p class="o_mb-xs" style="margin-top: 0px;margin-bottom: 8px;"><strong>Methodo di pagamento</strong></p>
                        <p style="margin-top: 0px;margin-bottom: 0px;">{{ $lead->pay_with }}</p>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>


            <!--[if mso]></td></tr></table><![endif]-->

          </td>
        </tr>
      </tbody>
    </table>
    <!-- spacer -->
@include(env('THEME_NAME').'.notifications.include.footer')
