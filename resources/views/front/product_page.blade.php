
@extends(env('THEME_NAME').'.main')
@section('content')


        <!--====== Page Banner Start ======-->

    <section class="page-banner bg_cover" style="background-image: url({{ $product->photos[0]['src'] }});">
        <div class="container">
            <div class="page-banner-content text-center">
                <h2 class="title">{{ $product->name }}</h2>
                <ol class="breadcrumb justify-content-center">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                    <li class="breadcrumb-item active"><a href="{{ Route('productGrid',['locale' => $locale]) }}">Shop</a></li>
                </ol>
            </div>
        </div>
    </section>

    <!--====== Page Banner Ends ======-->

       <!--====== Product Simple Start ======-->

    <section class="product-simple-area pt-20">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="product-simple-image product-simple-02-image flex-wrap mt-50">
                        {{--

                            <button class="product-gallery-popup" data-hint="Click to enlarge" data-images='[
                            {"src": "{{ asset('assets/images/product/product-37.jpg') }}", "w": 600, "h": 743},
                            {"src": "{{ asset('assets/images/product/product-38.jpg') }}", "w": 600, "h": 743},
                            {"src": "{{ asset('assets/images/product/product-25.jpg') }}", "w": 600, "h": 743},
                            {"src": "{{ asset('assets/images/product/product-26.jpg') }}", "w": 600, "h": 743}
                        ]'><i class="far fa-search-plus"></i></button>

                            --}}
                        <button class="product-gallery-popup" data-hint="Click to enlarge" data-images='@json($product->photos)'><i class="far fa-search-plus"></i></button>

                        <div class="product-simple-preview-image">
                            <img class="product-zoom" src="{{ $product->photos[0]['src'] }}" alt="">
                        </div>
                        <div id="gallery_01" class="product-simple-thumb-image">
                            @foreach($product->photos as $index => $photo)
                                <div class="single-product-thumb">
                                    <a class="@if($index == 0) active @endif" href="#" data-image="{{ $photo['src'] }}">
                                        <img src="{{ $photo['src'] }}" alt="">
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="product-simple-details mt-50">
                        <h4 class="title">{{ $product->name }}</h4>
                        <p class="sku-id">REF. {{ $product->sku }} <span>{{ $product->quantity }} disponibili</span></p>

                        @if(!is_null($product->reviews_score))

                            <p class="review">
                                @for($i = 1; $i <= ceil($product->reviews_score); $i ++)
                                    <i class="fas fa-star review-on"></i>
                                @endfor
                                @for($i = ceil($product->reviews_score); $i<5;  $i ++)
                                    <i class="fas fa-star review-off"></i>
                                @endfor
                                <a href="#">({{ $product->reviews->count() }} customer reviews)</a>
                            </p>

                        @endif

                        <div class="product-price">
                            <span class="price">@fp($product->price)</span>
                        </div>
<!--
                        <div class="product-size">
                            <p>Size: <strong>M</strong></p>
                            <ul class="size-list">
                                <li data-tooltip="tooltip" data-placement="top" title="L">L</li>
                                <li data-tooltip="tooltip" data-placement="top" title="M" class="active">M</li>
                                <li data-tooltip="tooltip" data-placement="top" title="S">S</li>
                                <li data-tooltip="tooltip" data-placement="top" title="X">X</li>
                                <li data-tooltip="tooltip" data-placement="top" title="XXl">XXl</li>
                            </ul>
                        </div>
                    -->
                    @if(!is_null($variants))
                        <div class="product-color">
                            <p>Varianti: <!--<strong>Blue</strong>--></p>


                                @foreach($variants->chunk(4) as $chunkIndex => $chunks)
                                <ul class="color-list @if($chunkIndex > 0) mt-2 @endif">
                                    @foreach($chunks as $variant)
                                        <li class="@if($variant->id == $product->id) active @endif" data-tooltip="tooltip" data-placement="top" title="{{ $variant->name }}" >
                                            <a href="{{ $variant->makeUrl() }}">
                                                <img src="{{ Route('ir', ['size' => 'h240', 'filename' => $variant->photos[0]]) }}" style="height: 70px;" alt="">
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                                @endforeach

                        </div>
                    @endif
                    <div class="product-attribute product-color">
                        <p>Attributi: <!--<strong>Blue</strong>--></p>
                        <ul>
                            @foreach($product->attributevalues as $attributeValue)
                                <li>
                                    {{ Str::ucfirst($attributeValue->attribute->name) }}: {{ Str::ucfirst($attributeValue->name) }}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                        <div class="product-quantity-cart-wishlist-compare flex-wrap">
                            @if ($product->quantity > 0)
                                <form action="{{ Route('addToCart') }}">
                                        <input type="hidden" name="product_id" value="{{ $product->id }}">
                                        <input type="hidden" name="locale" value="{{ App::getLocale() }}">
                                    <div class="product-quantity d-flex">
                                        <button type="button" class="sub"><i class="fal fa-minus"></i></button>
                                        <input max-message="Hai raggiunto la quantità massima di prodotti che puoi aggiungere al carrello  ({{ $product->quantity }})" type="text" value="1" min="1" max="{{ $product->quantity }}" name="quantity" />
                                        <button type="button" class="add"><i class="fal fa-plus"></i></button>
                                    </div>
                                    <div class="product-cart">
                                        <button type="submit" class="main-btn">Aggiungi al carrello</button>
                                    </div>
                                </form>
                            @else
                                <div class="alert alert-secondary">Il prodotto è esaurito</div>
                            @endif
                            <!--
                            <a href="#" data-tooltip="tooltip" data-placement="top" title="Add to Wishlist" class="product-wishlist"><i class="far fa-heart"></i></a>
                            <a href="#" data-tooltip="tooltip" data-placement="top" title="Add to Compare" class="product-compare"><i class="far fa-repeat-alt"></i></a>
                            -->
                         </div>

                         <div class="product-description">
                            {{ $product->short_description }}
                        </div>

                        <div class="product-share">
                            <ul class="social">
                                @if($defaults['facebook'] != '') <li><a target="_blank" href="{{ $defaults['facebook'] }}"><i class="fab fa-facebook-f"></i></a></li> @endif
                                @if($defaults['instagram'] != '') <li><a target="_blank" href="{{ $defaults['instagram'] }}"><i class="fab fa-instagram"></i></a></li> @endif
                                @if($defaults['twitter'] != '') <li><a target="_blank" href="{{ $defaults['twitter'] }}"><i class="fab fa-twitter"></i></a></li> @endif
                                @if($defaults['youtube'] != '') <li><a target="_blank" href="{{ $defaults['youtube'] }}"><i class="fab fa-youtube"></i></a></li> @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--====== Product Simple Ends ======-->

    <!--====== Product Description & Review Start ======-->

    <section class="product-description-review pt-150">
        <div class="container">
            <div class="product-simple-tab-menu">
                <ul class="nav justify-content-center">
                    <li><a class="active" data-toggle="tab" href="#description">Descrizione</a></li>
                    @if(!is_null($product->reviews_score))
                        <li><a data-toggle="tab" href="#review">Review ({{ $product->reviews->count() }})</a></li>
                    @endif
                </ul>
            </div>
            <div class="tab-content pt-30 mb-4">
                <div class="tab-pane fade show active" id="description">
                    <div class="product-description">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="description">
                                    {!! $product->description !!}
                                    <hr>
                                    {!! $product->extended_description !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if(!is_null($product->reviews_score))
                    <div class="tab-pane fade" id="review">
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div class="product-review pt-20">
                                    <div class="review-comment">
                                        <ul class="comment">
                                            @foreach($product->reviews as $review)
                                                <li>
                                                    <div class="single-review-comment">
                                                        <!--<div class="review-author">
                                                            <img src="assets/images/auhtor-1.jpg" alt="">
                                                        </div>-->
                                                        <div class="review-content">
                                                            <p>{{ $review->content }}</p>
                                                            <div class="review-name-rating">
                                                                <h6 class="review-name">{{ $review->alias }}</h6>
                                                                <ul class="review-rating">
                                                                    @for($i = 1; $i == $review->score; $i ++)
                                                                        <li class="rating-on"><i class="fas fa-star"></i></li>
                                                                    @endfor
                                                                    @for($i = $review->score; $i<5;  $i ++)
                                                                        <li class="rating-off"><i class="fas fa-star"></i></li>
                                                                    @endfor
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <!--
                                    <div class="review-form mt-45">
                                        <h2 class="form-title">Add a review </h2>

                                        <form action="#">
                                            <div class="rating-star">
                                                <a href="javascript:void(0)" class="star-1"></a>
                                                <a href="javascript:void(0)" class="star-2"></a>
                                                <a href="javascript:void(0)" class="star-3"></a>
                                                <a href="javascript:void(0)" class="star-4"></a>
                                                <a href="javascript:void(0)" class="star-5"></a>
                                            </div>

                                            <div class="review-textarea">
                                                <label>Your Review *</label>
                                                <textarea></textarea>
                                            </div>
                                            <div class="review-btn">
                                                <a href="#" class="main-btn">Submit</a>
                                            </div>
                                            <div class="review-checkbok">
                                                <input type="checkbox" id="checkbox">
                                                <label for="checkbox"><span></span> NOTIFY ME OF NEW POSTS BY EMAIL.</label>
                                            </div>
                                        </form>
                                    </div>
                                    -->
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>

    <!--====== Product Description & Review Ends ======-->

    <!--====== Related Products Start ======-->
<!--
    <section class="product-area pt-160 pb-145">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="section-title text-center">
                        <h2 class="title">Related Products</h2>
                    </div>
                </div>
            </div>
            <div class="row product-active">
                <div class="col-md-3">
                    <div class="single-product mt-50">
                        <div class="product-image">
                            <div class="image">
                                <img class="product-1" src="assets/images/product/product-7.jpg" alt="product">
                                <img class="product-2" src="assets/images/product/product-8.jpg" alt="product">
                                <a class="link" href="product-simple-01.html"></a>
                            </div>
                            <ul class="product-meta text-center">
                                <li><a data-tooltip="tooltip" data-placement="top" title="Add to Cart" href="#"><i class="fal fa-Shopping-cart"></i></a></li>
                                <li><a data-tooltip="tooltip" data-placement="top" title="Quick Shop" data-toggle="modal" data-target="#productQuick" href="#"><i class="fal fa-search-plus"></i></a></li>
                                <li><a data-tooltip="tooltip" data-placement="top" title="Add to Wishlist" href="#"><i class="fal fa-heart"></i></a></li>
                                <li><a data-tooltip="tooltip" data-placement="top" title="Add to Compare" href="#"><i class="fal fa-repeat-alt"></i></a></li>
                            </ul>
                            <span class="discount">40%</span>
                        </div>
                        <div class="product-content d-flex justify-content-between">
                            <div class="product-title">
                                <h4 class="title"><a href="product-simple-01.html">Basic Contrasting T-Shirt</a></h4>
                            </div>
                            <div class="product-price">
                                <span class="regular-price">£250.00</span>
                                <span class="sale-price">£200.00</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="single-product mt-50">
                        <div class="product-image">
                            <div class="image">
                                <img class="product-1" src="assets/images/product/product-9.jpg" alt="product">
                                <img class="product-2" src="assets/images/product/product-10.jpg" alt="product">
                                <a class="link" href="product-simple-01.html"></a>
                            </div>
                            <ul class="product-meta text-center">
                                <li><a data-tooltip="tooltip" data-placement="top" title="Add to Cart" href="#"><i class="fal fa-Shopping-cart"></i></a></li>
                                <li><a data-tooltip="tooltip" data-placement="top" title="Quick Shop" data-toggle="modal" data-target="#productQuick" href="#"><i class="fal fa-search-plus"></i></a></li>
                                <li><a data-tooltip="tooltip" data-placement="top" title="Add to Wishlist" href="#"><i class="fal fa-heart"></i></a></li>
                                <li><a data-tooltip="tooltip" data-placement="top" title="Add to Compare" href="#"><i class="fal fa-repeat-alt"></i></a></li>
                            </ul>
                        </div>
                        <div class="product-content d-flex justify-content-between">
                            <div class="product-title">
                                <h4 class="title"><a href="product-simple-01.html">Biker Jacket</a></h4>
                            </div>
                            <div class="product-price">
                                <span class="price">£250.00</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="single-product mt-50">
                        <div class="product-image">
                            <div class="image">
                                <img class="product-1" src="assets/images/product/product-21.jpg" alt="product">
                                <img class="product-2" src="assets/images/product/product-22.jpg" alt="product">
                                <a class="link" href="product-simple-01.html"></a>
                            </div>
                            <ul class="product-meta text-center">
                                <li><a data-tooltip="tooltip" data-placement="top" title="Add to Cart" href="#"><i class="fal fa-Shopping-cart"></i></a></li>
                                <li><a data-tooltip="tooltip" data-placement="top" title="Quick Shop" data-toggle="modal" data-target="#productQuick" href="#"><i class="fal fa-search-plus"></i></a></li>
                                <li><a data-tooltip="tooltip" data-placement="top" title="Add to Wishlist" href="#"><i class="fal fa-heart"></i></a></li>
                                <li><a data-tooltip="tooltip" data-placement="top" title="Add to Compare" href="#"><i class="fal fa-repeat-alt"></i></a></li>
                            </ul>
                        </div>
                        <div class="product-content d-flex justify-content-between">
                            <div class="product-title">
                                <h4 class="title"><a href="product-simple-01.html">Oversized Check Dress</a></h4>
                            </div>
                            <div class="product-price">
                                <span class="price">£150.00</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="single-product mt-50">
                        <div class="product-image">
                            <div class="image">
                                <img class="product-1" src="assets/images/product/product-23.jpg" alt="product">
                                <img class="product-2" src="assets/images/product/product-24.jpg" alt="product">
                                <a class="link" href="product-simple-01.html"></a>
                            </div>
                            <ul class="product-meta text-center">
                                <li><a data-tooltip="tooltip" data-placement="top" title="Add to Cart" href="#"><i class="fal fa-Shopping-cart"></i></a></li>
                                <li><a data-tooltip="tooltip" data-placement="top" title="Quick Shop" data-toggle="modal" data-target="#productQuick" href="#"><i class="fal fa-search-plus"></i></a></li>
                                <li><a data-tooltip="tooltip" data-placement="top" title="Add to Wishlist" href="#"><i class="fal fa-heart"></i></a></li>
                                <li><a data-tooltip="tooltip" data-placement="top" title="Add to Compare" href="#"><i class="fal fa-repeat-alt"></i></a></li>
                            </ul>
                        </div>
                        <div class="product-content d-flex justify-content-between">
                            <div class="product-title">
                                <h4 class="title"><a href="product-simple-01.html">Polyamide Dress With Long Sleeves</a></h4>
                            </div>
                            <div class="product-price">
                                <span class="price">£150.00</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
-->
    <!--====== Related Products Ends ======-->

@endsection
