@extends(env('THEME_NAME').'.main_home')
@section('content')

<!--====== preloader Start ======-->

    <div class="preloader">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <!--====== preloader Ends ======-->

    @include(env('THEME_NAME').'.include.off_canvas_sidebar')

    <!--====== Search Start ======-->

    <div class="search-wrapper">
        <div class="search-box">
            <a href="javascript:void(0)" class="search-close"><i class="fal fa-times"></i></a>
            <div class="search-form">
                <label>Start typing and press Enter to search</label>
                <div class="search-input">
                    <form action="#">
                        <input type="text" placeholder="Search entire store…">
                        <button><i class="far fa-search"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!--====== Search Ends ======-->

    @include(env('THEME_NAME').'.include.off_canvas_cart')

    <!--====== Slider Start ======-->

    <section id="sliderParallax" class="slider-area slider-11 slider-active">
        @foreach($carousels['homepage_main']->first()->slides as $slide)
            <div class="single-slider bg_cover d-flex align-items-center paroller" style="background-image: url('{{ Route('ir',['size' => 'h1600','filename' => $slide->file]) }}');" data-paroller-factor="-0.15" data-paroller-type="foreground" data-paroller-direction="vertical">

                <div class="container">
                    <div class="slider-content-11 text-center layer" data-depth="0.5">
                        <div class="content">
                            <span class="line" data-animation="lineLeft" data-delay="0.5s"></span>
                            <h1 class="title"><span data-animation="slideInLeft" data-delay="1.3s">{{ $slide->name }}</span></h1>
                            <span class="line" data-animation="lineLeft" data-delay="1s"></span>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

    </section>

    <!--====== Slider Ends ======-->

    <!--====== Header Start ======-->

    <header class="header-area">

        <div class="slider-bottom-header-sticky header-navbar-dark">
            <div class="container-fluid custom-container">
                <div class="header-wrapper d-flex justify-content-between align-items-center">

                    <div class="header-logo">
                        <a href="/">
                            <img style="height: 80px"  src="{{ asset('resources/logo_light.png') }}" alt="Logo">
                        </a>
                    </div>

                    <div class="header-menu site-nav d-none d-lg-block">
                        <ul class="main-menu">
                            @foreach($menus[1]->first()->items as $item)
                                <li class="@if(url()->current() == url($item->href)) active @endif"><a href="{{ $item->href }}">{{ $item->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>


                    <div class="header-meta">
                        <ul class="meta">
                            {{--
                            <li><a class="cart-toggle" href="javascript:void(0)"><i class="far fa-Shopping-cart"></i><span>3</span></a></li>
                            <li><a class="search-toggle" href="javascript:void(0)"><i class="far fa-search"></i></a></li>
                            <li><a class="sidebar-toggle" href="javascript:void(0)"><i class="fal fa-bars"></i></a></li>
                             --}}
                             <li><a href="{{ Route('cart',['locale'=> $locale]) }}"><i class="far fa-Shopping-cart"></i><span>{{ $lead->products->count() }}</span></a></li>
                            <li><a href="{{ Route('productGrid',['locale'=> $locale]) }}" href="javascript:void(0)"><i class="far fa-search"></i></a></li>
                            <li><a class="sidebar-toggle" href="javascript:void(0)"><i class="fal fa-bars"></i></a></li>
                        </ul>
                          <small ><span class="text-danger">Consegna gratis da €{{ (int)$cartTotalBeforeFreeShipping }}!</span></small>
                    </div>

                </div>
            </div>

            <div id="dl-menu" class="dl-menuwrapper d-lg-none">
                <button class="dl-trigger"></button>

                <ul class="dl-menu">
                    @foreach($menus[1]->first()->items as $item)
                        <li class="@if(url()->current() == url($item->href)) active @endif"><a href="{{ $item->href }}">{{ $item->name }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>

    </header>

    <!--====== Header Ends ======-->

    <!--====== Category Start ======-->

    @if(!is_null($shopWindow))
        <section class="category-area-3 pt-80">
            <div class="container-fluid custom-container-2">
                <div class="row category-active">
                    @foreach($shopWindow as $product)
                        @if(!is_null($product->photos))
                            <div class="col-lg-3">
                                <div class="single-category-3">
                                    <a target="_blank" href="{{ $product->makeUrl() }}"><img src="{{ Route('ir',['size' => 'h800','filename' => collect($product->photos)->first()]) }}" alt="">
                                    </a>
                                    <div class="category-content text-center">
                                        <h5 class="title"><a href="shop-sidebar.html">{{ $product->name }}</a></h5>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </section>
    @endif

    <!--====== Category Ends ======-->

    <!--====== Product Start ======-->
        @if(!is_null($newArrivals))
    <section class="product-area pt-85 pb-110">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="section-title-3 text-center pb-60">
                        <h2 class="title">NUOVI ARRIVI</h2>
                    </div>
                </div>
            </div>
            <div class="product-menu">
                <ul class="nav nav-2 justify-content-center">
                    @foreach($newArrivals as $index =>  $newArrival)
                    <li>
                      <a class="@if($index == 0) active @endif"  data-toggle="tab" href="#new-arrivals-tab-{{ Str::slug($newArrival->name) }}" >{{ $newArrival->name }}</a>
                    </li>
                    @endforeach
                  </ul>
            </div>
        </div>
        <div class="container-fluid custom-container-2">
            <div class="tab-content" >
                @foreach($newArrivals as $index =>  $newArrival)
                <div class="tab-pane fade show @if($index == 0) active @endif" id="new-arrivals-tab-{{ Str::slug($newArrival->name) }}">
                    <div class="row">
                        @foreach($newArrival->products as $product)
                            <div class="col-xl-3 col-md-4 col-sm-6">
                                <div class="single-product mt-50">
                                    <div class="product-image">
                                        <div class="image">
                                            <img class="product-1" src="{{  Route('ir',['size' => 'h800','filename' => collect($product->photos)->first()]) }}" alt="product">
                                            {{-- <img class="product-2" src="assets/images/product/product-87.jpg" alt="product">  --}}
                                            <a class="link" href="{{ $product->makeUrl() }}"></a>
                                        </div>
                                    </div>
                                    <div class="product-content d-flex justify-content-between">
                                        <div class="product-title">
                                            <h4 class="title"><a href="{{ $product->makeUrl() }}">{{ $product->name }}</a></h4>
                                        </div>
                                        <div class="product-price">
                                            <span class="price">@fp($product->price)</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    @endif

    <!--====== Product Ends ======-->

    {{--
    <!--====== Countdown Start ======-->

    <section class="countdown-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="countdown-image mt-50">
                        <img src="assets/images/m11-p-1.jpg" alt="">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="countdown-content mt-45 text-center">
                        <h3 class="title">District Side Table</h3>
                        <span class="price">$123.99</span>

                        <div data-countdown="2020/11/20"></div>

                        <a href="shop-sidebar.html" class="main-btn">Shop Now</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--====== Countdown Ends ======-->
     --}}

    <!--====== Blog Start ======-->

    <section class="blog-area pt-80 pb-55">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section-title-3 text-center pb-30">
                        <h2 class="title">ULTIME DAL BLOG</h2>
                    </div>
                </div>
            </div>
            <div class="row  blog-active">
                @foreach($newArticles as $article)
                <div class="col-lg-4 col-md-6">
                    <div class="single-blog mt-30">
                        <div class="blog-image">
                            <a href="{{ $article->makeUrl() }}">
                                <img src="{{ Route('ir',['size' => 'h800','filename' =>  $article->cover ])}}" alt="blog">
                                <i class="fal fa-search-plus"></i>
                            </a>
                        </div>
                        <div class="blog-content">
                            {{-- <ul class="blog-category">
                                <li><a href="#">Style</a></li>
                            </ul> --}}
                            <h4 class="title"><a href="{{ $article->makeUrl() }}">{{ $article->name }}</a></h4>
                            <ul class="blog-meta">
                                 {{--<li><a href="#">By <span> Jon Smith</span></a></li>--}}
                                <li><a href="#"><i class="fal fa-clock"></i> {{ $article->created_at->format('d F Y') }}</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>

    <!--====== Blog Ends ======-->

    <!--====== Newsletter Start ======-->

    <section class="newsletter-area pt-10 pb-60">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5">
                    <div class="newsletter-content mt-45">
                        <h4 class="title">Rimani aggiornato sui nostri prodotti</h4>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="newsletter-form mt-50">
                        <form action="{{ Route('subscribeNewsletter') }}" method="POST">
                            @csrf
                            <input type="hidden" name="locale" value="{{ $locale }}">
                            <input type="email" placeholder="Email" name="email" required>
                            <button class="main-btn" method="POST">Iscriviti</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--====== Newsletter Ends ======-->
{{--
    <!--====== Brand Logo Start ======-->

    <div class="brand-logo-area border-0 pt-50 pb-50">
        <div class="container">
            <div class="brand-row brand-active">
                <div class="brand-col">
                    <div class="single-brand">
                        <img src="assets/images/brand-1.png" alt="brand">
                    </div>
                </div>
                <div class="brand-col">
                    <div class="single-brand">
                        <img src="assets/images/brand-2.png" alt="brand">
                    </div>
                </div>
                <div class="brand-col">
                    <div class="single-brand">
                        <img src="assets/images/brand-3.png" alt="brand">
                    </div>
                </div>
                <div class="brand-col">
                    <div class="single-brand">
                        <img src="assets/images/brand-4.png" alt="brand">
                    </div>
                </div>
                <div class="brand-col">
                    <div class="single-brand">
                        <img src="assets/images/brand-5.png" alt="brand">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--====== Brand Logo Ends ======-->
 --}}
@endsection
