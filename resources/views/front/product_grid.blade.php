
@extends(env('THEME_NAME').'.main')
@section('content')

    <section class="page-banner bg_cover" style="">
        <div class="container">
            <div class="page-banner-content text-center">
                <h2 class="title">Shop</h2>
                <ol class="breadcrumb justify-content-center">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                </ol>
            </div>
        </div>
    </section>

    <!--====== Shop Page Start ======-->

    <section class="shop-page pt-30 pb-80">
        <div class="container-fluid shop-container">
            <div class="row flex-md-row-reverse justify-content-between">
                <div class="col-lg-9 content-col">
                    <div class="shop-top-bar mt-35">
                        <div class="shop-top-left">
                            <!--<p>Showing 1–12 of 80 results</p>
                            <div class="show">
                                <span>Show</span>
                                <ul>
                                    <li><a class="active" href="#">12</a></li>
                                    <li><a href="#">15</a></li>
                                    <li><a href="#">30</a></li>
                                </ul>
                            </div>-->
                        </div>
                        <div class="shop-top-right">
                            <!--<div class="shop-sort">
                                <select id="resizing_select">
                                    <option value="0" selected="selected">Sort By</option>
                                    <option value="1">Default</option>
                                    <option value="2">Popularity</option>
                                    <option value="3">Average Rating</option>
                                    <option value="4">Newsness</option>
                                    <option value="5">Price Low to High</option>
                                    <option value="6">Priche High to Low</option>
                                </select>
                                <i class="fal fa-chevron-down"></i>
                                <select id="width_tmp_select">
                                    <option id="width_tmp_option"></option>
                                </select>
                            </div>
                        -->
                        <div id="sort-by"></div>
                        </div>
                    </div>

                    <div class="shop-product" id="hits">

                    </div>
<!--
                    <div class="pagination-items mt-80">
                        <ul class="pagination justify-content-center">
                            <li><a class="prev" href="#">Previous</a></li>
                            <li><a href="#">1</a></li>
                            <li><a class="active" href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a class="next" href="#">Next</a></li>
                        </ul>
                    </div>
                -->
                </div>

                <div class="col-xl-3 sidebar-col">
                    <div class="shop-sidebar">
                        <div class="shop-sidebar-search mt-50">
                            <h4 class="sidebar-title mb-2">Cerca qui</h4>
                            <div id="searchbox"></div>
                            <div id="clear-refinements"></div>
                        </div>


                        <div class="shop-sidebar-color mt-50">
                            <h4 class="sidebar-title mb-2">Categoria</h4>
                            <div id="refinement-list-category"></div>
                        </div>

                        <div class="shop-sidebar-color mt-50">
                            <h4 class="sidebar-title mb-2">Colore</h4>
                            <div id="refinement-list-color"></div>
                        </div>

                        <div class="shop-sidebar-color mt-50">
                            <h4 class="sidebar-title mb-2">Fascia di prezzo</h4>
                            <div id="numeric-menu"></div>
                        </div>

                        <!--
                        <div class="shop-sidebar-color mt-50">
                            <h4 class="sidebar-title mb-2">Imposta il prezzo</h4>
                            <div id="range-slider"></div>
                        </div>
                    -->


<!--
                         <div class="shop-sidebar-color mt-50">
                            <h4 class="sidebar-title mb-2">Durata</h4>
                            <div id="refinement-list-durata"></div>
                        </div>
                    -->
<!--
                        <div class="shop-sidebar-color mt-50">
                            <h4 class="sidebar-title">Color</h4>

                            <div class="sidebar-color">
                                <ul class="color-list">
                                    <li data-tooltip="tooltip" data-placement="top" title="Black"  data-color="#171d3d" class="active"></li>
                                    <li data-tooltip="tooltip" data-placement="top" title="Blue"  data-color="#4b59a3"></li>
                                    <li data-tooltip="tooltip" data-placement="top" title="Bronze"  data-color="#b9afa1"></li>
                                    <li data-tooltip="tooltip" data-placement="top" title="Green"  data-color="#61c58d"></li>
                                    <li data-tooltip="tooltip" data-placement="top" title="Pink"  data-color="#f6b7cf"></li>
                                    <li data-tooltip="tooltip" data-placement="top" title="Red"  data-color="#ef5619"></li>
                                </ul>
                            </div>
                        </div>
                    -->





                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--====== Shop Page Ends ======-->
@endsection

@push('after-scripts')

    <style>
        @media only screen and (max-device-width: 812px) {
            .ais-Hits-item, .ais-InfiniteHits-item, .ais-InfiniteResults-item, .ais-Results-item{
                width: calc(50% - 1rem)!important;
            }
        }


    </style>



<script src="https://cdn.jsdelivr.net/npm/algoliasearch@4.0.0/dist/algoliasearch-lite.umd.js" integrity="sha256-MfeKq2Aw9VAkaE9Caes2NOxQf6vUa8Av0JqcUXUGkd0=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/instantsearch.js@4.0.0/dist/instantsearch.production.min.js" integrity="sha256-6S7q0JJs/Kx4kb/fv0oMjS855QTz5Rc2hh9AkIUjUsk=" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/instantsearch.css@7.3.1/themes/algolia-min.css" integrity="sha256-HB49n/BZjuqiCtQQf49OdZn63XuKFaxcIHWf0HNKte8=" crossorigin="anonymous">

    <script>
        const searchClient = algoliasearch('{{ env('ALGOLIA_APP_ID') }}', '{{ env('ALGOLIA_SECRET') }}');
        var locale = 'it';
        @verbatim
        const search = instantsearch({
            indexName: 'nuovecererieproducts',
            searchClient,
        });

        search.addWidgets([
            instantsearch.widgets.searchBox({
                container: '#searchbox',
            }),

            instantsearch.widgets.infiniteHits({
                container: '#hits',
                templates:{
                    empty: 'No results for <q>{{ query }}</q>',
                    item: `<div class="">
                                <div class="single-product mt-50">
                                    <div class="product-image">
                                        <div class="image">
                                            <img class="product-1" src="{{ cover }}" alt="product">
                                            <!--<img class="product-2" src="assets/images/product/product-4.jpg" alt="product">-->
                                            <a class="link" href="{{ url }}"></a>
                                        </div>
                                        <!--
                                        <ul class="product-meta text-center">
                                            <li><a data-tooltip="tooltip" data-placement="top" title="Add to Cart" href="#"><i class="fal fa-Shopping-cart"></i></a></li>
                                            <li><a data-tooltip="tooltip" data-placement="top" title="Quick Shop" data-toggle="modal" data-target="#productQuick" href="#"><i class="fal fa-search-plus"></i></a></li>
                                            <li><a data-tooltip="tooltip" data-placement="top" title="Add to Wishlist" href="#"><i class="fal fa-heart"></i></a></li>
                                            <li><a data-tooltip="tooltip" data-placement="top" title="Add to Compare" href="#"><i class="fal fa-repeat-alt"></i></a></li>
                                        </ul>
                                        -->
                                        <!--<span class="discount">40%</span>-->
                                    </div>
                                    <div class="product-content d-flex justify-content-between">
                                        <div class="product-title">
                                            <h4 class="title"><a href="{{ url }}">{{ name }}</a></h4>
                                        </div>
                                        <div class="product-price">
                                            <!--<span class="regular-price">£250.00</span>-->
                                            <span class="sale-price">{{ price }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>`,

                },
                 transformItems(items) {
                     return items.map(function(item){
                        console.log(item);
                            return{
                                name: item['name_'+locale],
                                url: item['url_'+locale],
                                price: '€ '+parseFloat(item.price).toFixed(2),
                                //price: item.price,
                                cover: item.cover
                            }
                     })
                },
            }),

            instantsearch.widgets.sortBy({
                container: '#sort-by',
                items: [
                    { label: 'Più rilevanti', value: 'nuovecererieproducts' },
                    { label: 'Prezzo (prima meno costosi)', value: 'instant_search_price_asc' },
                    { label: 'Prezzo (prima più costosi)', value: 'instant_search_price_desc' },
                ],
            }),

            instantsearch.widgets.clearRefinements({
                container: '#clear-refinements',
                cssClasses:{
                    button: ['orange-btn']
                }
            }),

            instantsearch.widgets.refinementList({
                container: '#refinement-list-color',
                attribute: 'colore_'+locale,
                searchable: true
            }),

            instantsearch.widgets.refinementList({
                container: '#refinement-list-category',
                attribute: 'category_'+locale,
            }),
/*
            instantsearch.widgets.refinementList({
                container: '#refinement-list-durata',
                attribute: 'durata',
            }),
*/


            instantsearch.widgets.numericMenu({
                container: '#numeric-menu',
                attribute: 'price',
                items: [
                    { label: 'Tutti' },
                    { label: 'Meno di 10€', end: 10 },
                    { label: 'Tra 10€ e 20€', start: 10, end: 20 },
                    { label: 'Tra 20€ e 50€', start: 20, end: 50 },
                    { label: 'Più di 50€', start: 50 },
                ],
            }),
/*
            instantsearch.widgets.rangeSlider({
                 container: '#range-slider',
                attribute: 'price',

            })
*/
        ]);
        search.start();
        @endverbatim
    </script>

@endpush
