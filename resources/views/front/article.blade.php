@extends(env('THEME_NAME').'.main')
@section('content')
        @if (!is_null($article->cover))
            <section class="page-banner bg_cover" style="background-image: url('{{ Route('ir',['size' => 'h1600','filename' =>  $article->cover ])}}');">
        @endif
        <div class="container">
            <div class="page-banner-content text-center">
                <h2 class="title">{{ $article->name  }}</h2>
                <ol class="breadcrumb justify-content-center">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                </ol>
            </div>
        </div>
    </section>

<section class="blog-page pt-20 pb-120">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="single-blog mt-80">
                        <div class="blog-image">
                            <a href="blog-details.html">
                                <img src="{{ Route('ir',['size' => 'h800','filename' =>  $article->cover ])}}" alt="blog">
                                <i class="fal fa-search-plus"></i>
                            </a>
                        </div>
                        <div class="blog-content">
                            <!--<ul class="blog-category">
                                <li><a href="#">Style</a></li>
                            </ul>-->
                            <h4 class="title text-center">{{ $article->name }}</h4>
                            <!--<ul class="blog-meta">
                                <li><a href="#">By <span> Jon Smith</span></a></li>
                                <li><a href="#"><i class="fal fa-clock"></i> June 15, 2020</a></li>
                            </ul>-->
                            <div>
                                {!! $article->description !!}
                            </div>

                            <a href="/" class="main-btn btn-block">Continua con lo shopping</a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
