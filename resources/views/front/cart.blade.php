@extends(env('THEME_NAME').'.main')
@section('content')
        <!--====== Page Banner Start ======-->

    <section class="page-banner bg_cover" style="">
        <div class="container">
            <div class="page-banner-content text-center">
                <h2 class="title">Carrello</h2>
                <ol class="breadcrumb justify-content-center">
                    <li class="breadcrumb-item"><a href="/"> @lang('all.home') </a></li>
                </ol>
            </div>
        </div>
    </section>

    <!--====== Page Banner Ends ======-->
      <section class="checkout-page pb-80">
        <div class="container">
            @if($products->count() > 0)
            <!--
            <div class="checkout-info mt-30">
                <p class="info-header error"><i class="fas fa-exclamation-circle"></i> <strong>Error:</strong> Username is required.</p>
            </div>
        -->

        <!--
            <div class="checkout-info mt-30">
                <p class="info-header"> <i class="fas fa-exclamation-circle"></i> Returning customer? <a data-toggle="collapse" href="#login">Click here to login</a></p>

                <div class="collapse" id="login">
                    <div class="card-body">
                        <p>If you have shopped with us before, please enter your details in the boxes below. If you are a new customer, please proceed to the Billing & Shipping section.</p>
                        <form action="#">

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="single-form">
                                        <label>Email *</label>
                                        <input type="email">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="single-form">
                                        <label>Password</label>
                                        <input type="password">
                                    </div>
                                </div>
                            </div>

                            <div class="single-form d-flex align-items-center">
                                <button class="main-btn">Login</button>
                                <div class="checkbox">
                                    <input type="checkbox" id="remember">
                                    <label for="remember"><span></span> Remember Me</label>
                                </div>
                            </div>
                            <div class="forget">
                                <a href="#">Lost Your Password</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="checkout-info mt-30">
                <p class="info-header"> <i class="fas fa-exclamation-circle"></i> Have a coupon? <a data-toggle="collapse" href="#coupon">Click here to enter your code</a></p>

                <div class="collapse" id="coupon">
                    <div class="card-body">
                        <form action="#">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="single-form">
                                        <input type="email" placeholder="Coupon code">
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="single-form">
                                        <button class="main-btn">Login</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        -->
            <form method="POST" action="{{ Route('confirmCart') }}" id="cart-confirmation-form">
                @csrf
                <input type="hidden" name="locale" value="{{ $locale }}">
                <input type="hidden" name="data[payment_method]">
                <input type="hidden" name="pay_with" value="stripe">
                <input type="hidden" name="billing_address_json">
                <input type="hidden" name="shipping_address_json">
                {{--
                <!-- FOR AUTHORIZATION VERSION -->
                <input type="hidden" name="paypal_authorization_id" value="">
                <input type="hidden" name="paypal_order_id" value="">

                 --}}
                <div class="row">
                    <div class="col-lg-7">
                        <div class="checkout-form mt-30">

                            <div class="checkout-title">
                                <h4 class="title">@lang('all.billing address')</h4>
                                <a href="{{ Route('FacebookLoginRedirectToProvider') }}" class="main-btn mt-2 btn-facebook">@lang('all.access with facebook')</a>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="single-form">
                                        <label>@lang('all.name') *</label>
                                        <input name="data[billing_address][name]" type="text" value="{{ old('data.billing_address.name',$billing_address['name']) }}">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="single-form">
                                        <label>@lang('all.surname') *</label>
                                        <input name="data[billing_address][surname]" type="text" value="{{ old('data.billing_address.surname',$billing_address['surname']) }}">
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="single-form">
                                        <label>@lang('all.address') *</label>
                                        <input id="billing-address-input" name="data[billing_address][address]" type="text" placeholder="Via Roma 1, 50124 Firenze" value="{{ old('data.billing_address.address',$billing_address['address']) }}">
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="single-form">
                                        <label>@lang('all.phone') *</label>
                                        <input name="data[billing_address][telephone]" type="text" value="{{ old('data.billing_address.telephone',$billing_address['telephone']) }}">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="single-form">
                                        <label>Email *</label>
                                        <input name="data[billing_address][email]" type="text" value="{{ old('data.billing_address.email',$billing_address['email']) }}">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="single-form">
                                        <label>@lang('all.fiscal code') *</label>
                                        <input name="data[billing_address][fiscal_code]" type="text" value="{{ old('data.billing_address.fiscal_code',$billing_address['fiscal_code']) }}">
                                    </div>
                                </div>
                            </div>

                             <div class="checkbox checkout-checkbox">
                                <input name="invoice_request" type="checkbox" id="invoice_request" value="1">
                                <label for="invoice_request"><span></span> @lang('all.do you need and invoice for this order')?</label>
                            </div>
                            <div class="checkout-invoice_request">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="single-form">
                                            <label>@lang('all.vat number') *</label>
                                            <input name="data[billing_address][vat_number]" type="text" value="{{ old('data.billing_address.vat_number',$billing_address['vat_number']) }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="single-form">
                                            <label>@lang('all.sdi code') *</label>
                                            <input name="data[billing_address][sdi_code]" type="text" value="{{ old('data.billing_address.sdi_code',$billing_address['sdi_code']) }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="single-form">
                                            <label>@lang('all.company name') *</label>
                                            <input name="data[billing_address][company_name]" type="text" value="{{ old('data.billing_address.company_name',$billing_address['company_name']) }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="single-form">
                                            <label>@lang('all.pec') </label>
                                            <input name="data[billing_address][pec]" type="email" value="{{ old('data.billing_address.pec',$billing_address['pec']) }}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="checkbox checkout-checkbox">
                                <input name="create_an_account" type="checkbox" id="account" value="true">
                                <label for="account"><span></span> @lang('all.would you like to create an account')?</label>
                            </div>

                            <div class="checkout-account">
                                <div class="single-form">
                                    <label>@lang('all.write a password') *</label>
                                    <input name="password" type="password" placeholder="Password">
                                </div>
                            </div>

                            <div class="checkbox checkout-checkbox">
                                <input name="different_shipping_address" type="checkbox" id="shipping" value="true"  @if(old('different_shipping_address') == 'true') checked @endif>
                                <label for="shipping"><span></span> @lang('all.do you want to ship this order to a different address than the billing one')?</label>
                            </div>

                            <div class="checkout-shipping" style="display:  @if(old('different_shipping_address') == 'true') block  @else none @endif">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="single-form">
                                            <label>@lang('all.name') *</label>
                                            <input name="data[shipping_address][name]" type="text" value="{{ old('data.shipping_address.name',$shipping_address['name']) }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="single-form">
                                            <label>@lang('all.surname') *</label>
                                            <input name="data[shipping_address][surname]" type="text" value="{{ old('data.shipping_address.surname',$shipping_address['name']) }}">
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="single-form">
                                            <label>@lang('all.address') *</label>
                                            <input id="shipping-address-input" name="data[shipping_address][address]" type="text" placeholder="Via Roma 1, 50124 Firenze" value="{{ old('data.shipping_address.address',$shipping_address['address']) }}">

                                        </div>
                                    </div>


                                    <div class="col-sm-6">
                                        <div class="single-form">
                                            <label>@lang('all.phone') *</label>
                                            <input name="data[shipping_address][telephone]" type="text" value="{{ old('data.shipping_address.telephone',$shipping_address['telephone']) }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="single-form">
                                            <label>@lang('all.email') *</label>
                                            <input name="data[shipping_address][email]" type="text" value="{{ old('data.shipping_address.email',$shipping_address['email']) }}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="single-form checkout-note">
                                <label>@lang('all.notes')</label>
                                <textarea name="data[notes]" placeholder="@lang('all.notes placeholder')">{{ old('data.notes',$notes) }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="checkout-review-order mt-30">
                            <div class="checkout-title">
                                <h4 class="title">@lang('all.order summary')</h4>
                            </div>

                            <div class="checkout-review-order-table table-responsive mt-15">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="Product-name">@lang('all.products')</th>
                                            <th class="Product-price">@lang('all.total')</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($products as $product)
                                        <tr>
                                            <td class="Product-name">

                                                <p><a href="{{ $product->makeUrl() }}">{{ $product->name }}</a>  × <b>{{ $product->pivot->quantity }}</b></p>
                                            </td>
                                            <td class="Product-price">
                                                <p>@fp($product->price)</p>
                                                <small><a href="javascript:removeProduct({{ $product->id }})" class="text-danger">@lang('all.remove')</a></small>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td class="Product-name">
                                                <p>@lang('all.subtotal')</p>
                                            </td>
                                            <td class="Product-price">
                                                <p>@fp($lead->products_total)</p>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="Product-name">
                                                <p>@lang('all.shipping')</p>
                                            </td>
                                              <td class="Product-price">
                                                <p>@fp($lead->shipping_total)</p>
                                            </td>
                                               <!--<td class="Product-price">

                                                <ul class="shipping-list">
                                                    <li class="radio">
                                                        <input type="radio" name="shipping" id="radio1" checked>
                                                        <label for="radio1"><span></span> Flat Rate</label>
                                                    </li>
                                                    <li class="radio">
                                                        <input type="radio" name="shipping" id="radio2">
                                                        <label for="radio2"><span></span> Free Shipping</label>
                                                    </li>
                                                    <li class="radio">
                                                        <input type="radio" name="shipping" id="radio3">
                                                        <label for="radio3"><span></span> Local Pickup</label>
                                                    </li>
                                                </ul>

                                            </td>
                                             -->
                                        </tr>
                                        <tr>
                                            <td class="Product-name">
                                                <p>@lang('all.discounts')</p>
                                            </td>
                                            <td class="Product-price">
                                                @if($lead->discount_total > 0)
                                                    @if(!is_null($lead->discountcodes))
                                                        @foreach ($lead->discountcodes as $code)
                                                           <p class="text-nowrap">
                                                               {{ $code->code }} -@if($code->discountcode_type_id == 1)@fp($code->value)@else{{ $code->value }}% @endif
                                                               <a href="javascript:" onclick="removeDiscountCode({{ $code->id }})" class="text-danger"><i class="fa fa-times"></i></a>
                                                            </p>
                                                        @endforeach
                                                    @endif
                                                    <p>@fp($lead->discount_total)</p>
                                                @else
                                                    @lang('all.none')
                                                @endif
                                                <div class="input-group mb-3 input-group-sm">
                                                    <input id="new-discount-code" type="text" class="form-control" placeholder="Codice sconto" aria-label="Codice sconto" aria-describedby="basic-addon2">
                                                    <div class="input-group-append">
                                                        <a href="javascript:" onclick="addDiscountCode()" class="btn btn-secondary" type="button"><i class="fa fa-plus"></i></a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr id="paymentMethodPriceRow" class="d-none">
                                            <td class="Product-name">
                                                <p></p>
                                            </td>
                                            <td class="total-price">
                                                <p></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="Product-name">
                                                <p>@lang('all.total')</p>
                                            </td>
                                            <td class="total-price">
                                                <p id="grandTotal">@fp($lead->grand_total)</p>
                                            </td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>

                            <div class="checkout-payment">

                                <!--
                                <ul>
                                    <li>
                                        <div class="single-payment">
                                            <div class="payment-radio radio">
                                                <input type="radio" name="radio" id="bank">
                                                <label for="bank"><span></span> Direct bank transfer </label>

                                                <div class="payment-details">
                                                    <p>Please send a Check to Store name with Store Street, Store Town, Store State, Store Postcode, Store Country.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="single-payment">
                                            <div class="payment-radio radio">
                                                <input type="radio" name="radio" id="check">
                                                <label for="check"><span></span> Check payments </label>

                                                <div class="payment-details">
                                                    <p>Please send a check to Store Name, Store Street, Store Town, Store State / County, Store Postcode.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="single-payment">
                                            <div class="payment-radio radio">
                                                <input type="radio" name="radio" id="cash" checked="checked">
                                                <label for="cash"><span></span> Cash on Delivery</label>

                                                <div class="payment-details">
                                                    <p>Pay with cash upon delivery.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="single-payment">
                                            <div class="payment-radio radio">
                                                <input type="radio" name="radio" id="paypal">
                                                <label for="paypal"><span></span> Paypal <img class="payment" src="assets/images/payment.png" alt=""> <a href="#">What is PayPal?</a></label>

                                                <div class="payment-details">
                                                    <p>Pay via PayPal; you can pay with your credit card if you don’t have a PayPal account.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                -->
                                <div class="text-center mb-4">
                                    <span class="text-muted"><small><i>@lang('all.pay with')</i></small></span>
                                </div>
                                <div>
                                    <ul class="nav nav-tabs" id="payment-methods-tab" role="tablist">
                                        <li class="nav-item" role="presentation">
                                            <a data-method="stripe" class="nav-link active" id="pm-credit-card-tab" data-toggle="tab" href="#pm-credit-card" role="tab" aria-controls="pm-credit-card" aria-selected="true"> @lang('all.credit card')</a>
                                        </li>
                                        <li class="nav-item" role="presentation">
                                            <a data-method="paypal" class="nav-link" id="pm-paypal-tab" data-toggle="tab" href="#pm-paypal" role="tab" aria-controls="pm-paypal" aria-selected="false"> PayPal</a>
                                        </li>
                                        <li class="nav-item" role="presentation">
                                            <a data-method="cashondelivery" data-method_price="5" data-method_info="Costo del contrassegno" class="nav-link" id="pm-cashondelivery-tab" data-toggle="tab" href="#pm-cashondelivery" role="tab" aria-controls="pm-cashondelivery" aria-selected="false"> @lang('all.cash on delivery')</a>
                                        </li>

                                    </ul>

                                    @if ($errors->any())
                                        <div class="alert alert-danger mt-3">
                                            <ul class="list">
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif


                                    <div class="tab-content">

                                        <div class="tab-pane active" id="pm-credit-card" role="pm-credit-card" aria-labelledby="pm-credit-card-tab">
                                            <!-- PAY WITH STRIPE TAB -->
                                            <div id="new-card-form-wrapper" class="mt-4"></div>

                                            <div class="checkout-btn">
                                                <button id="card-button" type="submit" class="main-btn btn-block">@lang('all.confirm and pay') @fp($lead->grand_total)</button>
                                            </div>

                                        </div>
                                        <div class="tab-pane" id="pm-paypal" role="pm-paypal" aria-labelledby="pm-paypal-tab">
                                            <!-- PAY WITH PAYPAL TAB -->

                                            <div id="paypal-button-container" class="mt-4"></div>
                                        </div>
                                         <div class="tab-pane" id="pm-cashondelivery" role="pm-cashondelivery" aria-labelledby="pm-cashondelivery-tab">
                                            <!-- PAY WITH CASHONDELIVERY TAB -->

                                            <button type="submit" class="btn main-btn btn-block mt-4">@lang('all.confirm the order')</button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>

            @else

                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-info">
                            @lang('all.continue shopping, your cart is still empty')
                        </div>
                    </div>
                </div>

            @endif
        </div>

    </section>

@endsection
@if($products->count() > 0)
    @push('after-scripts')

        <style>

            label.error{
                color: red;
            }

        </style>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/localization/messages_it.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/places.js@1.19.0"></script>
        <script src="https://www.paypal.com/sdk/js?client-id={{ env('PAYPAL_CLIENT_ID') }}&currency=EUR&intent=capture"></script>
        <script src="https://js.stripe.com/v3/"></script>


        <script>

            var paymentStatus = false;
            var cartConfirmationForm = $('#cart-confirmation-form');
            var lead_id = {{ $lead->id }};
            var stripe,elements,cardElement,PayPalActions;
            var paymentMethod = 'stripe';
            var formIsValid = false;
            var grandTotal = {{ $lead->grand_total }};


            function removeDiscountCode(id){
                $.post('{{ Route('removeDiscountCode') }}',{id:id,_token:'{{ csrf_token() }}'},function(r){ if(r.status == 1){ location.reload() } },'json')
            }



            $("#cart-confirmation-form").validate({
                // Specify validation rules
                rules: {
                        "data[billing_address][name]": "required",
                        "data[billing_address][surname]": "required",
                        "data[billing_address][address]": "required",
                        "data[billing_address][telephone]": "required",
                        "data[billing_address][email]": { required: {
                             depends:function(){
                                $(this).val($.trim($(this).val()));
                                return true;
                            }
                        }, email:true},
                        "data[billing_address][fiscal_code]": "required",

                        /*
                        password: {
                            required: true,
                            minlength: 5
                        }*/
                },
                messages: {

                },
                submitHandler: function(form) {
                    //alert('Daje');

                    formIsValid = true;

                    switch(paymentMethod){
                        case 'stripe':
                            if(!paymentStatus){
                                payWithStripe();
                            }else{
                                form.submit();
                            }
                        break;
                        case 'paypal':
                            PayPalActions.order.capture().then(function(details) {
                                // This function shows a transaction success message to your buyer.
                                //alert('Transaction completed by ' + details.payer.name.given_name);
                                //cartConfirmationForm.submit();
                                form.submit();
                            });
                        break;
                        case 'cashondelivery':
                            form.submit();
                        break;
                    }

                }
            });




            function addDiscountCode(){
                var code = $('#new-discount-code').val();
                if(code != ''){
                    location.href = '{{ Route('addDiscountCode',['locale' => $locale]) }}/'+code;
                }else{
                    alert('@lang('all.before trying insert')');
                }

            }



            $('#payment-methods-tab a[data-toggle="tab"]').on('shown.bs.tab', function (e) {

                e.target // newly activated tab
                e.relatedTarget // previous active tab
                var method = $(e.target);
                var paymentMethodPriceRow = $('#paymentMethodPriceRow');
                var methodPrice = (typeof(method.data('method_price')) != 'undefined') ? parseFloat(method.data('method_price')) : 0;
                if(methodPrice > 0){
                    paymentMethodPriceRow.removeClass('d-none');
                    paymentMethodPriceRow.find('p').eq(0).html(method.data('method_info'));
                    paymentMethodPriceRow.find('p').eq(1).html('€'+methodPrice.toFixed(2));
                }else{
                    paymentMethodPriceRow.addClass('d-none');
                }
                $('#grandTotal').html('€'+(grandTotal+methodPrice).toFixed(2));
                paymentMethod = method.data('method');
                $('input[type="hidden"][name="pay_with"]').val(paymentMethod)

            })

            function enableBlackScreen(){

            }

            paypal.Buttons({
                createOrder: function(data, actions) {
                    return actions.order.create({
                        purchase_units: [{
                            amount: {
                                value: '{{ $lead->grand_total }}'
                            }
                        }]
                    });
                },
                onApprove: function(data, actions) {

                    PayPalActions = actions;
                    cartConfirmationForm.submit();
    /*

                    if(formIsValid){

                        alert('');

                    }else{
                        cartConfirmationForm.submit();
                    }

    */

                        /*
                        #FOR AUTHORIZATION VERSION
                        var authorizationID = authorization.purchase_units[0].payments.authorizations[0].id;
                        var orderID = data.orderID;
                        $('input[name="paypal_authorization_id"]').val(authorizationID);
                        $('input[name="paypal_order_id"]').val(orderID);
                        cartConfirmationForm.submit();

                        */

                }
            }).render('#paypal-button-container');

            function removeProduct(id){
                $.post('{{ Route('removeFromCart') }}',{
                    _token:'{{ csrf_token() }}',
                    id:id
                },function(r){
                    if(r.status == 1){
                        location.reload();
                    }
                },'json');
            }


            $(function(){
                {{--
                $('input[data-name]').blur(function(){
                    $.post('{{ Route('setCartField') }}',{
                        _token:'{{ csrf_token() }}',
                        name:$(this).data('name'),
                        value:$(this).val()
                    })
                })
                --}}

                const billingAddressInput = document.getElementById("billing-address-input");
                const billingAddressAutocomplete = new google.maps.places.Autocomplete(billingAddressInput);
                setTimeout(function(){
                    $('#billing-address-input').attr('autocomplete','my-custom-field-name')
                },1000);
                billingAddressAutocomplete.addListener("place_changed", () => {
                        const place = billingAddressAutocomplete.getPlace();
                        var addressData = new Object();
                        $.each(place.address_components, function(index,item){
                            var data = {
                                long_name : item.long_name,
                                short_name : item.short_name
                            }
                            addressData[item.types[0]] = data
                        });
                        addressData['formatted_address'] = place.formatted_address;
                        console.log(addressData);
                        $('input[type="hidden"][name="billing_address_json"]').val(JSON.stringify(addressData));
                });


                const shippingAddressInput = document.getElementById("shipping-address-input");
                const shippingAddressAutocomplete = new google.maps.places.Autocomplete(shippingAddressInput);
                setTimeout(function(){
                    $('#shipping-address-input').attr('autocomplete','my-custom-field-name')
                },1000);
                shippingAddressAutocomplete.addListener("place_changed", () => {
                        const place = shippingAddressAutocomplete.getPlace();
                        var addressData = new Object();
                        $.each(place.address_components, function(index,item){
                            var data = {
                                long_name : item.long_name,
                                short_name : item.short_name
                            }
                            addressData[item.types[0]] = data
                        });
                        addressData['formatted_address'] = place.formatted_address;
                        console.log(addressData);
                        $('input[type="hidden"][name="shipping_address_json"]').val(JSON.stringify(addressData));
                });



    /*
                var billingAddressPlacesAutocomplete = places({
                    appId: '{{ env('ALGOLIA_ADDRESS_APP_ID') }}',
                    apiKey: '{{ env('ALGOLIA_ADDRESS_SECRET') }}',
                    container: document.querySelector('#billing-address-input')
                });

                var shippingAddressPlacesAutocomplete = places({
                    appId: '{{ env('ALGOLIA_ADDRESS_APP_ID') }}',
                    apiKey: '{{ env('ALGOLIA_ADDRESS_SECRET') }}',
                    container: document.querySelector('#shipping-address-input')
                });
    */

                stripe = Stripe('{{ env('STRIPE_KEY') }}');
                elements = stripe.elements();

                cardElement = elements.create('card');
                cardElement.mount('#new-card-form-wrapper');
            })
            async function payWithStripe(){
    /*
                placesAutocomplete.on('change', e => {
                    console.log(e);
                });
    */

                    const clientSecret = '{{ $client_secret_intent->client_secret }}';

                    //cardButton.addEventListener('click', async (e) => {

                    const { setupIntent, error } = await stripe.confirmCardPayment(
                        clientSecret, {
                            payment_method: {
                                card: cardElement,
                                billing_details: {
                                    name: $('input[name="data[billing_address][name]"]').val()+ ' '+$('input[name="data[billing_address][surname]"]').val()
                                }
                            }
                        }
                    );
                    if (error) {
                        console.log(error);
                    } else {
                        paymentStatus = true;
                        cartConfirmationForm.submit();
                    }
                    //});


            }
        </script>

    @endpush
@endif
