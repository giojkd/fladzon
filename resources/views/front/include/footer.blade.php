
    <!--====== Footer Start ======-->

    <footer class="footer-area pt-50 pb-55">
        <div class="footer-widget">
            <div class="container footer-container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="footer-logo-copyright mt-30">
                            <a href="index.html">
                                <img style="height:80px;" src="{{ asset('resources/logo_light.png') }}" alt="Logo">
                                <!--lid {{ $lead->id }}-->
                            </a>
                            {{--
                            <p>&copy; Copyright 2020 <a href="https://hasthemes.com/">HasThemes</a></p>
                             --}}
                        </div>
                        <div class="footer-social mt-30">
                            <ul class="social">
                                @if($defaults['facebook'] != '') <li><a target="_blank" href="{{ $defaults['facebook'] }}"><i class="fab fa-facebook-f"></i></a></li> @endif
                                @if($defaults['instagram'] != '') <li><a target="_blank" href="{{ $defaults['instagram'] }}"><i class="fab fa-instagram"></i></a></li> @endif
                                @if($defaults['twitter'] != '') <li><a target="_blank" href="{{ $defaults['twitter'] }}"><i class="fab fa-twitter"></i></a></li> @endif
                                @if($defaults['youtube'] != '') <li><a target="_blank" href="{{ $defaults['youtube'] }}"><i class="fab fa-youtube"></i></a></li> @endif
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="footer-link-wrapper flex-wrap">


                                @foreach($menus[2] as $menu)
                                    @if(!is_null($menu->items))
                                        <div class="footer-link mt-30">
                                            <h5 class="footer-title">{{$menu->name}}</h5>
                                            <ul class="link">
                                                @foreach($menu->items as $item)
                                                    <li class="@if(url()->current() == url($item->href)) active @endif"><a href="{{ $item->href }}">{{ $item->name }}</a></li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                @endforeach

                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="footer-map mt-30">
                            <div id="contact-map"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!--====== Footer Ends ======-->

    <!--====== BACK TOP TOP PART START ======-->

    <a href="#" class="back-to-top"><i class="fal fa-chevron-up"></i></a>

    <!--====== BACK TOP TOP PART ENDS ======-->

    <!--====== Product Quick View Start ======-->

    <div class="modal fade" id="productQuick">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fal fa-times"></i></button>
                </div>

                <div class="product-quick-view">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="quick-view-image">
                                <div class="quick-view-thumb">
                                    <div class="quick-thumb-active">
                                        <div class="single-quick-thumb">
                                            <img src="assets/images/product/product-37.jpg" alt="">
                                        </div>
                                        <div class="single-quick-thumb">
                                            <img src="assets/images/product/product-38.jpg" alt="">
                                        </div>
                                        <div class="single-quick-thumb">
                                            <img src="assets/images/product/product-27.jpg" alt="">
                                        </div>
                                        <div class="single-quick-thumb">
                                            <img src="assets/images/product/product-28.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="quick-view-preview">
                                    <div class="quick-preview-active">
                                        <div class="single-quick-preview">
                                            <img src="assets/images/product/product-37.jpg" alt="">
                                        </div>
                                        <div class="single-quick-preview">
                                            <img src="assets/images/product/product-38.jpg" alt="">
                                        </div>
                                        <div class="single-quick-preview">
                                            <img src="assets/images/product/product-27.jpg" alt="">
                                        </div>
                                        <div class="single-quick-preview">
                                            <img src="assets/images/product/product-28.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="quick-view-content">
                                <h4 class="title">Oversized Check Dress</h4>
                                <span class="sku-id">REF. 1104693 - TOMY</span>

                                <div class="quick-price">
                                    <span class="regular-price">£250.00</span>
                                    <span class="sale-price">£200.00</span>
                                </div>
                                <div class="quick-quantity-cart-wishlist-compare flex-wrap">
                                   <form action="#">
                                        <div class="quick-quantity d-flex">
                                            <button type="button" class="sub"><i class="fal fa-minus"></i></button>
                                            <input type="text" value="1" />
                                            <button type="button" class="add"><i class="fal fa-plus"></i></button>
                                        </div>
                                        <div class="quick-cart">
                                            <button class="main-btn">Add to Cart</button>
                                        </div>
                                   </form>
                                   <a href="#" data-tooltip="tooltip" data-placement="top" title="Add to Wishlist" class="quick-wishlist"><i class="fal fa-heart"></i></a>
                                   <a href="#" data-tooltip="tooltip" data-placement="top" title="Add to Compare" class="quick-compare"><i class="fal fa-repeat-alt"></i></a>
                                </div>
                                <div class="quick-description">
                                    <p>Sed vitae eros a quam malesuada porttitor nec nec orci. Ut lacus augue, bibendum at tristique at, ornare eget quam. Donec volutpat ut nibh id sagittis. Morbi fringilla ac libero in consequat.</p>
                                </div>
                                <div class="quick-share">
                                    <ul class="social">
                                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fab fa-linkedin"></i></a></li>
                                        <li><a href="#"><i class="fab fa-pinterest-p"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--====== Product Quick View Ends ======-->

    <!--====== Start ======-->

<!--
    <section class="">
        <div class="container">
            <div class="row">
                <div class="col-lg-">

                </div>
            </div>
        </div>
    </section>
-->

    <!--====== Ends ======-->

    <!--====== Overlay Start ======-->

    <div class="overlay"></div>

    <!--====== Overlay Ends ======-->




    <!--====== Jquery js ======-->
    <script src="{{ asset('assets/js/vendor/jquery-3.5.1.min.js') }}"></script>
    <script src="{{ asset('assets/js/vendor/modernizr-3.7.1.min.js') }}"></script>

    <!--====== All Plugins js ======-->
    <script src="{{ asset('assets/js/plugins/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/popper.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/slick.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/modernizr.custom.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery.dlmenu.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery.paroller.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery.countdown.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/select2.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/photoswipe.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/photoswipe-ui-default.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery.elevateZoom.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/masonry.pkgd.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery.appear.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery.sticky-sidebar.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/imagesloaded.pkgd.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/ajax-contact.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/parallax.min.js') }}"></script>

    <!-- <script src="assets/js/plugins/parallax.min.js"></script> -->

    <!--====== Use the minified version files listed below for better performance and remove the files listed above ======-->

    <!-- <script src="assets/js/plugins.min.js"></script> -->


    <!--====== Main Activation  js ======-->
    <script src="{{ asset('assets/js/main.js').'?v='.time() }}"></script>

    <!--====== Google Map js ======-->

    <script src="https://maps.googleapis.com/maps/api/js?key={{ config('services.google_places.key') }}&libraries=places"></script>

    <script>
        var coordinates = @json($defaults['coordinates']);
    </script>

    <script src="{{ asset('assets/js/map-script.js') }}"></script>
    @stack('after-scripts')




</body>

</html>
