@include(env('THEME_NAME').'.include.head')


    <!--====== preloader Start ======-->

    <div class="preloader">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <!--====== preloader Ends ======-->

    <!--====== Header Start ======-->

    <header class="header-area">

        <div class="header-navbar">
            <div class="container-fluid custom-container">
                <div class="header-wrapper d-flex justify-content-between align-items-center">

                    <div class="header-logo">
                        <a href="/">
                            <img style="height:60px " src="{{ asset('resources/logo_dark.png') }}" alt="Logo">
                        </a>
                    </div>

                    <div class="header-menu site-nav d-none d-lg-block">
                        <ul class="main-menu">
                            @foreach($menus[1]->first()->items as $item)
                                <li class="@if(url()->current() == url($item->href)) active @endif"><a href="{{ $item->href }}">{{ $item->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="header-meta">
                        <ul class="meta">
                            {{--
                            <li><a class="cart-toggle" href="javascript:void(0)"><i class="far fa-Shopping-cart"></i><span>3</span></a></li>
                            <li><a class="search-toggle" href="javascript:void(0)"><i class="far fa-search"></i></a></li>
                            <li><a class="sidebar-toggle" href="javascript:void(0)"><i class="fal fa-bars"></i></a></li>
                             --}}
                             <li><a href="{{ Route('cart',['locale' => $locale]) }}"><i class="far fa-Shopping-cart"></i><span>{{ $lead->products->count() }}</span></a></li>
                            <li><a href="{{ Route('productGrid',['locale'=>$locale]) }}"><i class="far fa-search"></i></a></li>
                            <li><a class="sidebar-toggle" href="javascript:void(0)"><i class="fal fa-bars"></i></a></li>
                        </ul>
                        <small class=""><span class="text-danger">Consegna gratis da €{{ (int)$cartTotalBeforeFreeShipping }}!</span></small>
                    </div>

                </div>
            </div>

            <div id="dl-menu" class="dl-menuwrapper d-lg-none">
                <button class="dl-trigger all-pages"></button>

                <ul class="dl-menu">
                    @foreach($menus[1]->first()->items as $item)
                        <li><a href="{{ $item->href }}">{{ $item->name }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>

    </header>

    <!--====== Header Ends ======-->

    <!--====== Search Start ======-->

    <div class="search-wrapper">
        <div class="search-box">
            <a href="javascript:void(0)" class="search-close"><i class="fal fa-times"></i></a>
            <div class="search-form">
                <label>Start typing and press Enter to search</label>
                <div class="search-input">
                    <form action="#">
                        <input type="text" placeholder="Search entire store…">
                        <button><i class="far fa-search"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!--====== Search Ends ======-->

    @include(env('THEME_NAME').'.include.off_canvas_cart')

    @include(env('THEME_NAME').'.include.off_canvas_sidebar')


