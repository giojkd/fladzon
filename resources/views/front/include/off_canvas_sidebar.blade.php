 <!--====== Off Canvas Sidebar Start ======-->

    <div class="off-canvas-sidebar">
        <div class="off-canvas-sidebar-wrapper">
            <a class="sidebar-close" href="javascript:void(0)"><i class="fal fa-times"></i></a>
            <div class="off-canvas-sidebar-box">
                <a class="logo" href="/">
                    <img style="height: 80px" src="{{ asset('resources/logo_dark.png') }}" alt="Logo">
                </a>
                <p class="text">{{ $defaults['motto'] }}</p>
                <ul class="sidebar-social">
                    @if($defaults['facebook'] != '') <li><a target="_blank" href="{{ $defaults['facebook'] }}"><i class="fab fa-facebook-f"></i></a></li> @endif
                    @if($defaults['instagram'] != '') <li><a target="_blank" href="{{ $defaults['instagram'] }}"><i class="fab fa-instagram"></i></a></li> @endif
                    @if($defaults['twitter'] != '') <li><a target="_blank" href="{{ $defaults['twitter'] }}"><i class="fab fa-twitter"></i></a></li> @endif
                    @if($defaults['youtube'] != '') <li><a target="_blank" href="{{ $defaults['youtube'] }}"><i class="fab fa-youtube"></i></a></li> @endif
                </ul>
                {{--
                <div class="sidebar-image">
                    <img src="assets/images/aside-image.jpg" alt="">
                </div>
                --}}
                <ul class="sidebar-info">
                    <li>
                        <div class="single-info">
                            <div class="info-icon">
                                <i class="fas fa-phone"></i>
                            </div>
                            <div class="info-content">
                                <p><a href="tel:{{ $defaults['telephone'] }}">{{ $defaults['telephone'] }}</a></p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="single-info">
                            <div class="info-icon">
                                <i class="fas fa-envelope"></i>
                            </div>
                            <div class="info-content">
                                <p><a href="mailto:{{ $defaults['email'] }}">{{ $defaults['email'] }}</a></p>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="single-info">
                            <div class="info-icon">
                                <i class="fas fa-map-marker-alt"></i>
                            </div>
                            <div class="info-content">
                                <p>{{ $defaults['address'] }}</p>
                            </div>
                        </div>
                    </li>
                </ul>
                {{-- <p class="copyright">&copy; Copyright 2020 Created  <a href="https://hasthemes.com/">HasThemes</a></p>  --}}
            </div>
        </div>
    </div>

    <!--====== Off Canvas Sidebar Ends ======-->
