@extends('admin_custom.top_left')



@section('content')

<h2>
    <span class="text-capitalize">Gestisci le disponibilità</span>
</h2>

<ul class="nav nav-tabs" role="tablist">
    @foreach ($warehouses as $index => $warehouse)
        <li class="nav-item" role="presentation">
            <a data-method="stripe" class="nav-link @if($warehouse->id == $filters['warehouse_id']) active @endif" href="{{ Route('inventory') }}?warehouse_id={{ $warehouse->id }}" role="tab" aria-controls="#warehouse-{{ $filters['warehouse_id'] }}" aria-selected="true"> {{ $warehouse->name }}</a>
        </li>
    @endforeach
</ul>

<div class="tab-content">
    <div class="tab-pane active" id="warehouse-{{ $filters['warehouse_id'] }}" role="#warehouse-{{ $filters['warehouse_id']}}" aria-labelledby="#warehouse-{{ $filters['warehouse_id'] }}">
        <div class="table-responsive">
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th></th>
                        <th>ExId</th>
                        <th>Name</th>
                        <th>Quantity</th>
                    </tr>
                </thead>
                <tbody>
                        @foreach ($productgroups as $productgroup)
                        <tr>
                            <td>
                                <img data-src="{{ $productgroup->product->cover(150) }}" alt="" class="lazyload">
                            </td>
                            <td>{{ $productgroup->ext_id }}</td>
                            <td>{{ $productgroup->product->name }}</td>
                            <td>Tot: ({{ $productgroup->productgroup_quantity }})</td>

                        </tr>
                        @foreach ($productgroup->products as $product)
                                <tr>
                                    <td>
                                        <td></td>
                                        <td>{{ $product->attributes_line }} | {{ $product->sku }}</td>
                                        <td><input data-warehouse_id="{{ $filters['warehouse_id'] }}" data-product_id="{{ $product->id }}" value="{{ $product->getQuantityByWarehouseId($filters['warehouse_id']) }}" type="number" class="form-control quantity-updater"></td>
                                    </td>
                                </tr>
                            @endforeach

                        @endforeach




                </tbody>
            </div>
        </table>
    </div>
</div>


@endsection

@push('after_scripts')

    <script>
        $(function(){
            /*$('.quantity-updater').blur(function(){
                setWarehouseProductQuantity($(this).data('warehouse_id'),$(this).data('product_id'),parseFloat($(this).val()))
            });*/
            $('.quantity-updater').change(function(){
                setWarehouseProductQuantity($(this).data('warehouse_id'),$(this).data('product_id'),parseFloat($(this).val()))
            });
        });

        function setWarehouseProductQuantity(warehouse_id,product_id,quantity){
            $.post('{{ Route('inventoryUpdate') }}',{warehouse_id,product_id,quantity},function(r){
                console.log(r);
            },'json')
        }

    </script>

@endpush
