@extends('admin_custom.top_left')



@section('content')
    <h1>Prepara le spedizioni per l'ordine {{ $lead->id }}</h1>
    <form action="{{ Route('shippingsManagerStore') }}" method="POST">

        @csrf
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th></th>
                    <th>Prodotto</th>
                    <th>Quantità</th>
                    <th>Spedizioni</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($lead->leadproducts as $leadproduct)
                    <tr>
                        <td>
                            <img src="{{  Route('ir',['size' => 'h75','filename' => collect($leadproduct->product->photos)->first()])  }}" alt="">
                        </td>
                        <td>
                            {{ $leadproduct->product->sku }}
                            {{ $leadproduct->product->name }}
                        </td>
                        <td>
                            {{ $leadproduct->quantity }}
                        </td>
                        <td>
                            <table class="table table-striped table-hover" id="shipping-rows-{{ $leadproduct->id }}" data-to_assign="{{ $leadproduct->quantity }}">
                                @php
                                    $assigned = 0;
                                @endphp
                                @foreach ($leadproduct->product->warehouseavailabilities as $wa)
                                    @php
                                        $leftToAssign = $leadproduct->quantity - $assigned;
                                        $newAssignment = ($leftToAssign < $wa->quantity ) ? $leftToAssign : $wa->quantity;
                                        $assigned += $newAssignment;
                                        $selectedQuantity = (isset($shippings[$wa->warehouse_id][$leadproduct->id])) ? $shippings[$wa->warehouse_id][$leadproduct->id] :  $newAssignment;
                                    @endphp
                                    <tr>
                                        <td>
                                            {{ $wa->warehouse->name }} (disp: {{ $wa->quantity }})
                                        </td>
                                        <td>
                                            <select
                                            name="shippingrows[{{ $lead->id }}][{{ $wa->warehouse->id  }}][{{ $leadproduct->id }}]"
                                            class="shipping-row form-control shipping-row-{{  $leadproduct->id  }}"
                                            data-id="{{ $leadproduct->id }}">
                                                @for ($i = 0; $i <= $wa->quantity; $i++)
                                                    <option @if($i ==  $selectedQuantity) selected @endif value="{{ $i }}">{{ $i }}</option>
                                                @endfor
                                            </select>
                                        </td>
                                    </tr>
                                @endforeach
                            </table>
                            <div class="d-none" id="error-row-{{ $leadproduct->id }}">
                                <div class="alert alert-danger"></div>
                            </div>
                            @if ( $leadproduct->quantity > $leadproduct->product->warehouseavailabilities->sum('quantity') )
                                <div class="alert alert-danger unassignable-rows" >
                                    Impossibile assegnare {{ ($leadproduct->quantity - $leadproduct->product->warehouseavailabilities->sum('quantity')) }} prodotti <a class="btn btn-sm btn-secondary" target="_blank" href="/admin/stockoperation/create">aggiungi disp</a>
                                </div>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        <hr>

        @if (!$lead->invoice_request)
            <div class="row">
                <div class="col-md-12">

                        <div class="form-group">
                            <label for="">Emette lo scontrino</label>
                            <select name="emits_receipt_warehouse_id" id="" class="form-control">
                                <option value="">Non emettere lo scontrino</option>
                                @foreach ($lead->availableWarehouses() as $item)
                                    <option value=" {{ $item->id }}  ">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>


                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label><input type="checkbox" name="abroad_invoice" value="1"> Fattura estero</label>
                    </div>
                </div>
            </div>
        @endif

        <button class="btn btn-primary btn-block" type="submit">Salva</button>

    </form>



@endsection

{{-- emits_receipt_warehouse_id --}}


@push('custom_styles')
@endpush

@push('before_closing_body')
@endpush

@push('after_scripts')
    <script>



        function checkAssignemntForRow(){
            var countErrors = 0;
            countErrors+=$('.unassignable-rows').length;
            $('.shipping-row').each(function(){
                var id = $(this).data('id');
                var toAssign = parseInt($('#shipping-rows-'+id).data('to_assign'));
                var assigned = 0;
                $('.shipping-row-'+id).each(function(){
                    assigned += parseInt($(this).val());
                });
                if(toAssign != assigned){
                    countErrors++;
                    $('#error-row-'+id).removeClass('d-none').find('.alert').html('Hai assegnato '+assigned+' su '+toAssign);
                }else{
                    $('#error-row-'+id).addClass('d-none')
                }
            });

            if(countErrors > 0){
                $('button[type="submit"]').hide();
            }else{
                $('button[type="submit"]').show();
            }
        }

        $('.shipping-row').change(function(){
            checkAssignemntForRow();
        })

        $(function(){
            checkAssignemntForRow();
        })

    </script>
@endpush
