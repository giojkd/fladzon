@extends('print_default')
@section('content')

    <table class="table table-striped">
         <tr>
            <td colspan="2">
                <b>
                    Ordine {{ $lead->id }} del {{ date('d/m/Y',strtotime($lead->confirmed_at)) }}
                    @if ($lead->has_requested_invoice)
                        richiesta fattura
                    @endif
                    pagato con {{ $lead->pay_with }}
                </b>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <b>Indirizzo di fatturazione</b>
            </td>
        </tr>
        <tr>
            <td>Nome</td>
            <td>{{ (isset($lead->billing_address['name'])) ? $lead->billing_address['name'] : '' }}</td>
        </tr>
        <tr>
            <td>Cognome</td>
            <td>{{ (isset($lead->billing_address['surname'])) ? $lead->billing_address['surname'] : '' }}</td>
        </tr>
        <tr>
            <td>Email</td>
            <td>{{ (isset($lead->billing_address['email'])) ? $lead->billing_address['email'] : '' }}</td>
        </tr>
        <tr>
            <td>Indirizzo</td>
            <td>{{ (isset($lead->billing_address['address'])) ? $lead->billing_address['address'] : '' }}</td>
        </tr>
        <tr>
            <td>Telefono</td>
            <td>{{ (isset($lead->billing_address['telephone'])) ? $lead->billing_address['telephone'] : '' }}</td>
        </tr>
        <tr>
            <td>Codice SDI</td>
            <td>{{ (isset($lead->billing_address['vat_number'])) ? $lead->billing_address['vat_number'] : '' }}</td>
        </tr>
        <tr>
            <td>Partita iva</td>
            <td>{{ (isset($lead->billing_address['sdi_code'])) ? $lead->billing_address['sdi_code'] : '' }}</td>
        </tr>
        <tr>
            <td>Codice fiscale</td>
            <td>{{ (isset($lead->billing_address['fiscal_code'])) ? $lead->billing_address['fiscal_code'] : '' }}</td>
        </tr>
        <tr>
            <td>Ragione sociale</td>
            <td>{{ (isset($lead->billing_address['company_name'])) ? $lead->billing_address['company_name'] : '' }}</td>
        </tr>
        @if (!is_null($lead->shipping_address))
            <tr>
                <td colspan="2">
                    <b>Indirizzo di spedizione</b>
                </td>
            </tr>
            <tr>
                <td>Nome</td>
                <td>{{ (isset($lead->shipping_address['name'])) ? $lead->shipping_address['name'] : '' }}</td>
            </tr>
            <tr>
                <td>Cognome</td>
                <td>{{ (isset($lead->shipping_address['surname'])) ? $lead->shipping_address['surname'] : '' }}</td>
            </tr>
            <tr>
                <td>Email</td>
                <td>{{ (isset($lead->shipping_address['email'])) ? $lead->shipping_address['email'] : '' }}</td>
            </tr>
            <tr>
                <td>Indirizzo</td>
                <td>{{ (isset($lead->shipping_address['address'])) ? $lead->shipping_address['address'] : '' }}</td>
            </tr>
            <tr>
                <td>Telefono</td>
                <td>{{ (isset($lead->shipping_address['telephone'])) ? $lead->shipping_address['telephone'] : '' }}</td>
            </tr>
        @endif
    </table>
    <table class="table table-striped">
        <tr>
            <td colspan="6"><b>Prodotti</b></td>
        </tr>
        @foreach ($lead->products as $product)
                <tr>
                    <td colspan="6">
                        <img height="160" src="{{ $product->pdfCover() }}" alt="">
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>{{ $product->sku }}</td>
                    <td>{{ $product->name }} {{ $product->attributes_line }}</td>
                    <td>@fp($product->pivot->sub_total / $product->pivot->quantity)</td>
                    <td>{{ $product->pivot->quantity }}</td>
                    <td class="text-right">@fp($product->pivot->sub_total )</td>
                </tr>
        @endforeach
        <tr>
            <td colspan="2" class="text-right"></td>
            <td>Totale prodotti</td>
            <td class="text-right">@fp($lead->products_total)</td>
            <td>Totale spedizione</td>
            <td class="text-right">@fp($lead->shipping_total)</td>
        </tr>

        <tr>
            <td colspan="2" class="text-right"></td>
            <td>Totale sconti</td>
            <td class="text-right">@fp($lead->discount_total)</td>
            <td>Totale ordine</td>
            <td class="text-right">@fp($lead->grand_total)</td>
        </tr>

    </table>
@endsection
