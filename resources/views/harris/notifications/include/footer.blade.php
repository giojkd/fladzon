 {{-- <table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
      <tbody>
        <tr>
          <td class="o_bg-white" style="font-size: 24px;line-height: 24px;height: 24px;background-color: #ffffff;">&nbsp; </td>
        </tr>
      </tbody>
    </table>  --}}
    <!-- footer-light -->
    <table width="100%" cellspacing="0" cellpadding="0" border="0" role="presentation">
      <tbody>
        <tr>
          <td class="o_bg-light o_px-md o_py-lg" align="center" style="background-color: #dbe5ea;padding-left: 24px;padding-right: 24px;padding-top: 32px;padding-bottom: 32px;">
            <!--[if mso]><table width="584" cellspacing="0" cellpadding="0" border="0" role="presentation"><tbody><tr><td align="center"><![endif]-->
            <div class="o_col-6s o_sans o_text-xs o_text-light o_center" style="font-family: Helvetica, Arial, sans-serif;margin-top: 0px;margin-bottom: 0px;font-size: 14px;line-height: 21px;max-width: 584px;color: #82899a;text-align: center;">
              <p class="o_mb" style="margin-top: 0px;margin-bottom: 16px;"><a class="o_text-primary" href="{{ env('APP_URL') }}" style="text-decoration: none;outline: none;color: #126de5;"><img src="{{ env('LOGO_DARK') }}" height="40" alt="{{ env('APP_NAME') }}" style="max-height: 40px;-ms-interpolation-mode: bicubic;vertical-align: middle;border: 0;line-height: 100%;height: auto;outline: none;text-decoration: none;"></a></p>
              <p class="o_mb" style="margin-top: 0px;margin-bottom: 16px;">©{{ date('Y') }} {{ env('COMPANY_NAME') }}</p>
              <p class="o_mb" style="margin-top: 0px;margin-bottom: 16px;">
                {{ env('COMPANY_ADDRESS') }}
              </p>
              <!--
              <p style="margin-top: 0px;margin-bottom: 0px;">
                <a class="o_text-light o_underline" href="https://example.com/" style="text-decoration: underline;outline: none;color: #82899a;">Help Center</a> <span class="o_hide-xs">&nbsp; • &nbsp;</span><br class="o_hide-lg" style="display: none;font-size: 0;max-height: 0;width: 0;line-height: 0;overflow: hidden;mso-hide: all;visibility: hidden;">
                <a class="o_text-light o_underline" href="https://example.com/" style="text-decoration: underline;outline: none;color: #82899a;">Preferences</a> <span class="o_hide-xs">&nbsp; • &nbsp;</span><br class="o_hide-lg" style="display: none;font-size: 0;max-height: 0;width: 0;line-height: 0;overflow: hidden;mso-hide: all;visibility: hidden;">
                <a class="o_text-light o_underline" href="https://example.com/" style="text-decoration: underline;outline: none;color: #82899a;">Unsubscribe</a>
              </p>
            -->
            </div>
            <!--[if mso]></td></tr></table><![endif]-->
            <div class="o_hide-xs" style="font-size: 64px; line-height: 64px; height: 64px;">&nbsp; </div>
          </td>
        </tr>
      </tbody>
    </table>
  </body>
</html>
