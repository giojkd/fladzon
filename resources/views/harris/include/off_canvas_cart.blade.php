<!--====== Off Canvas Cart Start ======-->

    <div class="off-canvas-cart-wrapper">
        <div class="off-canvas-cart-box">
            <a href="javascript:void(0)" class="cart-close"><i class="fal fa-times"></i></a>
            <div class="off-canvas-cart-content">
                <div class="cart-title">
                    <h5 class="title">Shopping Cart</h5>
                </div>
                <div class="cart-product-widget">
                    <ul>
                        <li>
                            <div class="cart-product d-flex">
                                <div class="cart-product-image">
                                    <a href="product-simple-01.html"><img src="assets/images/cart/product-1.jpg" alt="product"></a>
                                </div>
                                <div class="cart-product-content media-body">
                                    <h6 class="title"><a href="product-simple-01.html">Biker Jacket</a></h6>
                                    <span class="price">1x <span>£150.00</span></span>
                                </div>
                                <a href="#" class="product-cancel"><i class="fal fa-times"></i></a>
                            </div>
                        </li>
                        <li>
                            <div class="cart-product d-flex">
                                <div class="cart-product-image">
                                    <a href="product-simple-01.html"><img src="assets/images/cart/product-2.jpg" alt="product"></a>
                                </div>
                                <div class="cart-product-content media-body">
                                    <h6 class="title"><a href="product-simple-01.html">Biker Jacket</a></h6>
                                    <span class="price">1x <span>£150.00</span></span>
                                </div>
                                <a href="#" class="product-cancel"><i class="fal fa-times"></i></a>
                            </div>
                        </li>
                        <li>
                            <div class="cart-product d-flex">
                                <div class="cart-product-image">
                                    <a href="product-simple-01.html"><img src="assets/images/cart/product-3.jpg" alt="product"></a>
                                </div>
                                <div class="cart-product-content media-body">
                                    <h6 class="title"><a href="product-simple-01.html">Biker Jacket</a></h6>
                                    <span class="price">1x <span>£150.00</span></span>
                                </div>
                                <a href="#" class="product-cancel"><i class="fal fa-times"></i></a>
                            </div>
                        </li>
                    </ul>
                    <div class="cart-product-total">
                        <p class="value">Subtotal</p>
                        <p class="price">£600.00</p>
                    </div>
                    <div class="cart-product-btn">
                        <a href="#" class="main-btn btn-block">View cart</a>
                        <a href="#" class="main-btn btn-block">Checkout</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--====== Off Canvas Cart Ends ======-->
