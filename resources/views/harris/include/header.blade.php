@include(env('THEME_NAME').'.include.head')


    <!--====== preloader Start ======-->

    <div class="preloader">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <!--====== preloader Ends ======-->

    <!--====== Header Start ======-->

 <div class="header-wrapper">
     <header class="header-area">
    {{--
        <div class="header-top">
            <div class="container">
                <div class="header-content text-center">
                    <p><span>End of Season</span> Sale off 50%</p>
                </div>

            </div>
        </div>
         --}}
        <div class="header-banner d-none d-md-block">
            @lang('all.banner content')
        </div>
        <div class="header-navbar-8 d-none d-lg-block">
            <div class="header-navbar-main">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-3">
                            <div class="header-logo">
                                <a href="{{ url(App::getLocale()) }}">

                                    <img style="height: 40px" src="{{ asset('resources/logo_dark_harris.png') }}" alt="Logo">
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <form action="{{ Route('productGrid',['locale' => $locale]) }}">
                                <div class="header-search d-flex">

                                    <div class="search-header-input">
                                        <input name="keyword" type="text" placeholder="@lang('all.start searching here')...">
                                    </div>
                                    <div class="search-header-wrapper">
                                        <button type="submit"><i class="far fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-lg-3">
                            <div class="header-meta">
                                <ul class="meta">

                                    <li>
                                        <div class="account-dropdown">
                                            <a href="#"><i class="fas fa-globe"></i> {{ strtoupper($locale) }}</a>
                                            <ul class="dropdown-account">
                                                @foreach ($locales as $locale_ => $label)
                                                    @if($locale_ != $locale)
                                                        <li><a href="{{ url($locale_) }}">{{ $label }}</a></li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div>
                                    </li>

                                    <li>
                                        <a class="{{-- cart-toggle --}}" href="{{ Route('cart',['locale' => $locale]) }}">
                                            <i class="far fa-Shopping-cart"></i> <span>{{ $lead->products->count() }}</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="header-navbar-main-menu">
                <div class="container">
                    <div class="header-menu header-menu-3 site-nav d-none d-lg-flex">
                        {{--
                        <ul class="menu-category">
                            <li>
                                <a href="#">Shop by Categories <i class="fas fa-caret-down"></i></a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="shop-sidebar.html">Man</a>
                                        <ul class="sub-menu menu-man">
                                            <li>
                                                <div class="menu-menu-items menu-product-active">
                                                    <div class="single-product">
                                                        <div class="product-image">
                                                            <div class="image">
                                                                <img class="product-1" src="assets/images/product/product-23.jpg" alt="product">
                                                                <a class="link" href="product-simple-01.html"></a>
                                                            </div>
                                                            <ul class="product-meta text-center">
                                                                <li><a data-tooltip="tooltip" data-placement="top" title="Add to Cart" href="#"><i class="fal fa-Shopping-cart"></i></a></li>
                                                                <li><a data-tooltip="tooltip" data-placement="top" title="Quick Shop" data-toggle="modal" data-target="#productQuick" href="#"><i class="fal fa-search-plus"></i></a></li>
                                                                <li><a data-tooltip="tooltip" data-placement="top" title="Add to Wishlist" href="#"><i class="fal fa-heart"></i></a></li>
                                                                <li><a data-tooltip="tooltip" data-placement="top" title="Add to Compare" href="#"><i class="fal fa-repeat-alt"></i></a></li>
                                                            </ul>
                                                        </div>
                                                        <div class="product-content">
                                                            <div class="product-title">
                                                                <h4 class="title"><a href="product-simple-01.html">Trousers With Side Stripe</a></h4>
                                                            </div>
                                                            <div class="product-price">
                                                                <span class="price">£150.00</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="single-product">
                                                        <div class="product-image">
                                                            <div class="image">
                                                                <img class="product-1" src="assets/images/product/product-29.jpg" alt="product">
                                                                <a class="link" href="product-simple-01.html"></a>
                                                            </div>
                                                            <ul class="product-meta text-center">
                                                                <li><a data-tooltip="tooltip" data-placement="top" title="Add to Cart" href="#"><i class="fal fa-Shopping-cart"></i></a></li>
                                                                <li><a data-tooltip="tooltip" data-placement="top" title="Quick Shop" data-toggle="modal" data-target="#productQuick" href="#"><i class="fal fa-search-plus"></i></a></li>
                                                                <li><a data-tooltip="tooltip" data-placement="top" title="Add to Wishlist" href="#"><i class="fal fa-heart"></i></a></li>
                                                                <li><a data-tooltip="tooltip" data-placement="top" title="Add to Compare" href="#"><i class="fal fa-repeat-alt"></i></a></li>
                                                            </ul>
                                                        </div>
                                                        <div class="product-content">
                                                            <div class="product-title">
                                                                <h4 class="title"><a href="product-simple-01.html">Suit jacket – ink blue</a></h4>
                                                            </div>
                                                            <div class="product-price">
                                                                <span class="regular-price">£150.00</span>
                                                                <span class="sale-price">£150.00</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="single-product">
                                                        <div class="product-image">
                                                            <div class="image">
                                                                <img class="product-1" src="assets/images/product/product-9.jpg" alt="product">
                                                                <a class="link" href="product-simple-01.html"></a>
                                                            </div>
                                                            <ul class="product-meta text-center">
                                                                <li><a data-tooltip="tooltip" data-placement="top" title="Add to Cart" href="#"><i class="fal fa-Shopping-cart"></i></a></li>
                                                                <li><a data-tooltip="tooltip" data-placement="top" title="Quick Shop" data-toggle="modal" data-target="#productQuick" href="#"><i class="fal fa-search-plus"></i></a></li>
                                                                <li><a data-tooltip="tooltip" data-placement="top" title="Add to Wishlist" href="#"><i class="fal fa-heart"></i></a></li>
                                                                <li><a data-tooltip="tooltip" data-placement="top" title="Add to Compare" href="#"><i class="fal fa-repeat-alt"></i></a></li>
                                                            </ul>
                                                        </div>
                                                        <div class="product-content">
                                                            <div class="product-title">
                                                                <h4 class="title"><a href="product-simple-01.html">Ribbed High Neck Sweater</a></h4>
                                                            </div>
                                                            <div class="product-price">
                                                                <span class="price">£150.00</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="single-product">
                                                        <div class="product-image">
                                                            <div class="image">
                                                                <img class="product-1" src="assets/images/product/product-33.jpg" alt="product">
                                                                <a class="link" href="product-simple-01.html"></a>
                                                            </div>
                                                            <ul class="product-meta text-center">
                                                                <li><a data-tooltip="tooltip" data-placement="top" title="Add to Cart" href="#"><i class="fal fa-Shopping-cart"></i></a></li>
                                                                <li><a data-tooltip="tooltip" data-placement="top" title="Quick Shop" data-toggle="modal" data-target="#productQuick" href="#"><i class="fal fa-search-plus"></i></a></li>
                                                                <li><a data-tooltip="tooltip" data-placement="top" title="Add to Wishlist" href="#"><i class="fal fa-heart"></i></a></li>
                                                                <li><a data-tooltip="tooltip" data-placement="top" title="Add to Compare" href="#"><i class="fal fa-repeat-alt"></i></a></li>
                                                            </ul>
                                                        </div>
                                                        <div class="product-content">
                                                            <div class="product-title">
                                                                <h4 class="title"><a href="product-simple-01.html">Navy Light Cash Wool</a></h4>
                                                            </div>
                                                            <div class="product-price">
                                                                <span class="price">£150.00</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="shop-sidebar.html">Women</a>
                                        <ul class="sub-menu women-man">
                                            <div class="menu-menu-items">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="product-wrapper">
                                                            <div class="menu-title">
                                                                <h4 class="title">Top Sale</h4>
                                                            </div>
                                                            <div class="product-items">
                                                                <div class="single-product-mini">
                                                                    <div class="product-mini-image">
                                                                        <a href="product-simple-01.html"><img src="assets/images/product/product-1.jpg" alt=""></a>
                                                                    </div>
                                                                    <div class="product-mini-content">
                                                                        <h5 class="mini-title"><a href="product-simple-01.html">Basic Contrasting T-Shirt</a></h5>
                                                                        <div class="product-price">
                                                                            <span class="price">£150.00</span>
                                                                        </div>
                                                                        <a href="#" class="main-btn">Add to cart</a>
                                                                    </div>
                                                                </div>
                                                                <div class="single-product-mini">
                                                                    <div class="product-mini-image">
                                                                        <a href="product-simple-01.html"><img src="assets/images/product/product-13.jpg" alt=""></a>
                                                                    </div>
                                                                    <div class="product-mini-content">
                                                                        <h5 class="mini-title"><a href="product-simple-01.html">Basic Contrasting T-Shirt</a></h5>
                                                                        <div class="product-price">
                                                                            <span class="price">£150.00</span>
                                                                        </div>
                                                                        <a href="#" class="main-btn">Add to cart</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="product-btn mt-25">
                                                                <a href="shop-sidebar.html" class="view-product">View All Product</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="product-wrapper">
                                                            <div class="menu-title">
                                                                <h4 class="title">Sale Off</h4>
                                                            </div>
                                                            <div class="menu-product-active-2 mt-30">
                                                                <div class="single-product-2">
                                                                    <div class="product-image">
                                                                        <div class="image">
                                                                            <img class="product-1" src="assets/images/product/product-28.jpg" alt="product">
                                                                            <a class="link" href="product-simple-01.html"></a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="product-content text-center">
                                                                        <div class="product-content-wrapper">
                                                                            <div class="product-title">
                                                                                <h4 class="title"><a href="product-simple-01.html">Basic Contrasting T-Shirt</a></h4>
                                                                            </div>
                                                                            <div class="product-price">
                                                                                <span class="price">£150.00</span>
                                                                            </div>
                                                                        </div>
                                                                        <ul class="product-meta">
                                                                            <li><a data-tooltip="tooltip" data-placement="top" title="Add to Cart" href="#"><i class="fal fa-Shopping-cart"></i></a></li>
                                                                            <li><a data-tooltip="tooltip" data-placement="top" title="Quick Shop" data-toggle="modal" data-target="#productQuick" href="#"><i class="fal fa-search-plus"></i></a></li>
                                                                            <li><a data-tooltip="tooltip" data-placement="top" title="Add to Wishlist" href="#"><i class="fal fa-heart"></i></a></li>
                                                                            <li><a data-tooltip="tooltip" data-placement="top" title="Add to Compare" href="#"><i class="fal fa-repeat-alt"></i></a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <div class="single-product-2">
                                                                    <div class="product-image">
                                                                        <div class="image">
                                                                            <img class="product-1" src="assets/images/product/product-7.jpg" alt="product">
                                                                            <a class="link" href="product-simple-01.html"></a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="product-content text-center">
                                                                        <div class="product-content-wrapper">
                                                                            <div class="product-title">
                                                                                <h4 class="title"><a href="product-simple-01.html">Basic Contrasting T-Shirt</a></h4>
                                                                            </div>
                                                                            <div class="product-price">
                                                                                <span class="price">£150.00</span>
                                                                            </div>
                                                                        </div>
                                                                        <ul class="product-meta">
                                                                            <li><a data-tooltip="tooltip" data-placement="top" title="Add to Cart" href="#"><i class="fal fa-Shopping-cart"></i></a></li>
                                                                            <li><a data-tooltip="tooltip" data-placement="top" title="Quick Shop" data-toggle="modal" data-target="#productQuick" href="#"><i class="fal fa-search-plus"></i></a></li>
                                                                            <li><a data-tooltip="tooltip" data-placement="top" title="Add to Wishlist" href="#"><i class="fal fa-heart"></i></a></li>
                                                                            <li><a data-tooltip="tooltip" data-placement="top" title="Add to Compare" href="#"><i class="fal fa-repeat-alt"></i></a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </ul>
                                    </li>
                                    <li><a href="shop-sidebar.html">Shoes</a></li>
                                    <li><a href="shop-sidebar.html">Bags</a></li>
                                    <li><a href="shop-sidebar.html">Accessories</a></li>
                                </ul>
                            </li>
                        </ul>
                         --}}
                        <ul class="main-menu justify-content-center"> {{--
                            <li class="static active">
                                <a href="#">Demo</a>
                                <ul class="sub-menu sub-mega-menu flex-wrap">
                                    <li>
                                        <a class="menu-title" href="#">Column #1</a>
                                        <ul class="sub-mega-item">
                                            <li><a href="index.html">Home 01</a></li>
                                            <li><a href="index-2.html">Home 02</a></li>
                                            <li><a href="index-3.html">Home 03</a></li>
                                            <li><a href="index-4.html">Home 04</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a class="menu-title" href="#">Column #2</a>
                                        <ul class="sub-mega-item">
                                            <li><a href="index-5.html">Home 05</a></li>
                                            <li><a href="index-6.html">Home 06</a></li>
                                            <li><a href="index-7.html">Home 07</a></li>
                                            <li><a href="index-8.html">Home 08</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a class="menu-title" href="#">Column #3</a>
                                        <ul class="sub-mega-item">
                                            <li><a href="index-9.html">Home 09</a></li>
                                            <li><a href="index-10.html">Home 10</a></li>
                                            <li><a href="index-11.html">Home 11</a></li>
                                            <li><a href="index-12.html">Home 12</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a class="menu-title" href="#">Column #4</a>
                                        <ul class="sub-mega-item">
                                            <li><a href="index-13.html">Home 13</a></li>
                                            <li><a href="index-14.html">Home 14</a></li>
                                            <li><a href="index-15.html">Home 15</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="static">
                                <a href="#">Shop</a>
                                <ul class="sub-menu sub-mega-menu flex-wrap">
                                    <li>
                                        <a class="menu-title" href="#">Shop Page</a>
                                        <ul class="sub-mega-item">
                                            <li><a href="shop-sidebar.html">Shop Sidebar</a></li>
                                            <li><a href="shop-metro.html">Shop Metro</a></li>
                                            <li><a href="shop-masonry.html">Shop Masonry</a></li>
                                            <li><a href="shop-fullwidth.html">Shop Fullwidth</a></li>
                                            <li><a href="lookbook.html">Look Book</a></li>
                                            <li><a href="collections.html">Collections</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a class="menu-title" href="#">Product Page</a>
                                        <ul class="sub-mega-item">
                                            <li><a href="product-simple-01.html">Product Simple 01</a></li>
                                            <li><a href="product-simple-02.html">Product Simple 02</a></li>
                                            <li><a href="product-simple-03.html">Product Simple 03</a></li>
                                            <li><a href="product-simple-04.html">Product Simple 04</a></li>
                                            <li><a href="product-grouped.html">Product Grouped</a></li>
                                            <li><a href="product-affiliate.html">Product Affiliate</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a class="menu-title" href="#">Other Page</a>
                                        <ul class="sub-mega-item">
                                            <li><a href="my-account.html">My Account</a></li>
                                            <li><a href="checkout.html">Checkout</a></li>
                                            <li><a href="cart.html">Shopping Cart</a></li>
                                            <li><a href="order-tracking.html">Order Tracking</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a class="menu-image" href="shop-sidebar.html">
                                            <img src="assets/images/menu.jpg" alt="Menu">
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Page</a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="#">About Us <i class="fal fa-chevron-right"></i></a>
                                        <ul class="sub-menu">
                                            <li><a href="about-01.html">About US 01</a></li>
                                            <li><a href="about-02.html">About US 02</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="our-services.html">Our Service</a></li>
                                    <li><a href="contact.html">Contact Us</a></li>
                                    <li><a href="faq.html">FAQs</a></li>
                                    <li><a href="login.html">Login</a></li>
                                    <li><a href="register.html">Register</a></li>
                                    <li><a href="coming-soon.html">Coming Soon</a></li>
                                </ul>
                            </li>
                            <li><a href="lookbook.html">Look Book</a> <span>Hot</span></li>
                            <li>
                                <a href="#">Blog</a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="#">Blog Layout <i class="fal fa-chevron-right"></i></a>
                                        <ul class="sub-menu">
                                            <li><a href="blog.html">Blog</a></li>
                                            <li><a href="blog-list.html">Blog List</a></li>
                                            <li><a href="blog-sidebar.html">Blog Sidebar</a></li>
                                            <li><a href="blog-grid-01.html">Blog Grid 01</a></li>
                                            <li><a href="blog-grid-02.html">Blog Grid 02</a></li>
                                            <li><a href="blog-masonry.html">Blog Masonry</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="blog-details.html">Blog Details</a></li>
                                </ul>
                            </li> --}}
                            @foreach($menus[1]->first()->items as $item)
                                <li class="@if(url()->current() == url($item->href)) active @endif"><a href="{{ url($item->href) }}">{{ $item->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="header-navbar-2 d-lg-none">
            <div class="container-fluid custom-container">
                <div class="header-wrapper d-flex justify-content-between align-items-center">
                    <div class="header-logo">
                        <a href="{{ url($locale) }}">
                            <img src="{{ asset('resources/logo_dark_harris.png') }}" alt="Logo">
                        </a>
                    </div>

                    <div class="header-meta">
                        <ul class="meta">
                            <li>

                                <a href="{{ Route('cart',['locale' => $locale]) }}"><i class="far fa-Shopping-cart"></i> <span>{{ $lead->products->count() }}</span></a>
                            </li>
                            <li><a class="search-toggle" href="javascript:void(0)"><i class="far fa-search"></i></a>
                            </li>

                            <li><a class="sidebar-toggle" href="javascript:void(0)"><i class="fal fa-bars"></i></a></li>
                            <li><a class="d-inline-block d-md-none" href="javascript:" onclick="$('.dl-menu').toggleClass('dl-menuopen')"><i class="fa fa-bars"></i></a></li>

                        </ul>
                    </div>
                </div>
            </div>

            <div id="dl-menu" class="dl-menuwrapper d-lg-none">
                <button class="dl-trigger"></button>

                <ul class="dl-menu">
                    <li>
                        <a href="#">
                            @foreach ($locales as $locale_ => $label)
                                @if($locale_ == $locale)
                                    <i class="fas fa-globe"></i> {{ $label }}
                                @endif
                            @endforeach
                        </a>

                        <ul class="dl-submenu">
                            @foreach ($locales as $locale_ => $label)
                                @if($locale_ != $locale)
                                    <li><a href="{{ url($locale_) }}">{{ $label }}</a></li>
                                @endif
                            @endforeach
                        </ul>

                    </li>
                  {{--   <li>
                        <a href="#">Demo</a>
                        <ul class="dl-submenu">
                            <li>
                                <a href="#">Column #1</a>
                                <ul class="dl-submenu">
                                    <li><a href="index.html">Home 01</a></li>
                                    <li><a href="index-2.html">Home 02</a></li>
                                    <li><a href="index-3.html">Home 03</a></li>
                                    <li><a href="index-4.html">Home 04</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Column #2</a>
                                <ul class="dl-submenu">
                                    <li><a href="index-5.html">Home 05</a></li>
                                    <li><a href="index-6.html">Home 06</a></li>
                                    <li><a href="index-7.html">Home 07</a></li>
                                    <li><a href="index-8.html">Home 08</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Column #3</a>
                                <ul class="dl-submenu">
                                    <li><a href="index-9.html">Home 09</a></li>
                                    <li><a href="index-10.html">Home 10</a></li>
                                    <li><a href="index-11.html">Home 11</a></li>
                                    <li><a href="index-12.html">Home 12</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Column #4</a>
                                <ul class="dl-submenu">
                                    <li><a href="index-13.html">Home 13</a></li>
                                    <li><a href="index-14.html">Home 14</a></li>
                                    <li><a href="index-15.html">Home 15</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Shop</a>
                        <ul class="dl-submenu">
                            <li>
                                <a href="#">Shop Pages</a>
                                <ul class="dl-submenu">
                                    <li><a href="shop-sidebar.html">Shop Sidebar</a></li>
                                    <li><a href="shop-metro.html">Shop Metro</a></li>
                                    <li><a href="shop-masonry.html">Shop Masonry</a></li>
                                    <li><a href="shop-fullwidth.html">Shop Fullwidth</a></li>
                                    <li><a href="lookbook.html">Look Book</a></li>
                                    <li><a href="collections.html">Collections</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Product Pages</a>
                                <ul class="dl-submenu">
                                    <li><a href="product-simple-01.html">Product Simple 01</a></li>
                                    <li><a href="product-simple-02.html">Product Simple 02</a></li>
                                    <li><a href="product-simple-03.html">Product Simple 03</a></li>
                                    <li><a href="product-simple-04.html">Product Simple 04</a></li>
                                    <li><a href="product-grouped.html">Product Grouped</a></li>
                                    <li><a href="product-affiliate.html">Product Affiliate</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#">Other Pages</a>
                                <ul class="dl-submenu">
                                    <li><a href="my-account.html">My Account</a></li>
                                    <li><a href="checkout.html">Checkout</a></li>
                                    <li><a href="cart.html">Shopping Cart</a></li>
                                    <li><a href="order-tracking.html">Order Tracking</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">Page</a>
                        <ul class="dl-submenu">
                            <li>
                                <a href="#">About Us</a>
                                <ul class="dl-submenu">
                                    <li><a href="about-01.html">About US 01</a></li>
                                    <li><a href="about-02.html">About US 02</a></li>
                                </ul>
                            </li>
                            <li><a href="our-services.html">Our Service</a></li>
                            <li><a href="contact.html">Contact Us</a></li>
                            <li><a href="faq.html">FAQs</a></li>
                            <li><a href="login.html">Login</a></li>
                            <li><a href="register.html">Register</a></li>
                            <li><a href="coming-soon.html">Coming Soon</a></li>
                        </ul>
                    </li>
                    <li><a href="lookbook.html">Look Book</a></li>
                    <li>
                        <a href="#">Blog</a>
                        <ul class="dl-submenu">
                            <li>
                                <a href="#">Blog Layout</a>
                                <ul class="dl-submenu">
                                    <li><a href="blog.html">Blog</a></li>
                                    <li><a href="blog-list.html">Blog List</a></li>
                                    <li><a href="blog-sidebar.html">Blog Sidebar</a></li>
                                    <li><a href="blog-grid-01.html">Blog Grid 01</a></li>
                                    <li><a href="blog-grid-02.html">Blog Grid 02</a></li>
                                    <li><a href="blog-masonry.html">Blog Masonry</a></li>
                                </ul>
                            </li>
                            <li><a href="blog-details.html">Blog Details</a></li>
                        </ul>
                    </li> --}}
                     @foreach($menus[1]->first()->items as $item)
                                <li class="@if(url()->current() == url($item->href)) active @endif"><a href="{{ url($item->href) }}">{{ $item->name }}</a></li>
                            @endforeach
                </ul>
            </div>
        </div>

    </header>
 </div>

    <!--
    <header class="header-area">

        <div class="header-navbar">
            <div class="container-fluid custom-container">
                <div class="header-wrapper d-flex justify-content-between align-items-center">

                    <div class="header-logo">
                        <a href="/">
                            <img style="height:60px " src="{{ asset('resources/logo_dark.png') }}" alt="Logo">
                        </a>
                    </div>

                    <div class="header-menu site-nav d-none d-lg-block">
                        <ul class="main-menu">
                            @foreach($menus[1]->first()->items as $item)
                                <li class="@if(url()->current() == url($item->href)) active @endif"><a href="{{ $item->href }}">{{ $item->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="header-meta">
                        <ul class="meta">
                            {{--
                            <li><a class="cart-toggle" href="javascript:void(0)"><i class="far fa-Shopping-cart"></i><span>3</span></a></li>
                            <li><a class="search-toggle" href="javascript:void(0)"><i class="far fa-search"></i></a></li>
                            <li><a class="sidebar-toggle" href="javascript:void(0)"><i class="fal fa-bars"></i></a></li>
                             --}}
                             <li><a href="{{ Route('cart',['locale' => $locale]) }}"><i class="far fa-Shopping-cart"></i><span>{{ $lead->products->count() }}</span></a></li>
                            <li><a href="{{ Route('productGrid',['locale'=>$locale]) }}"><i class="far fa-search"></i></a></li>
                            <li><a class="sidebar-toggle" href="javascript:void(0)"><i class="fal fa-bars"></i></a></li>
                        </ul>
                        <small class=""><span class="text-danger">Consegna gratis da €{{ (int)$cartTotalBeforeFreeShipping }}!</span></small>
                    </div>

                </div>
            </div>

            <div id="dl-menu" class="dl-menuwrapper d-lg-none">
                <button class="dl-trigger all-pages"></button>

                <ul class="dl-menu">
                    @foreach($menus[1]->first()->items as $item)
                        <li><a href="{{ $item->href }}">{{ $item->name }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>

    </header>
-->
    <!--====== Header Ends ======-->

    <!--====== Search Start ======-->

    <div class="search-wrapper">
        <div class="search-box">
            <a href="javascript:void(0)" class="search-close"><i class="fal fa-times"></i></a>
            <div class="search-form">
                <label>@lang('all.start searching here')</label>
                <div class="search-input">
                    <form action="{{ Route('productGrid',['locale' => $locale]) }}">
                        <input name="keyword" type="text" placeholder="@lang('all.search entire store')…">
                        <button><i class="far fa-search"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!--====== Search Ends ======-->

    @include(env('THEME_NAME').'.include.off_canvas_cart')

    @include(env('THEME_NAME').'.include.off_canvas_sidebar')


