@extends(env('THEME_NAME').'.main')
@section('content')

<!--====== preloader Start ======-->

    <div class="preloader">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <!--====== preloader Ends ======-->

    @include(env('THEME_NAME').'.include.off_canvas_sidebar')

    <!--====== Search Start ======-->

    <div class="search-wrapper">
        <div class="search-box">
            <a href="javascript:void(0)" class="search-close"><i class="fal fa-times"></i></a>
            <div class="search-form">
                <label>@lang('all.start searching here')</label>
                <div class="search-input">
                    <form action="{{ Route('productGrid',['locale' => $locale]) }}">
                        <input type="text" placeholder="@lang('all.search entire store')…">
                        <button><i class="far fa-search"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!--====== Search Ends ======-->

    @include(env('THEME_NAME').'.include.off_canvas_cart')

    <!--====== Slider Start ======-->


{{--
      <section id="sliderParallax" class="slider-area slider-01 slider-active d-none d-md-block">
        @foreach($carousels['homepage_main']->first()->slides as $slide)
            <div class="single-slider bg_cover d-flex align-items-center paroller" style="background-image: url({{ Route('ir',['size' => 'h1600','filename' => $slide->file]) }});" data-paroller-factor="-0.15" data-paroller-type="foreground" data-paroller-direction="vertical">

                <div class="container">
                    <div id="slider" class="slider-content text-center layer" data-depth="0.3">
                        <h1 class="title"><span data-animation="slideInLeft" data-delay="0.6s">{{ $slide->name }}</span></h1>
                        <br>
                        <br>
                        <h1 class="title"><span data-animation="slideInLeft" data-delay="0.9s">{{ $slide->subtitle }}</span></h1>
                        <br>
                        <br>
                        <div class="text" data-animation="line" data-delay="0s">
                            <p><span data-animation="slideInUp" data-delay="1.3s">{{ $slide->short_description }}</span></p>
                        </div>
                        <div class="slider-btn">
                            <a data-animation="zoomIn" data-delay="2s" href="{{ url($slide->link) }}" class="main-btn main-btn-1">@lang('all.shop now')</a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </section>
 --}}
 @push('after-scripts')

 <script>

    $('.header-wrapper').addClass('header-wrapper-inverted');
    $('.header-logo img').attr('src','/resources/logo_light_harris.png');

    $(window).scroll(function(){
        if(!$('.homepage-background-video').isInViewport()){
            $('.header-wrapper').removeClass('header-wrapper-inverted');
            $('.header-logo img').attr('src','/resources/logo_dark_harris.png');
        }else{
            $('.header-wrapper').addClass('header-wrapper-inverted');
            $('.header-logo img').attr('src','/resources/logo_light_harris.png');
        }
    })
 </script>
 @endpush

<div class="homepage-background-video d-flex justify-content-center align-items-center">
    <div class="hp-video-container">
{{--
        @if (!$isMobile)

            <video autoplay muted loop preload="auto">
                <source src="{{ asset('assets/videos/harris-shoes-background.mp4') }}" type="video/mp4">
            </video>

        @else
 <div class="" style="overflow:hidden">
            <img src="/assets/images/harris/homepage-background-mobile.jpg" alt="" class="vh-100" style="max-width: none!important">
        </div>

        @endif
        --}}
         <div class="" style="overflow:hidden">
            <img src="/assets/images/harris/harris-homepage-cover.jpg" alt="" >
        </div>
    </div>
    <div class="d-none d-md-flex" style="z-index: 999">
        <img src="/resources/logo_light_harris.png" alt="" height="70" style="max-width: none!important; height: 70px!important">
        @if ($isMobile)
            @hss('320')
        @endif
    </div>
</div>
@hss('50')
   <div class="container my-4 homepage-main-claim">
            <div class="row justify-content-center mt-4">
                <div class="col-lg-12">
                    <div class="section-title text-center pb-25">
                        <h2 class="title">@lang('all.claim first line')</h2>
                        <div class="product-btn text-center ">
                            <a href="http://www.harrisshoes.it/{{ $locale }}/pg" class="view-product">@lang('all.claim second line')</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @foreach($menus[3] as $menu)
<section class="category-area">

        <div class="category-wrapper d-flex flex-wrap">
            @foreach($menu->items as $item)
             <div class="category-item">
                <a href="{{ $item->href }}">
                    <img src="{{ url($item->photos[0]) }}" alt="">
                    <div class="category-content">
                        <h4 class="title">{{ $item->name }}</h4>
                    </div>
                </a>
            </div>
            @endforeach

        </div>
    </section>
    @endforeach
{{--
    <section id="sliderParallax" class="slider-area slider-11 slider-active">
        @foreach($carousels['homepage_main']->first()->slides as $slide)
            <div class="single-slider bg_cover d-flex align-items-center paroller" style="background-image: url('{{ Route('ir',['size' => 'h1600','filename' => $slide->file]) }}');" data-paroller-factor="-0.15" data-paroller-type="foreground" data-paroller-direction="vertical">

                <div class="container">
                    <div class="slider-content-11 text-center layer" data-depth="0.5">
                        <div class="content">
                            <span class="line" data-animation="lineLeft" data-delay="0.5s"></span>
                            <h1 class="title"><span data-animation="slideInLeft" data-delay="1.3s">{{ $slide->name }}</span></h1>
                            <span class="line" data-animation="lineLeft" data-delay="1s"></span>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

    </section>
 --}}
    <!--====== Slider Ends ======-->

    <!--====== Header Start ======-->
<!--
    <header class="header-area">

        <div class="slider-bottom-header-sticky header-navbar-dark">
            <div class="container-fluid custom-container">
                <div class="header-wrapper d-flex justify-content-between align-items-center">

                    <div class="header-logo">
                        <a href="/">
                            <img style="height: 80px"  src="{{ asset('resources/logo_light.png') }}" alt="Logo">
                        </a>
                    </div>

                    <div class="header-menu site-nav d-none d-lg-block">
                        <ul class="main-menu">
                            @foreach($menus[1]->first()->items as $item)
                                <li class="@if(url()->current() == url($item->href)) active @endif"><a href="{{ $item->href }}">{{ $item->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>


                    <div class="header-meta">
                        <ul class="meta">
                            {{--
                            <li><a class="cart-toggle" href="javascript:void(0)"><i class="far fa-Shopping-cart"></i><span>3</span></a></li>
                            <li><a class="search-toggle" href="javascript:void(0)"><i class="far fa-search"></i></a></li>
                            <li><a class="sidebar-toggle" href="javascript:void(0)"><i class="fal fa-bars"></i></a></li>
                             --}}
                             <li><a href="{{ Route('cart',['locale'=> $locale]) }}"><i class="far fa-Shopping-cart"></i><span>{{ $lead->products->count() }}</span></a></li>
                            <li><a href="{{ Route('productGrid',['locale'=> $locale]) }}" href="javascript:void(0)"><i class="far fa-search"></i></a></li>
                            <li><a class="sidebar-toggle" href="javascript:void(0)"><i class="fal fa-bars"></i></a></li>
                        </ul>
                          <small ><span class="text-danger">Consegna gratis da €{{ (int)$cartTotalBeforeFreeShipping }}!</span></small>
                    </div>

                </div>
            </div>

            <div id="dl-menu" class="dl-menuwrapper d-lg-none">
                <button class="dl-trigger"></button>

                <ul class="dl-menu">
                    @foreach($menus[1]->first()->items as $item)
                        <li class="@if(url()->current() == url($item->href)) active @endif"><a href="{{ $item->href }}">{{ $item->name }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>

    </header>
-->
    <!--====== Header Ends ======-->



    <!--====== Featured Products Start ======-->
@hss('40')
       @if(!is_null($shopWindow))
    <section class="product-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section-title text-center pb-25">
                        <h2 class="title">@lang('all.featured products')</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-h-scroll-wrapper">
        <div class="container-h-scroll" style="width: {{ count($shopWindow)*265 }}px">
           @foreach($shopWindow as $index => $product)
                        @if(!is_null($product->photos))
                            <div class="product-thumb">
                                <div class="single-product mt-50">
                                    <div class="product-image">
                                        <div class="image">

                                            <img class="product-1" src="@cim(Route('ir',['size' => 'h320','filename' => collect($product->photos)->first()]))" alt="product">
                                            {{-- <img class="product-2" src="assets/images/product/product-34.jpg" alt="product">  --}}
                                            <a class="link" href="{{ $product->makeUrl() }}" tabindex="0"></a>
                                        </div>
                                    </div>
                                    <div class="product-content">
                                        <div class="product-title">
                                            <h4 class="title"><a href="{{ $product->makeUrl() }}" tabindex="0">{{ $product->name }}</a></h4>
                                        </div>
                                        <div class="product-price">
                                            <span class="price">@fp($product->price)</span>
                                            @if ($product->price != $product->compare_price)
                                                <small><span class="text-muted "><del>@fp($product->compare_price)</del> (-{{ $product->discount_percentage}}%)</span></small>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
        </div>
        </div>



            </div>
            @hss('20')
            <div class="product-btn text-center ">
                <a href="{{ Route('productGrid',['locale' => $locale]) }}" class="view-product">@lang('all.view all products')</a>
            </div>
        </div>
    </section>
@hss('40')
       @endif
    <!--====== Featured Products End ======-->

    <img src="/assets/images/harris/harris-hommepage-interstitial-1.jpg" alt="" class="w-100">
    @hss('150')
    <!--====== Product Start ======-->
        @if(!is_null($newArrivals))
    <section class="product-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section-title text-center pb-25">
                        <h2 class="title">@lang('all.new arrivals')</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="product-menu">
                <ul class="nav nav-2 justify-content-center">
                    @foreach($newArrivals as $index =>  $newArrival)
                    <li>
                      <a class="@if($index == 0) active @endif"  data-toggle="tab" data-target="#new-arrivals-tab-{{ Str::slug($newArrival->name) }}" >{{ $newArrival->name }}</a>
                    </li>
                    @endforeach
                  </ul>
            </div>
        </div>
        <div class="container-fluid custom-container-2">
            <div class="tab-content" >
                @foreach($newArrivals as $index =>  $newArrival)
                <div class="tab-pane fade show @if($index == 0) active @endif" id="new-arrivals-tab-{{ Str::slug($newArrival->name) }}">
                    <div class="row">
                        @foreach($newArrival->products as $product)
                            <div class="col-xl-3 col-md-4 col-6">
                                <div class="single-product mt-50">
                                    <div class="product-image">
                                        <div class="image">
                                            <img class="product-1" src="@cim(Route('ir',['size' => 'h800','filename' => collect($product->photos)->first()]))" alt="product">
                                            {{-- <img class="product-2" src="assets/images/product/product-87.jpg" alt="product">  --}}
                                            <a class="link" href="{{ $product->makeUrl() }}"></a>
                                        </div>
                                    </div>
                                    <div class="product-content d-flex justify-content-between">
                                        <div class="product-title">
                                            <h4 class="title"><a href="{{ $product->makeUrl() }}">{{ $product->name }}</a></h4>
                                        </div>
                                        <div class="product-price">
                                            <span class="price">@fp($product->price)</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    @endif

    <!--====== Product Ends ======-->

    {{--
    <!--====== Countdown Start ======-->

    <section class="countdown-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="countdown-image mt-50">
                        <img src="assets/images/m11-p-1.jpg" alt="">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="countdown-content mt-45 text-center">
                        <h3 class="title">District Side Table</h3>
                        <span class="price">$123.99</span>

                        <div data-countdown="2020/11/20"></div>

                        <a href="shop-sidebar.html" class="main-btn">Shop Now</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--====== Countdown Ends ======-->
     --}}

@hss('150')
     <img src="/assets/images/harris/harris-hommepage-interstitial-2.jpg" alt="" class="w-100">


    <!--====== Blog Start ======-->

    <section class="blog-area pt-80 pb-55">
         <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section-title text-center pb-25">
                        <h2 class="title">@lang('all.latest from the blog')</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row  blog-active">
                @foreach($newArticles as $article)
                <div class="col-lg-4 col-md-6">
                    <div class="single-blog mt-30">
                        <div class="blog-image">
                            <a href="{{ $article->makeUrl() }}">
                                <img src="@cim(Route('ir',['size' => 'h800','filename' =>  $article->cover ]))" alt="blog">
                                <i class="fal fa-search-plus"></i>
                            </a>
                        </div>
                        <div class="blog-content">
                            {{-- <ul class="blog-category">
                                <li><a href="#">Style</a></li>
                            </ul> --}}
                            <h4 class="title"><a href="{{ $article->makeUrl() }}">{{ $article->name }}</a></h4>
                            <ul class="blog-meta">
                                 {{--<li><a href="#">By <span> Jon Smith</span></a></li>--}}
                                {{-- <li><a href="#"><i class="fal fa-clock"></i> {{ $article->created_at->format('d F Y') }}</a></li>  --}}
                            </ul>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>

    <!--====== Blog Ends ======-->

    <!--====== Newsletter Start ======-->
{{--
    <section class="newsletter-area pt-10 pb-60">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5">
                    <div class="newsletter-content mt-45">
                        <h4 class="title">@lang('all.stay up to date on our products')</h4>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="newsletter-form mt-50">
                        <form action="{{ Route('subscribeNewsletter') }}" method="POST">
                            @csrf
                            <input type="hidden" name="locale" value="{{ $locale }}">
                            <input type="email" placeholder="Email" name="email" required>
                            <button class="main-btn" method="POST">@lang('sign up')</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
--}}
    <!--====== Newsletter Ends ======-->
{{--
    <!--====== Brand Logo Start ======-->

    <div class="brand-logo-area border-0 pt-50 pb-50">
        <div class="container">
            <div class="brand-row brand-active">
                <div class="brand-col">
                    <div class="single-brand">
                        <img src="assets/images/brand-1.png" alt="brand">
                    </div>
                </div>
                <div class="brand-col">
                    <div class="single-brand">
                        <img src="assets/images/brand-2.png" alt="brand">
                    </div>
                </div>
                <div class="brand-col">
                    <div class="single-brand">
                        <img src="assets/images/brand-3.png" alt="brand">
                    </div>
                </div>
                <div class="brand-col">
                    <div class="single-brand">
                        <img src="assets/images/brand-4.png" alt="brand">
                    </div>
                </div>
                <div class="brand-col">
                    <div class="single-brand">
                        <img src="assets/images/brand-5.png" alt="brand">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--====== Brand Logo Ends ======-->
 --}}
@endsection
