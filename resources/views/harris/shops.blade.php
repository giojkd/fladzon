@extends(env('THEME_NAME').'.main')
@section('content')
    <section class="page-banner banner-03 bg_cover" style="background-image: url('{{ url('assets/images/harris-milano.jpg') }}'); background-size: cover;
    background-position: center;
    background-repeat: no-repeat;
    background-attachment: fixed;;">
        <div class="container">
            <div class="page-banner-content page-banner-content-2 text-center">
                <h2 class="title">@lang('all.shops')</h2>
                <ol class="breadcrumb justify-content-center">
                    <li class="breadcrumb-item"><a href="{{ url('locale') }}">Home</a></li>
                    <li class="breadcrumb-item active">@lang('all.shops')</li>
                </ol>
            </div>
        </div>
    </section>

    <!--====== Page Banner Ends ======-->

    <!--====== Service Start ======-->

    <section class="services-page pt-100 pb-160">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="services-title text-center">
                        <h2 class="title">{{-- Best Servives  --}} <img src="{{ asset('resources/logo_dark_harris.png') }}" alt=""></h2>
                        <img class="signature" src="assets/images/services-sign.png" alt="">
                        <p>@lang('all.shops intro after logo')</p>
                    </div>
                </div>
            </div>
            {{--
            <div class="services-wrapper pt-30">
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <div class="services-image mt-50">
                            <img src="assets/images/services/services-bg1.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="services-content mt-50">
                            <p class="subheading">“Fashion is the armor to survive the reality of everyday life”</p>
                            <h4 class="name">Fernande Garvin</h4>
                        </div>
                    </div>
                </div>
            </div>
             --}}



            <div class="services-element pt-110">
                @foreach ($shops as $index => $shop)
                    <div class="row @if ($index % 2 == 0) flex-md-row-reverse @endif">
                        <div class="col-md-6">
                            <div class="services-element-image mt-50">
                                <img src="{{ isset($shop->photos[0]) ? url($shop->photos[0]) : '' }}" alt="">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="services-element-content mt-50">
                                <h4 class="title">{{ $shop->name }}</h4>
                                <p>{{ $shop->short_description }}</p>
                                <hr>
                                {!! $shop->opening_details !!}
                                <hr>
                                <i class="fas fa-map-marker-alt"></i> {{ $shop->address['value'] }}
                                <hr>
                                <p>
                                    <i class="fas fa-phone"></i> {{ $shop->phone }} <br>
                                    <i class="fas fa-at"></i> {{ $shop->email }}
                                </p>
                                {{-- <a href="blog-details.html" class="main-btn">Read More</a>  --}}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>


        </div>
    </section>

    <!--====== Service Ends ======-->

@endsection
