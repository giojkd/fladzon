
@extends(env('THEME_NAME').'.main')
@section('content')
{{--
    <section class="page-banner bg_cover" style="">
        <div class="container">
            <div class="page-banner-content text-center">
                <h2 class="title">Shop</h2>
                <ol class="breadcrumb justify-content-center">
                    <li class="breadcrumb-item"><a href="/">Home</a></li>
                </ol>
            </div>
        </div>
    </section>
     --}}

    <!--====== Shop Page Start ======-->


    <section class="shop-page vh-100">
        <div class="container-fluid shop-container">
            <div class="row flex-md-row justify-content-between">


                <div class="col-md-2 @if(!$isMobile) vh-100 @else p-0 @endif" style="overflow: scroll">
                    <div class="d-none d-md-block">
                        @hss('100')
                    </div>
                    @if (!$isMobile)
                        @hss('50')
                    @else
                        @hss('70')
                    @endif

                    <div class="d-block d-md-none">
                        <a style="height: 40px; line-height: 40px" href="javascript:" onclick="$('#filtersBox').toggleClass('d-none')" class="main-btn d-block orange-bg">@lang('all.show filters')</a>
                        @if (!$isMobile)
                            @hss('200')
                        @endif
                    </div>
                    <div class="d-none d-md-block shop-sidebar @if ($isMobile) p-3 @endif" id="filtersBox">
                        <div class="shop-sidebar-search mt-50">
                            <h4 class="sidebar-title mb-2">@lang('all.search here')</h4>
                            <div id="searchbox"></div>
                            <div id="clear-refinements"></div>
                        </div>

                         <div class="shop-sidebar-color mt-40">
                            <h4 class="sidebar-title mb-2">@lang('all.outlet')</h4>
                            <div id="only-on-sale-refinement"></div>
                        </div>

                        <div class="shop-sidebar-color mt-25">
                            <h4 class="sidebar-title mb-2">@lang('all.search for')</h4>
                            <div id="refinement-list-gender"></div>
                        </div>

                         <div class="shop-sidebar-collection mt-25">
                            <h4 class="sidebar-title mb-2">@lang('all.collections')</h4>
                            <div id="refinement-list-collection"></div>
                        </div>

                        <div class="shop-sidebar-color mt-25">
                            <h4 class="sidebar-title mb-2">@lang('all.category')</h4>
                            <div id="refinement-list-category"></div>
                        </div>

                        <div class="shop-sidebar-color mt-25">
                            <h4 class="sidebar-title mb-2">@lang('all.color')</h4>
                            <div id="refinement-list-color"></div>
                        </div>





                        <div class="shop-sidebar-color mt-25">
                            <h4 class="sidebar-title mb-2">@lang('all.price range')</h4>
                            <div id="numeric-menu"></div>
                        </div>

                        <!--
                        <div class="shop-sidebar-color mt-50">
                            <h4 class="sidebar-title mb-2">Imposta il prezzo</h4>
                            <div id="range-slider"></div>
                        </div>
                    -->


<!--
                         <div class="shop-sidebar-color mt-50">
                            <h4 class="sidebar-title mb-2">Durata</h4>
                            <div id="refinement-list-durata"></div>
                        </div>
                    -->
<!--
                        <div class="shop-sidebar-color mt-50">
                            <h4 class="sidebar-title">Color</h4>

                            <div class="sidebar-color">
                                <ul class="color-list">
                                    <li data-tooltip="tooltip" data-placement="top" title="Black"  data-color="#171d3d" class="active"></li>
                                    <li data-tooltip="tooltip" data-placement="top" title="Blue"  data-color="#4b59a3"></li>
                                    <li data-tooltip="tooltip" data-placement="top" title="Bronze"  data-color="#b9afa1"></li>
                                    <li data-tooltip="tooltip" data-placement="top" title="Green"  data-color="#61c58d"></li>
                                    <li data-tooltip="tooltip" data-placement="top" title="Pink"  data-color="#f6b7cf"></li>
                                    <li data-tooltip="tooltip" data-placement="top" title="Red"  data-color="#ef5619"></li>
                                </ul>
                            </div>
                        </div>
                    -->





                    </div>
                </div>
                <div class="col-md-10 vh-100" style="overflow: scroll" id="productsWrapper">
                    <div class="d-none d-md-block">
                        @hss('150')
                    </div>
                    <div class="shop-top-bar mt-35">
                        <div class="shop-top-left">
                            <!--<p>Showing 1–12 of 80 results</p>
                            <div class="show">
                                <span>Show</span>
                                <ul>
                                    <li><a class="active" href="#">12</a></li>
                                    <li><a href="#">15</a></li>
                                    <li><a href="#">30</a></li>
                                </ul>
                            </div>-->
                        </div>
                        <div class="shop-top-right">
                            <!--<div class="shop-sort">
                                <select id="resizing_select">
                                    <option value="0" selected="selected">Sort By</option>
                                    <option value="1">Default</option>
                                    <option value="2">Popularity</option>
                                    <option value="3">Average Rating</option>
                                    <option value="4">Newsness</option>
                                    <option value="5">Price Low to High</option>
                                    <option value="6">Priche High to Low</option>
                                </select>
                                <i class="fal fa-chevron-down"></i>
                                <select id="width_tmp_select">
                                    <option id="width_tmp_option"></option>
                                </select>
                            </div>
                        -->
                        <div id="sort-by" class="text-center"></div>
                        </div>
                    </div>

                    <div class="shop-product" id="hits">

                    </div>
<!--
                    <div class="pagination-items mt-80">
                        <ul class="pagination justify-content-center">
                            <li><a class="prev" href="#">Previous</a></li>
                            <li><a href="#">1</a></li>
                            <li><a class="active" href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a class="next" href="#">Next</a></li>
                        </ul>
                    </div>
                -->
                </div>
            </div>
        </div>
    </section>

    <!--====== Shop Page Ends ======-->
@endsection

@push('after-scripts')

    <style>
        .ais-InfiniteHits-loadMore{
            width: 100px;
            opacity: 0;
        }
        @media only screen and (max-device-width: 812px) {
            .ais-Hits-item, .ais-InfiniteHits-item, .ais-InfiniteResults-item, .ais-Results-item{
                width: calc(50% - 1rem)!important;
            }
        }
    </style>



    <script src="https://cdn.jsdelivr.net/npm/algoliasearch@4.0.0/dist/algoliasearch-lite.umd.js" integrity="sha256-MfeKq2Aw9VAkaE9Caes2NOxQf6vUa8Av0JqcUXUGkd0=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/instantsearch.js@4.0.0/dist/instantsearch.production.min.js" integrity="sha256-6S7q0JJs/Kx4kb/fv0oMjS855QTz5Rc2hh9AkIUjUsk=" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/instantsearch.css@7.3.1/themes/algolia-min.css" integrity="sha256-HB49n/BZjuqiCtQQf49OdZn63XuKFaxcIHWf0HNKte8=" crossorigin="anonymous">

    <script>

        var labels = {
            clean_refinements : '@lang('all.clean refinements')',
            on_deal : '@lang('all.on deal')'
        }

        var cloudimage = 'https://awkxoooseq.cloudimg.io/v7/https://harrisshoes.it';

        $.fn.isInViewport = function() {
            if($(this).length > 0){
                var elementTop = $(this).offset().top - 200;
                var elementBottom = elementTop + $(this).outerHeight();
                var viewportTop = $(window).scrollTop();
                var viewportBottom = viewportTop + $(window).height();
                return elementBottom > viewportTop && elementTop < viewportBottom;
            }
        };

        $(function(){
            $('footer,.newsletter-area-2').hide();
        })

        $('#productsWrapper').on('resize scroll', function() {

            /*
            if($(window).scrollTop() > 500){
                $('.scrollTopBtn').fadeIn()
            }else{
                $('.scrollTopBtn').fadeOut()
            }
            */

            if($('.ais-InfiniteHits-loadMore').isInViewport()){
                $('.ais-InfiniteHits-loadMore').trigger('click');
            }

        });

        var presetFacets = @json($facets);
        var onlyOnSale = {{ $onlyOnSale }};
        const searchClient = algoliasearch('{{ env('ALGOLIA_APP_ID') }}', '{{ env('ALGOLIA_SECRET') }}');
        var locale = '{{ $locale }}';
        var fzProducts = [];

        var keyword = '{{ $keyword }}';

        @verbatim

         var showMoreText = `
                        {{#isShowingMore}}
                            Mostra di meno
                        {{/isShowingMore}}
                        {{^isShowingMore}}
                            Mostra di più
                        {{/isShowingMore}}
                    `;

        $('.header-search').removeClass('d-flex').hide();

        const search = instantsearch({
            indexName: 'harrisproductgroups',
            searchClient,
            initialUiState: {
                harrisproductgroups: {
                    query: keyword,
                    refinementList:presetFacets,
                    toggle:{
                        is_on_sale: onlyOnSale
                    }
                },
            },
        });

        search.addWidgets([
            instantsearch.widgets.searchBox({
                container: '#searchbox',
            }),

            instantsearch.widgets.infiniteHits({
                container: '#hits',
                templates:{
                    empty: 'No results for <q>{{ query }}</q>',
                    item: `<div class="">
                                <div class="single-product mt-50">
                                    <div class="product-image">
                                        <div class="image">
                                            <img class="product-1" src="{{ cover }}" alt="product">
                                            <img class="product-2" src="{{ second_cover }}" alt="product">
                                            <a class="link" href="{{ url }}"></a>
                                        </div>
                                        <!--
                                        <ul class="product-meta text-center">
                                            <li><a data-tooltip="tooltip" data-placement="top" title="Add to Cart" href="#"><i class="fal fa-Shopping-cart"></i></a></li>
                                            <li><a data-tooltip="tooltip" data-placement="top" title="Quick Shop" data-toggle="modal" data-target="#productQuick" href="#"><i class="fal fa-search-plus"></i></a></li>
                                            <li><a data-tooltip="tooltip" data-placement="top" title="Add to Wishlist" href="#"><i class="fal fa-heart"></i></a></li>
                                            <li><a data-tooltip="tooltip" data-placement="top" title="Add to Compare" href="#"><i class="fal fa-repeat-alt"></i></a></li>
                                        </ul>
                                        -->
                                        <!--<span class="discount">40%</span>-->
                                    </div>
                                    <div class="product-content d-flex justify-content-between">
                                        <div class="product-title">
                                            <span class="pg-on-sale-badge-{{ is_on_sale }} badge badge-warning orange-bg text-white">{{ on_deal_label }}</span>
                                            <h4 class="title"><a href="{{ url }}">{{ name }} </a></h4>
                                            <small>{{ price }}</small>
                                            <span class="pg-compare-price-{{ has_discount }}"><del>{{ compare_price }}</del></span>
                                            <span class="pg-discount-percentage-{{ has_discount }}">{{ discount_percentage }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>`,

                },
                 transformItems(items) {

                    fzProducts = [];
                    var productIndex = 0 ;

                    var itemsReturn = items.map(function(item){

                        productIndex++;

                        var discountAmount = item.compare_price-item.price;
                        var discountPercentage = discountAmount*100/item.compare_price;
                        var returnItem = {
                            name: item['category_'+locale],
                            url: item['url_'+locale],
                            price: '€ '+parseFloat(item.price).toFixed(2),
                            //price: item.price,
                            cover: cloudimage+item.cover,
                            second_cover: cloudimage+item.second_cover,
                            is_on_sale: (item.is_on_sale == 1) ? 1 : 0,
                            has_discount: (item.price != item.compare_price) ? 1 : 0,
                            compare_price:  '€ '+parseFloat(item.compare_price).toFixed(2),
                            discount_percentage: '-'+Math.round(discountPercentage)+'%',
                            on_deal_label: labels.on_deal
                        };

                        var fzReturnItem = {
                            id: item.objectID,
                            name: item.name_it,
                            price: parseFloat(item.price).toFixed(2),
                            brand: item.brand,
                            category: item.category_it,
                            variant: item.attributes_line_it,
                            position: productIndex
                        };

                        fzProducts.push(fzReturnItem);

                        return returnItem;

                    })

                    ft.productImpressions({
                         products:fzProducts
                    });



                    return itemsReturn;
                },
            }),

            instantsearch.widgets.sortBy({
                container: '#sort-by',
                items: [
                    { label: 'Più rilevanti', value: 'harrisproductgroups' },
                    /*{ label: 'Prezzo (prima meno costosi)', value: 'harrisproductgroups_price_asc' },
                    { label: 'Prezzo (prima più costosi)', value: 'harrisproductgroups_price_desc' },*/
                ],
            }),

            instantsearch.widgets.clearRefinements({
                container: '#clear-refinements',
                cssClasses:{
                    button: ['orange-btn']
                },
                templates:{
                    resetLabel() {
                        return labels.clean_refinements;
                    },
                }
            }),

             instantsearch.widgets.refinementList({
                container: '#refinement-list-collection',
                attribute: 'collections_'+locale,
                 limit: 5,
                showMore: true,
                templates:{
                    showMoreText: showMoreText
                }
                //searchable: true
            }),

            instantsearch.widgets.refinementList({
                container: '#refinement-list-color',
                attribute: 'colore_'+locale,
                 limit: 5,
                showMore: true,
                templates:{
                    showMoreText: showMoreText
                }
                //searchable: true
            }),

            instantsearch.widgets.refinementList({
                container: '#refinement-list-gender',
                attribute: 'gender_'+locale,
                 limit: 3,
                showMore: true,
                templates:{
                    showMoreText: showMoreText
                }
                //searchable: true
            }),

            instantsearch.widgets.refinementList({
                container: '#refinement-list-category',
                attribute: 'category_'+locale,
                limit: 5,
                showMore: true,
                templates:{
                    showMoreText: showMoreText
                }
                   //searchable: true
            }),
/*
            instantsearch.widgets.refinementList({
                container: '#refinement-list-durata',
                attribute: 'durata',
            }),
*/


            instantsearch.widgets.numericMenu({
                container: '#numeric-menu',
                attribute: 'price',
                items: [
                    { label: 'Tutti' },
                    { label: 'Meno di 200€', end: 200 },
                    { label: 'Tra 200€ e 400€', start: 200, end: 400 },
                    { label: 'Tra 400€ e 700€', start: 400, end: 700 },
                    { label: 'Più di 700€', start: 700 },
                ],
            }),

            instantsearch.widgets.toggleRefinement({
                container: '#only-on-sale-refinement',
                attribute: 'is_on_sale',
                templates: {
                    labelText: 'Show only on sale',
                },
                on: 1
            }),

            instantsearch.widgets.configure({
                //query: keyword,
                //disjunctiveFacetsRefinements: presetFacets,
            }),


/*
            instantsearch.widgets.rangeSlider({
                 container: '#range-slider',
                attribute: 'price',

            })
*/
        ]);
        search.start();
        @endverbatim
    </script>

@endpush
