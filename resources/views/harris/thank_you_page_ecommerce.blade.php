@extends(env('THEME_NAME').'.main')
@section('content')
@hss('100')
<section class="blog-page pt-20 pb-120">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="single-blog mt-80">
                        <!--<div class="blog-image">
                            <a href="blog-details.html">
                                <img src="assets/images/blog/blog-1-5.jpg" alt="blog">
                                <i class="fal fa-search-plus"></i>
                            </a>
                        </div>-->
                        <div class="blog-content">
                            <!--<ul class="blog-category">
                                <li><a href="#">Style</a></li>
                            </ul>-->
                            <h4 class="title text-center">@lang('all.thank you for your order')!</h4>
                            <!--<ul class="blog-meta">
                                <li><a href="#">By <span> Jon Smith</span></a></li>
                                <li><a href="#"><i class="fal fa-clock"></i> June 15, 2020</a></li>
                            </ul>-->
                            <p>@lang('all.thank your for your order extended text')</p>

                            <a href="/" class="main-btn btn-block">@lang('all.continue shopping')!</a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @if (!is_null($lead->ref_sub))
        <img src="https://affiliate.across.it/v2/az16466/{{ $lead->id }}" width="1" height="1" border="0"/>
        {{-- <img style="opacity: 0" src="https://affiliate.across.it/v2/az1.json?track={{ $lead->ref_sub }}&ip={{ $user_ip }}&order_id={{ $lead->id }}" alt="">  --}}
    @endif

@endsection
{{--  @push('before-closing-head') --}}
@push('after-scripts')
    <!-- Event snippet for Acquisto sito HarrisShoes.it - FlorenceAdv conversion page -->
    <script>
        ft.purchase(@json(['actionfield'=>$leadForConversion['data'],'products'=>$leadForConversion['products']]));
        sendinblue.track('order_completed');
    </script>
@endpush
