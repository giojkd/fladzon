@extends(env('THEME_NAME').'.main')
@section('content')

<section class="page-banner banner-02 bg_cover" style="background-image: url({{ asset('assets/images/harris-about-us-background.jpg') }});">
        <div class="container">
            <div class="page-banner-content text-center">
                <h2 class="title text-white">Harris</h2>
                <ol class="breadcrumb justify-content-center text-white">
                    <li class="breadcrumb-item"><a class="text-white" href="{{ url($locale) }}">Home</a></li>
                    <li class="breadcrumb-item active text-white">@lang('all.about')</li>
                </ol>
            </div>
        </div>
    </section>

<section class="about-area pt-50">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="about-content mt-45">
                        <h2 class="title">@lang('all.welcome to') Harris</h2>
                        <p>
                            @lang('all.about us')
                        </p>
                        <div class="about-signature">
                            <img src="assets/images/m7-sign1.png" alt="" class="w-100">
                            <p>Bruno Famiglietti • CEO Founder</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="about-image mt-50">
                        <img src="/assets/images/harris/about-us-image.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>

   <section class="team-area pt-110">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="section-title text-center pb-30">
                        <h2 class="title">@lang('all.our team')</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-sm-12">
                    <div class="single-team text-center mt-30">
                        <div class="team-image">
                            <img src="/assets/images/harris/bruno.png" alt="">
                        </div>
                        <div class="team-content">
                            <h5 class="name"><a href="#">Bruno Famiglietti</a></h5>
                            <span class="designation">CEO/ Art Director</span>
                            {{--
                            <ul class="social">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-pinterest-p"></i></a></li>
                                <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                <li><a href="#"><i class="fal fa-envelope"></i></a></li>
                            </ul>
                             --}}
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-12">
                    <div class="single-team text-center mt-30">
                        <div class="team-image">
                            <img src="/assets/images/harris/jari.png" alt="">
                        </div>
                        <div class="team-content">
                            <h5 class="name"><a href="#">Jari Famiglietti</a></h5>
                            <span class="designation">CMO/ Marketing Director</span>

                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-12">
                    <div class="single-team text-center mt-30">
                        <div class="team-image">
                            <img src="/assets/images/harris/angela.png" alt="">
                        </div>
                        <div class="team-content">
                            <h5 class="name"><a href="#">Angela Bambusa</a></h5>
                            <span class="designation">CFO/ Chief Financial Officer</span>

                        </div>
                    </div>
                </div>
                  <div class="col-lg-3 col-sm-12">
                    <div class="single-team text-center mt-30">
                        <div class="team-image">
                            <img src="/assets/images/harris/maila.png" alt="">
                        </div>
                        <div class="team-content">
                            <h5 class="name"><a href="#">Maila Famiglietti</a></h5>
                            <span class="designation">CIO/ Chief Information Officer</span>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
{{--
    <div class="brand-logo-area border-0 pt-75 pb-100">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="section-title text-center pb-30">
                        <h2 class="title">OUR PARTNERS</h2>
                    </div>
                </div>
            </div>
            <div class="brand-row brand-active slick-initialized slick-slider">
                <div class="slick-list draggable"><div class="slick-track" style="opacity: 1; width: 1200px; transform: translate3d(0px, 0px, 0px);"><div class="brand-col slick-slide slick-current slick-active" data-slick-index="0" aria-hidden="false" tabindex="0" style="width: 240px;">
                    <div class="single-brand-2 mt-30">
                        <img src="assets/images/brand-1.png" alt="brand">
                    </div>
                </div><div class="brand-col slick-slide slick-active" data-slick-index="1" aria-hidden="false" tabindex="0" style="width: 240px;">
                    <div class="single-brand-2 mt-30">
                        <img src="assets/images/brand-2.png" alt="brand">
                    </div>
                </div><div class="brand-col slick-slide slick-active" data-slick-index="2" aria-hidden="false" tabindex="0" style="width: 240px;">
                    <div class="single-brand-2 mt-30">
                        <img src="assets/images/brand-3.png" alt="brand">
                    </div>
                </div><div class="brand-col slick-slide slick-active" data-slick-index="3" aria-hidden="false" tabindex="0" style="width: 240px;">
                    <div class="single-brand-2 mt-30">
                        <img src="assets/images/brand-4.png" alt="brand">
                    </div>
                </div><div class="brand-col slick-slide slick-active" data-slick-index="4" aria-hidden="false" tabindex="0" style="width: 240px;">
                    <div class="single-brand-2 mt-30">
                        <img src="assets/images/brand-5.png" alt="brand">
                    </div>
                </div></div></div>




            </div>
        </div>
    </div>
 --}}
@endsection
