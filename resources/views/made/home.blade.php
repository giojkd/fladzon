@extends(env('THEME_NAME').'.main')
@section('content')

<!--====== preloader Start ======-->

    <div class="preloader">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <!--====== preloader Ends ======-->

    @include(env('THEME_NAME').'.include.off_canvas_sidebar')

    <!--====== Search Start ======-->

    <div class="search-wrapper">
        <div class="search-box">
            <a href="javascript:void(0)" class="search-close"><i class="fal fa-times"></i></a>
            <div class="search-form">
                <label>@lang('all.start searching here')</label>
                <div class="search-input">
                    <form action="{{ Route('productGrid',['locale' => $locale]) }}">
                        <input type="text" placeholder="@lang('all.search entire store')…">
                        <button><i class="far fa-search"></i></button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!--====== Search Ends ======-->

    @include(env('THEME_NAME').'.include.off_canvas_cart')

    <!--====== Slider Start ======-->


{{--
      <section id="sliderParallax" class="slider-area slider-01 slider-active d-none d-md-block">
        @foreach($carousels['homepage_main']->first()->slides as $slide)
            <div class="single-slider bg_cover d-flex align-items-center paroller" style="background-image: url({{ Route('ir',['size' => 'h1600','filename' => $slide->file]) }});" data-paroller-factor="-0.15" data-paroller-type="foreground" data-paroller-direction="vertical">

                <div class="container">
                    <div id="slider" class="slider-content text-center layer" data-depth="0.3">
                        <h1 class="title"><span data-animation="slideInLeft" data-delay="0.6s">{{ $slide->name }}</span></h1>
                        <br>
                        <br>
                        <h1 class="title"><span data-animation="slideInLeft" data-delay="0.9s">{{ $slide->subtitle }}</span></h1>
                        <br>
                        <br>
                        <div class="text" data-animation="line" data-delay="0s">
                            <p><span data-animation="slideInUp" data-delay="1.3s">{{ $slide->short_description }}</span></p>
                        </div>
                        <div class="slider-btn">
                            <a data-animation="zoomIn" data-delay="2s" href="{{ url($slide->link) }}" class="main-btn main-btn-1">@lang('all.shop now')</a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </section>
 --}}
 @push('after-scripts')

 <script>
    var isMobile = {{ ($isMobile) ? 1 : 0 }};
    var themeName = '{{ env('THEME_NAME') }}';

    $('.header-wrapper').addClass('header-wrapper-inverted');
    $('.header-logo img').attr('src','/resources/'+themeName+'/logo-light.png');
    if(!isMobile){
        $(window).scroll(function(){
            if(!$('.homepage-background-video').isInViewport()){
                $('.header-wrapper').removeClass('header-wrapper-inverted');
                $('.header-logo img').attr('src','/resources/'+themeName+'/logo-dark.png');
            }else{
                $('.header-wrapper').addClass('header-wrapper-inverted');
                $('.header-logo img').attr('src','/resources/'+themeName+'/logo-light.png');
            }
        })
    }
 </script>
 @endpush

<div class="homepage-background-video d-flex justify-content-center align-items-center">
    <div class="hp-video-container">
{{--
        @if (!$isMobile)

            <video autoplay muted loop preload="auto">
                <source src="{{ asset('assets/videos/harris-shoes-background.mp4') }}" type="video/mp4">
            </video>

        @else
 <div class="" style="overflow:hidden">
            <img src="/assets/images/harris/homepage-background-mobile.jpg" alt="" class="vh-100" style="max-width: none!important">
        </div>

        @endif
        --}}
         <div class="" style="overflow:hidden">
            @if (!$isMobile)
                <img class="w-100" src="/resources/{{ env('THEME_NAME') }}/homepage-cover.jpg" alt="" >
            @else
                <img class="w-100" src="/resources/{{ env('THEME_NAME') }}/homepage-cover-vertical.jpg" alt="" >
            @endif

        </div>
    </div>
    <div class="d-none d-md-flex" style="z-index: 999">
       {{--  <img src="/resources/{{ env('THEME_NAME') }}/logo-dark.png" alt="" style="max-width: none!important; height: 210px!important">  --}}
        @if ($isMobile)
            @hss('320')
        @endif
    </div>
</div>
<div class="d-none d-md-block">
    @hss('50')
</div>


{{--
    <section id="sliderParallax" class="slider-area slider-11 slider-active">
        @foreach($carousels['homepage_main']->first()->slides as $slide)
            <div class="single-slider bg_cover d-flex align-items-center paroller" style="background-image: url('{{ Route('ir',['size' => 'h1600','filename' => $slide->file]) }}');" data-paroller-factor="-0.15" data-paroller-type="foreground" data-paroller-direction="vertical">

                <div class="container">
                    <div class="slider-content-11 text-center layer" data-depth="0.5">
                        <div class="content">
                            <span class="line" data-animation="lineLeft" data-delay="0.5s"></span>
                            <h1 class="title"><span data-animation="slideInLeft" data-delay="1.3s">{{ $slide->name }}</span></h1>
                            <span class="line" data-animation="lineLeft" data-delay="1s"></span>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

    </section>
 --}}
    <!--====== Slider Ends ======-->

    <!--====== Header Start ======-->
{{--
    <header class="header-area">

        <div class="slider-bottom-header-sticky header-navbar-dark">
            <div class="container-fluid custom-container">
                <div class="header-wrapper d-flex justify-content-between align-items-center">

                    <div class="header-logo">
                        <a href="/">
                            <img style="height: 80px"  src="{{ asset('resources/logo_light.png') }}" alt="Logo">
                        </a>
                    </div>

                    <div class="header-menu site-nav d-none d-lg-block">
                        <ul class="main-menu">
                            @foreach($menus[1]->first()->items as $item)
                                <li class="@if(url()->current() == url($item->href)) active @endif"><a href="{{ $item->href }}">{{ $item->name }}</a></li>
                            @endforeach
                        </ul>
                    </div>


                    <div class="header-meta">
                        <ul class="meta">

                            <li><a class="cart-toggle" href="javascript:void(0)"><i class="far fa-Shopping-cart"></i><span>3</span></a></li>
                            <li><a class="search-toggle" href="javascript:void(0)"><i class="far fa-search"></i></a></li>
                            <li><a class="sidebar-toggle" href="javascript:void(0)"><i class="fal fa-bars"></i></a></li>

                             <li><a href="{{ Route('cart',['locale'=> $locale]) }}"><i class="far fa-Shopping-cart"></i><span>{{ $lead->products->count() }}</span></a></li>
                            <li><a href="{{ Route('productGrid',['locale'=> $locale]) }}" href="javascript:void(0)"><i class="far fa-search"></i></a></li>
                            <li><a class="sidebar-toggle" href="javascript:void(0)"><i class="fal fa-bars"></i></a></li>
                        </ul>
                          <small ><span class="text-danger">Consegna gratis da €{{ (int)$cartTotalBeforeFreeShipping }}!</span></small>
                    </div>

                </div>
            </div>

            <div id="dl-menu" class="dl-menuwrapper d-lg-none">
                <button class="dl-trigger"></button>

                <ul class="dl-menu">
                    @foreach($menus[1]->first()->items as $item)
                        <li class="@if(url()->current() == url($item->href)) active @endif"><a href="{{ $item->href }}">{{ $item->name }}</a></li>
                    @endforeach
                </ul>
            </div>
        </div>

    </header>
--}}
    <!--====== Header Ends ======-->

<div class="d-block d-md-none">

    <section class="product-area mt-4">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section-title text-center pb-25">
                        <h2 class="">@lang('all.banner content')</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

    <!--====== Featured Products Start ======-->
@hss('40')
       @if(!is_null($shopWindow))
    <section class="product-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section-title text-center pb-25">
                        <h2 class="title">@lang('all.featured products')</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                @foreach($shopWindow as $index => $product)
                        @if(!is_null($product->photos))
                            <div class="col-md-3">
                                <div class="product-thumb text-center w-100">
                                    <div class="single-product mt-50">
                                        <a style="background-image: url('@cim(Route('ir',['size' => 'h320','filename' => collect($product->photos)->first()]))')" class="product-image" href="{{ $product->makeUrl() }}"></a>
                                        <div class="product-content">
                                            <div class="product-title">
                                                <h4 class="title"><a href="{{ $product->makeUrl() }}" tabindex="0">{{ $product->name }}</a></h4>
                                            </div>
                                            <div class="product-price">
                                                <span class="price">@fp($product->price)</span>
                                                @if ($product->price != $product->compare_price)
                                                    <small><span class="text-muted "><del>@fp($product->compare_price)</del> (-{{ $product->discount_percentage}}%)</span></small>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
            </div>
        </div>



            </div>
            @hss('20')
            <div class="product-btn text-center ">
                <a href="{{ Route('productGrid',['locale' => $locale]) }}" class="view-product">@lang('all.view all products')</a>
            </div>
        </div>
    </section>
@hss('40')
       @endif
        <div class="container my-4 homepage-main-claim">
            <div class="row justify-content-center mt-4">
                <div class="col-lg-12">
                    <div class="section-title text-center pb-25">
                        <h2 class="title">@lang('all.claim first line')</h2>
                        <div class="my-4">@lang('all.claim third line')</div>
                        <div class="product-btn text-center ">
                            <a href="{{ url($locale.'/pg') }}" class="view-product">@lang('all.claim second line')</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if(isset($menus[3]))
            @foreach($menus[3] as $menu)
                <section class="category-area">

                    <div class="category-wrapper d-flex flex-wrap">
                        @foreach($menu->items as $item)
                            @if (isset($item->photos[0]))
                                <div class="category-item">
                                    <a href="{{ $item->href }}">
                                        <img src="{{ url($item->photos[0]) }}" alt="">
                                        <div class="category-content">
                                            <h4 class="title">{{ $item->name }}</h4>
                                        </div>
                                    </a>
                                </div>
                            @endif
                        @endforeach

                    </div>
                </section>
            @endforeach
            @hss('50')
        @endif

    <!--====== Featured Products End ======-->

    <div class="video-container d-none d-md-block">
        <iframe src="https://www.youtube.com/embed/l0wlIxr753c?controls=0&autoplay=1&mute=1&loop=1&playlist=l0wlIxr753c"></iframe>
        <div class="video-container-interaction-preventor"></div>
    </div>
    @hss('50')
    <style>
        .video-container{
            width: 100vw;
            height: 800px;
            position: relative;
            display: block;
            overflow: hidden;
        }

        iframe {
            display: block;
            position: absolute;
            top: -180px;
            left: 0px;
            width: 100%;
            height: 160%;
            /*transform: translate(-50%, -50%);*/
        }

        .video-container-interaction-preventor{
            position: absolute;
            top: 0px;
            left: 0px;
            width: 100%;
            height: 100%;
        }

    </style>

    {{-- <img src="/assets/images/harris/harris-hommepage-interstitial-1.jpg" alt="" class="w-100">  --}}
    @hss('150')
    <!--====== Product Start ======-->
        @if(!is_null($newArrivals))
    <section class="product-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section-title text-center pb-25">
                        <h2 class="title">@lang('all.new arrivals')</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="product-menu">
                <ul class="nav nav-2 justify-content-center">

                    <li>
                      <a class="active"  data-toggle="tab" data-target="#new-arrivals-tab-new-arrivals" >@lang('all.new arrivals')</a>
                    </li>

                  </ul>
            </div>
        </div>
        <div class="container-fluid custom-container-2">
            <div class="tab-content" >

                <div class="tab-pane fade show active " id="new-arrivals-tab-new-arrivals">
                    <div class="row">
                        @foreach($newArrivals->pluck('products')->flatten() as $product)
                            <div class="col-xl-3 col-md-4 col-6">
                                <div class="single-product mt-50">
                                    <div class="product-image">
                                        <div class="image">
                                            <div class="img" style="background-image: url('@cim(Route('ir',['size' => 'h800','filename' => collect($product->photos)->first()]))')"></div>
                                            {{-- <img class="product-2" src="assets/images/product/product-87.jpg" alt="product">  --}}
                                            <a class="link" href="{{ $product->makeUrl() }}"></a>
                                        </div>
                                    </div>
                                    <div class="product-content d-flex justify-content-between">
                                        <div class="product-title">
                                            <h4 class="title"><a href="{{ $product->makeUrl() }}">{{ $product->name }}</a></h4>
                                        </div>
                                        <div class="product-price">
                                            <span class="price">@fp($product->price)</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

            </div>
        </div>
    </section>
    @endif

    <!--====== Product Ends ======-->

    {{--
    <!--====== Countdown Start ======-->

    <section class="countdown-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="countdown-image mt-50">
                        <img src="assets/images/m11-p-1.jpg" alt="">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="countdown-content mt-45 text-center">
                        <h3 class="title">District Side Table</h3>
                        <span class="price">$123.99</span>

                        <div data-countdown="2020/11/20"></div>

                        <a href="shop-sidebar.html" class="main-btn">Shop Now</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--====== Countdown Ends ======-->
     --}}
<div class="d-none d-md-block">
    @hss('150')
</div>
<div class="parallax d-none  d-md-block" style="height: 640px; width: 100%; background-image:url('/assets/images/{{ env('THEME_NAME') }}/homepage-interstitial.jpg')"></div>



    <!--====== Blog Start ======-->

    <section class="blog-area pt-80 pb-55">
         <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section-title text-center pb-25">
                        <h2 class="title">@lang('all.latest from the blog')</h2>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row  blog-active">
                @foreach($newArticles as $article)
                <div class="col-lg-4 col-md-6">
                    <div class="single-blog mt-30">
                        <div class="blog-image">
                            <a href="{{ $article->makeUrl() }}">
                                <img src="@cim(Route('ir',['size' => 'h800','filename' =>  $article->cover ]))" alt="blog">
                                <i class="fal fa-search-plus"></i>
                            </a>
                        </div>
                        <div class="blog-content">
                            {{-- <ul class="blog-category">
                                <li><a href="#">Style</a></li>
                            </ul> --}}
                            <h4 class="title"><a href="{{ $article->makeUrl() }}">{{ $article->name }}</a></h4>
                            <ul class="blog-meta">
                                 {{--<li><a href="#">By <span> Jon Smith</span></a></li>--}}
                                {{-- <li><a href="#"><i class="fal fa-clock"></i> {{ $article->created_at->format('d F Y') }}</a></li>  --}}
                            </ul>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>

    <!--====== Blog Ends ======-->

    <!--====== Newsletter Start ======-->
{{--
    <section class="newsletter-area pt-10 pb-60">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5">
                    <div class="newsletter-content mt-45">
                        <h4 class="title">@lang('all.stay up to date on our products')</h4>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="newsletter-form mt-50">
                        <form action="{{ Route('subscribeNewsletter') }}" method="POST">
                            @csrf
                            <input type="hidden" name="locale" value="{{ $locale }}">
                            <input type="email" placeholder="Email" name="email" required>
                            <button class="main-btn" method="POST">@lang('sign up')</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
--}}
    <!--====== Newsletter Ends ======-->
{{--
    <!--====== Brand Logo Start ======-->

    <div class="brand-logo-area border-0 pt-50 pb-50">
        <div class="container">
            <div class="brand-row brand-active">
                <div class="brand-col">
                    <div class="single-brand">
                        <img src="assets/images/brand-1.png" alt="brand">
                    </div>
                </div>
                <div class="brand-col">
                    <div class="single-brand">
                        <img src="assets/images/brand-2.png" alt="brand">
                    </div>
                </div>
                <div class="brand-col">
                    <div class="single-brand">
                        <img src="assets/images/brand-3.png" alt="brand">
                    </div>
                </div>
                <div class="brand-col">
                    <div class="single-brand">
                        <img src="assets/images/brand-4.png" alt="brand">
                    </div>
                </div>
                <div class="brand-col">
                    <div class="single-brand">
                        <img src="assets/images/brand-5.png" alt="brand">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--====== Brand Logo Ends ======-->
 --}}
@endsection
