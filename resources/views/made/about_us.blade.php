@extends(env('THEME_NAME').'.main')
@section('content')

<section class="page-banner banner-02 bg_cover" style="background-image: url({{ asset('resources/'.env('THEME_NAME').'/about-us-background.jpg') }});">
        <div class="container">
            <div class="page-banner-content text-center">
                <h2 class="title text-white text-shadow">Madé Firenze</h2>
                <ol class="breadcrumb justify-content-center text-white">
                    <li class="breadcrumb-item"><a class="text-white" href="{{ url($locale) }}">Home</a></li>
                    <li class="breadcrumb-item active text-white">@lang('all.about')</li>
                </ol>
            </div>
        </div>
    </section>

<section class="about-area pt-50">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="about-content mt-45">
                        <h2 class="title">@lang('all.welcome to') Madé Firenze</h2>
                        <p class="text-help">
                            <i>— “Un brivido a fior di pelle in perfetto stile italiano”</i>
                        </p>
                        <p>
                            MF (Madè Firenze) nasce dall’incontro tra la passione di Ilaria Iemmi (Creative Director) e lo spirito imprenditoriale di Marco Matteini (CEO). <br>
                            Il nome identifica lo spirito del marchio, <b>legato al territorio di origine con originalità ed innovazione</b>, in un’espressione di italianità decisamente internazionale.<br>
                            Obbiettivo della casa Fiorentina è quello di far si che le donne si possano riconoscere in un brand giovane e dinamico, ideato per un quotidiano decisamente glamour. <br>
                            Abbandonato il superato concetto di donna fashionista  MADE’ FIRENZE si propone ad una donna completa: dinamica, romantica e femminile. <br>
                            In una parola, una donna moderna, che si distingue non solo perché al passo coi tempi ma perché esprime senza filtri la sua autenticità. La sua vera identità. <br>
                            I nostri valori? <br>
                            <b>
                            Qualità, ricercatezza, innovazione, italianità, versatilità ed emozione.
                            </b> <br>
                            I modelli esprimono il concetto di eleganza, unicita’ e qualita’. <br>
                            I punti fondamentali per chi ama i dettagli racchiusi nel <b>Made in Italy</b> che fanno dell’accessorio, un prodotto di <b> stile ed eccellenza.</b>
                        </p>
                        <div class="about-signature">
                            <img src="resources/{{ env('THEME_NAME') }}/ceo.png" alt="" class="w-100">
                            <p>Ilaria Iemmi</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="about-image mt-50">
                        <img src="/resources/{{ env('THEME_NAME') }}/about-us.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="about-area pt-50">
        <div class="container">
            <div class="row  align-items-center">

                <div class="col-lg-6">
                    <div class="about-image mt-50">
                        <img src="/resources/{{ env('THEME_NAME') }}/about-us-2.jpg" alt="">
                    </div>
                </div>
                 <div class="col-lg-6">
                    <div class="about-content mt-45">
                        <h2 class="title">Sara Di Vaira</h2>
                        <p>
                            <b>Sara Di Vaira</b> STAR televisiva, ballerina e vincitrice 2019 del programma TV RAI <b>“Ballando con le Stelle”</b> rappresenta la DONNA “MADE’ FIRENZE” come Testimonial del brand.<br>La scelta di Sara come Testimonial nasce da una perfetta armonia di Bellezza, Semplicità, Freschezza, Vitalità, Femminilità, Personalità, Sportività e Solarità che la contraddistinguono.<br>“LA PIU’ AMATA DAGLI ITALIANI!!!<br>Tutte caratteristiche che troviamo all’interno delle borse del brand “Made’ Firenze” e che ne fanno un punto di forza particolarmente stimolante.<br>Ci rivolgiamo a tutte le Donne, con un concetto di “Moda Non Moda” perchè oggi la Donna ama distinguersi, stupire… e indossare creazioni uniche, belle e di qualità.<br> Made’ Firenze ha saputo cogliere questo messaggio creando collezioni in semplicità, pezzi quasi unici, dai dettagli preziosi e dal mood senza tempo.<br>Rappresentiamo con orgoglio un azienda con un forte legame al territorio di origine (Firenze – Italia), alla scrupolosa scelta dei materiali, dei dettagli, di una produzione 100% made in Italy, che guarda al futuro creando in ecosostenibilità le proprie borse. <br><br> “Se le Stelle staranno  a guardare, noi ci metteremo a ballare!” <br><i>(Cit. Ilaria iemmi)</i>
                        </p>
                        <div class="about-signature">

                            {{-- <p>Ilaria Iemmi</p>  --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @hss('100')
    {{--
   <section class="team-area pt-110">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="section-title text-center pb-30">
                        <h2 class="title">@lang('all.our team')</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-sm-12 offset-md-3">
                    <div class="single-team text-center mt-30">
                        <div class="team-image">
                            <img src="/assets/images/harris/bruno.png" alt="">
                        </div>
                        <div class="team-content">
                            <h5 class="name"><a href="#">Ilaria Iemmi</a></h5>
                            <span class="designation">Fashion Designer</span>

                            <ul class="social">
                                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fab fa-pinterest-p"></i></a></li>
                                <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                                <li><a href="#"><i class="fal fa-envelope"></i></a></li>
                            </ul>

                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-12">
                    <div class="single-team text-center mt-30">
                        <div class="team-image">
                            <img src="/assets/images/harris/jari.png" alt="">
                        </div>
                        <div class="team-content">
                            <h5 class="name"><a href="#">Marco Matteini</a></h5>
                            <span class="designation">CEO</span>

                        </div>
                    </div>
                </div>


            <div class="col-lg-3 col-sm-12">
                    <div class="single-team text-center mt-30">
                        <div class="team-image">
                            <img src="/assets/images/harris/angela.png" alt="">
                        </div>
                        <div class="team-content">
                            <h5 class="name"><a href="#">Angela Bambusa</a></h5>
                            <span class="designation">CFO/ Chief Financial Officer</span>

                        </div>
                    </div>
                </div>
                  <div class="col-lg-3 col-sm-12">
                    <div class="single-team text-center mt-30">
                        <div class="team-image">
                            <img src="/assets/images/harris/maila.png" alt="">
                        </div>
                        <div class="team-content">
                            <h5 class="name"><a href="#">Maila Famiglietti</a></h5>
                            <span class="designation">CIO/ Chief Information Officer</span>

                        </div>
                    </div>
                </div>



            </div>
            @hss('100')
        </div>
    </section>
     --}}
{{--
    <div class="brand-logo-area border-0 pt-75 pb-100">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="section-title text-center pb-30">
                        <h2 class="title">OUR PARTNERS</h2>
                    </div>
                </div>
            </div>
            <div class="brand-row brand-active slick-initialized slick-slider">
                <div class="slick-list draggable"><div class="slick-track" style="opacity: 1; width: 1200px; transform: translate3d(0px, 0px, 0px);"><div class="brand-col slick-slide slick-current slick-active" data-slick-index="0" aria-hidden="false" tabindex="0" style="width: 240px;">
                    <div class="single-brand-2 mt-30">
                        <img src="assets/images/brand-1.png" alt="brand">
                    </div>
                </div><div class="brand-col slick-slide slick-active" data-slick-index="1" aria-hidden="false" tabindex="0" style="width: 240px;">
                    <div class="single-brand-2 mt-30">
                        <img src="assets/images/brand-2.png" alt="brand">
                    </div>
                </div><div class="brand-col slick-slide slick-active" data-slick-index="2" aria-hidden="false" tabindex="0" style="width: 240px;">
                    <div class="single-brand-2 mt-30">
                        <img src="assets/images/brand-3.png" alt="brand">
                    </div>
                </div><div class="brand-col slick-slide slick-active" data-slick-index="3" aria-hidden="false" tabindex="0" style="width: 240px;">
                    <div class="single-brand-2 mt-30">
                        <img src="assets/images/brand-4.png" alt="brand">
                    </div>
                </div><div class="brand-col slick-slide slick-active" data-slick-index="4" aria-hidden="false" tabindex="0" style="width: 240px;">
                    <div class="single-brand-2 mt-30">
                        <img src="assets/images/brand-5.png" alt="brand">
                    </div>
                </div></div></div>




            </div>
        </div>
    </div>
 --}}
@endsection
