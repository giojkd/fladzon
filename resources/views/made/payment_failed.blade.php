@extends(env('THEME_NAME').'.main')
@section('content')
@hss('100')
<section class="blog-page pt-20 pb-120">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="single-blog mt-80">
                        <!--<div class="blog-image">
                            <a href="blog-details.html">
                                <img src="assets/images/blog/blog-1-5.jpg" alt="blog">
                                <i class="fal fa-search-plus"></i>
                            </a>
                        </div>-->
                        <div class="blog-content">
                            <!--<ul class="blog-category">
                                <li><a href="#">Style</a></li>
                            </ul>-->
                            <h4 class="title text-center">@lang('all.payment failed title')!</h4>
                            <!--<ul class="blog-meta">
                                <li><a href="#">By <span> Jon Smith</span></a></li>
                                <li><a href="#"><i class="fal fa-clock"></i> June 15, 2020</a></li>
                            </ul>-->
                            <p>@lang('all.payment failed content text')</p>

                            <a href="/" class="main-btn btn-block">@lang('all.continue shopping')!</a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



@endsection
