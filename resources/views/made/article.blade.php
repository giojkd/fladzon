@extends(env('THEME_NAME').'.main')
@section('content')
    @hss('190')
          @if (!is_null($article->cover) && $article->cover != '')
            <section class="page-banner bg_cover" style="background-image: url('{{ Route('ir',['size' => 'h1600','filename' =>  $article->cover ])}}');">
        @else
                @hss('300')
        @endif
        <div class="container">
            <div class="page-banner-content text-center">
                @hss('100')
                <h2 class="title text-white"  >{{ $article->name  }}</h2>
                <ol class="breadcrumb justify-content-center">
                    <li class="breadcrumb-item "><a class="text-white" href="/">Home</a></li>
                </ol>
                @hss('100')
            </div>
        </div>
    </section>

<section class="blog-page pt-20 pb-120">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="single-blog mt-80">
                        <div class="blog-image">
                            <a>
                                {{--
                                @if (!is_null($article->cover) && $article->cover != '')
                                <img src="{{ Route('ir',['size' => 'h800','filename' =>  $article->cover ])}}" alt="blog">
                                @endif
                                 --}}
                                {{--  <i class="fal fa-search-plus"></i> --}}
                            </a>
                        </div>
                        <div class="blog-content">
                            <!--<ul class="blog-category">
                                <li><a href="#">Style</a></li>
                            </ul>-->
                            <h4 class="title text-center">{{ $article->name }}</h4>
                            <!--<ul class="blog-meta">
                                <li><a href="#">By <span> Jon Smith</span></a></li>
                                <li><a href="#"><i class="fal fa-clock"></i> June 15, 2020</a></li>
                            </ul>-->
                            <div>
                                {!! $article->description !!}
                            </div>

                            @if ($article->is_contact_page)
                                @hss('100')
                                <h4 class="title text-center">@lang('all.get in touch with us fill the form')</h4>
                                <script charset="utf-8" type="text/javascript" src="//js-eu1.hsforms.net/forms/shell.js"></script>
                                <script>
                                hbspt.forms.create({
                                    region: "eu1",
                                    portalId: "25158670",
                                    formId: "8bd2cf83-6b2b-4c92-ad80-636fc9f1e111"
                                });
                                </script>
                                @hss('100')
                            @endif

                            <a href="/" class="main-btn btn-block text-white">Continua con lo shopping</a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
