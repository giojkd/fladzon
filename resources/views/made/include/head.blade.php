<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">

    <!--====== Title ======-->

    @include(env('THEME_NAME').'.include.meta')

    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="ScreenOrientation" content="autoRotate:disabled">

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','{{ env('GOOGLE_TAG_MANAGER') }}');</script>
    <!-- End Google Tag Manager -->



        {{--
    <!--====== Favicon Icon ======-->

    <link rel="shortcut icon" href="{{ asset('assets/images/favicon.png') }}" type="image/png">
         --}}

    <!-- CSS
    ============================================ -->

    <!--===== Vendor CSS (Bootstrap & Icon Font) =====-->

    <link rel="stylesheet" href="{{ asset('assets/css/plugins/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/fontawesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/default.css') }}">


    <!--===== Plugins CSS (All Plugins Files) =====-->
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/component.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/jquery-ui.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/photoswipe.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/plugins/photoswipe-default-skin.css') }}">


    <!--====== Main Style CSS ======-->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}?v={{ rand(0,10000000) }}">
    <link rel="stylesheet" href="{{ asset('assets/css/'.env('THEME_NAME').'.css') }}?v={{ rand(0,10000000) }}">

    <!--====== Custom Style CSS ======-->


    <!--====== Use the minified version files listed below for better performance and remove the files listed above ======-->

    <!-- <link rel="stylesheet" href="assets/css/vendor/plugins.min.css">
    <link rel="stylesheet" href="assets/css/style.min.css">  -->
    @if (env('GOOGLE_SITE_VERIFICATION')  != '')
        <meta name="google-site-verification" content="{{ env('GOOGLE_SITE_VERIFICATION') }}" />
    @endif

    <link rel="apple-touch-icon" sizes="60x60" href="/favicon/{{ env('THEME_NAME') }}/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon/{{ env('THEME_NAME') }}/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon/{{ env('THEME_NAME') }}/favicon-16x16.png">
    <link rel="manifest" href="/favicon/{{ env('THEME_NAME') }}/site.webmanifest">
    <link rel="mask-icon" href="/favicon/{{ env('THEME_NAME') }}/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    @stack('before-closing-head')
</head>

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id={{ env('GOOGLE_TAG_MANAGER') }}"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
