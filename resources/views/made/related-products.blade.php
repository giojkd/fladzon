<div class="row product-active">
    @foreach ($products as $product)


    <div class="col-md-3 col-6">
        <div class="single-product mt-50">
            <div class="product-image">
                <div class="image">
                    <img class="product-1" src="@cim(url($product->product->cover()))" alt="{{ Str::ucfirst($product->product->name) }}">
                    <img class="product-2" src="@cim(url($product->product->secondCover()))" alt="{{ Str::ucfirst($product->product->name) }}">
                    <a class="link" href="{{  $product->product->makeUrl() }}"></a>
                </div>
                @if ($product->product->price != $product->product->price_compare && $product->product->price_compare > 0)
                    <span class="discount">{{ $product->product->discount_percentage }}%</span>
                @endif
            </div>
            <div class="product-content d-flex justify-content-between">
                <div class="product-title">
                    <h4 class="title"><a href="{{ $product->product->makeUrl() }}">{{ Str::ucfirst($product->product->name) }}</a></h4>
                </div>
                <div class="product-price">
                    <span class="text-dark">
                        <b>
                            @fp($product->product->price)
                        </b>
                    </span>
                    @if ($product->product->price != $product->product->price_compare && $product->product->price_compare > 0)
                        <span class="sale-price">@fp($product->product->price_compare)</span>
                    @endif

                </div>
            </div>
        </div>
    </div>

    @endforeach
</div>
