
@extends(env('THEME_NAME').'.main')
@section('content')

<div class="d-none d-md-block">
    @hss('150')
</div>


    <!--====== Page Banner Start ======-->
    {{--
        <section class="page-banner bg_cover" style="background-image: url({{ $product->photos[0]['src'] }});">
            <div class="container">
                <div class="page-banner-content text-center">
                    <h2 class="title">{{ $product->name }}</h2>
                    <ol class="breadcrumb justify-content-center">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active"><a href="{{ Route('productGrid',['locale' => $locale]) }}">Shop</a></li>
                    </ol>
                </div>
            </div>
        </section>
    --}}

    <!--====== Page Banner Ends ======-->

    <!--====== Product Simple Start ======-->

    <div class="d-block d-md-none">@hss('100')</div>

    <div class="container-h-scroll-wrapper d-block d-md-none">
        <div class="container-h-scroll" style="width: {{ count($product->photos) * 325 }}px">
            @foreach($product->photos as $index => $photo)
                <img style="width: 320px" src="{{ $photo['src'] }}" alt="">
            @endforeach
        </div>
    </div>
    <section class="product-simple-area">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-8">
                    <div class="product-simple-image product-simple-02-image flex-wrap mt-50 d-none d-md-block">
                        {{--

                            <button class="product-gallery-popup" data-hint="Click to enlarge" data-images='[
                            {"src": "{{ asset('assets/images/product/product-37.jpg') }}", "w": 600, "h": 743},
                            {"src": "{{ asset('assets/images/product/product-38.jpg') }}", "w": 600, "h": 743},
                            {"src": "{{ asset('assets/images/product/product-25.jpg') }}", "w": 600, "h": 743},
                            {"src": "{{ asset('assets/images/product/product-26.jpg') }}", "w": 600, "h": 743}
                        ]'><i class="far fa-search-plus"></i></button>

                            --}}

                        @foreach($product->photos as $index => $photo)
                            <div class="product-simple-preview-image">
                                <img class="{{-- product-zoom --}}" src="@cim(url($photo['src']))" alt="">
                            </div>
                        @endforeach

                    </div>
                </div>
                <div class="col-lg-4 ">

                    <div class="@if (!$isMobile) sticky-top @endif" style="top: 180px;">
                        @hss('30')
                        <div class="product-simple-details ">
                            <h4 class="title">{{ $product->name }}</h4>
                            {{--
                            <p class="sku-id">REF. {{ $product->sku }} <span>{{ $product->quantity }} disponibili</span></p>
                            --}}
                            @if(!is_null($product->reviews_score))

                                <p class="review">
                                    @for($i = 1; $i <= ceil($product->reviews_score); $i ++)
                                        <i class="fas fa-star review-on"></i>
                                    @endfor
                                    @for($i = ceil($product->reviews_score); $i<5;  $i ++)
                                        <i class="fas fa-star review-off"></i>
                                    @endfor
                                    <a href="#">({{ $product->reviews->count() }} customer reviews)</a>
                                </p>

                            @endif

                            <div class="product-price">
                                <span class="price">@fp($product->price)</span>
                                <span class="price-cut">
                                    @if($product->compare_price != $product->price)
                                        @fp($product->compare_price)
                                    @endif
                                </span>
                                <span class="discount-percentage">
                                    @if($product->compare_price != $product->price)
                                        (-{{ $product->discount_percentage }}%)
                                    @endif
                                </span>
                                {{--
                                <scalapay-widget amount="{{ $lead->grand_total }}"></scalapay-widget>
                                 --}}
                                @if(is_object($product->group) && $product->group->is_on_sale)
                                    <span class="badge badge-warning orange-bg text-white">@lang('all.on deal')</span>
                                @endif
                                <span class="badge badge-success text-white">@lang('all.free shipping')</span>
                            </div>

                            <p class="product-short-description">
                                <small>{!! $product->getShortDescription(160) !!} <a class="text-dark" href="#description-section">@lang('all.continue')...</a></small>
                            </p>

                        @if(!is_null($variants))

@push('modals')
    <div class="modal fade" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="helpModal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 480px">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="helpModal"><i class="fas fa-question-circle"></i> @lang('all.help')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="mb-3"><i class="fas fa-percentage"></i> @lang('all.price production')</p>
                    <p><i class="fas fa-ruler"></i> @lang('all.size hints')</p>
                </div>
                {{--
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
                --}}
            </div>
        </div>
    </div>
@endpush

                               {{--  <ul class="nav nav-tabs scales-tabs">
                                @foreach ($conversionscales as $index => $scale)
                                    <li class="nav-item cursor-pointer">
                                        <a data-target="#conversion_scale_{{ $scale->id }}" class="nav-link @if($index == 0) active @endif" data-toggle="tab">@lang('all.sizes') {{ $scale->name }}</a>
                                    </li>
                                @endforeach

                                 <li class="nav-item" style="padding: 7px 14px 0px 14px">
                                        <a href="javascript:" class="orange-text" data-toggle="tooltip" title="@lang('all.size hints')"><i class="fas fa-ruler"></i></a>
                                </li>

                                 <li class="nav-item" data-toggle="modal" data-target="#helpModal">
                                        <a href="javascript:" class="orange-text nav-link"><i class="fas fa-question-circle"></i> @lang('all.help')</a>
                                </li>

                            </ul>
                              --}}
                            <div class="tab-content">
                                @foreach ($conversionscales as $index => $scale)
                                    <div class="tab-pane fade show @if($index == 0) active @else fade @endif" id="conversion_scale_{{ $scale->id }}">
                                        <p class="help-block my-2" style="line-height: 18px;">
                                            <small>

                                            </small>
                                        </p>
                                         <div class="product-color">
                                            @foreach($variants->chunk(5) as $chunkIndex => $chunks)
                                                <ul class="color-list @if($chunkIndex > 0) mt-1 @endif">
                                                    @foreach($chunks as $variantIndex => $variant)

                                                            <li

                                                                @if ($variant->quantity > 0 || $variant->continue_selling)
                                                                    onclick="setProductId({{ $variant->id }},'{{ $variant->sku }}',true,'{{ $variant->attributes_line }}',{{ $variant->price }},{{ $variant->compare_price }},{{ $variant->discount_percentage }})"
                                                                @else
                                                                    onclick="setProductId({{ $variant->id }},'{{ $variant->sku }}',false,'{{ $variant->attributes_line }}',{{ $variant->price }},{{ $variant->compare_price }},{{ $variant->discount_percentage }})"
                                                                @endif
                                                                variant-index="{{ $variantIndex }}"
                                                                class="size-variant-{{ $variant->id }} size-variants @if ($variant->quantity <= 0 && !$variant->continue_selling) size-variant-disabled @else size-variant-enabled @endif @if($variant->id == $product->id) active @endif text-center"
                                                                data-toggle="tooltip"
                                                                data-placement="middle"
                                                                title="{{ $variant->name }}"
                                                            >
                                                                {{--
                                                                <img src="{{ Route('ir', ['size' => 'h240', 'filename' => $variant->photos[0]]) }}" style="height: 70px;" alt="">
                                                                --}}

                                                                @if(isset($conversionscalesindexed[$scale->id][$variant->attributevalues->keyBy('attribute_id')[2]->id][$product->gender_id]))

                                                                    {{ $conversionscalesindexed[$scale->id][$variant->attributevalues->keyBy('attribute_id')[2]->id][$product->gender_id] }}

                                                                @else

                                                                    {{ $variant->attributevalues->keyBy('attribute_id')[2]->name }}

                                                                @endif

                                                                <br>


                                                                @if (!is_null($product->attributevalueextensiongroup))
                                                                    <small>
                                                                        {{
                                                                            isset($product->attributevalueextensiongroup->attributevalueextensiongrouprows->keyBy('attributevalue_id')[$variant->attributevalues->keyBy('attribute_id')[2]->id]) ? $product->attributevalueextensiongroup->attributevalueextensiongrouprows->keyBy('attributevalue_id')[$variant->attributevalues->keyBy('attribute_id')[2]->id]->name : ''
                                                                        }}
                                                                    </small>
                                                                @endif

                                                            </li>

                                                    @endforeach
                                                </ul>
                                            @endforeach
                                        </div>
                                    </div>
                                @endforeach
                            </div>




                        @endif
                        <div class="product-attribute product-color">
                            <p>@lang('all.attributes'): <!--<strong>Blue</strong>--></p>
                            <ul>
                                @foreach($product->attributevalues as $attributeValue)
                                    @if($attributeValue->attribute->is_visual)
                                        <li>
                                            {{ Str::ucfirst($attributeValue->attribute->name) }}: {{ Str::ucfirst($attributeValue->name) }}
                                        </li>
                                    @endif
                                @endforeach
                                @if (is_object( $product->productgroup))
                                    <li>@lang('all.code'): {{ $product->productgroup->ext_id }}</li>
                                @endif
                                {{-- <li>@lang('all.sku'): <span id="selected-sku">{{ $product->sku }}</span></li>  --}}
                            </ul>
                        </div>
                            {{-- <div class="product-quantity-cart-wishlist-compare flex-wrap" style="@if ($variants->sum('quantity') <= 0 && !$product->productgroup->continue_selling) display:none; @endif" id="addToCartFormWrapper">  --}}
                            <div class="product-quantity-cart-wishlist-compare flex-wrap" style="@if (($variants->sum('quantity') <= 0 && !$product->continue_selling) || (!$product->enabled)) display:none; @endif" id="addToCartFormWrapper">

                                <form action="{{ Route('addToCart') }}" id="addToCartForm">
                                        <input type="hidden" name="product_id" value="{{ $product->id }}">
                                        <input type="hidden" name="locale" value="{{ App::getLocale() }}">
                                    <div class="product-quantity d-flex">
                                        <button type="button" class="sub"><i class="fal fa-minus"></i></button>
                                        <input max-message="Hai raggiunto la quantità massima di prodotti che puoi aggiungere al carrello  ({{ $product->quantity }})" type="text" value="1" min="1" max="{{ (!$product->continue_selling) ? $product->quantity : 100000 }}" name="quantity" />
                                        <button type="button" class="add"><i class="fal fa-plus"></i></button>
                                    </div>
                                    <div class="product-cart">
                                        <button type="submit" class="main-btn orange-bg">@lang('all.add to cart')</button>
                                    </div>
                                </form>
                                <!--
                                <a href="#" data-tooltip="tooltip" data-placement="top" title="Add to Wishlist" class="product-wishlist"><i class="far fa-heart"></i></a>
                                <a href="#" data-tooltip="tooltip" data-placement="top" title="Add to Compare" class="product-compare"><i class="far fa-repeat-alt"></i></a>
                                -->
                            </div>
                            <div id="requestInfoFormWrapper" style="@if (($variants->sum('quantity') <= 0 && !$product->continue_selling) || (!$product->enabled)) display:block; @else display:none; @endif" class="mt-2">
                                <small>@lang('all.product is not purchaseable')</small> <br>
                                <a href="javascript:" onclick="tidioChatApi.messageFromVisitor('@lang('all.i am interested into :name',['name' => $product->name])'+selectedVersionName);" class="main-btn orange-bg">@lang('all.request info')</a>
                            </div>

                            @if ($siblings->count() > 0)
                                <hr>
                                <div class="product-attribute product-color">
                                    <p>@lang('all.available also in'): <!--<strong>Blue</strong>--></p>
                                    @foreach($siblings->chunk(4) as $chunkIndex => $chunks)
                                    <div class="row @if($chunkIndex > 0) mt-2 @endif">
                                         @foreach($chunks as $variant)
                                            <a style="background-image: url('{{ Route('ir', ['size' => 'h240', 'filename' => $variant->photos[0]]) }}')" href="{{ $variant->makeUrl() }}" class="product-variation col-md-3 col-6 @if($variant->id == $product->id) active @endif text-center" data-tooltip="tooltip" data-placement="middle" title="{{ $variant->name }}" ></a>
                                        @endforeach
                                        </div>
                                    @endforeach
                                </div>
                            @endif

                            <div class="product-description">
                                {{ $product->short_description }}
                            </div>
                            {{--
                            <div class="product-share">
                                <ul class="social">
                                    @if($defaults['facebook'] != '') <li><a target="_blank" href="{{ $defaults['facebook'] }}"><i class="fab fa-facebook-f"></i></a></li> @endif
                                    @if($defaults['instagram'] != '') <li><a target="_blank" href="{{ $defaults['instagram'] }}"><i class="fab fa-instagram"></i></a></li> @endif
                                    @if($defaults['twitter'] != '') <li><a target="_blank" href="{{ $defaults['twitter'] }}"><i class="fab fa-twitter"></i></a></li> @endif
                                    @if($defaults['youtube'] != '') <li><a target="_blank" href="{{ $defaults['youtube'] }}"><i class="fab fa-youtube"></i></a></li> @endif
                                </ul>
                            </div>
                             --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--====== Product Simple Ends ======-->

    <!--====== Product Description & Review Start ======-->

    <section class="product-description-review pt-150" id="description-section">
        <div class="container">
            <div class="product-simple-tab-menu">
                <ul class="nav justify-content-center">
                    <li><a class="active" data-toggle="tab" >@lang('all.description')</a></li>
                    @if(!is_null($product->reviews_score))
                        <li><a data-toggle="tab" href="#review">Review ({{ $product->reviews->count() }})</a></li>
                    @endif
                </ul>
            </div>
            <div class="tab-content pt-30 mb-4">
                <div class="tab-pane fade show active" id="description">
                    <div class="product-description">
                        <div class="row">
                            <div class="col-md-4 offset-md-4">
                                <div class="description ">
                                    {!! str_replace('*','',nl2br($product->description)) !!}
                                    <hr>
                                    {!! $product->extended_description !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if(!is_null($product->reviews_score))
                    <div class="tab-pane fade" id="review">
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div class="product-review pt-20">
                                    <div class="review-comment">
                                        <ul class="comment">
                                            @foreach($product->reviews as $review)
                                                <li>
                                                    <div class="single-review-comment">
                                                        <!--<div class="review-author">
                                                            <img src="assets/images/auhtor-1.jpg" alt="">
                                                        </div>-->
                                                        <div class="review-content">
                                                            <p>{{ $review->content }}</p>
                                                            <div class="review-name-rating">
                                                                <h6 class="review-name">{{ $review->alias }}</h6>
                                                                <ul class="review-rating">
                                                                    @for($i = 1; $i == $review->score; $i ++)
                                                                        <li class="rating-on"><i class="fas fa-star"></i></li>
                                                                    @endfor
                                                                    @for($i = $review->score; $i<5;  $i ++)
                                                                        <li class="rating-off"><i class="fas fa-star"></i></li>
                                                                    @endfor
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <!--
                                    <div class="review-form mt-45">
                                        <h2 class="form-title">Add a review </h2>

                                        <form action="#">
                                            <div class="rating-star">
                                                <a href="javascript:void(0)" class="star-1"></a>
                                                <a href="javascript:void(0)" class="star-2"></a>
                                                <a href="javascript:void(0)" class="star-3"></a>
                                                <a href="javascript:void(0)" class="star-4"></a>
                                                <a href="javascript:void(0)" class="star-5"></a>
                                            </div>

                                            <div class="review-textarea">
                                                <label>Your Review *</label>
                                                <textarea></textarea>
                                            </div>
                                            <div class="review-btn">
                                                <a href="#" class="main-btn">Submit</a>
                                            </div>
                                            <div class="review-checkbok">
                                                <input type="checkbox" id="checkbox">
                                                <label for="checkbox"><span></span> NOTIFY ME OF NEW POSTS BY EMAIL.</label>
                                            </div>
                                        </form>
                                    </div>
                                    -->
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>

    <!--====== Product Description & Review Ends ======-->

    <!--====== Related Products Start ======-->

    <section class="product-area pt-160 pb-145 d-none">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="section-title text-center">
                        <h2 class="title">@lang('all.you might also like')</h2>
                    </div>
                </div>
            </div>
            <div id="related-products-wrapper"></div>
        </div>
    </section>

    <!--====== Related Products Ends ======-->

@endsection

@push('after-scripts')

    <script src="https://cdn.scalapay.com/js/scalapay-widget/webcomponents-bundle.js"></script>
    <script src="https://cdn.scalapay.com/js/scalapay-widget/scalapay-widget.js"></script>

    <script>

        $(function(){
            //$('li.size-variants[variant-index="0"]').trigger('click');
            $('li.size-variants.size-variant-enabled').eq(0).trigger('click');
        })

        var currencySymbol = '€';
        var selectedVersionName = '';

        ft.productPageView(@json(['products'=>[$product->ft_data]]));

        function setProductId(id,sku,is_purchasable,selectedVersionNameVar,price,compare_price,discount_percentage){

            selectedVersionName = selectedVersionNameVar+' ('+sku+')';

            if(is_purchasable){
                $('#addToCartFormWrapper').show();
                $('#requestInfoFormWrapper').hide();
            }else{
                $('#addToCartFormWrapper').hide();
                $('#requestInfoFormWrapper').show();
            }

            price = parseFloat(price);
            compare_price = parseFloat(compare_price);

            $('.price').html(currencySymbol+price.toFixed(2));

            if(compare_price != price){

                $('.price-cut').html(currencySymbol+compare_price.toFixed(2));
                $('.discount-percentage').html('(-'+discount_percentage+'%)');

            }else{

                $('.price-cut').html('');
                $('.discount-percentage').html('');

            }

            $('#selected-sku').html(sku);
            $('.size-variants ').removeClass('active');
            $('.size-variant-'+id).addClass('active');
            $('#addToCartForm').find('input[name="product_id"]').val(id);

        }
        @if(is_object($product->productgroup))
            $(function(){
                $('#related-products-wrapper').load("{{ Route('relatedProductsToProduct',['product_id' => $product->productgroup->id,'user_id' => 'user-'.$guest->id,'how_many_products' => 8]) }}")
            })
        @endif
    </script>



@endpush
