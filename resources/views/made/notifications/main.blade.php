@include(env('THEME_NAME').'.notifications.include.header')

@yield('content')

@include(env('THEME_NAME').'.notifications.include.footer')
